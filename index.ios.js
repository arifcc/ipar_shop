/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import {AppRegistry} from 'react-native';
import MainController from './MainController'

AppRegistry.registerComponent('ipar_shop', () => MainController);
