//
//  RNBridgeManager.h
//  ipar_shop
//
//  Created by Yoda on 2019/2/6.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface RNBridgeManager : NSObject <RCTBridgeModule>

@end
