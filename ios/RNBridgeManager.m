//
//  RNBridgeManager.m
//  ipar_shop
//
//  Created by Yoda on 2019/2/6.
//  Copyright © 2019 Facebook. All rights reserved.
//
#import "RNBridgeManager.h"
@implementation RNBridgeManager

@synthesize bridge = _bridge;

RCT_EXPORT_MODULE(IparAppVersion);
//  对外提供调用方法,Callback
RCT_EXPORT_METHOD(getAppVersion:(RCTResponseSenderBlock)callback)
{
  NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];//获取项目版本号
  callback(@[[NSNull null],version]);
}

@end
