/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

#import "AppDelegate.h"

#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import "RCTUmengPush.h"


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  NSURL *jsCodeLocation;

  
  //注册友盟推送
  [RCTUmengPush registerWithAppkey:@"5c4a8659f1f556e232000746" launchOptions:launchOptions];
  
 
  
  /**
   *开发环境
   */
   jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index.ios" fallbackResource:nil];
  /**
   *真实环境
   */
//jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"bundle/index.ios" withExtension:@"jsbundle"];

 
  
  RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                      moduleName:@"ipar_shop"
                                               initialProperties:nil
                                                   launchOptions:launchOptions];
  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];

  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
//  [RNSplashScreen show];
  return YES;
  
//  //fonts
//  NSInteger totalCount = 0;
//  for (NSString *familyName in [UIFont familyNames]) {
//    NSArray *familyNameArr = [UIFont fontNamesForFamilyName:familyName];
//    NSLog(@"familyName:%@   count=%ld", familyName,[familyNameArr count]);
//    NSInteger tempCount = [familyNameArr count];
//    totalCount += tempCount;
//    for (NSString *fontName in familyNameArr) {
//      NSLog(@"++++     %@", fontName);
//    }
//  }
//  NSLog(@"totalCount=%ld",totalCount);
  
  
  
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
  //获取deviceToken
  [RCTUmengPush application:application didRegisterDeviceToken:deviceToken];
}



- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
  //获取远程推送消息
  [RCTUmengPush application:application didReceiveRemoteNotification:userInfo];

}



@end
