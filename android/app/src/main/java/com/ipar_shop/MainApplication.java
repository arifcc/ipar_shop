package com.ipar_shop;

import android.app.Application;
import android.util.Log;

import com.facebook.react.ReactApplication;
import com.liuyun.upush.UPushPackage;
import com.reactnativecomponent.barcode.RCTCapturePackage;
import com.beefe.picker.PickerViewPackage;
import com.imagepicker.ImagePickerPackage;
import com.AlexanderZaytsev.RNI18n.RNI18nPackage;
import com.cmcewen.blurview.BlurViewPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.message.IUmengRegisterCallback;
import com.umeng.message.PushAgent;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(
                    new MainReactPackage(),
                    new UPushPackage(),
//            new UmengPushPackage(),
                    new MyReactPackage(),
                    new RCTCapturePackage(),
                    new PickerViewPackage(),
                    new ImagePickerPackage(),
                    new RNI18nPackage(),
                    new BlurViewPackage()
            );
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //友盟配置
        UMConfigure.init(this, "576b73f367e58eb257000493", null, UMConfigure.DEVICE_TYPE_PHONE, "05290038bb75fbcbca1ec2cb6cf693f1");

        //推送服务注册
        registerUPush();
        SoLoader.init(this, /* native exopackage */ false);
    }

    private void registerUPush() {
        PushAgent mPushAgent = PushAgent.getInstance(this);
        //注册推送服务
        mPushAgent.register(new IUmengRegisterCallback() {
            @Override
            public void onSuccess(String deviceToken) {
                Log.d("U_push", "onSuccess: deviceToken=" + deviceToken);
            }

            @Override
            public void onFailure(String s, String s1) {
                Log.d("U_push", "onFailure: s=" + s + "\ns1" + s1);
            }
        });
        PushAgent.getInstance(this).onAppStart();
    }

}
