import React, {Component} from 'react'
import {View, DeviceEventEmitter} from 'react-native'
import IparNetwork, {SERVER_TYPE} from "../ipar/httpUtils/IparNetwork";
import CommonUtils from "../ipar/common/CommonUtils";

export default class TestPage extends Component {

    constructor(props) {
        super(props);
        this.state = {}


    }


    componentDidMount() {
        CommonUtils.showLoading();
        new IparNetwork().getRequest(SERVER_TYPE.uiAdminIpar + 'region')
            .then((result) => {
                CommonUtils.dismissLoading();
                DeviceEventEmitter.emit('toast', 'success')
            }).catch((error) => {
            CommonUtils.dismissLoading();
            alert(error)
        });
    }

    render() {
        return (
            <View style={[]}>

            </View>
        )
    }
}
