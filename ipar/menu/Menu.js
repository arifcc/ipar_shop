import React from 'react';
import PropTypes from 'prop-types';
import {
    Dimensions,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    View,
    Image,
    Text,
} from 'react-native';
import {menuClickTypes} from "./SlideMenu";
import I18n from '../res/language/i18n'
import StyleUtils from "../res/styles/StyleUtils";
import {IMAGE_URL} from "../httpUtils/IparNetwork";
import IparImageView from "../custom/IparImageView";


const window = Dimensions.get('window');

Menu.propTypes = {
    onItemSelected: PropTypes.func.isRequired,
};


export default function Menu({userData, onItemSelected, messageNum}) {

    let userName = userData ? userData.nick_name ? userData.nick_name : userData.user_name : I18n.t('default.login');

    return (
        <ScrollView
            scrollsToTop={false}
            style={styles.menu}>


            <TouchableOpacity
                onPress={() => onItemSelected(menuClickTypes.userAvatar)}
                style={[styles.avatarContainer]}>

                <IparImageView resizeMode={'cover'} width={48} height={48} borderRadius={24} url={userData && userData.head_img}
                               errorImage={require('../res/images/ic_avatar.png')}/>
                <Text
                    style={[styles.name, StyleUtils.smallFont]}>{
                    userName + (userData ? '\n' + userData.rand_id : '')
                }</Text>
            </TouchableOpacity>

            {getTextView(I18n.t('default.myBusiness'), () => onItemSelected(menuClickTypes.business), {marginTop: 40})}
            {getTextView(I18n.t('default.message'), () => onItemSelected(menuClickTypes.message), null, messageNum)}
            {getTextView(I18n.t('default.myOrders'), () => onItemSelected(menuClickTypes.orders))}
            {getTextView(I18n.t('default.country'), () => onItemSelected(menuClickTypes.country))}
            {getTextView(I18n.t('default.language'), () => onItemSelected(menuClickTypes.language))}
            {/*{getTextView(I18n.t('default.startBusiness'), () => onItemSelected(menuClickTypes.business))}*/}
            {/*{getTextView(I18n.t('default.serviceAgreement'), () => onItemSelected(menuClickTypes.agreement))}*/}
            {/*{getTextView(I18n.t('default.customerService'), () => onItemSelected(menuClickTypes.customer))}*/}
            {getTextView(I18n.t('about.about'), () => onItemSelected(menuClickTypes.about))}

        </ScrollView>
    );
}

function getTextView(text, callBack, styleBox, num) {
    return (
        <TouchableOpacity
            style={[{marginTop: 16}, StyleUtils.rowDirection, styleBox]}
            onPress={callBack}>
            <Text
                style={[styles.item_text, StyleUtils.smallFont]}>
                {text}
            </Text>

            {num && num > 0 ? <View style={[{
                backgroundColor: 'red',
                height: 18,
                width: 18,
                marginTop: 8,
                borderRadius: 18,
                padding: 1
            }, StyleUtils.center]}>
                <Text
                    numberOfLines={1}
                    style={{backgroundColor: 'transparent', fontSize: 12, color: 'white'}}>{num}</Text>
            </View> : null}
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    menu: {
        flex: 1,
        width: window.width,
        backgroundColor: '#e6e6e6',
        padding: 20,
    },
    avatarContainer: {
        marginBottom: 20,
        marginTop: '20%',
        justifyContent: 'center'
    },
    avatar: {
        width: 48,
        height: 48,
        borderRadius: 24,
        flex: 1,
    },
    name: {
        position: 'absolute',
        left: 70,
    },
    item_text: {
        fontSize: 16,
        paddingTop: 16,
        color: '#333',
    },
});
