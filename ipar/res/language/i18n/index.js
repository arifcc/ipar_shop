import I18n from 'react-native-i18n'
import en from '../en'
import zh from '../zh'
import ru from '../ru'
import uy from '../uy'

I18n.defaultLoval = 'en';
I18n.fallbacks = true;

I18n.translations = {
	zh,
	ru,
	en,
	uy
};
export default I18n