export default {

    default: {
        tab_name: 'باش بەت',
        seeMore: 'تەپسىلاتى',
        shopGlobally: 'خەلىقئارا بازا',
        highValueSets: 'ئالاھىدە يۈرۈشلۈك',
        newProducts: 'يېڭى مەھسۇلات',
        promotions: 'ئېتىبار',
        discover: 'يېڭىلىق',
        brands: 'تۈرلەر',
        search: 'ئىزدەش',
        showAll: 'بارلىق',
        selectConShopFrom: 'خەلىقئارا بازا',
        startShoppingBtn: 'سېتىۋېلىش',
        checkoutBtn: 'ھېسابات',
        subtotal: 'جەمئىي',
        checkDetails: 'تەپسىلى ئۇچۇرلىرىم',
        identity: 'سالاھىيەت',
        wallet: 'ھەميان',
        discountCards: 'ئېتىبار كارتىسى، سوۋغات',
        myBusiness: 'مېنىڭ بازىرىم',
        businessPlan: 'رېغبەتلەندۆرۈش پىلانى',
        myOrders: 'زاكاسلار',
        address: 'ئادرىسىم',
        bankCard: 'بانكا كارتىسى',
        favourites: 'ساقلىۋالغېنىم',
        verPersonal: 'ھەقىقىي ئۇچۇرنى دەلىللەش',

        security:
            'ھېسابات بىخەتەرلىكى',
        settings:
            'تەڭشەك',

        country: 'دۆلەت',
        SerLang:
            'مۇلازىمەت تىلى',

        receNotifi: 'ئۇچۇر قۇبۇل قىلىش',

        logout: 'چېكىنىش',
        phoneNumber: 'يانفون نومۇرى',
        _phoneNumber: 'يانفون نومۇرى',
        emailAddress: 'ئېلخەت ئادرىسى',
        password: 'مەخپىي نومۇر',
        edit: 'تەھرىر',
        socialMediaAccount: 'ھېسابات باغلاش',
        bonusPoints:
            'مۇكاپات جۇغلانمىسى',
        bonusCards: 'نومۇر  كارتىسى',
        giftCard: 'سوۋغاتلىق كارتا',
        coupons:
            'ئېتىبار كارتىسى',
        more: 'تەپسىلاتى',
        addNewCard: 'بانكا كارتىسى قوشۇش',
        allDebitCard: 'بانكا كارتا ئۇچۇرى',
        debitCard: 'ئامانەت كارتىسى',
        creditCard: 'ئىناۋەتلىك كارتا',
        cardHolName: 'كارتا ئىگىسى ئىسىمى',
        cardNumber: 'كارتا نومۇرى',
        bankName: 'بانكا ئىسمى',
        submit: 'تاپشۇرۇش',
        cvv: 'CVV2',
        year: 'يىل',
        month: 'ئاي',

        newAddAddress: 'ئادرىس قوشۇش',
        myAddressTitle:
            'مال تاپشۇرۇپ ئېلىش تەپسىلىي ئادرىسى',
        selectCountry: 'تۇرۇشلۇق دۆلەتنى تاللاڭ',
        selectProvince: 'تۇرۇشلۇق ئۆلكەئ ئاپتونوم رايون',
        selectCity: 'تۇرۇشلۇق شەھەرنى تاللاڭ',
        selectDistrict: 'تۇرۇشلۇق رايوننى تاللاڭ',
        streetNumber: 'كوچا نامى ۋە كوچا نومۇرى',
        buildingName: 'بىنا نومۇرى',
        postNumber: 'پوچتا نومۇرى',
        addAddressTitle:
            'تەپسىلىي ئادرىسىڭىزنى قوشۇۋېلىڭ',
        interDistrict: 'رايون',

        postalCode: 'پوچتا نومۇرى'
        ,
        orderTitle: 'زاكاز تەپسىلاتى ۋە ۋە ئەڭ يېى ھالىتى',
        selectOrderType: 'زاكاز ھالىتى بويچە تاللاڭ',
        close: 'تاقاش',
        orderNo: 'زاكاز نومۇرى',
        amount: 'سوممىسى',
        status: 'ھالىتى',
        all: 'ھەممىسى',
        paid: 'پۇل تۆلەندى',
        shipped: 'يولغا سېلىندى',
        return: 'مال قايتۇرۇش',
        completed: 'تاماملانغان',
        payNow: 'پۇل تۆلەش',
        cancel: 'ئەمەلدىن قالدۇرۇش',
        orderDetail: 'تەپسىلاتى',
        orderDate: 'زاكاز ۋاقتى',
        orderStatus: 'زاكاز ھالىتى',
        deliverDetails: 'مال يەتكۈزۈش ئادرىسى',
        orderProducts: 'زاكاز تەپسىلاتى',
        orderTotal: 'زاكاس سوممىسى',
        savings: 'ئېتىبار',
        shippingCost: 'توشۇش ھەققى',
        vat: 'قوشۇلما قىممەت بېجى',
        importTaxIf: 'ئىمپورت بېجى',
        serviceFee: 'مۇلازىمەت ھەققى',
        total: 'ئومۇمىي سومما',
        paymentMethod: 'پۇل تۆلەش ئۇسۇلى',
        trace: 'تەكشۈرۈش',
        businessInfor: 'مېنىڭ سودا ئۇچۇرلىرىم',

        local: 'مۇكاپات',
        global: 'خەلىقئارا بازا',
        myActivityFor: 'ئۆتكەن ئالتە ئايلىق پائالىيەت',

        myRewards: 'مۇكاپاتلىرىم',

        localBv:
            'يەرلىك BV رايون',
        pv: 'PV',

        pha: 'باش لېدىر شەرەپ مۇكاپاتى',
        bonusesTheLocal: 'يەرلىك بازىرى',
        bonusesTheGlobal: 'يەرلىك بازار ۋە چەتئەل بازىرىدىن كەلگەن مۇكاپات',
        rewardsForMar: 'تەشۋىقاتىڭىز ئۈچۈن بېرىلگەن مۇكاپات',
        specialHonory:
            'ئالاھىدە شەرەپ مۇكاپاتى ۋە پېنسىيە پۇلى',
        marketingPlan:
            'مۇكاپاتلىق پىلان',
        currentLevel: 'ھازىرقى دەرىجە',
        silverIBM: 'Silver IBM',
        activity: 'ئاكىتىپلىق دەرىجىسى',
        averageScore: 'ئوتتۇرىچە دەرىجە',
        iparId: 'ئىپار كارتا نومۇرىڭىزنى كىرگۈزۈك',
        myIparId: 'ئىپار كارتام',

        iparDiscountCard: 'ئىپار ھېسابات ئۇچۇرى',
        userNotExist: 'ئابۇنىت  مەۋجۇت ئەمەس، تىزىملىتىڭ',
        Key: 'مەخپىي  شىفىر',
        sponsorInfo: 'تەۋسىيە قىلغۇچى ئۇچۇرى',
        businessPreference: 'سودا ئالاھىدىلىكى',
        healthFood: 'ساغلاملىق بويۇملىرى',
        personalCare: 'شەخسىي ئاسراش',
        skinCare: 'تېرە ئاسراش',
        suncellaFemale: 'كۈنچېچەك',
        electrronics: 'ئېلىكترونلۇق مەھسۇلات',
        vip: 'VIP',
        GoodToProgram: 'Good to go program',
        GrowPlan: 'Grow together plan',
        youCanApplyIBM: 'جۇغلانما نومۇر بازارىدا  ئىشلىتىلىدۇ',
        youCanTransfer: 'دوستىڭىزغا يوللاپ بېرىشكە ۋە ياكى زاكاسقا پۇل تۆلەشكە بولىدۇ!',
        youCanApplyTo: 'زاكاسقا پۇل تۆلەشكە بولىدۇ',

        accumulatedBonus: 'ئىستېمال جۇغلانما نومۇرى',

        withdraw: 'نەق پۇل ئېلىش',
        commonQand: 'دائىم كۆرىلىدىغان مەسىلىلەر',
        expired: 'ۋاقتى ئۆتتى',
        apply: 'ئىشلىتىش',
        myWallet: 'ھەميان',
        availableBalance: 'قالدۇق سومما',
        recharge: 'قىممەت تولۇقلاش',

        getCode: 'تەستىق نومۇر',
        addCard: 'بانكا كارتىسى قوشۇش',
        withdrawal: 'نەق پۇل ئېلىش',
        enterWithdrawAmount: 'ئالماقچى بولغان نەق پۇل سوممىسنى كىرگۈزۈڭ',
        selectBankCard: 'بانكا كارتىسى تاللاڭ',
        verCode: 'تەستىق نومۇر',
        rocketPlan: 'راكىتا پىلانى',
        failedVerify: 'تەستىقلاش مەغلۇپ بولدى',
        notVerified: 'تەستىقلانمىدى',
        underReview: 'تەستىقلىنىۋاتىدۇ',
        verified: 'تەستىقلاندى',
        notActivated: 'ئاكىتىپلانمىغان',
        activateBusinessPlan: 'بازا ئېچىش',
        contract: 'توختام',
        signTheIBM: 'ھەمكارلاشقۇچى توختامىنى ئىمزالىغاندىن كېيىن شۇ يەردىكى ئىپار شىركىتىگە ئەۋەتىپ بەرسە بولىدۇ',
        ibmStarterPack: 'سودا بوغچىسى',
        iparIBMBusiness: 'ئىپار سودا بوغچىسى سىزنى تېخىمۇ كۆپ سودا ۋە ساغلاملىق ھەققىدىكى  تەربىيلەش پۇرسىتى بىلەن تەمىنلەيدۇ',
        verifyPerInf: 'ھەقىقىي نامدا دەلىللەش',
        verifiedAccounts: 'تەستىقلانغان ئابۇنىتلار تېخىمۇ كۆپ مۇلازىمەتتىن بەھرىمەن بولىدۇ',
        business: 'سودا ئۇچۇرى',

        notActivatedYet: 'سىز تېخى ھېچقانداق پىلان ئاچقۇزمىدىڭىز،\n دەرھال ئ‍ېچىۋېلىڭ',
        iparProvidesYou: 'ئىپار سىزنى تەننەرخى تۆۋەن، بىخەتەر، جانلىق سودا پۇرسىتى بىلەن تەمىنلەيدۇ',

        myAccount: 'مېنىڭ ھېسابات نومۇرۇم',
        language: 'تىل',
        startBusiness: 'ئىپارغا قوشۇلۇش',
        haveInformationVerify: 'شەخسىي ئۇچۇر ھەقىقىي نامدا دەلىللەنگەندىن كېيىن تېخىمۇ كۆپ مۇلازىمەتكە ئېرىشەلەيسىز\n',


        applyActivate: 'ئاكىتىپلاش ساخلىنىۋاتىدۇ',
        serviceAgreement: 'مۇلازىمەت ئىشلىتىش كىلىشىمنامىسى',
        customerService: 'مۇلازىمەت',
        userName: 'ئابۇنىت  نامى',
        signIn: 'كىرىش',
        forgotPass: 'مەخپىي نومۇرنى ئۇنتۇپ قالدېڭىزمۇ ؟',
        login: 'كىرىش',
        findPass: 'مەخپىي نومۇرنى  ئەسلىگە كەلتۈرۈش',
        resetPassword: 'قىسقا ئۇچۇر ئارقىلىق مەخپىي نومۇرنى قايتا ئورنىتىش',
        countryCode: 'دۆلەت رايون نومۇرى',
        enterPhoneNum: 'يانفون نومۇرىڭىزنى كىرگۈزۈڭ',
        enterPass: 'مەخپىي نومۇرنى كىرگۈزۈك',
        register: 'تىزىملىتىش',
        welcomeToIPAR: 'ئىپار ساغلاملىق ساھەسىگە قوشۇلغانلىقىڭىزنى قارشى ئالىمىز',
        createAnIbm: 'يېڭى ئابۇنىتلار تىزىملىتىش',
        registerAs: 'VIP مۆتىۋەر خېرىدار تىزىملىتىش',
        activateAccount: 'ئىپار ھېسابات نومۇرىنى ئاكتىپلاش',
        orVip: '\nئابونتلار سۈپەتلىك ئئپار مەخسۇس مەھسۇلاتلىرىدىن بەھرىمان بولىدۇ',
        ifYouAlready: 'ئىپار ID سى شەكىللىنىپ بولغان ،دەرھال ئاكتىپلاڭ',
        alreadyHave: 'ئىپار ھېسابات نومۇرى بارمۇ؟',
        userHasAlready: 'مەزكۇر ئابۇنىت مەۋجۇت،كىرىڭ',
        fail: 'مەغلۇپ بولدى',
        enterUserName: 'ئابۇنىت  ئىسمىنى كىرگۈزۈڭ',
        check: 'تەكشۈرۈش',
        yourName: 'فامىلىڭىز',
        next: 'كېيىنكى قەدەم',
        selectIBM: 'بىر ۋاكالەتچىنى تاللاپ ئالاقىلىشىپ، تونۇشتۇرغۇچىنىڭ ھېسابات نومۇرى ۋە مەخپىي نومۇرىغا ئئرىشىڭ',
        contactUs: 'تېخىمۇ كۆپ ئۇچۇرغا ئېرىشمەكچى بولسىڭىز بىز بىلەن ئالاقىلىشىڭ',
        enterBirthday: 'تۇغۇلغان يىل ئاي كۈننى كىرگۈزۈڭ',
        nickName: 'ئابۇنىت  نامى',
        fullName: 'تولۇق ئىسىم',
        sex: 'جىنىس',
        dateOfBirth: 'تۇغۇلغان ۋاقتى',
        idNumber: 'كىملىك نومۇرى',
        contactAddress: 'ئالاقىلىشىش ئادرىس',
        languagesSpoken: 'تىل ئالاھىدىلىكى',
        firstName: 'ئىسىم',
        middleName: '中间名(如有)',
        familyName: 'فامىلە',
        email: 'Email',
        gender: 'جىنىس',
        male: 'ئەر',
        female: 'ئايال',
        secrecy: 'مەخپىي',
        enterFirstName: 'ئىسمىڭىزنى كىرگۈزۈڭ',
        enterFamilyName: 'فامىلىڭىزنى كىرگۈزۈڭ',
        personalTitle: 'تەپسىلى ئۇچۇرۇم',
        taxInfoTitle: 'باج ئۇچۇرى',
        taxId: 'شەخسىي باج نومۇرى',
        taxRegion: 'باج تاپشۇرۇش رايونى',
        english: 'English',
        russian: 'русский язык',
        chines: '汉语',
        uyghur: 'ئۇيغۇرچە',
        date: 'ۋاقتى',
        type: 'تۈر',
        success: 'مۇۋاپپىقىيەتلىك بولدى',
        spend: 'spend',
        bonus: 'مۇكاپات',
        enterCity: 'سىز تۇرۇشلۇق شەھەر',
        rocketProgram: 'تىز ئۈسۈش پىلانى',
        defaultTv: 'دائىملىق',
        setDefault: 'دائىملىق بېكىتىش',
        delete: 'ئۆچۈرۈش',
        deleteNote: 'ئۆچۈرۈشنى جەزىملەشتۈرۈش',
        shippingAddress: 'مال تاپشۇرۇپ ئېلىش ئادېرىسى',
        masterCard: 'Master card',
        cardType: 'بانكا كارتىسى تۈرى',
        allDiscount: 'بارلىق ئېتىبارلىق كارتا ۋە جۇغلانما  كارتا',

        myBusinessNot: 'مېنىڭ سودا ئۇچۇرلىرىم',
        suspended: 'توختىتىلغان',
        editProfile: 'شەخسىي ئۇچۇرنى ئۆزگەرتىش',
        editYourAccount: 'ھېسابات نومۇرىنى ئۆزگەرتىش',
        editTaxing: 'باج ئۇچۇرىنى ئۆزگەرتىش',
        yourTaxingRegion: 'سىزنىڭ شەخسىي باج نومۇر ۋە باج تاپشۇرۇش رايونىڭىز',

        changeYourCredentials: 'مەخپىي نومۇرىڭىزنى قەرەللىك ئۆزگەرتىپ تۇرشىڭىزنى تەۋسىيە قىلىمىز',

        editPasswordSuc: 'مەخپىي نومۇر ئۆزگەرتىش ئوڭۇشلۇق بولدى',

        interEmail: 'ئېلخەت ئادرىسىڭىزنى كىرگۈزۈڭ',

        interVerCode: 'تەستىق نومۇرىنى كىرگۈزۈڭ',

        interNewPass: 'يېڭى مەخپىي نومۇرىڭىزنى كىرگۈزۈڭ',

        logoutContent: 'راستىنلا چېكىنەمسىز',

        yes: 'ھەئە',
        change: 'ئۆزگەرتىش',
        donHaveAn: 'تېزىملاتمىغان بولسىڭىز تىزىملىتىڭ',

        sendVerCode: 'دەلىللىلەش نومۇرى يوللاش',

        card: 'مال ھارۋىسى',

        chooseProduct: 'مال تاللاش',
        cartIsCurrently: 'مال ھارۋىڭىزدا ھېچقانداق مەھسۇلات يوق،\n مال قوشۇڭ',
        activated: 'ئاكتىپلاندى',
        chooseMarketingPlan: 'بازار پىلانى تاللاڭ',
        sponsorId: 'تونۇشتۇرغۇچى ID نومۇرىنى كىرگۈزۈڭ',
        sponsorKey: 'تونۇشتۇرغۇچى مەخپىي شىفىرىنى كىرگۈزۈڭ',
        activateMarketing: 'ئاكتىپلاش',
        marketingPlanThatSuits: 'ئۆزىڭىزگە ماس كېلىدىغان بازار پىلاننى ئئچىۋېلىڭ',

        haveSponsor: 'تونۇشتۇرغۇچى  ئۇچۇرى بارمۇ؟',

        canActivateYourPlan: 'تونۇشتۇرغۇچى ئۇچۇرىنى ئىگەللىگەندىن كېيىن ئاندىن مۇكاپاتلىق پىلاننى ئاكىتىپلاشتۇرالايسىز \n ئەگەر تونۇشتۇرغۇچىڭىز بولمىسا «كېيىنكى قەدەم»نى بېسىڭ، سېستىما سىز ئۈچۈن يېقىن ئەتراپتىكى تونۇشتۇرغۇچىلارنى كۆرسىتىپ بېرىدۇ \n تونۇشتۇرغۇچى بىلەن ئالاقىلىشىپ مەخپىي شىفىرگە ئېرىشىڭ',
        ibmUpgrade: 'IBM دەرىجىسىگە ئۆسۈش ئوڭۇشلۇق بولدى!',
        noReferee: 'تونۇشتۇرغۇچى  ID نومۇرىدا خاتالىق بار',

        no: 'ياق',
        informationStatus: 'سىزنىڭ ھازىرقى ھالىتىڭىز\n',

        redemptionOrders: 'مەخسۇس ۋاكالەتچىلەر جۇغلانما  بازىرىدىكى زاكاز  ئۈچۈن ئىشلىتىلىدۇ',

        ordersReduce: 'ئىشلەتكەندىن كېيىن مۇناسىپ زاكاس سومما ئازىيىدۇ',
        yorOrder: 'زاكاز',
        shippingStatus: 'يەتكۈزۈش ھالىتى',
        carrier: 'توشۇغۇچى شىركەت',
        waybillNo: 'پوچتا زاكاس نومۇرى',
        deliverAddress: 'ئادرىس',
        traceOrder: 'زاكازنى ئىز قوغلاش',
        latestShipping: 'زاكازنىڭ ئەڭ يېڭى ھالىتى',
        selectPayment: 'پۇل تاپشۇرۇش شەكلىنى تاللاڭ',
        orderPay: 'پۇل تۆلەش',
        paymentMethods: 'پۇل تۆلەش ئۇسۇسلىنى تاللاپ پۇل تۆلەڭ',
        orderTotalAmount: 'زاكاز سوممىسى',
        onlinePayment: 'توردا پۇل تۆلەش',
        reducedAmount: 'ئېتىبار قىلىنغان باھا',
        payingAmount: 'تۆلەشكە تىگىشلىك سومما',
        mobile: 'يانفون نومۇرى',
        verCodeError: 'جەزىملەشتۈرۈش نومۇرى توغرا ئەمەس',
        bestSellers: 'بازارلىق مەھسۇلات',
        notConfirmed: 'تەستىقلاش كۈتىلىۋاتىدۇ',
        unpaid: 'پۇل تۆلەنمىگەن',
        processing: 'بىر تەرەپ قىلىنىۋاتىدۇ',
        readyToShip: 'مال سېلىنىۋاتىدۇ',
        appliedRefund: 'سېلىنمىغان مالنىڭ پۇلىنى قايتۇرۇش',
        appliedForRefund: 'تاپشۇرۋېلىنغان مالنى قايتۇرۇش',
        appliedToReturn: 'مال قايتۇرۇشقا قوشۇلۇش',
        returning: 'ماقۇللاندى، مال قايتۇرۇش كۈتىلىۋاتىدۇ',
        returningOne: 'قايتۇرۇلغان مال تاپشۇرۋىلىندى، تەكشۈرۈپ ئۆتكۈزۈۋېلىش كۈتۈلۋاتىدۇ',

        reviewingReturn: 'تەكشۈرۈپ ئۆتكۈزۈۋىلىندى، پۇل قايتۇرۇشنى ساقلاڭ',

        refundApproved: 'پۇل قايتۇرۇش تەستىقلاندى',
        refunded: 'پۇل قايتۇرۇلدى',
        received: 'پۇل تاپشۇرۇۋالغانلىقىنى جەزىملەشتۈرۈش',
        goodsUserName: 'مال تاپشۇرۇۋالغۇچى ئىسمى',
        goodsUserMobile: 'مال تاپشۇرۇۋالغۇچى تېلىفون  نومۇرى',
        editAddressTitle: 'ئادرىس ئۆزگەرتىش',
        selectLanguage: 'تىل تاللاڭ',
        editLanguage: 'تىل ئۆزگەرتىش',
        discountDetail: 'ئېتبار كارتا، سوۋغاتلىق كارتا، جۇغلانما كارتا',
        transactionDetails: 'ھەميان تەپسىلىي سودا ئۇچۇرى',
        newRegistrants: 'يېڭى قوشۇلغان ۋاكالەتچى',

        myTeam: 'كوللىكتىپ',

        myActivity: 'ئاكىتىپلىق دەرىجە',

        businessActivity: 'سودا پائالىيەتلەر',

        newRegisNote: 'ئالدىنقى ئىككى ئايدا يېڭى قۇشۇلغان ۋاكالەتچىلەر',

        myTeamNote: 'مېنىڭ بىۋاستا ھەمكارلاشقۇچىلىرىم',

        myLocalBV: 'BV',
        myLocalBVNote: 'يەرلىك بازاردىكى بارلىق شەخسى سودا ۋە سېتىش ئالاھىدە مۇكاپاتىلىرى',

        sentTo: 'ئەۋەتىلىش ئالدىدا',

        already: 'ئەۋەتىلدى',

        passPortNumber: 'كىملىك نومۇرىڭىزنى كىرگۈزۈڭ',
        uploadPassPortPhoto: '请上传护照照片',

        zanCun: 'ۋاقىتلىق ساقلاش',
        oneMoreExit: 'يەنە بىر قېتىم باسسڭىز APPتىن چىكىنىسىز',

        orderAmountNotEnough: 'زاكاز سوممىسى ئېتبكار كارتىسى ئىشلىتىش تەلىپىگە ئۇيغۇن ئەمەس',

        runOut: '! ئىشلىتىپ بولدىڭىز',

        recommendedIBM: 'سېستىما  تەۋسىيە قىلغان تونۇشتۇرغۇچى',

        _no: 'نومۇر',

        weight: 'ئېغىرلىق',

        specification: 'ئۆلچىمى',

        shippedFromIpar: 'دىن كەلگەن',

        addToCart: 'مال ھارۋىسىغا قوشۇش',

        number: 'سان',

        editNickName: 'ئابۇنىت  نامىنى ئۆزگەرتىش',

        editNickNameMinContent: 'ئۆزىڭىز ياخشى كۆرىدىغان ئابونىت نامىنى ئىشلەتسىڭىز بولىدۇ',

        editAddressMInContent: 'تەپسىلىي ئادرىس',

        iparIdUserName: 'ئىپار ID،يانفون نومۇر،email',

        registerBasic: 'ئاساسىي ئۇچۇرىڭىزنى كىرگۈزۈڭ',

        selectCountryCalling: 'رايون نومۇرىنى تاللاڭ',
        selectSex: 'جىنسىڭىزنى تاللاڭ',

        keepShopping: 'داۋاملىق مال سېتىۋىلىش',

        paySuccess: 'پۇل تۆلەش مۇۋاپپىقىيەتلىك بولدى',

        paySuccessNote: 'زاكازغا پۇل تۆلەش مۇۋاپپىقىيەتلىك بولدى، سىز ئۈچۈن ئەڭ تېزسۈرئەتتە بىر تەرەپ قىلىمىز',

        shopNote: 'ئەسكەرتىش:بىر قىتىمدا پەقەت بىرلا دۆلەتنىڭ مېلىنى تاللىغىلى بولىدۇ',

        selectShippingMethod: 'مال يەتكۈزۈش ئۇسۇلىنى تاللاش',

        chooseReceivingAddress: 'مال يەتكۈزۈش ئادرىسىنى تاللاش',

        shippingMethod: 'مال يەتكۈزۈش ئۇسۇلى',

        shippingCompany: 'مال يەتكۈزۈش شىركىتى',

        shippingMethodDeliver: 'ئۆزىڭىز ماس كېلىدىغان مال يەتكۈزۈش مۇلازىمىتىنى تاللاڭ',

        selectFeature: 'مال سانى ۋە خاراكتېرىنى تاللاڭ',

        addedToCart: 'مەھسۇلات مال ھارۋىسىغا قوشۇلدى',

        confirmOrder: 'زاكازنى جەزىملەشتۈرۈش',

        receiver: 'مال تاپشۇرۇپ ئالغۇچى',

        noData: 'ھېچقانداق سانلىق مەلۇمات يوق',

        applyCouponsIf: 'ئېتىبار كارتىسى ئارقىلىق پۇل تۆلەش',

        transactionId: 'سودا نومۇرى',

        payNote: 'دەرھال پۇل تۆلەڭ، زاكاس 24سائەت ئىچىدە ئۆچۈرۈلۈپ كېتىدۇ',
        completeYourPayment: 'سودا ئۇچۇرى ئارقىلىق زاكازنى تاماملاڭ',
        orderPayment: 'پۇل تۆلەش',
        startTime: 'باشلانغان ۋاقىت',
        endTime: 'ئاخىرلاشقان ۋاقىت',
        dateSelection: 'ۋاقىت تاللاش',

        ok: 'ماقۇل',
        pending: 'ئەۋەتىلىۋاتىدۇ',
        moneyPaid: 'ئەۋەتىلدى',
        moneySuspended: 'توڭلىتىلدى',
        hereAreThe: 'شەخسىي ئۇچۇرۇم',
        settingsNote: 'مۇلازىمەت خاراكتېرىنى تاللاڭ',
        verifyYourInfo: 'سەخسىي ئۇچۇرنى تەستىقلاش',
        selectCamera: 'تاللاڭ',
        camera: 'كامېرا',
        photo: 'ئالبوم',
        deficiency: 'قالدۇق پۇلى يىتىشمەيدىكەن',
        getCoupon: 'ئىشلىتىش',
        copy: 'كۆچۈرۈش',
        qr_code: 'كود',
        childDevelopment: 'بازار ئئچىش',
        product: 'مەھسۇلات',
        article: 'ماقالە',
        searchHistory: 'ئىزدەش تارىخى：',
        MyPersonalBv: 'مېنىڭ BV بانكام',

        MyPersonalPv: 'مېنىڭ PVبانكام',

        MyPersonalSales: 'سېتىشتىن كەلگەن مۇكاپات',
        MyGrowth: 'تەرەققىيات مۇكاپاتى',
        Leadership: 'رەھبەرلىك مۇكاپاتى',
        AwardPv: 'PV مۇكاپات',

        growthNote: 'شەخسىي سېتىش ۋە مۇكاپاتتىن كېلىدۇ',

        leadershipNote: 'يېتەكچىلىك مۇكاپاتى',

        notAvailabelInYourCountry: 'مەزكۇر كەسىپ تېخى ئىچىلمىدى،تېخىمۇ كۆپ ئۇچۇرغا ئېرىشمەكچى بولسىڭىز ئىپار شىركىتى بىلەن ئالاقىلىشىڭ',

        canNotSubscribe: 'سىزنىڭ تىزىملاتقان پىلانىڭىز بار،ئوخشاش ۋاقىىتا مەزكۇر پىلانغا قاتناشسىڭىز بولمايدۇ',


        confirmationGoods: 'مالنى تاپشۇرۇپ ئېلىش',

        invoice: 'تالون',
        notNow: 'ھارزىرچە ياق',
        userInfo: 'ئابونىت ئۇچۇرى',
        loginConfirmation: 'كىرىشىنى جەزىملەشتۈرۈش',
        nullValue: 'ھازىرچە ھېچقانداق ئۇچۇر يوق، قوشىۋېلىڭ',
        loading: 'قوشۇلۇۋاتىدۇ...',
        editYourLang: 'تىلنى ئۆزگەرتىش',
        selectionClassification: 'تۈرنى تاللاڭ',
        shippingAmount: 'مال يەتكۈزۈش  ھەققى تەخمىنەن',
        nullDat: 'ھازىرچە سانلىق مەلۇمات يوق',
        balance: 'قالدۇق',
        selectWithdrawalType: 'نەق پۇل ئېلىش ئۇسۇلىنى تاللاش',

        shop: 'تور ۋاكالەتچى',

        enterYourPassword: 'مەخپىي نومۇرىنى كىرگۈزۈپ ، پۇل تۆلەڭ',

        canNotEmpty: 'قۇرۇق قالدۇرۇشقا بولمايدۇ',

        pleaseInterSelf: 'ئۆزىڭىزنى تونۇشتۇرۇڭ',

        directSponsorId: 'بىۋاستە تونۇشتۇرغۇچى ID نومۇرى',

        volume: 'نەتىجە',
        identityContent: 'تۆۋەندىكى ئۈچ باسقۇچنى  تاماملىغاندىن  كېيىن ئىپار تور سودىسىنى باشلىسىڭىز بولىدۇ',
        phone: 'يانفون نومۇرى',
        preRegistration: 'ئالدىن تىزىملىتىش',

        preRegistrationStatus: 'ئالدىن تىزىملاتقان ھالەت',
        signedOn: 'ئئمزالانغان',
        notSign: 'ئىمزالانمىدى',

        selectShop: 'دۇكان تاللاش',
        registerIsIBM_minTitle: 'بازار پىلانغا قاتناشسىڭىز 1~5 % بولغان مۇكاپات ۋە تېخىمۇ كۆپ بولغان ئېتىبارلىق سىياسەتلەردىن بەھىرلىنەلەيسىز.',
        average: 'ئوتتۇرچە  ھېسابى',
        tryAgain: 'قايتا سىناڭ',
        message: 'ئۇچۇر',
        remarks: '备注',

        confirmUserInformation: 'Confirm user information',

        upgrade:'Upgrade',
        upgradeText:'There is a new version of the app, please upgrade now to access service',


    },
    toast: {
        holName: 'كارتا ئىگىسى ئىسىم-فامىلىسىنى كىرگۈزۈڭ',

        cardNumber: 'كارتا نومۇرىنى كىرگۈزۈڭ',

        bankName: 'بانكا نامىنى كىرگۈزۈڭ',

        selectYear: 'يىلنى تاللڭ',
        selectMonth: 'ئاينى تاللڭ',
        enterCvv: 'مەزكۇر بانكا كارتىسىنىڭ cvv2سىنى كىرگۈزۈڭ',

        againGetCode: 'تەستىق نومۇرىنى قايتا ئەۋەتىش',

        withdrawAmount: 'نەق پۇل ئېلىش سوممىسى كىرگۈزۈش',

        enterVerification: 'تەستىق نومۇرىنى كىرگۈزۈش',

        registerSuccessful: 'تىزىملىتىش مۇۋاپپىقىيەتلىك بولدى',

        registerFail: 'تىزىملىتىش مەغلۇپ بولدى، قايتا سىناڭ',

        registerIsOver: 'تىزىملاتقۇچى 18 ياشقا توشقانمۇ؟',

        interNickName: 'ئابۇنىت  نومۇرىنى كىرگۈزۈش',

        isErrorShippingAddress: 'ئادېرىس شەكلىدە خاتارلىق كۆرۈلدى قايتا كىرگۈزۈڭ',

        onBack: 'يەنە بىر قېتىم بېسىپ چىكىنىش',

        loginFail: 'كىرىش مەغلۇپ بولدى',

        isNotPassword: 'مەخپىي نومۇر 8~16خانىگىچە بولغان سان-سېفىر ۋە ھەرىپتىن تۈزىلىشى كېرەك',

        registerSuccessfulIBM: 'تىزىملىتىش ئوڭۇشلۇق بولدى،داۋاملىق مال سېتىۋىلىپ ئىپار ساغلاملىق سودىسىدىن بەھىرلىنىڭ',

        registrantOver: 'تىزىملاتقۇچى چوقۇم 18ياشقا توشقان بولۇش كېرەك',

        sendVerCodeSuccess: 'تەستىق نومۇرى يانفونىڭىزغا يوللاندى',

        loginUserNotExist: 'ئابونىت مەۋجۈت ئەمەس',

        passwordNo: 'ئابونىت مەخپىي نومۇرى توغرا ئەمەس. قايتا كىرگۈزۈڭ',


        emailFormatError: 'Invalid email format， try again',

        emailAddrssTaken: 'This Email addrss taken, try a different one',

    },
    registerContractModal: {
        modalTitle: 'قانۇنى ئىززاھات',

        topContent: 'ئىپار مۇلازىمىتىنى ئىشلىتىش ئۈچۈن، سىز چوقۇم تۆۋەندىكى كېلىشىمنى تەپسىلىي ئوقۇپ قوشۇلىشىڭىز كېرەك. چوقۇم تەپسىلى ئوقۇپ،ھەر قايسى ماددىلارنىڭ مەزمۇنىنى تولۇق چۈشىنىڭ.سىز ئىپار مۇلازىمىتىنى ھەم تۆۋەندىكى كېلىشىمدىكى چەكلىمىلەرگە قوشۇلدىغانلىقىڭىزنى بىلدۈرۈڭ',
        bottomContent: 'مەن يۇقىرىدىكى ئىپار خېرىدارلار كېلىشىمى ۋە قانۇن باياناتىغا قوشۇلىمەن',

        contractOne: 'ئىپار خېرىدارلار  كېلىشىمنامىسى',

        contractTow: 'ئىپار شەخسىي ئۇچۇر سىياسىتى',

        contractThree: 'ئىپار ۋاكالەتچى كېلىشىمنامىسى',

        contractFour: 'سىز 18ياشقا توشقان',

        registerSuccessfulIBMEnd: 'سىز مۇۋەپپەقىيەتلىك ھالدا سودا مەركىزىنى قوزغاتتىڭىز،  مەزكۇر پىلاندىكى  تەلەپلەر بويىچە بازار ئئچىپ ،IBM سالاھىتىگە ئېرىشىپ  ئىپار ساغلاملىق بازىرىدىن بەھرىلىنىڭ',

    },
    monthsJson: {
        whole: 'ھەممىسى',
        january: '1_ئاي',
        february: '2_ئاي',
        march: '3_ئاي',
        april: '4_ئاي',
        may: '5_ئاي',
        june: '6_ئاي',
        july: '7_ئاي',
        august: '8_ئاي',
        september: '9_ئاي',
        october: '10_ئاي',
        november: '11_ئاي',
        december: '12_ئاي',
    },
    pay: {
        rechargeRemind: 'قالدۇق سوممىڭىز يېتىشمەيدۇ باشقا ئۇسۇل ئارقىلىق پۇل تۆلەڭ',

        selectVeriType: 'تەستىق ئۇسۇلىنى تاللاش',

        bankCardError: 'بانكا  كارتىسىدا خاتالىق كۆرۈلدى، قايتا كىرگۈزۈڭ ياكى بانكا ئۇچۇرىنى تەكشۈرۈڭ',

        receiver :'Receiver phone/ipar ID',
        amount :'Enter transfer amount',
        transfer :'Transfer',

    },

    about: {
        about: 'بىز ھەققىدە',

        officialSite: 'ئورگان تورى',

        customerSupport: 'ياردەم',

        userPrivacy: 'شەخسىي ئۇچۇر كېلىشىمنامىسى',
    },

    b: {//business
        matrix: 'Matrix',
        back: 'قايتىش',
        checkDetails: 'تەپسىلىي كۆرۈش',
        reentryNotifications: 'قايتا كىرىشكە روخسەت قىلىش ئۇقتۇرۇشى',
        ActivatedBusiness: 'سودا مەركىزى ئاكتىپلاندى',
        businessStatus: 'سودا  ھالىتى',
        businessCenter: 'سودا مەركىزى',
// bsuinessSet:'يۈرۈشلۈك',

        bsuinessSet: 'سودا  بوغچىسى',
        regsitrationPack: 'تىزىملىتىش بوغچىسى',
        volumeThreshold: 'نەتىجە',
        businessCenterID: 'سودا مەركىزى ID',

        entryDate: 'قوشۇلغان ۋاقىت',
        renewMyQualification: 'گۇۋاھنامىنى يېڭىلاش',
        qualifiedBusinessCenters: 'ئاكتىپ سودا مەركىزى',
        noQualifiedBusinessCenters: 'ئاكتىپ بولمىغان سودا مەركىزى',
        leftVolume: 'سول يېڭى سودىسى',
        rightVolume: 'ئوڭ يېڭى سودىسى',
        leftRollover: 'سول دومىلىما',
        rightRollover: 'ئوڭ دومىلىما',
        activationDate: 'گۇۋاھنامە ۋاقتى',
        estimatedPV: 'ئەڭ يېڭى مۇكاپاتىم',

        salesCommission: 'پارچە سېتىش مۇكاپاتى',

        developmentAward: 'بازار ئېچىش مۇكاپاتى',

        weeklyAward: 'ھەپتىلىك ئۈنۈم مۇكاپاتى',

        certificateDate: 'گۇۋاھنامە ۋاقتى',

        leadershipBonusDate: 'يېتەكچىلىك مۇكاپات ۋاقتى',

        activeMonthsInRow: 'ئاكتىپلىق قەرەل سان',

        myPerformance: 'مېنىڭ سودا ئەھۋالىم',

        careerProspective: 'مېنىڭ سودا ئەھۋالىم',

        myLevel: 'دەرىجەم',

        rocketProgramContent: 'سىز تەرەققى قىلىسىڭىز مۇكاتلىنىسىز، ئئمۈرلۈك مۇۋاپىقىيىتىڭىز مۇشۇ يەردىن باشلانغۇسى',
        goodToGoContent: 'بۇ پروگرامما، سىزنىڭ قىزغىنلىقىڭىز ۋە تېرىشچانلىڭىزنى مۇكاپاتلاپ تورىدۇ',
        growTogetherContent: 'ئئپار ھەمكارلاشقۇچىلىرى ئۈچۈنلا يولغا قويۇلغان، سىزنىڭ ئەڭ تىز سۈرئەتتە تەرەققى قېلىشىڭىزغا ياردەم قېلىدۇ',
        matrixContent: 'ئەڭ جانلىق سودا پىلانى بىلان تەمىنلەن ھەر قايسى جەھەتتىن ئونۈىرسال نەپكە ئېرىشىسىز',
        u_2Program: 'تەڭ تېرىشىپ تەڭ مۇۋاپىقىيەت قازىنىڭ',
        autoOrder: 'ئاپتوماتىك زاكاز',

        PVBank: 'PVبانكا',

        BVBank: 'BVبانكا',

        treeView: 'سودا تورى قۇرۇلمىسى',

        activatedNow: 'دەرھال ئاكتىپلاش',

        businessText: 'مەزكۇر سودا مەركىزى تېخى ئىچىلمىغان،تەلەپلەر بويىچە ئاكتىپلاپ،ئىپار ساغلاملىق كەسپىنىڭ خۇشاللىقىدىن بەھرىلىنىڭ',

        activateBusiness: 'سودا مەركىزىنى ئاكىتىپلاڭ',

        defaultBusiness: 'داۋاملىق سېتىۋالىمەن',

        selectDefaultBusiness: 'داۋاملىق بازار بىكىتىمەن',

        defaultBC: 'داۋاملىق بازار',

        leadershipAward: 'يېتەكچىلىك مۇكاپاتى',

        qualificationDate: 'گۇۋاھنامە ۋاقتى',

        u_2ProgramName: 'U-2 program',
        allAboutProgram: 'مېنىڭ u-2 program',

        allAboutMyMatrix: 'مېنىڭ matrix program',
        newAssociates: 'يېڭى قۇشۇلغان ھەمكارلاشقۇچى',
        subscribeNow: 'سىز مەزكۇر پىلاننى  تېخى تىزىمغا ئالدۇرمىغان، قوزغىتامسىز؟',

        accomulatedPurchaseVolume: 'ئومۇمىي نەتىجە',

        completedCycles: 'تاماملانغان سودا مەركىزى',

        myTeamTree: 'كوللىكتېپىمنىڭ دەرەخسىمان شەكلى',

        myTeamTreeNote: 'كوللىكتېپىمنىڭ تەپسىلاتى',

        day: 'كۈن',

        hours: 'سائەت',

        statementUpdate: 'ھېسابات يېڭىلاش ۋاقتى',

        levelBonus: 'دەرىجە مۇكاپاتى',

        specialBonus: 'ئالاھىدە مۇكاپات',

        targetAward: 'ھەپتىلىك نىشان مۇكاپاتى',

        associatesNeedService: 'يېتەكلەشكە تىگىشلىك ھەمكارلاشقۇچىلار',

        registerPakContent: 'تىزىملىتىش بوغچىسىنى ئاكىتىپلاپ،تېخىمۇ كۆپ مۇكاپاتلارغا ئېرىشەلەيسىز',
        is_buyContent: 'ئىپار تور سودىڭىز ھازىردى باشلىنىدۇ',
        is_preregContent: 'سودا سوممىسى تەلەپكە يەتكەن ھامان،رەسمىي ھالدا ئىپار ۋاكالەتچىسى سالاھىتىگە ئېرىشىسىز',


        sponsorKeyError: 'تونۇشتۇرغۇچى مەخپىي شىفىرىدا خاتالىق بار، قايدا كىرگۈزۈڭ',

        leftHistoryVolume: 'سول تەرەپ تارىخى',
        rightHistoryVolume: 'ئوڭ تەرەپ تارىخى',

        cashBack: 'نەق پۇل قايتۇرۇش',

        newVolume: 'يېڭى قوشۇلغان نەتىجە',

        rollover: 'دۇمىلانما',
        historyVolume: 'ئومۇمىي يىغىلغان نەتىجە',


        level: 'دەرىجە',
        levelOne: 'بىرىنچى دەرىجىلىك ۋاكالەتچى',
        levelTwo: 'ئىككىنچى دەرىجىلىك ۋاكالەتچى',
        levelThree: 'ئۈچىنچى دەرىجىلىك ۋاكالەتچى',
        levelFour: 'تۆتىنچى دەرىجىلىك ۋاكالەتچى',
        levelFive: 'بەشىنچى دەرىجىلىك ۋاكالەتچى',


        oppsStartNewBusiness: 'سىز ۋاقتىنچە يېڭى سودا مەركىزى ئاچالمايسىز',

        activateText: 'ئىپار سېستىمىسىنى ئىشلەتكىنىڭىزگە رەھمەت.    ئىپار سودا مەركىزىنى قوغزىتىش ئارقىلىق ئېتىبار ۋە مۇكاپاتتىن بەھىرلىنىڭ',


        leadingAssociates: 'مۇنەۋۋەر سودا ھەمكارلاشقۇچىسى',
        notSubsribed: 'سىز بۇبازارنى تېخى ئاچۇزمىغان، ئاچقۇزامسىز؟',

        registerIsIBMMinContent: 'Only activated users can enjoy 1 ~ 5  % cash back program and more benefits',

        plan7: 'It is a dynamic and more progressive program to get your business grow faster and help you succeed',
        plan8: 'You can sponsor global associates without limits, stay connected',

        productAdvertisingMsj: 'Activate your starter kit to be eligible for cash back and other benefits',


        myBusinessCard: 'My business card',
        packages: 'Joining packages',
        qualification: 'Qualification',

        oldLevel: '原级别',
        newLevel: '新级别',
        time: 'time',
    },

    /*order // */
    o: {
        estimated: 'تەخمىن',
        notQualified: 'لاياقەتسىز',
        calculated: 'ھېسابلاندى',

        autoOrderContent: 'ئاپتوماتىك زاكاز تېخىمۇ كۆپ ئېتبار ئېلىپ كېلىدۇ، ھەرقانداق مۇكاپاتتىن قۇرۇق قالماسىز',
        standingOrder: 'ئاپتۇماتىك زاكاز',
        soldOut: 'SOLD OUT',
        replaceOrderMsj: 'Your exchanging order needs some extra shipping cost to be paid before it can be  handled',

        expiresIn: '距结束还剩',
        returning: '配货中',

        isNotAvailableForPickup: '该产品没有您附近的仓库信息，请选择其他配送方式',

    },

    //activated
    a: {
        status1: '您的账号异常，请联系客服',
        status2: '您的个人信息尚未认证，请立即登录并提交实名认证.',
        status3: '您的资料正在审核中，请立即登录并享受购物',
        status4: '您的信息已审核通过，请继续购物',
        status5: '请确认您的个人信息并继续激活',
        status6: '您的资料正在审核中，请继续购物',

        activateYourAccountTitle: 'Activate your account once for all',
        activateYourAccountMsj: 'you can enjoy IPAR services with peace of mind',
    },


    RT: {//ReturnGoods
        refundAmount: "退款金额:",
        returnAmountMsj: '退款金额在24小时内到达您的订单支付账号',
        returnAddress: '退货地址(您可以选择一个IPAR 自营店退货)',
        warehouse: '店铺',
        returnMethod: '选择退货方式',
        selfDeliver: '自送',
        logistics: '物流',
        logisticsCompany: '物流公司',
        logisticsNumber: '请输入物流编号',
        receivingMethod: '选择您的收货方式',
        autologous: '自体',
        receivingAddress: '选择您的收货地址',
        selectWareHouse: '您可以自体的推荐仓库',
        exchangeGoods: '换货',
        exchangeRefund: '退货退款',
        refund: '退款',
        selectGoods: '请选择退还产品',
        selectReturnType: '请选择退货方式',
        selectReturnReason: '请选择退货原因',
        returnType: '退货方式：',
        returnReason: '退货原因：',
        returnGoods: '退货产品：',
        returnOrderList: '退货订单',
        returnGoodsPageMinContent: 'Exchanging orders may require reshipment costs that you need to pay for before an exchange order is handled',


        do_not_want: 'Do not want',
        damaged_product: 'Damaged product',
        not_shipped: 'Not shipped',
        wrong_product: 'Wrong product',
        exchange_other: 'Exchange other',
        receivingPickingMethod: '选择您的自体店',

    },

    alert: {
        whoToldYouAboutIPAR: 'Who told you about IPAR?',
        referingMemberr: 'Please enter the refering member\'s BC ID',
        warning: 'Note !',
        unsubscribeAccountMsj: 'Once the account is unsubscribed, you can not login or view any previous business information, Are you sure you want to ubsubscribe your ipar account?',
        unsubscribeAccount: 'unsubscribe account',

    }
}



