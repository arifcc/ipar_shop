/**
 *  2018-6-22
 *  email:nur01@qq.com
 */


import {Platform, StyleSheet} from "react-native";
import TypeUtils from "../../utils/TypeUtils";

export default StyleUtils = StyleSheet.create({
    flex: {
        flex: 1,
        backgroundColor: 'white'
    },

    /**
     * 从左边到右边
     */
    rowDirection: {
        flexDirection: 'row',
    },
    /**
     *
     * tabBar icons 的宽高度
     * */
    tabIconSize: {
        width: 22,
        height: 22,
    },

    /**
     * 宽度和高度的中间--（gravity=center）
     * */
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },

    /**
     * view里面的items的布局
     *
     * 从左边到右边
     */
    justifySpace: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
    },

    /**
     * 输入框的样式
     */
    input: {
        height: 50,
        width: '100%',
        padding: 8,
        paddingLeft: 14,
        marginTop: 12,
        textAlign:'left'
    },

    /**
     * 输入框&label的border
     */
    labelBorder: {
        borderColor: '#b4b4b4',
        borderWidth: 1,
    }
    ,

    /**
     * view 下面的 elevation
     * */
    shadowBottom: {
        shadowOffset: {
            width: 0, height:
                0.5
        },
        shadowOpacity: 0.4,
        shadowRadius: 2,
        shadowColor: '#000',
        elevation: 4,
        zIndex: Platform.OS === 'ios' ? 1 : 0,
    }
    ,
    /**
     * view 上面的 elevation
     * */
    shadowTop: {
        shadowColor: 'gray',
        shadowOpacity:
            0.1,
        shadowOffset:
            {
                width: 0, height:
                    -2
            }
        ,
        shadowRadius: 2,
        elevation:
            14,
        zIndex:
            Platform.OS === 'ios' ? 1 : 0,
    }
    ,
    /**
     * 题目的大小&&颜色
     */
    title: {
        fontSize: 18,
        color: '#313131',
        fontWeight: 'bold',
        textAlign: 'center',
        backgroundColor: 'transparent',
        fontFamily: 'ukijekran'
    }
    ,


    /**
     * 注册登陆页面的title
     */
    loginTitle: {
        fontSize: 34,
        textAlign:
            'center',
        color:
            '#313131',
        padding:
            8,
    }
    ,


    /**
     * 普通的文字
     */
    text: {
        fontSize: 16,
        padding: 8,
        color: '#898b8d',
        fontFamily: 'ukijekran'
    },

    /**
     * 产品打折text
     */
    goodsLineThroughText: {
        fontSize: 16,
        color: '#898b8d',
        textDecorationLine: 'line-through'
    },

    /**
     * status bar size
     */
    statusBarSize: {
        height: 0,
    },


    /**
     * label的宽高度
     */
    labelSize: {
        width: '100%',
        padding:
            14,
        height:
            50,
    }
    ,


    marginTop: {
        marginTop: 12,
    }
    ,
    marginBottom: {
        marginBottom: 12,
    }
    ,
    /**
     * listVIew item宽和高度
     */
    listItem: {
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        //borderBottomColor:'#cccccc',
        //borderBottomWidth:0.7
    }
    ,
    listItemTv: {
        height: '100%',
        lineHeight:
            Platform.OS === 'ios' ? 50 : 37,
        fontSize:
            16,
        color:
            'black',
        paddingLeft:
            8,
    }
    ,
    defaultTv: {
        flex: 1,
        lineHeight: Platform.OS === 'ios' ? 50 : 35,
        color: 'black',
        fontSize: 17,
        textAlign: 'left'
    },
    netTv: {
        flex: 1,
        color: 'gray'
    },
    tvBox: {
        height: 80,
        borderBottomColor: '#eeeeee',
        borderBottomWidth: 1,
        flexDirection: 'row',
    },
    lineStyle: {
        marginTop: 8,
        height: 1,
        width: '100%',
        backgroundColor: '#eeeeee'
    },
    defaultTabTv: {
        textAlign: 'center',
        fontSize: 17,
        color: 'black'
    },
    tabListItemTv: {
        textAlign: 'center',
        color: '#515151'
    },
    /*默认tv颜色和fontSize*/
    smallTvColor: {
        color: '#202020',
        fontSize: 14,
    },
    defaultTextColor: {
        color: '#555657'
    },
    /*
    自定义字体
    ukijekran.ttf
    */
    smallFont: {
        fontFamily: 'ukijekran'
    },
    /*
    自定义字体
    UyEkran_Bold.ttf
    */
    // boldFont: {
    // 	fontFamily: 'UyEkran_Bold'
    // },
    /*edit btn*/
    editTv: {
        padding: 8,
        fontSize: 14,
        color: 'gray',
        //flex: 1,
        marginTop: 10,
        textAlign: 'right',
    },
    /*没有数据TV*/
    noData: {
        fontSize: 16,
        color: 'black',
        textAlign: 'center',
        marginTop: 30
    }
})
;
