import moment from 'moment';
import I18n from '../language/i18n'

export const DATE_DATA = {


    year: moment(new Date()).format("YYYY"),
    month: moment(new Date()).format("MM"),
    day: moment(new Date()).format("DD"),
    time: moment(new Date()).format("YYYY-MM-DD HH:mm:ss"),
};
