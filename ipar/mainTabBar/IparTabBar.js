//
import React, {Component} from 'react';
import {
    Image,
    StyleSheet,
    TouchableOpacity,
    View,
    DeviceEventEmitter,
    Platform,
    Text,

} from 'react-native';
import StyleUtils from "../res/styles/StyleUtils";
import TypeUtils from "../utils/TypeUtils";
import LoginPage from "../pages/common/LoginPage";

//默认图标
const tabIcon = [
    require('../res/images/tabIcons/ic_home.png'),
    require('../res/images/tabIcons/ic_category.png'),
    require('../res/images/tabIcons/ic_ipar.png'),
    require('../res/images/tabIcons/ic_shop.png'),
    require('../res/images/tabIcons/ic_me.png'),
];
export default class IparTabBar extends Component {

    propType: {
        goToPage: PropTypes.func,
        activeTab: PropTypes.number,
        tabs: PropTypes.array,
        tabNames: PropTypes.array,
    };


    componentWillUnmount() {
        this.deEmitterGoto && this.deEmitterGoto.remove();
    }


    componentDidMount() {
        // this.props.scrollValue.addListener(IparTabBar.setAnimationValue);


        this.deEmitterGoto = DeviceEventEmitter.addListener('goToPage', (index) => {
            this.props.goToPage(index);
        });


    }

    // static setAnimationValue({value}) {
    //     //console.log('---->' + value);
    // }


    /**
     * 点击tab bar
     * @param index
     */
    onTabClick(index) {

        if (this.props.activeTab === index) {
            return
        }
        /**
         * 如果用户还没登陆的话不能看三和四页面（shopPage&&mePage）
         */
        if ((index === 3 || index === 4) && !this.props.isLogin) {
            this.props.navigator.push({
                component: LoginPage,
                name: 'LoginPage',
            })
        } else {
            this.props.goToPage(index)
        }
    }


    render() {
        return (
            <View style={[styles.tabs, StyleUtils.shadowTop]}>
                {this.props.tabs.map((tab, i) => {
                    // let icon = this.props.activeTab === i ? tabSelectedIcon[i] : tabIcon[i];


                    let tintStyle = {tintColor: this.props.activeTab !== i ? '#cdcdcd' : '#000000'};


                    return (
                        <TouchableOpacity
                            key={i}
                            style={[StyleUtils.flex, StyleUtils.center]}
                            onPress={() => this.onTabClick(i)}>
                            <View
                                style={i === 2 ? [styles.tabCenterIcon, StyleUtils.shadowTop, StyleUtils.center, {elevation: 0}] : StyleUtils.center}>


                                <Image
                                    style={[i === 2 && Platform.OS === 'ios' ? {
                                        width: 32,
                                        height: 32
                                    } : StyleUtils.tabIconSize, tintStyle]}
                                    source={tabIcon[i]}/>
                                {i === 3 && this.props.shopCardLength > 0 ? <View style={styles.shopCardLengthViewBox}>

                                    <View style={styles.shopCardLengthView}>
                                        <Text
                                            style={[styles.shopCardLengthText, StyleUtils.smallFont]}>{this.props.shopCardLength}</Text>
                                    </View>
                                </View> : null}
                            </View>
                        </TouchableOpacity>
                    )
                })}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    tabs: {
        flexDirection: 'row',
        height: TypeUtils.getPhoneType() === 'x' ? 68 : 48,
        backgroundColor: 'white',
        paddingBottom: TypeUtils.getPhoneType() === 'x' ? 28 : 0,
    },

    shopCardLengthText: {
        width: 12,
        height: 12,
        fontSize: 10,
        color: 'white',
        textAlign: 'center',
        lineHeight: Platform.OS === 'ios' ? 13 : 11,
        borderRadius: 100,

    },
    shopCardLengthView: {
        padding: 2,
        borderRadius: 100,
        backgroundColor: 'red',

    },
    shopCardLengthViewBox: {
        alignItems: 'flex-end',
        width: 40,
        top: -4,
        position: 'absolute',
    },

    tabCenterIcon: {
        borderRadius: 55,
        width: 55,
        height: 55,
        backgroundColor: 'white',
        marginTop: Platform.OS === 'ios' ? -20 : 0,
    }
});