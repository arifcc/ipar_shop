export default class RegionsIcon {
    /**
     临时解决
     */
    getIcons(region_name) {
        if (!region_name) return;
        let icon;
        switch (region_name) {
            case 'AE':
                icon = require('../res/images/region/AE.png');
                break;
            case 'AZ':
                icon = require('../res/images/region/AZ.png');
                break;
            case 'BY':
                icon = require('../res/images/region/BY.png');
                break;
            case 'CN':
                icon = require('../res/images/region/CN.png');
                break;
            case 'DE':
                icon = require('../res/images/region/DE.png');
                break;
            case 'ID':
                icon = require('../res/images/region/ID.png');
                break;
            case 'IN':
                icon = require('../res/images/region/IN.png');
                break;
            case 'KG':
                icon = require('../res/images/region/KG.png');
                break;
            case 'KZ':
                icon = require('../res/images/region/KZ.png');
                break;
            case 'MN':
                icon = require('../res/images/region/MN.png');
                break;
            case 'MY':
                icon = require('../res/images/region/MY.png');
                break;
            case 'RU':
                icon = require('../res/images/region/RU.png');
                break;
            case 'TJ':
                icon = require('../res/images/region/TJ.png');
                break;
            case 'TR':
                icon = require('../res/images/region/TR.png');
                break;
            case 'US':
                icon = require('../res/images/region/US.png');
                break;
            case 'UZ':
                icon = require('../res/images/region/UZ.png');
                break;
        }

        return icon;
    }
}