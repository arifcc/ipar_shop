import React, {Component} from 'react';

import {NativeModules} from 'react-native';

export default class MyCommonJavaJs {


    downloadImage(url,successCallback, errorCallback) {
        NativeModules.MyCommonJava.downloadImage(url, successCallback, errorCallback);
    }

}
