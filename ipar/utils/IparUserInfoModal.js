import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Modal,
    Image,
    ScrollView,
    Platform
} from 'react-native';
import StyleUtils from "../res/styles/StyleUtils";
import I18n from "../res/language/i18n";
import ViewUtils from "./ViewUtils";
import CommonUtils from "../common/CommonUtils";

export default class IparUserInfoModal extends Component {


    render() {

        let userInfo = this.props.userInfo;

        let userName = '';
        let isRemoveUser = false;
        if (userInfo) {
            userName = (userInfo.first_name || '') + ' ' + (userInfo.last_name || '');


            let userRank = userInfo.user_rank;
            if (userRank) {
                let resultDay = new CommonUtils().addDate(userName.reg_time, userRank.reg_pack_day);

                let date = new Date(resultDay);
                let serverTime = new Date(userName.server_time);
                isRemoveUser = (date < serverTime && userName.is_reg_pack === 0);
            }

        }


        return (
            <Modal
                animationType='fade'
                transparent={true}
                visible={this.props.visible}
                onRequestClose={this.props.requestClose}
            >
                <View style={[styles.boxStyle, StyleUtils.center]}>
                    {userInfo ?
                        <View style={styles.dialogBox}>

                            <ScrollView>

                                {ViewUtils.gitUserInformationTv(I18n.t('default.fullName'), userName)}

                                {ViewUtils.gitUserInformationTv(I18n.t('b.businessCenterID'), userInfo.rand_id,)}

                                {ViewUtils.gitUserInformationTv(I18n.t('b.entryDate'), userInfo.reg_time)}

                                {ViewUtils.gitUserInformationTv(I18n.t('b.regsitrationPack'), userInfo.is_reg_pack === 1 ? (I18n.t('default.activated') + '\n' + (userInfo.reg_pack_time || '')) : I18n.t('default.notActivated'))}

                                {ViewUtils.gitUserInformationTv(I18n.t('default.ibmStarterPack'), userInfo.is_buy === 1 ? (I18n.t('default.activated') + '\n' + (userInfo.buy_time || '')) : I18n.t('default.notActivated'))}

                                {ViewUtils.gitUserInformationTv(I18n.t('b.volumeThreshold'), userInfo.is_prereg === 1 ? I18n.t('default.activated') : I18n.t('default.notActivated'))}

                            </ScrollView>

                            {isRemoveUser ? ViewUtils.getButton('remove', this.props.removeUnderUser, {
                                height: 42,
                            }) : null}

                            {ViewUtils.getButton('close', this.props.requestClose, {
                                height: 42,
                                marginTop: 16,
                                backgroundColor: 'white',
                                borderWidth: 1,
                                borderColor: '#888'
                            }, {color: 'black'})}

                        </View>
                        : null}
                </View>
            </Modal>
        )
    }


}
const styles = StyleSheet.create({
        boxStyle: {
            backgroundColor: 'rgba(0, 0, 0, 0.39)',
            flex: 1,
            paddingRight: '5%',
            paddingLeft: '5%',
        },

        dialogBox: {
            width: '100%',
            maxHeight: '80%',
            backgroundColor: 'white',
            paddingTop: 18,
            paddingRight: 25,
            paddingLeft: 25,
            paddingBottom: 18,
            borderRadius: 4,
        },

    })
;

