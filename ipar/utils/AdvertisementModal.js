import React, {Component} from 'react';
import {
    DeviceEventEmitter,
    Image,
    Modal,
    Platform,
    RefreshControl,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import StyleUtils from "../res/styles/StyleUtils";
import ViewUtils from "./ViewUtils";
import I18n from '../res/language/i18n'
import IparNetwork, {IMAGE_URL, SERVER_TYPE} from "../httpUtils/IparNetwork";
import CommonUtils from "../common/CommonUtils";
import LoginPage from "../pages/common/LoginPage";
import SubAccountsPage from "../pages/mePage/business/SubAccountsPage";
import IparImageView from "../custom/IparImageView";

export default class AdvertisementModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            onLoadEndImg: false,
        }

    }


    submit() {
        let userInfo = this.props.userInfo;
        if (userInfo) {

            if (userInfo.user_id === userInfo.main_user_id && userInfo.open_plan_num > 1) {//TA用的是不是主账号
                this.props.requestClose();
                DeviceEventEmitter.emit('toast', I18n.t('b.selectDefaultBusiness'));

                this.props.navigator.push({
                    component: SubAccountsPage,
                    name: 'SubAccountsPage',
                    params: {userId: userInfo.main_user_id}
                });
                return;
            }


            CommonUtils.showLoading();


            let formData = new FormData();
            formData.append('id', this.props.dataSource.id);
            formData.append('user_id', userInfo.user_id);
            formData.append('main_user_id', userInfo.main_user_id);
            formData.append('desc', ' ');
            new IparNetwork().getPostFrom(SERVER_TYPE.admin + 'addUsersVoucher?', formData)
                .then((result) => {
                    CommonUtils.dismissLoading();
                    if (result.code === 200 || result.code === 500) {
                        this.props.requestClose();
                        DeviceEventEmitter.emit('toast', I18n.t('default.success'));
                    } else {
                        DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
                        console.log('AdvertisementModal---submit---fail: ', result)
                    }
                })
                .catch((error) => {
                    CommonUtils.dismissLoading();

                    DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
                    console.log('AdvertisementModal---submit---error: ', error)
                })
        } else {
            this.startLogin();
        }


    }


    /**
     * 跳登陆页面
     * */
    startLogin() {
        this.props.requestClose();


        this.props.navigator.push({
            component: LoginPage,
            name: 'LoginPage'
        })
    }

    render() {

        let data = this.props.dataSource;
        return (
            data ?
                <Modal//* 初始化Modal */
                    animationType='fade'           // 从底部滑入
                    transparent={true}             // 不透明
                    visible={this.props.visible}    // 根据isModal决定是否显示
                    onRequestClose={this.props.requestClose}  // android必须实现
                >

                    <View style={[styles.boxStyle, StyleUtils.center]}>
                        <View style={styles.dialogBox}>
                            <View style={styles.viewBox}>

                                <View style={{padding: 4}}>
                                    <IparImageView
                                        width={'100%'}
                                        height={300}
                                        borderRadius={4}
                                        resizeMode={'cover'}
                                        url={data.img}/>
                                </View>
                                <View style={[{
                                    alignSelf: 'flex-end',
                                    flexDirection: 'row',
                                    marginTop: 10,
                                    alignItems: 'center'
                                }]}>

                                    <TouchableOpacity
                                        style={{
                                            paddingHorizontal: 16,
                                            paddingVertical: 8,
                                            marginRight: 8,
                                        }}
                                        onPress={this.props.requestClose}>
                                        <Text style={{
                                            color: 'black',
                                        }}>{I18n.t('default.cancel')}</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        style={{
                                            backgroundColor: 'black', paddingHorizontal: 16,
                                            paddingVertical: 8, borderRadius: 4,
                                            marginRight: 8,
                                        }}
                                        onPress={() => this.submit()}>
                                        <Text style={{
                                            color: 'white',
                                        }}>{I18n.t('default.getCoupon')}</Text>
                                    </TouchableOpacity>
                                    {/*{ViewUtils.getButton(this.state.isGoods ? I18n.t('default.startShoppingBtn') : I18n.t('default.getCoupon'),*/}
                                    {/*    () => this.submit())}*/}
                                </View>
                            </View>


                        </View>
                    </View>
                </Modal> :
                <View></View>

        )
    }


}
const styles = StyleSheet.create({
    boxStyle: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        flex: 1,
        flexDirection: 'column-reverse',
    },


    viewBox: {
        backgroundColor: 'white',
        width: 300,
        borderRadius: 10,
        paddingBottom: 16,
    },


});

