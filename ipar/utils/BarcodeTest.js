import React, {Component,} from 'react'
import {
	View,
	StyleSheet,
	Alert,
	TouchableOpacity,
	Text
} from 'react-native'

import Barcode from 'react-native-smart-barcode'
import TimerEnhance from 'react-native-smart-timer-enhance'
import Navigation from "../custom/Navigation";
import I18n from '../res/language/i18n'
import GoodsPage from "../pages/common/GoodsPage";
import CustomerServicePage from "../pages/common/CustomerServicePage";
import BasWebViewPage from "../pages/common/BasWebViewPage";
import QrCodeLoginPage from "../pages/common/QrCodeLoginPage";
import LoginPage from "../pages/common/LoginPage";
import CommonUtils from "../common/CommonUtils";


class BarcodeTest extends Component {

	// 构造
	constructor(props) {
		super(props);
		// 初始状态


		this.state = {
			viewAppear: false,
		};
	}


	onClick() {
		this._startScan()
	}

	render() {

		return (
			<View style={{flex: 1, backgroundColor: 'black',}}>
				<Navigation
					onClickLeftBtn={() => this.props.navigator.pop()}
					title={I18n.t('default.qr_code')}
					titleColor={'white'}
					bgColor={'black'}
				/>
				{this.state.viewAppear ?
					<TouchableOpacity
						activeOpacity={0.8}
						style={{flex: 1}}
						onPress={() => this.onClick()}
					>
						<Barcode
							style={{flex: 1}}
							ref={component => this._barCode = component}
							onBarCodeRead={this._onBarCodeRead}/>
					</TouchableOpacity>
					: null}
			</View>
		)
	}

	componentDidMount() {
		new CommonUtils().requestCameraPermission();
		let viewAppearCallBack = (event) => {
			this.timer = setTimeout(() => {
				this.setState({
					viewAppear: true,
				});
			}, 255)
		};
		this._listeners = [
			this.props.navigator.navigationContext.addListener('didfocus', viewAppearCallBack)
		]

	}

	componentWillUnmount() {
		this.oldUrl = '';
		this.timer && clearTimeout(this.timer);
		this._listeners && this._listeners.forEach(listener => listener.remove());
	}


	resultQrCode(_url) {

		// if (this.oldUrl !== _url) {
		// 	this.oldUrl = _url;
		// } else {
		// 	return null
		// }
		//

		let url = "?" + _url.split("?")[1];


		url = decodeURI(url);
		let theRequest = {};
		let Request = {};

		let str = '';

		if (url.indexOf("?") !== -1) {
			let str = url.substr(1);
			let strs = str.split("&");
			for (let i = 0; i < strs.length; i++) {
				theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
			}

		}
		Request['url'] = "?" + str;
		Request['param'] = theRequest;

		return Request;
	}


	/**
	 *  <e.nativeEvent.data.code> Result
	 * @param e
	 * @private
	 */
	_onBarCodeRead = (e) => {
		this._barCode.stopScan();
		let code = e.nativeEvent.data.code;
		if (this.oldUrl !== code) {
			this.oldUrl = code;
			this.startPages(this.resultQrCode(code), code)
		}
	};


	/**
	 * 二维码扫码成功
	 */
	startPages(qrCode, code) {
		let _component = null;
		let params = {};


		if (qrCode !== null && qrCode.param.pid) {//二维码查找产品
			_component = GoodsPage;
			params = {
				goods_id: qrCode.param.pid
			};
		} else if (qrCode !== null && qrCode.param.rid) {//二维码登陆

			if (this.props.userData && this.props.userData.main_user_id) {//已登陆
				_component = QrCodeLoginPage;
				params = {
					rid: qrCode.param.rid,
					user_id: this.props.userData.main_user_id,
				};
			} else {//未登录
				_component = LoginPage;

			}
		} else {//别的
			_component = BasWebViewPage;
			params = {
				uri: code,
			};
		}
		if (_component) {
			this.props.navigator.push({
				component: _component,
				name: _component + '',
				params: params
			});
		}
	}


	_startScan = (e) => {
		this.oldUrl = '';
		this._barCode.startScan()
	};
	_stopScan = (e) => {
		this._barCode.stopScan()
	}


}

export default TimerEnhance(BarcodeTest)
