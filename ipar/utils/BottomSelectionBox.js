import React, {Component} from 'react'
import StyleUtils from "../res/styles/StyleUtils";
import {Modal, Text, View, StyleSheet, TouchableHighlight, TouchableOpacity} from "react-native";
import I18n from '../res/language/i18n'

/**
 * selection box (phone || email)(synPays)
 * onPress
 * @param type === 1 top / 2 bottom
 */
export default class BottomSelectionBox extends Component {


    propTypes: {
        visible: PropTypes.bold,
        onClick: PropTypes.func,//1 === top / 2 === bottom
        requestClose: PropTypes.func,
        textTop: PropTypes.string,
        textBottom: PropTypes.string,
    };


    render() {
        return <Modal//* 初始化Modal */
            animationType='slide'           // 从底部滑入
            transparent={true}             // 不透明
            visible={this.props.visible}    // 根据isModal决定是否显示
            onRequestClose={this.props.requestClose}  // android必须实现
        >

            <View style={[styles.boxStyle]}>

                <View style={{width: '100%', backgroundColor: 'white',}}>
                    <View style={[StyleUtils.rowDirection, StyleUtils.center, styles.titleBox]}>
                        <Text style={[StyleUtils.flex, {fontSize: 17, color: 'black'}]}>
                            {this.props.title}
                        </Text>


                        <TouchableOpacity
                            onPress={this.props.requestClose}>
                            <Text
                                style={[styles.closeTv, StyleUtils.smallFont]}>
                                {I18n.t('default.close')}
                            </Text>
                        </TouchableOpacity>
                    </View>


                    <View style={{padding: 8, marginBottom: 40}}>
                        {this.props.textTop ? <TouchableHighlight
                            style={{margin: 8,}}
                            underlayColor={'#e8e8e8'}
                            onPress={() => this.props.onClick(1)}>

                            <Text style={[StyleUtils.title, StyleUtils.smallFont, styles.text]}>
                                {this.props.textTop}
                            </Text>
                        </TouchableHighlight> : null}

                        {this.props.textBottom ? <TouchableHighlight
                            style={{margin: 8,}}
                            underlayColor={'#e8e8e8'}
                            onPress={() => this.props.onClick(2)}>
                            <Text style={[StyleUtils.smallFont, StyleUtils.title, styles.text]}>
                                {this.props.textBottom}
                            </Text>
                        </TouchableHighlight> : null}

                    </View>
                </View>

            </View>
        </Modal>

    }
}


const styles = StyleSheet.create({
    boxStyle: {
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        flex: 1,
        flexDirection: 'column-reverse'
    },
    titleBox: {
        height: 50,
        paddingLeft: 16
    },
    closeTv: {
        width: 70,
        textAlign: 'center',
        color: '#3c3c3c',
        fontSize: 15
    },

    text: {
        padding: 16,
        borderWidth: 1,

        borderColor: '#cecece',
    }
});