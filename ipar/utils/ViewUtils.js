/**
 *  2018-6-22
 *  email:nur01@qq.com
 */

import {
    Image,
    Platform,
    StatusBar,
    DeviceEventEmitter,
    Text,
    TextInput,
    TouchableHighlight,
    TouchableOpacity,
    View, UIManager
} from 'react-native'
import React from "react";
import StyleUtils from "../res/styles/StyleUtils";
import I18n from "../res/language/i18n";
import IparInput from "../custom/IparInput";
import CommonUtils from "../common/CommonUtils";
import GoodsIsBuyPage from "../pages/common/GoodsIsBuyPage";

export default class ViewUtils {


    /**
     * image btn
     * @param boxStyle => box 的 Style
     * @param img =》btn icon
     * @param imgStyle => btn icon 的 Style
     * @param callback =》点击事件
     * @param onPressColor =》点击的时候变的颜色
     * @returns {*}
     */
    static getImageBtnView(boxStyle, img, imgStyle, callback, onPressColor) {
        return <TouchableHighlight
            underlayColor={onPressColor ? onPressColor : '#EEEEEE'}
            activeOpacity={0.7}
            onPress={callback}
            style={boxStyle}>
            <Image
                source={img} style={imgStyle}/>
        </TouchableHighlight>
    }

    /**
     * btn
     * @param text == 文字
     * @param callBack
     * @param style
     * @param textStyle
     * @returns {*}
     */
    static getButton(text, callBack, style, textStyle) {
        return (<TouchableOpacity
                onPress={callBack}
                style={[StyleUtils.labelSize, StyleUtils.center, {backgroundColor: 'black',}, style]}>
                <Text
                    numberOfLines={1}
                    style={[StyleUtils.title, {
                        fontSize: 16,
                        color: 'white',
                    }, StyleUtils.smallFont, textStyle]}>{text ? text.toUpperCase() : ''}</Text>
            </TouchableOpacity>
        );

    }


    /**
     * 选择输入框
     * @returns {*}
     */
    static getButtunMenu(text, callback, style) {
        return (<TouchableOpacity
            onPress={callback}
            style={[StyleUtils.labelSize, StyleUtils.center, StyleUtils.justifySpace, StyleUtils.labelBorder, {
                backgroundColor: 'white'
            }, style]}>
            <Text
                numberOfLines={2}
                style={[StyleUtils.text, {
                    padding: 0,
                    flex: 1,
                    textAlign: 'left'
                }, StyleUtils.smallFont,]}>{text ? text.toUpperCase() : ''}</Text>
            <Image
                style={StyleUtils.tabIconSize}
                source={require('../res/images/ic_more.png')}/>
        </TouchableOpacity>)
    }

    /**
     *
     * */
    static getTitleView(title, content, callback, titleStyle, contentStyle) {
        return (
            <View style={[StyleUtils.center, {height: 200}]}>
                {/*title*/}
                <Text style={[StyleUtils.loginTitle, titleStyle, StyleUtils.smallFont,]}>{title}</Text>

                <TouchableOpacity
                    onPress={callback}>
                    <Text
                        style={[StyleUtils.text, StyleUtils.smallFont, {textAlign: 'center'}, contentStyle]}>{content}</Text>
                </TouchableOpacity>
            </View>
        );
    }


    /**
     * 用户信息显示text
     *@param defaultTv 默认显示text
     * @param content 获取网络content
     * @param image
     * @param callback
     */
    static gitUserInformationTv(defaultTv, content, image, callback, tvBtn, boxStyle, tvBtnStyle) {

        let itemImage = image ? <Image
            source={image} style={{width: 22, height: 22}}/> : null;
        let itemTv = tvBtn !== undefined ? <Text
            numberOfLines={1}
            style={[{
                maxWidth: 130,
                minWidth: 70,
                textAlign: 'right',
                color: 'gray',
                marginRight: 5
            }, StyleUtils.smallFont, tvBtnStyle]}>{tvBtn}</Text> : null;

        return <TouchableOpacity
            activeOpacity={0.5}
            onPress={callback}
            style={[StyleUtils.tvBox, StyleUtils.center, boxStyle]}
        >
            <View style={StyleUtils.flex}>
                <Text
                    numberOfLines={2}
                    style={[StyleUtils.defaultTv, StyleUtils.smallFont, {textAlign: 'left',}]}>{defaultTv}</Text>
                <Text
                    numberOfLines={2}
                    style={[StyleUtils.netTv, StyleUtils.smallFont, {textAlign: 'left',}]}>{content}</Text>
            </View>
            {itemTv}
            {itemImage}
        </TouchableOpacity>
    }

    /**
     * 显示text
     *@param defaultTv 默认显示text
     * @param content 获取网络content
     */
    static getDoubleTv(defaultTv, content) {
        return <View style={[StyleUtils.flex, StyleUtils.center, StyleUtils.rowDirection, {height: 25}]}>
            <Text style={[StyleUtils.flex, {
                color: '#202020',
                textAlign: 'left',
            }, StyleUtils.smallFont]}>{defaultTv}</Text>
            <Text
                style={[StyleUtils.flex, {textAlign: 'right', color: '#343434'}, StyleUtils.smallFont]}>{content}</Text>
        </View>
    }


    /**
     * 显示text
     * @param tabTv 默认显示或者listItemText
     * @param icon
     * @param boxStyle viewBox风格
     * @param tvStyle itemTv风格
     */
    static getDefaultTab(tabTv, calback, icon, boxStyle, tvStyle) {


        return <TouchableOpacity
            onPress={calback}
            style={[StyleUtils.flex, StyleUtils.center, StyleUtils.rowDirection, boxStyle, {height: 40}]}
        >
            <View style={[StyleUtils.flex, StyleUtils.center, StyleUtils.rowDirection]}>
                {tabTv.map((item, i) => {
                    return item != null ? <Text
                        numberOfLines={1}
                        style={[StyleUtils.flex,
                            StyleUtils.tabListItemTv,
                            StyleUtils.smallFont,
                            tvStyle]}>
                        {item}
                    </Text> : null
                })}
            </View>
            <Image
                style={{height: 18, width: 18}}
                source={icon}/>
        </TouchableOpacity>
    }


    static renderOperationView(userData, _this, callBack, title, msj, isShow, isTransparent, onClose, btnSrt, nullMsj) {

        let _title = '';
        let type = -1;
        if (!callBack && userData && userData.user_id !== userData.main_user_id) {
            if (userData.is_reg_pack === 0) {
                type = 0;
                _title = I18n.t('b.regsitrationPack');
            } else if (userData.is_buy === 0) {
                type = 1;
                _title = I18n.t('default.ibmStarterPack');
            } else {
                return;
            }
        } else {
            if (!title) return;
        }
        return <View

            style={[{
                backgroundColor: isTransparent ? 'rgba(213,186,124,0.9)' : '#d5ba7c',
                height: isShow ? 44 : 0,
                marginTop: isTransparent ? 0 : 10,
                paddingRight: 8,
            }, StyleUtils.justifySpace]}>

            {isShow && <View style={[{
                justifyContent: 'space-between', paddingLeft: 8, paddingRight: 18, flex: 1,
            }]}>
                <Text
                    style={[{
                        color: 'white',
                        height: nullMsj ? null : 16,
                    }, StyleUtils.smallFont]}
                    numberOfLines={nullMsj ? 2 : 1}
                >{title || _title}</Text>


                {!nullMsj && <Text
                    style={[{
                        color: 'white',
                        fontSize: 10,
                        marginTop: 2,
                        height: 14,
                    }, StyleUtils.smallFont]}
                    numberOfLines={1}
                >{msj || I18n.t('default.notActivated')}</Text>}
            </View>}


            {isShow && <TouchableOpacity
                onPress={() => {
                    if (callBack) {
                        callBack();
                        return;
                    }
                    let _params = {};

                    if (type === 0) {
                        _params = {
                            goods_id: userData.user_rank ? userData.user_rank.reg_goods_id : '',
                            title: I18n.t('b.regsitrationPack'),
                        };
                    } else {
                        _params = {
                            goods_id: userData.user_rank ? userData.user_rank.sell_goods_id : '',
                            title: I18n.t('default.ibmStarterPack'),
                        };
                    }
                    DeviceEventEmitter.emit('upDateUserData', userData);
                    _this.props.navigator.push({
                        component: GoodsIsBuyPage,
                        name: 'GoodsIsBuyPage',
                        params: _params,
                    });
                }}
                style={[StyleUtils.center, {
                    height: 24,
                    backgroundColor: '#333',
                    padding: 8,
                    marginRight: 8
                }]}>
                <Text style={[{
                    color: 'white',
                    fontSize: 12,

                }, StyleUtils.smallFont]}>{btnSrt || I18n.t('b.activatedNow')}</Text>
            </TouchableOpacity>}
            {/*<Image*/}
            {/*style={{width: 14, height: 14, tintColor: 'white'}}*/}
            {/*source={require('../../../res/images/ic_right_more.png/')}/>*/}


            {onClose && isShow &&
            <TouchableOpacity
                onPress={() => {
                    onClose();
                }}
                style={{
                    justifyContent: 'flex-start',
                    padding: 8,
                    paddingRight: 0,
                    height: '100%'
                }}>
                <Image
                    style={{width: 12, height: 12, tintColor: 'white'}}
                    source={require('../res/images/ic_close.png')}/>
            </TouchableOpacity>}
        </View>
    }


    static getEditBtn(callback, styles) {
        return <Text style={[StyleUtils.editTv, StyleUtils.smallFont, styles]}
                     onPress={callback}>{I18n.t('default.edit')}</Text>
    }

}

