import React, {Component} from 'react';

import {
    AppRegistry,
    Modal,
    StyleSheet,
    Switch,
    Text,
    TouchableHighlight,
    View,
    ListView,
    TouchableOpacity,
    DeviceEventEmitter,
    Image,
} from 'react-native';
import StyleUtils from "../res/styles/StyleUtils";
import ViewUtils from "./ViewUtils";
import I18n from "../res/language/i18n";


export default class ModalListUtils extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: this.props.visible,
            dataSource: this.props.dataSource,
            dataParam: this.props.dataParam,
        }
    }


    /**
     * 刷新props
     * @param nexProps
     */
    componentWillReceiveProps(nexProps) {
        this.setState({
            visible: nexProps.visible,
            dataSource: nexProps.dataSource,
            dataParam: nexProps.dataParam,
        });
    }


    /**
     * item 的点击事件
     * @param index
     */
    onItemClick(index, text) {

        if (this.props.onPress) {
            this.props.onPress(index, text)
        } else {
            this.setState({
                visible: false,
            });
        }
    }

    /**
     * 初始化list view的items
     *
     * @param rowData
     * @returns {*}
     */
    renderRow(rowData, rowId) {

        let text = this.state.dataParam ? rowData[this.state.dataParam] : rowData;

        return (
            <TouchableHighlight
                onPress={() => this.onItemClick(rowId, text)}
                key={rowId}
                underlayColor={'#e8e8e8'}
                activeOpacity={0.7}
                style={{padding: 8}}>
                <Text style={[StyleUtils.title, StyleUtils.smallFont]}>{text}</Text>
            </TouchableHighlight>
        )
    }


    close() {
        this.setState({visible: false});
        this.props.close && this.props.close()
    }

    render() {
        return (
            <Modal
                animationType={'fade'}
                transparent={true}
                visible={this.state.visible}
            >
                <View
                    style={[StyleUtils.flex, StyleUtils.center, styles.boxStyle]}>
                    <View style={[styles.listBoxStyle]}>
                        {/*<View style={[StyleUtils.justifySpace, {backgroundColor: '#333333',}]}>*/}

                        {/*    <Text*/}
                        {/*        numberOfLines={1}*/}
                        {/*        style={[StyleUtils.title, StyleUtils.smallFont, styles.titleStyle]}>{this.props.title}</Text>*/}

                        {/*    /!*close btn*!/*/}
                        {/*    /!*{ViewUtils.getImageBtnView({alignSelf: 'flex-end'}, require('../res/images/ic_close.png'),*!/*/}
                        {/*    /!*    [styles.iconStyle], () => {*!/*/}
                        {/*    /!*        this.close()*!/*/}
                        {/*    /!*    }, '#494949')}*!/*/}

                        {/*</View>*/}

                        {this.props.title &&
                        <Text style={[StyleUtils.smallFont, styles.title]}>{this.props.title}</Text>}
                        {this.props.title &&
                        <View style={{height: 0.3, backgroundColor: '#f3f3f3', marginBottom: 16}}/>}
                        <ListView
                            dataSource={this.state.dataSource}
                            renderRow={(rowData, sectionId, rowId) => this.renderRow(rowData, rowId)}
                        />

                        {/*<View style={[StyleUtils.rowDirection, {alignSelf: 'flex-end'}]}>*/}
                        <TouchableOpacity
                            onPress={() => this.close()}
                            style={[{
                                paddingHorizontal: 12,
                                marginTop: 16,
                                paddingVertical: 8,
                                alignSelf: 'flex-end',
                                borderRadius: 10
                            }]}>
                            <Text style={[StyleUtils.smallFont, {color: 'black'}]}>{I18n.t('default.cancel')}</Text>
                        </TouchableOpacity>
                        {/*</View>*/}

                    </View>
                </View>
            </Modal>
        )
    }
}
const styles = StyleSheet.create({
    boxStyle: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    title: {
        fontSize: 18,
        paddingBottom: 8,
        fontWeight: 'bold',
        color: 'black'
    },
    listBoxStyle: {
        backgroundColor: 'white',
        width: '80%',
        maxHeight: '70%',
        borderRadius: 10,
        minHeight: 180,
        paddingHorizontal: 20,
        paddingVertical: 14
    },

    titleStyle: {
        color: 'white',
        paddingLeft: 16,
        paddingRight: 8,
        flex: 1,
        textAlign: 'left',
    },

    iconStyle: {
        width: 16,
        height: 16,
        margin: 14,
        tintColor: 'white',
    },

});
