import React, {Component} from 'react'
import {Modal, Text, Linking, Image, Platform, TouchableOpacity, NativeModules, StyleSheet, View} from 'react-native'
import StyleUtils from "../res/styles/StyleUtils";
import NativeCommon from "../common/NativeCommon";
import IparNetwork from "../httpUtils/IparNetwork";
import I18n from "../res/language/i18n";

export default class UpgradeView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            data: null,
            update_type: 0,
            systemVersion: '',
        };
        this.iparNetwork = new IparNetwork();
    }

    componentDidMount() {
        this.getVerSion();
    }


    /**
     * 获取新版版
     */
    requestVertion() {

        let {systemVersion} = this.state;

        let appType = Platform.OS === 'ios' ? 1 : 2;

        this.iparNetwork.getRequest('https://admin.iparbio.com/getVersion?', 'app_type=' + appType + '&status=1&version=' + systemVersion)
            .then((res) => {
                console.log(res);
                if (res.code === 200) {
                    this.version(res.data);
                }
            })
            .catch((err) => {
                console.log('UpgradeView---componentDidMount-err: ' + err);
            });
    }

    /**
     * 获取版本号
     */
    getVerSion() {
        let iparAppVersion = NativeModules.IparAppVersion;
        if (!iparAppVersion) return;
        iparAppVersion.getAppVersion((err, event) => {
            this.setState({
                systemVersion: Platform.OS === 'ios' ? event : err,
            });
            this.requestVertion();

        })
    }


    version(data) {
        if (!data) return;
        if (data.length <= 0) return;


        let forcedUpdates = false;//强制更新
        let updadig = false;//优化中
        let _data;
        for (let item of data) {
            if (item.update_type == 2) {
                _data = item;
                forcedUpdates = true;
            } else if (item.update_type == 3) {
                if (!forcedUpdates) {
                    _data = item;
                }
                updadig = true;
            }
        }

        let update_type = 1;
        if (forcedUpdates)
            update_type = 2;
        else if (updadig)
            update_type = 3;

        console.log(_data);
        this.setState({
            data: _data,
            update_type,
            visible: true,
        })
    }

    render() {
        const {onRequestClose} = this.props;
        const {visible, data, update_type, systemVersion} = this.state;
        let message = 'content_' + [I18n.locale];
        return (data ?
                <Modal
                    animationType={'fade'}
                    transparent={true}
                    visible={visible}
                    onRequestClose={onRequestClose}
                >
                    <View style={[styles.box]}>
                        <View style={styles.upgradeView}>

                            <Image
                                style={styles.icon}
                                source={require('../res/images/ic_ipar_logo.png')}/>
                            <Text style={styles.verTv}>V{systemVersion}</Text>
                            <View style={{padding: 20, width: '100%'}}>
                                <Text style={styles.title}>{data && data[message]}</Text>

                                <View style={{flexDirection: 'row', justifyContent: 'flex-end', paddingTop: 32}}>

                                    {update_type === 1 && <TouchableOpacity
                                        style={[StyleUtils.center, {
                                            paddingHorizontal: 16,
                                            paddingVertical: 8,
                                        }]}
                                        onPress={onRequestClose}>
                                        <Text>{I18n.t('default.cancel')}</Text>
                                    </TouchableOpacity>}

                                    {update_type !== 3 && <TouchableOpacity
                                        style={[styles.btn, StyleUtils.center, {marginLeft: 16}]}
                                        onPress={() => {
                                            let url = 'market://details?id=com.ipar_shop';
                                            if (Platform.OS === 'ios')
                                                url = 'itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?mt=8&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software&id=1434506518';

                                            Linking.openURL(url);
                                        }}>
                                        <Text style={{color: 'white'}}>Upgrade</Text>
                                    </TouchableOpacity>}

                                </View>
                            </View>
                            {/*<Image*/}
                            {/*    style={{position: 'absolute', right: 0, width: 60, height: 60, top: 0}}*/}
                            {/*    source={require('../res/images/ic_new.png')}/>*/}
                        </View>
                    </View>
                </Modal> : <View/>
        )
    }
}

const styles = StyleSheet.create({
    box: {
        backgroundColor: 'rgba(0,0,0,0.6)',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    upgradeView: {
        borderRadius: 12,
        backgroundColor: 'white',
        width: '80%',
        alignItems: 'center'
    },

    icon: {
        width: '60%',
        marginTop: 16,
        marginBottom: 6,
        height: 30,
        resizeMode: 'contain'
    },
    title: {
        fontSize: 18,
        color: 'black',
        fontWeight: 'bold',
        marginBottom: 6,
        marginTop: 16,
        textAlign: 'center'
    },


    verTv: {
        color: 'gray'
    },

    btn: {
        backgroundColor: 'black',
        borderRadius: 4,
        paddingHorizontal: 16,
        paddingVertical: 8,
    }


});
