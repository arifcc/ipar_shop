import React, {Component} from 'react';

import {
    Modal,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    ListView,

} from 'react-native';

import StyleUtils from "../res/styles/StyleUtils";
import I18n from "../res/language/i18n";


export default class ModalBottomUtils extends Component {

    propTypes: {
        visible: PropTypes.isRequired,
        renderRow: PropTypes.isRequired,
        requestClose: PropTypes.isRequired,
        dataSource: PropTypes.array,
        title: PropTypes.string,
    };

    static defaultProps = {
        isModal: false,
        dataSource: [],

    };


    render() {
        return (
            <Modal//* 初始化Modal */
                animationType='slide'           // 从底部滑入
                transparent={true}             // 不透明
                visible={this.props.visible}    // 根据isModal决定是否显示
                onRequestClose={this.props.requestClose}  // android必须实现
            >
                <View style={[styles.boxStyle]}>

                    <View style={{height: '45%', width: '100%', backgroundColor: 'white'}}>
                        <View style={[StyleUtils.rowDirection, StyleUtils.center, styles.titleBox]}>
                            <Text style={[StyleUtils.flex, StyleUtils.smallFont, {
                                fontSize: 17,
                                color: 'black',
                                textAlign: 'left'
                            }]}>{this.props.title}</Text>
                            <Text
                                onPress={this.props.requestClose}
                                style={[styles.closeTv, StyleUtils.smallFont]}>{I18n.t('default.close')}</Text>
                        </View>

                        <ListView
                            refreshControl={this.props.refreshControl}
                            style={{paddingLeft: 8, paddingRight: 8}}
                            renderRow={this.props.renderRow}
                            dataSource={this.props.dataSource}
                        />
                    </View>
                    <TouchableOpacity style={{flex: 1}} onPress={this.props.requestClose}/>
                </View>
            </Modal>

        );
    }


}
const styles = StyleSheet.create({
    boxStyle: {
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        flex: 1,
        flexDirection: 'column-reverse'
    },
    titleBox: {
        height: 50,
        borderBottomColor: '#ddd',
        borderBottomWidth: 1,
        paddingLeft: 8
    },
    closeTv: {
        width: 70,
        textAlign: 'center',
        color: '#3c3c3c',
        fontSize: 15
    },


});