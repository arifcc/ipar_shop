import React, {Component} from 'react';

import {
    AppRegistry,
    DeviceEventEmitter,
    Dimensions,
    Image,
    ListView,
    Modal,
    StyleSheet,
    Switch,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View,
} from 'react-native';
import StyleUtils from "../res/styles/StyleUtils";
import ViewUtils from "./ViewUtils";
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'
import I18n from '../res/language/i18n'
import IparInput from "../custom/IparInput";

let height = Dimensions.get('window').height;


export default class InputModal extends Component {

    constructor(props) {
        super(props);
        this.inputValue = '';
        this.state = {
            bottomHeight: 0,
            canNotEmpty: false
        }
    }

    propTypes: {
        inputPlaceholder: PropTypes.isRequired,
        btnPlaceholder: PropTypes.isRequired,
        isPassword: PropTypes.isRequired,
        visible: PropTypes.isRequired,
        requestClose: PropTypes.requestClose,
        submit: PropTypes.requestClose,
    };


    onLayout(event) {
        this.setState({
            bottomHeight: height - event.nativeEvent.layout.height
        })
    }


    submit() {
        if (!this.inputValue.trim()) {
            this.setState({
                canNotEmpty: true
            });
            return;
        }
        this.props.requestClose();
        this.props.submit(this.inputValue);
    }

    render() {
        let props = this.props;
        return (
            <Modal
                animationType={'slide'}
                transparent={true}
                onRequestClose={props.requestClose}
                visible={props.visible}>
                <View
                    style={[StyleUtils.flex, StyleUtils.center, styles.boxStyle]}>
                    <KeyboardAwareScrollView style={{width: '100%'}}>
                        <View
                            onLayout={(event) => this.onLayout(event)}
                            style={[styles.bottomBox, {marginTop: this.state.bottomHeight}]}>

                            <View style={StyleUtils.justifySpace}>
                                <Text style={StyleUtils.smallFont}>{this.props.inputPlaceholder}</Text>
                                <TouchableOpacity
                                    onPress={props.requestClose}
                                    style={{padding: 6}}
                                >
                                    <Text
                                        style={[styles.closeText, StyleUtils.smallFont]}>{I18n.t('default.close')}</Text>
                                </TouchableOpacity>
                            </View>
                            <IparInput
                                {...this.props}
                                style={{
                                    height: 46,
                                    marginTop: 26,
                                    fontSize: 16,
                                    borderColor: this.state.canNotEmpty ? '#b60000' : 'gray',
                                }}
                                onChangeText={(text) => {
                                    this.inputValue = text;
                                    if (this.state.canNotEmpty) {
                                        this.setState({
                                            canNotEmpty: false,
                                        })
                                    }
                                }}
                                autoFocus={true}
                                placeholder={props.inputPlaceholder}/>


                            {this.state.canNotEmpty ?
                                <Text
                                    style={[styles.text, StyleUtils.smallFont]}>{I18n.t('default.canNotEmpty')}</Text> : null}


                            {ViewUtils.getButton(props.btnPlaceholder, () => this.submit(), {
                                marginTop: 26,
                                height: 46
                            })}

                            <TouchableOpacity
                                onPress={this.props.onPressPass}
                                style={{padding: 8, marginTop: 8}}>

                                <Text style={[{
                                    width: '100%',
                                    textAlign: 'center'
                                }, StyleUtils.smallFont]}>{I18n.t('default.forgotPass')}</Text>

                            </TouchableOpacity>

                        </View>
                    </KeyboardAwareScrollView>
                </View>
            </Modal>
        )
    }
}
const styles = StyleSheet.create({
    boxStyle: {
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        justifyContent: 'flex-end',
    },


    closeText: {
        fontSize: 16,
    },
    text: {
        fontSize: 12,
        paddingTop: 4,
    },
    bottomBox: {
        backgroundColor: '#fff',
        width: '100%',
        padding: 16,
        paddingBottom: 100,
    },

});