import React, {Component} from 'react';
import StyleUtils from "../res/styles/StyleUtils";
import {
	Text,
	View,
	TouchableOpacity
} from 'react-native';

export default class SelectTopBtn extends Component {
	render() {
		return (<TouchableOpacity
			onPress={this.props.onPress}
			activeOpacity={0.8}
			style={[StyleUtils.center, {
				ight: '100%',
				borderBottomColor: this.props.type ? '#000000' : '#00000000',
				borderBottomWidth: 1.5,
				paddingLeft: 8,
				paddingRight: 8
			}]}>
			<Text numberOfLines={1} style={[StyleUtils.smallFont,{
				color: this.props.type ? 'black' : 'gray',
				fontSize: this.props.type ? 15 : 14
			}]}>{this.props.text}</Text>
		</TouchableOpacity>)
	}
}
