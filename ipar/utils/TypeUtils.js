/**
 *  2018-6-22
 *  email:nur01@qq.com
 */


import {Platform, Dimensions} from "react-native";

export let screenW = Dimensions.get('window').width;
export let screenH = Dimensions.get('window').height;
// iPhoneX
const X_WIDTH = 375;
const X_HEIGHT = 812;


export default class TypeUtils {

    /**
     * phone types
     *
     * (x) => iPhoneX
     * (ios) => ios (iphone)
     * (android) => android (安卓)
     *
     */
    static getPhoneType() {
        //iPhoneX
        // if (Platform.OS === 'ios' &&
        //     !Platform.isPad &&
        //     !Platform.isTVOS &&
        //     ((screenH === X_HEIGHT && screenW === X_WIDTH) ||
        //         (screenH === X_WIDTH && screenW === X_HEIGHT))) {
        //     return 'x'
        // } else
        if (Platform.OS === 'ios') {
            return 'ios'
        } else if (Platform.OS === 'android') {
            return 'android'
        }
    }

}