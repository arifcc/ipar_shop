import React, {Component} from 'react';


import {Modal, View} from 'react-native';

export default class BaseModal extends Component {


    propTypes: {
        visible: PropTypes.request.boolean,
        requestClose: PropTypes.request.func,
    };


    render() {
        let props = this.props;
        return (
            <Modal
                animationType={'slide'}
                transparent={true}
                onRequestClose={props.requestClose}
                visible={props.visible}>

                <View
                    style={{backgroundColor: 'rgba(0,0,0,0.5)', flex: 1}}>
                    {this.props.children}
                </View>

            </Modal>
        )
    }
}
