/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    DeviceEventEmitter,
    Dimensions,
    Image,
    ListView,
    RefreshControl,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import StyleUtils from "../../res/styles/StyleUtils";
import CategoryMinPage from "./CategoryMinPage";
import CommonUtils from "../../common/CommonUtils";
import IparNetwork, {IMAGE_URL, SERVER_TYPE} from "../../httpUtils/IparNetwork";
import I18n from '../../res/language/i18n'
import IparImageView from "../../custom/IparImageView";

let dataArrayTop = ["page one", "page two", "page three", "page four", "page four", "page four", "page four"];
const {width} = Dimensions.get('window');

export default class CategoryTableTowView extends Component {

    constructor(props) {
        super(props);
        this.iparNetwork = new IparNetwork();
        this.country = '';
        // 初始状态
        let ds = new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2,
        });
        this.state = {
            adverData: null,
            dataSource: ds,
            loading: false,
        }
    }


    componentDidMount() {
        CommonUtils.getAcyncInfo('country_iso')
            .then((country) => {
                this.country = country;
                this.loadData()
            });

    }


    /**
     * 获取信息
     * @param country
     */
    loadData() {
        this.setState({
            loading: true,
        });
        this.iparNetwork.getRequest(SERVER_TYPE.goods + 'goodsCate?',
            'country=' + this.country + '&cate_id=0')//cate_id=0为父分类
            .then((result) => {
                if (result.code === 200) {
                    this.setState({
                        adverData: result.data.top,
                        dataSource: this.state.dataSource.cloneWithRows(result.data.category),
                        loading: false,
                    })
                } else {
                    this.setState({
                        loading: false,
                    });
                    DeviceEventEmitter.emit('toast', I18n.t('default.fail'))
                }
            })
            .catch((error) => {
                this.setState({
                    loading: false,
                });
                console.log('CategoryTableTowView---loadData--error: ', error);
                DeviceEventEmitter.emit('toast', I18n.t('default.fail'))
            })

    }


    /**
     * list view 的 item 点击
     */
    onItemClick(rowData) {

        let _name = rowData.name;
        switch (I18n.locale) {
            case 'uy':
                _name = rowData.name_uy ? rowData.name_uy : _name;
                break;
            case 'en':
                _name = rowData.name_en ? rowData.name_en : _name;
                break;
            case 'ru':
                _name = rowData.name_ru ? rowData.name_ru : _name;
                break;
        }


        this.props.navigator.push({
            component: CategoryMinPage,
            params: {
                title: _name,
                cate_id: rowData.cate_id,
                country: this.country,
            }
        })
    }

    /**
     * Brand items
     * @param rowData
     * @returns {*}
     * @private
     */
    _renderRowBrand(rowData) {


        let icon;
        switch (rowData.cate_id) {
            case 1:
                icon = require('../../res/images/categoryIcon/saglamlik.png');
                break;
            case 2:
                icon = require('../../res/images/categoryIcon/kunqiqak.png');
                break;
            case 3:
                icon = require('../../res/images/categoryIcon/sikin.png');
                break;
            case 4:
                icon = require('../../res/images/categoryIcon/huli1.png');
                break;
            case 6://商务
                icon = require('../../res/images/ic_shangwu.jpg');
                break;
            case 366:
                icon = require('../../res/images/ic_cosmetics.jpg');
                break;
            default: {
                icon = require('../../res/ic_brand_test.png')
            }
        }


        //goods name
        let goodsName = rowData.name;

        switch (I18n.locale) {
            case 'en':
                goodsName = rowData.name_en ? rowData.name_en : goodsName;
                break;
            case 'ru':
                goodsName = rowData.name_ru ? rowData.name_ru : goodsName;
                break;
            case 'uy':
                goodsName = rowData.name_uy ? rowData.name_uy : goodsName;
                break;
        }


        return (
            <TouchableOpacity
                onPress={() => this.onItemClick(rowData)}
                style={{paddingLeft: 16, marginTop: 8}}>
                <Image
                    resizeMode="cover"
                    style={{zIndex: 99, width: (width / 2) - 24, height: (width / 4), resizeMode: 'cover'}}
                    source={icon}/>
                <Text
                    style={[StyleUtils.text, StyleUtils.smallFont, StyleUtils.marginBottom, {width: (width / 2) - 24}, {
                        paddingLeft: 0,
                        marginTop: 8,
                        color: '#333'
                    }]}>{goodsName}</Text>

            </TouchableOpacity>
        )
    }

    /**
     * 初始化list view
     * */
    renderListView(dataSource, renderRow) {
        return (
            <ListView
                renderRow={renderRow}
                contentContainerStyle={styles.gridListStyle}
                dataSource={dataSource}/>
        );
    }


    render() {
        return (
            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.loading}
                        onRefresh={() => {
                            this.loadData()
                        }}
                    />
                }>
                {/**广告*/}
                <View style={{padding: 16,}}>
                    {this.state.adverData &&
                    <IparImageView
                        errorImage={require('../../res/images/ic_image_fail.png')}
                        resizeMode={'cover'}
                        width={'100%'} height={200}
                        url={this.state.adverData.adver_img}/>}
                </View>

                {/**分类*/}
                {this.renderListView(this.state.dataSource, this._renderRowBrand.bind(this))}

                {/**分类题目*/}
                {/*<TouchableOpacity style={[StyleUtils.justifySpace, {paddingRight: 16}]}>*/}
                {/*<Text style={[StyleUtils.text, styles.title]}>CATEGORY</Text>*/}
                {/*<Image source={require('../../res/images/ic_right_more.png')}*/}
                {/*style={styles.more}/>*/}
                {/*</TouchableOpacity>*/}


            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({

    gridListStyle: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    title: {
        paddingLeft: 16,
        paddingBottom: 0,
        color: '#333',
        fontSize: 18
    },
    more: {
        width: 18,
        height: 18,
        tintColor: '#333'
    }
});
