/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import StyleUtils from "../../res/styles/StyleUtils";
import CategoryOneView from "./CategoryTableOneView";
import CategoryTowView from "./CategoryTableTowView";
import ScrollableTabView from 'react-native-scrollable-tab-view';
import I18n from '../../res/language/i18n'

let clickTypes = {tabLeOne: I18n.t('default.discover'), tabLeTow: I18n.t('default.brands')};
export default class CategoryPage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            selectTable: 0,
            globalIsNotNull: true,
        }
    }


    render() {
        return (
            <View style={[StyleUtils.flex, {backgroundColor: 'white'}]}>


                {/*{this.renderContent()}*/}


                <ScrollableTabView
                    renderTabBar={() => <RenderTabLe
                        globalIsnull={this.state.globalIsNotNull}
                        selectTable={this.state.selectTable}/>}
                    onScroll={(postion) => {
                        // float类型 [0, tab数量-1]
                        if (postion === 0) {
                            this.setState({
                                selectTable: postion,
                            })
                        } else if (postion === 1) {
                            this.setState({
                                selectTable: postion,
                            })
                        }
                    }}
                >
                    {this.state.globalIsNotNull ?
                        <CategoryOneView
                            isDataNull={() => {
                                this.setState({
                                    globalIsNotNull: false,
                                })
                            }}
                            {...this.props}/> : null
                    }
                    <CategoryTowView {...this.props}/>
                </ScrollableTabView>


            </View>
        );
    }
}

/**
 * table layout
 */
class RenderTabLe extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectTable: this.props.selectTable,
        }
    }

    /**
     * props该的时候
     * @param nexProps
     */
    componentWillReceiveProps(nexProps) {
        if (nexProps.selectTable !== this.state.selectTable) {
            this.setState({
                selectTable: nexProps.selectTable,
            })
        }
    }

    onClick(type) {
        let globalIsNotNull = this.props.globalIsnull;

        switch (type) {
            case clickTypes.tabLeOne:
                if (this.state.selectTable !== 0) {
                    this.props.goToPage(0);
                    this.setState({
                        selectTable: 0
                    })
                }
                break;
            case clickTypes.tabLeTow:
                if (this.state.selectTable !== globalIsNotNull ? 1 : 0) {
                    this.props.goToPage(globalIsNotNull ? 1 : 0);
                    this.setState({
                        selectTable: globalIsNotNull ? 1 : 0,
                    })
                }
                break;
        }
    }

    render() {


        let globalIsNotNull = this.props.globalIsnull;
        let towTableSelect = this.state.selectTable === (globalIsNotNull ? 1 : 0);
        return <View style={[StyleUtils.rowDirection, {alignItems: 'center', marginTop: 8, marginBottom: 16}]}>
            {globalIsNotNull ? <TouchableOpacity onPress={() => this.onClick(clickTypes.tabLeOne)}>
                <Text
                    style={[StyleUtils.text, StyleUtils.smallFont, this.state.selectTable === 0 ? styles.selectTable : null]}>
                    {I18n.t('default.discover')}
                </Text>
            </TouchableOpacity> : null}
            <TouchableOpacity onPress={() => this.onClick(clickTypes.tabLeTow)}>
                <Text
                    style={[StyleUtils.text, StyleUtils.smallFont, towTableSelect ? styles.selectTable : null]}>
                    {I18n.t('default.brands')}
                </Text>
            </TouchableOpacity>
        </View>
    }
}


const styles = StyleSheet.create({
    selectTable: {
        fontSize: 18,
        color: '#333333',
        fontWeight: ('bold', '900')
    }
});