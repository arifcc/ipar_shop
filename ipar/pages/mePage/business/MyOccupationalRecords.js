import React, {Component} from 'react'
import {View, FlatList} from 'react-native'
import IparNetwork, {SERVER_TYPE} from "../../../httpUtils/IparNetwork";
import CommonUtils from "../../../common/CommonUtils";
import StyleUtils from "../../../res/styles/StyleUtils";
import ViewUtils from "../../../utils/ViewUtils";
import I18n from "../../../res/language/i18n";

export default class MyOccupationalRecords extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
        };

        this.iparNetwork = new IparNetwork();
    }

    componentDidMount() {
        this.getMyOccupationalRecordsData();
    }


    /**
     * 获取用户的升级记录
     */
    getMyOccupationalRecordsData() {
        const {userData} = this.props;

        CommonUtils.showLoading();
        this.iparNetwork.getRequest(SERVER_TYPE.admin + 'getUserLevelHistory?', 'user_id=001139b4-57d9-32b4-b320-a841551654d9')
            .then((res) => {
                CommonUtils.dismissLoading();

                console.log(res);
                if (res.code === 200) {
                    this.setState({
                        data: res.data
                    })
                } else {

                }
            })
            .catch((err) => {
                CommonUtils.dismissLoading();

            })
    }

    render() {
        const {data} = this.state;

        return (
            <View style={[StyleUtils.flex]}>
                {ViewUtils.getTitleView('我的职业记录')}

                <View style={{height: 50, widget: '100%'}}>
                    {ViewUtils.getDefaultTab([I18n.t('b.oldLevel'), I18n.t('b.newLevel'), I18n.t('b.time')], '', '', {height: 50}, StyleUtils.defaultTabTv)}
                </View>

                <FlatList//item.
                    renderItem={(item) => {
                        let item1 = item.item;
                        if (!item1) return <View/>;
                        if (!item1.old_level) return <View/>;
                        if (!item1.new_level) return <View/>;
                        return ViewUtils.getDefaultTab([item1.old_level.level_name, item1.new_level.level_name, item1.years],
                            '', '', {height: 50}, StyleUtils.defaultTabTv)
                    }}
                    data={data}/>

            </View>
        )
    }
}
