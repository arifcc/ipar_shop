import React, {Component} from 'react'
import {
    ScrollView,
    Text,
    DeviceEventEmitter,
    View,
    StyleSheet,
    LayoutAnimation,
    UIManager,
    Platform
} from "react-native";
import StyleUtils from "../../../res/styles/StyleUtils";
import Navigation from "../../../custom/Navigation";
import ViewUtils from "../../../utils/ViewUtils";
import I18n from "../../../res/language/i18n";
import ProgressBar from "../../../custom/ProgressBar";
import GoodsPage from "../../common/GoodsPage";
import GoodsIsBuyPage from "../../common/GoodsIsBuyPage";
import CommonUtils from "../../../common/CommonUtils";
import SlideMenu from "../../../menu/SlideMenu";

export default class BusinessStatusPage extends Component {


    constructor(props) {
        super(props);

        let userData = this.props.userData;
        this.getActivatedData(userData);


        let _progressValue = userData.history_total_pv > 300 ? 300 : userData.history_total_pv;
        this.state = {
            progressBoxW: 0,
            progressTextW: 0,
            progressValue: _progressValue,
            progressMaxValue: 300,
        };
    }


    /**
     * 点击事件
     */
    onClick(data, isActiv) {
        let userData = this.props.userData;
        let _component = null;
        let _params = {};
        switch (data.code) {
            case 'is_reg_pack'://注册包
                if (isActiv) {
                    return
                }
                _component = GoodsIsBuyPage;
                _params = {
                    goods_id: userData.user_rank ? userData.user_rank.reg_goods_id : '',
                    title: I18n.t('b.regsitrationPack'),
                };
                DeviceEventEmitter.emit('upDateUserData', userData);
                break;
            case 'is_buy':
                if (isActiv) {
                    return
                }
                _component = GoodsIsBuyPage;
                _params = {
                    goods_id: userData.user_rank ? userData.user_rank.sell_goods_id : '',
                    title: I18n.t('default.ibmStarterPack'),
                };
                DeviceEventEmitter.emit('upDateUserData', userData);
                break;
            case 'is_prereg':
                this.startNewShop();
                break;
        }

        if (_component) {
            this.props.navigator.push({
                component: _component,
                name: _component + '',
                params: _params,
            });
        }

    }

    /**
     * 初始化progress bar
     */
    renderProgress() {

        let userData = this.props.userData;

        let state = this.state;
        let progressBoxW = state.progressBoxW;
        let progressTextW = state.progressTextW;
        let progressValue = state.progressValue / (state.progressMaxValue / 100);
        let textValue = 0;

        if (progressBoxW !== 0 && progressTextW !== 0) {
            let _per = progressBoxW / 100;
            textValue = (_per * progressValue) - progressTextW;

            if (textValue <= progressTextW / 2) {
                textValue = 0;
            }

            let defValue = progressBoxW - progressTextW;
            if (textValue > defValue) {
                textValue = defValue;
            }
        }


        return (<View
            onLayout={(event) => {
                this.setState({
                    progressBoxW: event.nativeEvent.layout.width,
                });
            }}
            style={[, {width: '100%', flex: 1}]}>
            <View style={StyleUtils.justifySpace}>
                <Text style={{marginBottom: 4}}>VIP</Text>
                {this.props.userData.history_total_pv <= 300 ? <View style={{flex: 1}}>
                    <Text
                        onLayout={(event) => {
                            this.setState({
                                progressTextW: event.nativeEvent.layout.width,
                            });
                        }}
                        style={{
                            marginBottom: 4,
                            marginLeft: textValue,
                            width: 100,
                            textAlign: 'center'
                        }}>{this.props.userData.history_total_pv} PV</Text>
                </View> : null}
                <Text
                    style={{marginBottom: 4}}>IBM</Text>
            </View>
            <ProgressBar progress={progressValue + '%'}/>
        </View>)
    }


    render() {
        let userData = this.props.userData;

        return (<View style={StyleUtils.flex}>

            <Navigation
                onClickLeftBtn={() => this.props.navigator.pop()}/>
            <ScrollView
                style={{padding: 16}}>

                {ViewUtils.getTitleView(I18n.t('b.businessStatus'), userData.user_level ? userData.user_level.level_name : '')}

                <Text style={[StyleUtils.text, {paddingBottom: 8,textAlign:'left'}]}>{I18n.t('b.accomulatedPurchaseVolume')}</Text>
                <View style={[StyleUtils.rowDirection,]}>

                    {this.renderProgress()}

                </View>
                {this.isActivated.length > 0 ?
                    <View>
                        <Text style={[styles.title, StyleUtils.smallFont]}>{I18n.t('default.activated')}</Text>

                        {this.isActivated.map((item, index) => {
                            return ViewUtils.gitUserInformationTv(item.name, item.content, require('../../../res/images/ic_selected_in.png', () => this.onClick(item, true)))
                        })}
                    </View>
                    : null}


                {this.isNotActivateds.length > 0 ?
                    <View>
                        <Text style={[styles.title, StyleUtils.smallFont]}>{I18n.t('default.notActivated')}</Text>

                        {this.isNotActivateds.map((item, index) => {
                            return ViewUtils.gitUserInformationTv(item.name, item.content, require('../../../res/images/ic_selected.png'), () => this.onClick(item, false))
                        })}
                    </View>
                    : null}

            </ScrollView>
        </View>)
    }


    startNewShop(userData) {
        if (userData && userData.user_id == userData.main_user_id) {
            DeviceEventEmitter.emit('toast', I18n.t('b.selectDefaultBusiness'))
        } else {
            CommonUtils.saveUserInfo(userData);
            const {navigator} = this.props;
            navigator.resetTo({
                component: SlideMenu,
                name: 'SlideMenu',
                params: {
                    ...this.props,
                }
            });

        }
    }


    getActivatedData(userData) {
        this.isActivated = [];
        this.isNotActivateds = [];
        let isReg = {
            name: I18n.t('b.regsitrationPack'),
            code: 'is_reg_pack',
            content: I18n.t('b.registerPakContent')
        };
        let isBuy = {
            name: I18n.t('default.ibmStarterPack'),
            code: 'is_buy',
            content: I18n.t('b.is_buyContent')
        };
        let isPrereg = {
            name: I18n.t('b.volumeThreshold'),
            code: 'is_prereg',
            content: I18n.t('b.is_preregContent')
        };


        if (userData.is_reg_pack === 1) {
            this.isActivated.push(isReg);
        } else {
            this.isNotActivateds.push(isReg);
        }
        if (userData.is_buy === 1) {
            this.isActivated.push(isBuy);
        } else {
            this.isNotActivateds.push(isBuy);
        }
        if (userData.is_prereg === 1) {
            this.isActivated.push(isPrereg);
        } else {
            this.isNotActivateds.push(isPrereg);
        }

    }

}

const styles = StyleSheet.create({
    title: {
        fontSize: 25,
        color: 'black',
        textAlign: 'left',
        marginTop: 28,
    },

    progressBox: {
        width: '100%',
        height: 22,
        borderWidth: 1,
    },

});