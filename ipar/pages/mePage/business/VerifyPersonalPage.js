/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {DeviceEventEmitter, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import StyleUtils from "../../../res/styles/StyleUtils";
import ViewUtils from '../../../utils/ViewUtils'


import Navigation from "../../../custom/Navigation";
import UserInformationEdit from "../userInfo/UserInformationEdit";
import CommonUtils from "../../../common/CommonUtils";
import IparNetwork, {SERVER_TYPE} from "../../../httpUtils/IparNetwork";
import UserEditAddress from "../userInfo/UserEditAddress";
import EditTax from "../userInfo/EditTax";
import I18n from "../../../res/language/i18n";

export default class VerifyPersonalPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userData: null,
            userId: null,
        };

        this.iparNetwork = new IparNetwork();
    }


    componentWillUnmount() {
        this.iparNetwork = null;
    }

    componentDidMount() {
        CommonUtils.getAcyncInfo('userInfo')
            .then((result) => {
                // this.getUserInfo(JSON.parse(result).user_id);
                this.setState({
                    userId: JSON.parse(result).main_user_id,
                    userData: JSON.parse(result),
                });

            });


    }

    /**
     *网络请求
     * 获取用户详细信息
     */
    // getUserInfo(userID) {
    // 	this.iparNetwork.getRequest(SERVER_TYPE.admin + 'getUserInfo?', 'user_id=' + userID)
    // 		.then((result) => {
    // 			this.setState({showLoading: false});
    // 			if (result.code === 200) {
    // 				this.setState({userData: result.data,});
    // 			}
    // 		})
    // 		.catch((error) => {
    // 			this.setState({showLoading: false});
    // 			console.log(error)
    // 		})
    // }

    /*
    *  状态（1已冻结，2未认证，3资料审核中，4已认证）
    * 获取用户详细信息
    */
    getStatusInfo() {
        CommonUtils.showLoading();

        let fromData = new FormData();
        fromData.append("user_id", this.state.userId);
        let userData = this.state.userData;
        fromData.append("nick_name", userData.nick_name);
        fromData.append("status", "3");
        this.iparNetwork.getPostFrom(SERVER_TYPE.admin + 'updateUserInfo', fromData)
            .then((result) => {
                CommonUtils.dismissLoading();

                if (result.code === 200) {
                    this.props.navigator.popToTop();

                    userData.status = "3";

                    CommonUtils.saveUserInfo(userData);
                    DeviceEventEmitter.emit('toast', I18n.t('default.success'));
                } else {
                    DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
                }
                console.log(result)
            }).catch((error) => {
            DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
            CommonUtils.dismissLoading();

            console.log(error);
        })

    }

    /*用户信息Text*/
    getUserTv(arry) {
        let arrTv = [];
        for (let i = 0; i < arry.length; i++) {
            arrTv.push(
                <Text style={[StyleUtils.defaultTextColor, StyleUtils.smallFont, styles.tvStyle]}>{arry[i]}</Text>
            )
        }
        return arrTv
    }

    render() {
        let gender = I18n.t('default.secrecy');
        let statusType = '';
        let userUiData;
        let userInfo = this.state.userData;

        if (userInfo) {


            /**状态（1已冻结，2未认证，3资料审核中，4已认证）*/
            switch (userInfo.status) {
                case 1:
                    statusType = I18n.t('default.suspended');
                    break;
                case 2:
                    statusType = I18n.t('default.notVerified');
                    break;
                case 3:
                    statusType = I18n.t('default.underReview');
                    break;
                case 4:
                    statusType = I18n.t('default.verified');
                    break;
                case 5:
                    statusType = I18n.t('default.applyActivate');
                    break;
            }


            let nullText = I18n.t('default.nullValue');

            let firstName = userInfo.first_name ? userInfo.first_name + ' ' : '';
            let middleName = userInfo.middle_name ? userInfo.middle_name + ' ' : '';
            let lastName = userInfo.last_name ? userInfo.last_name + ' ' : '';

            // let name = firstName + middleName + lastName;

            if (userInfo.sex === 0) {
                gender = I18n.t('default.female')
            } else if (userInfo.sex === 1) {
                gender = I18n.t('default.male')
            }

            // let isNotNullData = name.trim() && gender && userInfo.birth_day && userInfo.id_number;

            userUiData = <View>
                {/*middle_name*/}
                <Text style={[styles.topTv, {color: 'black'}, StyleUtils.smallFont]}>{I18n.t('default.fullName')}</Text>
                {this.getUserTv([!firstName && !middleName && !lastName ? nullText : firstName + middleName + lastName])}

                <Text style={[styles.topTv, {color: 'black'}, StyleUtils.smallFont]}>{I18n.t('default.sex')}</Text>
                {this.getUserTv([gender ? gender : nullText])}


                <Text
                    style={[styles.topTv, {color: 'black'}, StyleUtils.smallFont]}>{I18n.t('default.dateOfBirth')}</Text>
                {this.getUserTv([userInfo.birth_day ? userInfo.birth_day : nullText])}

                <Text style={[styles.topTv, {color: 'black'}, StyleUtils.smallFont]}>{I18n.t('default.idNumber')}</Text>
                {this.getUserTv([userInfo.id_number ? userInfo.id_number : nullText])}
                {/*/!*编辑用户信息editBn*!/*/}
                {ViewUtils.getEditBtn(() => {
                    this.props.navigator.push({
                        component: UserInformationEdit,
                        params: {
                            callback: (result) => {
                                this.setState({userData: result});
                                CommonUtils.saveUserInfo(result)
                            },
                            userData: userInfo,

                        }
                    })
                })}
                {/*/!*编辑用户收货地址*!/*/}
                {/*<View style={StyleUtils.lineStyle}/>*/}
                {/*<Text*/}
                {/*style={[styles.title, StyleUtils.smallFont, {textAlign: 'left'}]}>{I18n.t('default.contactAddress')}</Text>*/}
                {/*{this.getUserTv([!userInfo.building && !userInfo.street_name && !userInfo.address ? nullText : userInfo.building, userInfo.street_name, userInfo.street_name, userInfo.address])}*/}
                {/*{userInfo.status < 3 ? ViewUtils.getEditBtn(() => {*/}
                {/*this.props.navigator.push({*/}
                {/*component: UserEditAddress,*/}
                {/*params: {*/}
                {/*callback: (result) => {*/}
                {/*this.setState({userData: result});*/}
                {/*CommonUtils.saveUserInfo(result);*/}
                {/*},*/}
                {/*editData: userInfo,*/}
                {/*}*/}
                {/*})*/}
                {/*}) : null}*/}


                {/*/!*编辑用户 Taxing region*!/*/}
                {/*<View style={StyleUtils.lineStyle}/>*/}
                {/*<Text*/}
                {/*style={[styles.title, StyleUtils.smallFont, {textAlign: 'left'}]}>{I18n.t('default.taxRegion')}</Text>*/}
                {/*<Text*/}
                {/*style={[StyleUtils.defaultTextColor, styles.tvStyle, StyleUtils.smallFont, {textAlign: 'left'}]}>{userInfo.tex_area ? userInfo.tex_area : nullText}</Text>*/}

                {/*<Text style={[styles.title, StyleUtils.smallFont, {textAlign: 'left'}]}>{I18n.t('default.taxId')}</Text>*/}
                {/*<Text*/}
                {/*style={[StyleUtils.defaultTextColor, styles.tvStyle, StyleUtils.smallFont, {textAlign: 'left'}]}>{userInfo.personal_tax ? userInfo.personal_tax : nullText}</Text>*/}

                {/*/!*编辑用户信息editBn*!/*/}
                {/*{userInfo.status < 3 ? ViewUtils.getEditBtn(() => {*/}
                {/*this.props.navigator.push({*/}
                {/*component: EditTax,*/}
                {/*params: {*/}
                {/*userData: userInfo,*/}
                {/*callback: (result) => {*/}
                {/*this.setState({userData: result},*/}
                {/*CommonUtils.saveUserInfo(result))*/}
                {/*},*/}
                {/*}*/}
                {/*})*/}
                {/*}) : null}*/}


                {/*{userInfo.status < 3 && isNotNullData ? ViewUtils.getButton(I18n.t('default.submit'), () => {*/}
                {/*if (userInfo.status === 2) {*/}
                {/*CommonUtils.showLoading();*/}

                {/*this.getStatusInfo()*/}
                {/*}*/}
                {/*}, {marginTop: 20}) : null}*/}
            </View>;

        }

//+ '\n' + I18n.t('default.informationStatus') + statusType
        return (
            <View style={StyleUtils.flex}>
                <Navigation
                    onClickLeftBtn={() => this.props.navigator.pop()}
                    titleImage={require('../../../res/images/ic_ipar_logo.png')}
                />
                <ScrollView>
                    <View style={[StyleUtils.flex, {padding: 16}]}>
                        {ViewUtils.getTitleView(I18n.t('default.verPersonal'), I18n.t('default.haveInformationVerify'))}
                        {/*{ViewUtils.getTitleView(I18n.t('default.verPersonal'), I18n.t('default.informationStatus') + statusType)}*/}
                        {userUiData}


                    </View>

                </ScrollView>
            </View>
        );
    }


}


const styles = StyleSheet.create({
    tvStyle: {
        marginTop: 4,
        textAlign: 'left'
    },
    title: {
        marginTop: 10,
        fontSize: 18,
        color: 'black'
    },
    conAddress: {
        marginTop: 16,
        fontSize: 18,
        color: 'black'
    },
    topTv: {
        marginTop: 10,
        fontSize: 18,
        textAlign: 'left'
    }
});