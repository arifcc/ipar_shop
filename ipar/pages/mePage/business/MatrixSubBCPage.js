import React, {Component} from 'react'


import {
    DeviceEventEmitter,
    FlatList,
    Image,
    ListView,
    RefreshControl,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from "react-native";
import StyleUtils from "../../../res/styles/StyleUtils";
import Navigation from "../../../custom/Navigation";
import ViewUtils from "../../../utils/ViewUtils";
import PerformancePage from "./PerformancePage";
import IparNetwork, {SERVER_TYPE} from "../../../httpUtils/IparNetwork";
import I18n from "../../../res/language/i18n";
import MatrixBusinessInfoPage from "./MatrixBusinessInfoPage";
import ModalBottomUtils from "../../../utils/ModalBottomUtils";
import BusinessStatusPage from "./BusinessStatusPage";
import OrdersPage from "../order/OrdersPage";
import CommonUtils, {getFormatDate} from "../../../common/CommonUtils";
import SlideMenu from "../../../menu/SlideMenu";
import SubAccountsPage from "./SubAccountsPage";
import BasWebViewPage from "../../common/BasWebViewPage";
import IparTreeView from "../../../custom/IparTreeView";
import TreeViewPage from "../../common/TreeViewPage";
import BvPage from "./BvPage";
import {DATE_DATA} from "../../../res/json/monthsJson";
import AutoOrderPage from "../../common/order/AutoOrderPage";
import BaseModal from "../../../utils/BaseModal";
import IparImageView from "../../../custom/IparImageView";


const ds = new ListView.DataSource({
    rowHasChanged: (r1, r2) => r1 !== r2,
});


export default class MatrixSubBCPage extends Component {

    constructor(props) {
        super(props);
        this.iparNetwork = new IparNetwork();
        this.country = '';

        let userData = this.props.usersData[props.selectDataIndex];

        this.isMatrixPlan = userData.rank === 5;

        this.planNamesData = this.isMatrixPlan ? [
            {_name: I18n.t('b.cashBack'), color: 'black', value: 0, code: 0},
            {_name: I18n.t('b.salesCommission'), color: '#333', value: 0, code: 0},
            {_name: I18n.t('b.developmentAward'), color: '#626262', value: 0, code: 0},
            {_name: I18n.t('b.weeklyAward'), color: '#A0A0A0', value: 0, code: 0},
            {_name: I18n.t('default.Leadership'), color: '#D2D2D2', value: 0, code: 0},
            {_name: I18n.t('b.targetAward'), color: '#D2D2D2', value: 0, code: 0},
            // {_name: I18n.t('b.specialBonus'), color: '#A0A0A0', value: 0,code:0},
        ] : [
            {_name: I18n.t('b.cashBack'), color: 'black', value: 0, code: 0},
            {_name: I18n.t('b.salesCommission'), color: '#333', value: 0, code: 0},
            {_name: I18n.t('b.developmentAward'), color: '#626262', value: 0, code: 0},
            {_name: I18n.t('b.levelBonus'), color: '#626262', value: 0, code: 0},
            {_name: I18n.t('default.Leadership'), color: '#D2D2D2', value: 0, code: 0},
            {_name: I18n.t('b.specialBonus'), color: '#A0A0A0', value: 0, code: 0},
        ];

        this.planNamesBottomData = [
            {_name: I18n.t('b.certificateDate'), color: 'black', value: '19 days'},
            // {_name: I18n.t('b.newAssociates'), color: '#363636', value: '19 days', code: 'New associates'},
            // {_name: I18n.t('b.activeMonthsInRow'), color: '#4f4f4f', value: '3 weeks'},
            {_name: I18n.t('b.autoOrder'), color: '#636363', value: 'image', code: 'autoOrder'},
            {_name: I18n.t('default.myOrders'), color: '#a7a7a7', value: 'image', code: 'myOrders'},
            {_name: I18n.t('b.myTeamTree'), color: '#c0c0c0', value: 'image', code: 'myTeamTree'},
            {_name: I18n.t('b.treeView'), color: '#e3e3e3', value: 'image', code: 'IparTreeView'},
            this.isMatrixPlan ? {
                _name: I18n.t('b.leadingAssociates'),
                color: '#f3f3f3',
                value: '0',
                data: [],
                code: 'leadingAssociates'
            } : null,
        ];


        this.state = {
            isLoading: false,
            modalDialog: false,
            plansData: [],
            newMembers: [],
            cardImageUrl: null,
            baseModalVisible: false,
            u_2StatisticsData: null,
            rewardStatistics: null,
            selectIndex: this.props.selectDataIndex,
        }

    }

    componentDidMount() {

        // CommonUtils.getAcyncInfo('country_iso')
        // 	.then((country) => {
        // 		this.country = country;
        // 	})


        this.getData();

    }


    /**
     * 获取需要的数据
     */
    getData() {
        let userData = this.props.usersData[this.state.selectIndex];
        this.setState({
            isLoading: true,
        });
        this.getNewMembers(userData);//获取新增成员

        this.getRewardStatistics(userData);//获取奖金


        if (!this.isMatrixPlan) {
            this.getUserStatistics(userData);
        }

    }


    getUserStatistics(userData) {
        this.iparNetwork.getRequest(SERVER_TYPE.admin + 'getUserStatistics?', 'user_id=' + userData.user_id)
            .then((result) => {
                if (result.code === 200) {
                    console.log(result.data);
                    this.setState({
                        u_2StatisticsData: result.data,
                    })
                }
            })
            .catch((error) => {
                this.setState({isLoading: false});
                console.log('MatrixSubBCPage--getUserStatistics--error-', error);
                DeviceEventEmitter.emit('toast', I18n.t('default.fail'))
            })
    }


    /**
     * 获取新增成员
     */
    getNewMembers(userData) {

        this.iparNetwork.getRequest(SERVER_TYPE.admin + 'getTeam?', 'user_id=' + userData.user_id)
            .then((result) => {
                if (result.code === 200) {
                    this.setState({
                        newMembers: result.data,
                    })
                }
            })
            .catch((error) => {
                this.setState({isLoading: false});
                console.log('MatrixSubBCPage--getNewMembers--error-', error);
                DeviceEventEmitter.emit('toast', I18n.t('default.fail'))
            })

    }


    /**
     *  start order page
     */
    startOrderPage(param) {
        let userData = this.props.usersData[this.state.selectIndex];


        this.props.navigator.push({
            component: OrdersPage,
            params: {
                userId: userData.user_id,
                moneyType: param
            },
        })
    }


    /**
     * 获取奖金
     * */
    getRewardStatistics(userData) {


        let _time = DATE_DATA.year + '-' + (DATE_DATA.month);
        if (parseInt(DATE_DATA.day) < 0) {
            _time = DATE_DATA.year + '-' + (getFormatDate(DATE_DATA.month - 1));
        }

        this.iparNetwork.getRequest(SERVER_TYPE.admin + 'getRewardStatistics?', 'user_id=' + userData.user_id + '&time=' + _time)
            .then((result) => {
                if (result.code === 200 && this.planNamesData) {

                    let _data = result.data;

                    let splitElement = _data.split[userData.rank];

                    // let level0 = splitElement['0'];
                    // let level1 = splitElement['1'];
                    // let level2 = splitElement['2'];
                    // let level3 = splitElement['3'];
                    // let level4 = splitElement['4'];
                    // let level5 = splitElement['5'];

                    // let wholeLevel =


                    let value0 = 0;
                    let value1 = 0;
                    let value2 = 0;
                    let value3 = 0;
                    let value4 = 0;
                    let value5 = 0;

                    for (let i = 0, len = splitElement.length; i < len; i++) {
                        value0 += splitElement[i][10].pv;
                        value1 += splitElement[i][1].pv;
                        value2 += splitElement[i][11].pv;
                        value3 += splitElement[i][this.isMatrixPlan ? 12 : 15].pv;
                        value4 += splitElement[i][13].pv;
                        value5 += splitElement[i][this.isMatrixPlan ? 14 : 16].pv;
                    }

                    this.planNamesData[0].value = value0.toFixed(2);
                    this.planNamesData[1].value = value1.toFixed(2);
                    this.planNamesData[2].value = value2.toFixed(2);
                    this.planNamesData[3].value = value3.toFixed(2);
                    this.planNamesData[4].value = value4.toFixed(2);
                    this.planNamesData[5].value = value5.toFixed(2);

                    this.planNamesData[0].code = 10;
                    this.planNamesData[1].code = 1;
                    this.planNamesData[2].code = 11;
                    this.planNamesData[3].code = this.isMatrixPlan ? 12 : 15;
                    this.planNamesData[4].code = 13;
                    this.planNamesData[5].code = this.isMatrixPlan ? 14 : 16;


                    this.setState({
                        rewardStatistics: _data,
                    })
                }
                this.setState({isLoading: false});
            })
            .catch((error) => {
                this.setState({isLoading: false});
                DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
                console.log('PlansMainPage-----getRewardStatistics--error: ', error);
            })
    }


    componentWillUnmount() {
        this.iparNetwork = null;
        this.country = null;
        this.planNamesData = null;
        this.planNamesBottomData = null;
    }


    onPlansPress(rowData) {
        let userData = this.props.usersData[this.state.selectIndex];

        let _component = null;
        let _params = {};
        switch (rowData.code) {
            case 'myOrders':
                _component = OrdersPage;
                _params = {userId: userData.user_id};
                break;
            case 'New associates':
                _component = SubAccountsPage;
                _params = {
                    users: this.state.newMembers,
                    title: rowData._name,
                    type: 'newAssociates'
                };
                break;
            case 'myTeamTree':
                _component = TreeViewPage;
                _params = {
                    userId: userData.user_id,
                    urlPram: 'getTreeInfo?'
                };
                break;
            case 'IparTreeView':
                _component = TreeViewPage;
                _params = {
                    userId: userData.user_id,
                };
                break;
            case 'autoOrder':
                _component = AutoOrderPage;
                _params = {
                    user_id: userData.user_id,
                    main_user_id: userData.main_user_id
                };
                break;
            case 'leadingAssociates':
                _component = SubAccountsPage;
                _params = {
                    users: rowData.data,
                    title: I18n.t('b.leadingAssociates')
                };
                break;
        }
        if (_component) {
            this.props.navigator.push({
                component: _component,
                name: _component + '',
                params: _params,
            })
        }

    }


    startBvPage(item) {

        this.props.navigator.push({
            component: BvPage,
            params: {
                bvType: item.code,
                user_id: this.props.usersData[this.state.selectIndex].user_id,
                title: item._name,
            }
        })
    }

    renderPlanLable(rowData, callback) {
        if (!rowData) return;
        return (<TouchableOpacity
            onPress={callback}
            style={[StyleUtils.justifySpace, {marginBottom: 22}]}>
            <View style={{width: 22, height: 22, backgroundColor: rowData.color}}/>
            <Text style={[styles.planLable, StyleUtils.smallFont]}>{rowData._name}</Text>
            {rowData.value !== 'image' ?
                <Text style={[{fontSize: 16, color: 'black'}, StyleUtils.smallFont]}>{rowData.value}</Text> :
                <Image
                    style={[StyleUtils.tabIconSize]}
                    source={require('../../../res/images/ic_right_more.png')}
                />}
        </TouchableOpacity>)
    }

    /**
     * 获取更新的时间
     * */
    getWeekDate() {
        let date = new Date();
        let day = date.getDay();
        let resultDate = 6 - day;
        if (day >= 6) {
            resultDate = (resultDate === 0 ? 7 : 7 + resultDate) - 1;
        } else {
            resultDate -= 1;
        }

        return resultDate + ' ' + I18n.t('b.day') + ' ' + (24 - date.getHours()) + ' ' + I18n.t('b.hours');
    }


    /**
     * 获取名片图片
     */
    createLabel() {

        CommonUtils.showLoading();
        let formData = new FormData();
        let state = this.state;
        let userData = this.props.usersData[state.selectIndex];
        formData.append("user_id", userData.user_id);
        formData.append("real_name", (userData.first_name + " " + userData.middle_name + " " + userData.last_name + ".").replace("null", ""));
        formData.append("area_code", userData.area_code);
        formData.append("mobile", userData.mobile);
        formData.append("rank_name", this.props.titleText || I18n.t('b.matrix'));
        formData.append("business_center_id", userData.rand_id);
        formData.append("sponser_key", userData.code);
        formData.append("bar_content", userData.rand_id);

        this.iparNetwork.getPostFrom(SERVER_TYPE.payServer + 'createLabel', formData)
            .then((result) => {
                CommonUtils.dismissLoading();
                if (result.code === 200) {
                    this.setState({
                        baseModalVisible: true,
                        cardImageUrl: result.data,
                    })
                }
            })
            .catch((error) => {
                CommonUtils.dismissLoading();
                console.log("MyBusinessPage--createLabel--error-" + error)
            })
    }


    render() {
        let props = this.props;
        let state = this.state;
        let userData = props.usersData[state.selectIndex];

        let isActivated = false;

        if (userData.is_reg_pack === 1 && userData.is_buy === 1 && userData.is_prereg === 1) {
            isActivated = true;
        }


        this.planNamesBottomData[0].value = userData.active_time && (userData.active_time + '').substring(0, 11);

        if (this.isMatrixPlan) {
            this.planNamesBottomData[5].data = [];
            for (let i = 0, len = state.newMembers.length; i < len; i++) {
                let newMember = state.newMembers[i];
                if (newMember.history_total_pv >= 5000) {
                    this.planNamesBottomData[5].data.push(newMember)
                }
            }
            this.planNamesBottomData[5].value = this.planNamesBottomData[5].data.length;
        }


        let _bonus = 0;
        if (state.rewardStatistics) {
            _bonus = state.rewardStatistics.rank[userData.rank].pv
        }


        let statisticsView = this.isMatrixPlan ? [
            {data: ['item', 'left', 'right']},
            {data: [I18n.t('b.newVolume'), (userData.left_total_group_pv || 0).toFixed(2) + ' PV', (userData.right_total_group_pv || 0).toFixed(2) + ' PV']},
            {data: [I18n.t('b.rollover'), (userData.left_balance || 0).toFixed(2) + ' PV', (userData.right_balance || 0).toFixed(2) + ' PV']},
            {data: [I18n.t('b.historyVolume'), (userData.left_history_total_group_pv || 0).toFixed(2) + ' PV', (userData.right_history_total_group_pv || 0).toFixed(2) + ' PV']},
            {data: ['BV', (userData.left_total_bbv || 0).toFixed(2) + ' BV', (userData.right_total_bbv || 0).toFixed(2) + ' BV']},
        ] : state.u_2StatisticsData ? [
            {data: [I18n.t('b.level'), 'left', 'right']},
            {data: [I18n.t('b.levelOne'), state.u_2StatisticsData.left_level_2, state.u_2StatisticsData.right_level_2]},
            {data: [I18n.t('b.levelTwo'), state.u_2StatisticsData.left_level_3, state.u_2StatisticsData.right_level_3]},
            {data: [I18n.t('b.levelThree'), state.u_2StatisticsData.left_level_4, state.u_2StatisticsData.right_level_4]},
            {data: [I18n.t('b.levelFour'), state.u_2StatisticsData.left_level_5, state.u_2StatisticsData.right_level_5]},
            {data: [I18n.t('b.levelFive'), state.u_2StatisticsData.left_level_6, state.u_2StatisticsData.right_level_6]},
        ] : [];


        const centerUi = (isActivated ? <View>


            {/*<TouchableOpacity*/}
            {/*onPress={() => this.onPlansPress({*/}
            {/*code: 'New associates'*/}
            {/*},*/}
            {/*)}*/}
            {/*style={[StyleUtils.center, {paddingTop: 26, paddingBottom: 26,}]}>*/}

            {/*<Text style={StyleUtils.text}>{I18n.t('b.newAssociates')}</Text>*/}

            {/*<View style={[StyleUtils.rowDirection, StyleUtils.center]}>*/}

            {/*<Image*/}
            {/*style={{width: 56, height: 56}}*/}
            {/*source={require('../../../res/images/ic_class.png')}*/}
            {/*/>*/}

            {/*<Text style={[StyleUtils.title, {fontSize: 20}]}>+ {state.newMembers.length}</Text>*/}

            {/*</View>*/}

            {/*</TouchableOpacity>*/}
            {/*{this.isMatrixPlan ? <Text style={{*/}
            {/*fontSize: 18,*/}
            {/*color: 'black',*/}
            {/*marginTop: 36,*/}
            {/*width: '100%'*/}
            {/*}}>{I18n.t('default.myTeam')}</Text> : null}*/}


            {/*{this.isMatrixPlan ?*/}
            {/*<View style={[StyleUtils.justifySpace, StyleUtils.center, {paddingTop: 26, paddingBottom: 22}]}>*/}

            {/*<View style={[{*/}
            {/*borderColor: '#c6c6c6',*/}
            {/*borderRightWidth: 1,*/}
            {/*flex: 1,*/}
            {/*height: 80*/}
            {/*}, StyleUtils.center]}>*/}
            {/*<Text style={{*/}
            {/*textAlign: 'center',*/}
            {/*fontSize: 12,*/}
            {/*color: '#909090'*/}
            {/*}}>{I18n.t('b.leftVolume') + '\n'}<Text*/}
            {/*style={[styles.titleText]}>{(userData.left_total_pv || 0).toFixed(2)} PV</Text></Text>*/}
            {/*</View>*/}
            {/*<View style={[{*/}
            {/*borderColor: '#c6c6c6',*/}
            {/*borderLeftWidth: 1,*/}
            {/*flex: 1,*/}
            {/*height: 80*/}
            {/*}, StyleUtils.center]}>*/}
            {/*<Text style={{*/}
            {/*textAlign: 'center',*/}
            {/*fontSize: 12,*/}
            {/*color: '#909090'*/}
            {/*}}>{I18n.t('b.rightVolume') + '\n'}<Text*/}
            {/*style={[styles.titleText]}>{(userData.right_total_pv || 0).toFixed(2)} PV</Text></Text>*/}
            {/*</View>*/}

            {/*</View>*/}
            {/*: null}*/}


            <Text style={[{
                fontSize: 18,
                color: 'black',
                marginTop: 36,
                textAlign: 'center',
                width: '100%'
            }, StyleUtils.smallFont]}>{I18n.t('b.estimatedPV')}</Text>


            <PerformancePage
                performanceValues={this.planNamesData}
                value={_bonus || 0}
                type={0}
            />


            {ViewUtils.getButton(I18n.t('b.myPerformance'), null, styles.topBtnStyle, {color: 'black'})}


            {this.planNamesData.map((item, i) => {
                return this.renderPlanLable(item, () => this.startBvPage(item));
            })}

            <View style={[StyleUtils.justifySpace]}>
                <Text style={[styles.titleText, {marginTop: 0}]}>
                    {I18n.t('b.statementUpdate')}
                </Text>
                <Text style={[StyleUtils.text, {padding: 0}]}>
                    {this.getWeekDate()}
                </Text>
            </View>
            {/*<Text style={styles.titleText}>*/}
            {/*{I18n.t('b.careerProspective')}*/}
            {/*</Text>*/}

            {this.isMatrixPlan ?
                <View style={[StyleUtils.justifySpace, StyleUtils.center, {paddingTop: 26, paddingBottom: 22}]}>

                    <TouchableOpacity
                        onPress={() => this.startOrderPage('PV')}
                        style={[{
                            borderColor: '#c6c6c6',
                            borderRightWidth: 1,
                            flex: 1,
                            height: 80
                        }, StyleUtils.center]}>
                        <Text style={[{
                            textAlign: 'center',
                            fontSize: 12,
                            color: '#909090'
                        }, StyleUtils.smallFont]}>{I18n.t('b.PVBank') + '\n'}<Text
                            style={[styles.titleText]}>{userData.total_pv} PV</Text></Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.startOrderPage('BV')}
                        style={[{
                            borderColor: '#c6c6c6',
                            borderLeftWidth: 1,
                            flex: 1,
                            height: 80
                        }, StyleUtils.center]}>
                        <Text style={[{
                            textAlign: 'center',
                            fontSize: 12,
                            color: '#909090'
                        }, StyleUtils.smallFont]}>{I18n.t('b.BVBank') + '\n'}<Text
                            style={[styles.titleText]}>{userData.total_bbv} BV</Text></Text>
                    </TouchableOpacity>

                </View> : null}


            <TouchableOpacity
                onPress={() => this.onPlansPress({
                        code: 'New associates'
                    },
                )}
                style={[StyleUtils.center, {paddingTop: 26, paddingBottom: 26,}]}>

                <Text
                    style={StyleUtils.text}>{I18n.t('b.newAssociates')}</Text>

                <View style={[StyleUtils.rowDirection, StyleUtils.center]}>

                    <Image
                        style={{width: 56, height: 56}}
                        source={require('../../../res/images/ic_class.png')}
                    />

                    <Text style={[StyleUtils.title, {fontSize: 20}]}>+ {state.newMembers.length}</Text>

                </View>

            </TouchableOpacity>

            {/* Statistics view*/}
            {userData.rank !== 8 && <FlatList
                style={{marginBottom: 20}}
                renderItem={(item) => this.renderStatistics(item.item, item.index)}
                data={statisticsView}/>}


            {/*{ViewUtils.getButton(I18n.t('b.myPerformance'), () => this.onPlansPress({*/}
            {/*code: 'New associates'*/}
            {/*},*/}
            {/*), styles.topBtnStyle, {color: 'black'})}*/}

            {this.planNamesBottomData.map((item, i) => {
                return this.renderPlanLable(item, () => this.onPlansPress(item));
            })}


            {this.renderPlanLable({
                _name: I18n.t('b.myBusinessCard'),
                color: '#e8e8e8',
                value: 'image'
            }, () => this.createLabel())}

            {ViewUtils.getButton(I18n.t('default.keepShopping'), () => {
                CommonUtils.saveUserInfo(userData);
                const {navigator} = this.props;
                navigator.resetTo({
                    component: SlideMenu,
                    name: 'SlideMenu',
                    params: {
                        ...this.props,
                    }
                });
            }, [styles.topBtnStyle, StyleUtils.marginTop], {color: 'black'})}


        </View> : <View style={[StyleUtils.center, {marginTop: 60}]}>
            <Text style={[{textAlign: 'center'}, StyleUtils.smallFont]}>{I18n.t('b.businessText')}</Text>
            {ViewUtils.getButton(I18n.t('b.activatedNow'), () => this.props.navigator.push({
                component: BusinessStatusPage,
                name: 'BusinessStatusPage',
                params: {
                    userData: userData
                }
            }), [styles.topBtnStyle, {marginTop: 42}], {color: 'black'})}
        </View>);

        return (<View style={StyleUtils.flex}>

            <Navigation
                titleImage={require('../../../res/images/ic_ipar_logo.png')}
                onClickLeftBtn={() => props.navigator.pop()}/>
            {userData ?

                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={state.isLoading}
                            onRefresh={() => this.getData()}
                        />
                    }
                >

                    <View style={{padding: 20}}>

                        {ViewUtils.getTitleView(this.props.titleText || I18n.t('b.matrix'), I18n.t('default.businessInfor') + '\n' + userData.rand_id, () => props.navigator.push(({
                            component: MatrixBusinessInfoPage,
                            params: {
                                userData: userData
                            }
                        })))}

                        {/*{ViewUtils.getButton('Check details', null, styles.topBtnStyle, {color: 'black'})}*/}

                        <View
                            style={[StyleUtils.justifySpace, {
                                backgroundColor: 'black',
                                paddingRight: 16,
                                paddingLeft: 16
                            }]}>

                            <TouchableOpacity
                                onPress={() => this.setState({
                                    modalDialog: true,
                                })}
                                style={{height: 42, justifyContent: 'center'}}>
                                <Text
                                    style={{color: 'white'}}>{userData.rand_id}</Text>
                            </TouchableOpacity>


                            <TouchableOpacity
                                onPress={() => this.props.navigator.push({
                                    component: BusinessStatusPage,
                                    name: 'BusinessStatusPage',
                                    params: {
                                        userData: userData
                                    }
                                })}
                                style={[StyleUtils.rowDirection, StyleUtils.center, {height: 42}]}>
                                <Text style={[{
                                    color: 'white',
                                    paddingRight: 8
                                }, StyleUtils.smallFont]}>{userData.user_level ? userData.user_level.level_name : ''}</Text>
                                <Image
                                    style={{width: 12, height: 12, tintColor: 'white',}}
                                    source={require('../../../res/images/ic_right_more.png')}/>
                            </TouchableOpacity>

                        </View>


                        {ViewUtils.renderOperationView(userData, this, null, null, null, true)}

                        {centerUi}

                    </View>


                    <ModalBottomUtils
                        visible={state.modalDialog}
                        dataSource={ds.cloneWithRows(this.props.usersData)}
                        requestClose={() => this.setState({
                            modalDialog: false,
                        })}
                        renderRow={(rowData, sectionID, rowID) => this.modalRow(rowData, rowID)}
                        title={I18n.t('b.businessCenter')}
                    />


                    <BaseModal
                        requestClose={() => {
                            this.setState({baseModalVisible: true})
                        }
                        }
                        visible={state.baseModalVisible}>
                        <TouchableOpacity
                            activeOpacity={1}
                            onPress={() => {
                                this.setState({baseModalVisible: false,})
                            }}
                            style={{flex: 1}}>
                            <IparImageView
                                width={"100%"}
                                height={'100%'}
                                url={state.cardImageUrl}
                                httpUrlNotShow={true}
                                errorImage={require('../../../res/images/ic_image_fail.png')}/>

                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({baseModalVisible: false}, CommonUtils.downloadImage(state.cardImageUrl))
                                }}
                                style={[{
                                    position: 'absolute',
                                    bottom: 16,
                                    right: 16,
                                    width: 40,
                                    padding: 4,
                                    height: 40,
                                    borderWidth: 1,
                                }]}>
                                <Image
                                    style={{tintColor: 'black', width: 30, height: 30}}
                                    source={require('../../../res/images/ic_download.png')}/>

                            </TouchableOpacity>

                        </TouchableOpacity>
                    </BaseModal>


                </ScrollView>


                : null}

        </View>)
    }

    renderStatistics(item, index) {
        return (<View style={[StyleUtils.flex, {
            backgroundColor: index === 0 ? 'black' : index % 2 === 0 ? '#f3f3f3' : 'white',
            height: 30,
        }, StyleUtils.justifySpace]}>
            {item.data.map((item, i) => {
                return <Text style={[{
                    flex: 1,
                    textAlign: 'center',
                    fontSize: 12,
                    color: index === 0 ? 'white' : 'gray'
                }, StyleUtils.smallFont]}>{item}</Text>
            })}
        </View>)
    }


    /**
     * init modal
     * @param rowData
     * @param index
     * @returns {*}
     */
    modalRow(rowData, index) {
        return (<TouchableOpacity
            onPress={() => {
                if (this.state.selectIndex !== index) {
                    this.setState({
                        modalDialog: false,
                        selectIndex: index,
                    });
                    //重新获取
                    this.getData();
                } else {
                    this.setState({
                        modalDialog: false,
                    });
                }
            }}
            style={StyleUtils.flex}>
            <Text style={[StyleUtils.text, {color: 'black'}]}>{rowData.rand_id}</Text>
        </TouchableOpacity>)
    }


}

const styles = StyleSheet.create({

    topBtnStyle: {
        backgroundColor: 'white',
        borderWidth: 1,
        height: 42,
        borderColor: '#e7e7e7',
        marginBottom: 28,
    },

    planLable: {
        borderColor: 'black',
        flex: 1,
        paddingLeft: 8,
        textAlign: 'left'
    },
    reentryViewStyle: {
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: '#e7e7e7',
        marginTop: 32,
        paddingTop: 16,
        paddingBottom: 16,

    },

    circularText: {
        color: 'white',
        textAlign: 'center',
    },

    menuTitle: {
        width: 88,
        height: 42,
        backgroundColor: 'black'
    },

    titleText: {
        marginTop: 16,
        fontSize: 18,
        color: 'black',
    },
});