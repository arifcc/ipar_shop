/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {DeviceEventEmitter, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import StyleUtils from "../../../res/styles/StyleUtils";
import ViewUtils from "../../../utils/ViewUtils";
import Navigation from "../../../custom/Navigation";
import IparNetwork, {SERVER_TYPE} from "../../../httpUtils/IparNetwork";
import CommonUtils from "../../../common/CommonUtils";
import I18n from "../../../res/language/i18n";
import SlideMenu from "../../../menu/SlideMenu";
import GoodsIsBuyPage from "../../common/GoodsIsBuyPage";

export default class MatrixBusinessInfoPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userData: props.userData,
            parentData: null,
            sponsorData: null,
            swap_amount: '',
            dataSourceLength: []
        };

        this.iparNetwork = new IparNetwork()
    }

    componentWillUnmount() {
        this.iparNetwork = null;
    }

    componentDidMount() {

        this.getUserInfo(this.state.userData.sponsor_id, 0);
        this.getUserInfo(this.state.userData.parent_id, 1);

        CommonUtils.getAcyncInfo('getPv')
            .then((result) => {
                this.setState({swap_amount: JSON.parse(result).swap_amount});
            })

    }

    /*
    *网络请求
    * 获取用户详细信息
    */
    getUserInfo(userID, type) {
        if (!userID) {
            return
        }
        CommonUtils.showLoading();

        this.iparNetwork.getRequest(SERVER_TYPE.admin + 'getSubAccount?', 'user_id=' + userID)
            .then((result) => {
                CommonUtils.dismissLoading();

                if (result.code === 200) {
                    this.setState({
                        sponsorData: type === 0 ? result.data : this.state.sponsorData,
                        parentData: type === 1 ? result.data : this.state.parentData,
                    });
                }
            })
            .catch((error) => {
                CommonUtils.dismissLoading();

                console.log(error)
            })
    }


    onClick(type) {
        let userData = this.state.userData;
        let _component = null;
        let _params = {};
        switch (type) {
            case 'is_reg_pack'://注册包
                _component = GoodsIsBuyPage;
                _params = {
                    goods_id: userData.user_rank ? userData.user_rank.reg_goods_id : '',
                    title: I18n.t('b.regsitrationPack'),
                };
                DeviceEventEmitter.emit('upDateUserData', userData);

                break;
            case 'is_buy':
                _component = GoodsIsBuyPage;
                _params = {
                    goods_id: userData.user_rank ? userData.user_rank.sell_goods_id : '',
                    title: I18n.t('default.ibmStarterPack'),
                };
                DeviceEventEmitter.emit('upDateUserData', userData);

                break;
            case 'is_prereg':
                DeviceEventEmitter.emit('goToPage', 0);
                new CommonUtils().popToRoute(this, 'SlideMenu');
                break;
        }

        if (_component) {
            this.props.navigator.push({
                component: _component,
                name: _component + '',
                params: _params,
            });
        }

    }


    startNewShop(userData) {
        if (userData.user_id == userData.main_user_id) {
            DeviceEventEmitter.emit('toast', I18n.t('b.selectDefaultBusiness'))
        } else {
            CommonUtils.saveUserInfo(userData);
            const {navigator} = this.props;
            navigator.resetTo({
                component: SlideMenu,
                name: 'SlideMenu',
                params: {
                    ...this.props,
                }
            });

        }
    }


    render() {


        let userUi;
        let userData = this.state.userData;//获取用户详细信息


        if (userData) {
            let parentData = this.state.parentData;
            let sponsorData = this.state.sponsorData;

            let nick_name = userData.first_name ? userData.first_name : '';
            let last_name = userData.last_name ? ' ' + userData.last_name : '';


            let parentName = parentData && (parentData.first_name || '') + (parentData.family_name || '');
            let sponsorName = parentData && (parentData.first_name || '') + (parentData.family_name || '');

            userUi = <View>


                {ViewUtils.gitUserInformationTv(I18n.t('default.fullName'), nick_name + last_name,)}
                {ViewUtils.gitUserInformationTv(I18n.t('b.businessCenterID'), userData.rand_id,)}

                {userData.is_ibm === 1 ? ViewUtils.gitUserInformationTv(I18n.t('default.Key'), userData.code,) : null}


                <Text style={[styles.titleTv, StyleUtils.smallFont, {textAlign: 'left',}]}>{I18n.t('default.directSponsorId')}</Text>
                {sponsorData ?
                    <View>
                        {sponsorName ? <Text
                            style={[styles.marginTop, StyleUtils.smallFont]}>{sponsorName}</Text> : null}

                        {sponsorData.area_code || sponsorData.mobile ? <Text
                            style={[StyleUtils.smallFont]}>{sponsorData.area_code + ' ' + sponsorData.mobile}</Text> : null}

                        {sponsorData.email ? <Text
                            style={[StyleUtils.smallFont]}>{sponsorData.email}</Text> : null}
                        <View style={[StyleUtils.lineStyle, StyleUtils.smallFont]}/>
                    </View>
                    : null}


                <Text style={[styles.titleTv, StyleUtils.smallFont, {textAlign: 'left',}]}>{I18n.t('default.sponsorInfo')}</Text>
                {parentData ?
                    <View>
                        {parentName ? <Text
                            style={[styles.marginTop, StyleUtils.smallFont]}>{parentName}</Text> : null}
                        <Text
                            style={[StyleUtils.smallFont]}>{parentData.rand_id}</Text>

                        {parentData.area_code || parentData.mobile ? <Text
                            style={[StyleUtils.smallFont]}>{parentData.area_code + ' ' + parentData.mobile}</Text> : null}

                        {parentData.email ? <Text
                            style={[StyleUtils.smallFont]}>{parentData.email}</Text> : null}

                        <View style={[StyleUtils.lineStyle, StyleUtils.smallFont]}/>
                    </View>
                    : null}


                {ViewUtils.gitUserInformationTv(I18n.t('b.entryDate'), userData.reg_time)}

                {ViewUtils.gitUserInformationTv(I18n.t('b.regsitrationPack'), userData.is_reg_pack === 1 ? (I18n.t('default.activated') + '\n' + (userData.reg_pack_time || '')) : I18n.t('default.notActivated'),
                    require('../../../res/images/ic_right_more.png'), () => userData.is_reg_pack === 1 ? null : this.onClick('is_reg_pack'))}

                {ViewUtils.gitUserInformationTv(I18n.t('default.ibmStarterPack'), userData.is_buy === 1 ? (I18n.t('default.activated') + '\n' + (userData.buy_time || '')) : I18n.t('default.notActivated'),
                    require('../../../res/images/ic_right_more.png'), () => userData.is_buy === 1 ? null : this.onClick('is_buy'))}

                {ViewUtils.gitUserInformationTv(I18n.t('b.volumeThreshold'), userData.is_prereg === 1 ? I18n.t('default.activated') : I18n.t('default.notActivated'),
                    require('../../../res/images/ic_right_more.png'), () => this.startNewShop(userData))}

                {/*选择子账号*/}
                {/*{ViewUtils.gitUserInformationTv(I18n.t('b.defaultBusiness'), userData.rand_id,*/}
                {/*require('../../../res/images/ic_right_more.png'), () => this.startNewShop(userData))}*/}

                {ViewUtils.getButton(I18n.t('default.startShoppingBtn'), () => this.startNewShop(userData), {marginBottom: 34})}

            </View>
        }


        return (

            <View style={[StyleUtils.flex, {backgroundColor: 'white'}]}>
                <Navigation
                    onClickLeftBtn={() => {
                        this.props.navigator.pop()
                    }}
                    leftButtonIcon={require('../../../res/images/ic_back.png')}
                    titleImage={require('../../../res/images/ic_ipar_logo.png')}

                />

                <ScrollView style={StyleUtils.flex}>
                    <View style={{paddingLeft: 16, paddingRight: 16}}>
                        {ViewUtils.getTitleView(I18n.t('default.myBusiness'))}
                        {userUi}
                    </View>
                </ScrollView>
            </View>
        );
    }

//

}
const styles = StyleSheet.create({
    marginTop: {
        marginTop: 16
    },
    titleTv: {
        marginTop: 16,
        fontSize: 18,
        color: 'black',
        textAlign:'left'
    }
});
