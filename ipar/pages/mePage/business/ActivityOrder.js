import React, {Component} from 'react'
import {View, ScrollView} from 'react-native'
import StyleUtils from "../../../res/styles/StyleUtils";
import ViewUtils from "../../../utils/ViewUtils";
import I18n from "../../../res/language/i18n";
import MyOccupationalRecords from "./MyOccupationalRecords";
import OrdersPage from "../order/OrdersPage";

export default class ActivityOrder extends Component {

    constructor(props) {
        super(props);
        this.state = {};


        const {userData} = this.props;
        this.userData = userData;
    }

    onPress(pushOrder) {
        const {navigator, userData} = this.props;

        navigator.push({
            component: pushOrder ? OrdersPage : MyOccupationalRecords,
            name: pushOrder ? 'OrdersPage' : 'MyOccupationalRecords',
            params: {
                ...this.props,
                userId: userData.user_id
            }
        })
    };


    getUserInformationView(name, value, onPress, icon,) {
        return (ViewUtils.gitUserInformationTv(name, '',
            icon && require('../../../res/images/ic_right_more.png'), onPress, value))
    }

    render() {
        let {userData} = this.props;

        return (
            <View style={[StyleUtils.flex]}>
                <ScrollView>
                    {ViewUtils.getTitleView(I18n.t('default.myActivity'), I18n.t('default.bonusesTheGlobal'))}
                    <View style={{padding: 16}}>
                        {this.getUserInformationView(I18n.t('b.newPerformance'), userData && userData.group_pv)}
                        {this.getUserInformationView(I18n.t('b.historicalAchievement'), userData && userData.history_total_pv)}
                        {this.getUserInformationView(I18n.t('b.personalNewPerformance'), userData && userData.total_pv, () => this.onPress(true), true)}
                        {this.getUserInformationView(I18n.t('b.myCareerRecord'), '', () => this.onPress(), true)}
                    </View>
                </ScrollView>
            </View>
        )
    }
}
