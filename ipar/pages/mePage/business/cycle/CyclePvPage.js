import React, {Component} from 'react'
import {Image, ListView, ScrollView, StyleSheet, Text, TouchableHighlight, View} from 'react-native'
import StyleUtils from "../../../../res/styles/StyleUtils";
import Navigation from "../../../../custom/Navigation";
import I18n from "../../../../res/language/i18n";
import {DATE_DATA} from "../../../../res/json/monthsJson";
import ViewUtils from "../../../../utils/ViewUtils";
import CommonUtils from "../../../../common/CommonUtils";
import IparNetwork, {SERVER_TYPE} from "../../../../httpUtils/IparNetwork";
import ModalListUtils from "../../../../utils/ModalListUtils";
import CycleContent from "./CycleContent";

let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
export default class CyclePvPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            cyclePv: {a: {}, b: {}, c: {}, d: {}},
            selectMount: DATE_DATA.month,
        };
        this.iparNetwork = new IparNetwork();
    }


    componentDidMount() {
        this.getCyclePv();
    }

    getCyclePv() {
        let userData = this.props.userData;
        if (!userData) {
            return
        }
        const {selectMount} = this.state;
        let user_id = userData.user_id;
        CommonUtils.showLoading();
        this.iparNetwork.getRequest(SERVER_TYPE.payServer + 'getCyclePv?', 'user_id=' + user_id + "&years=" + DATE_DATA.year + '-' + selectMount)
            .then((result) => {
                CommonUtils.dismissLoading();
                if (result.code === 200) {
                    this.setState({
                        cyclePv: result.data,
                    })
                }
            })
            .catch((error) => {
                CommonUtils.dismissLoading();
            })
    }


    render() {

        let {cyclePv} = this.state;

        let icon = require('../../../../res/images/ic_right_more.png');
        return (
            <View style={[StyleUtils.flex]}>

                <Navigation
                    onClickLeftBtn={() => {
                        this.props.navigator.pop()
                    }}/>


                <ScrollView>
                    <View style={{flex: 1, padding: 16}}>

                        {ViewUtils.getTitleView(I18n.t('b.checkDetails'))}

                        <TouchableHighlight
                            activeOpacity={0.7}
                            underlayColor={'#ededed'}
                            onPress={() => this.setState({
                                modalVisible: true,
                            })}
                        >
                            <Text
                                style={styles.startTimeText}>{DATE_DATA.year + '-' + (this.state.selectMount)}</Text>
                        </TouchableHighlight>
                        {ViewUtils.getDefaultTab([I18n.t('b.cycle'), I18n.t('default.volume'), I18n.t('b.threshold'), I18n.t('b.totalPPV'), I18n.t('default.activ')],
                            null, null, {height: 50}, [StyleUtils.defaultTabTv, {color: 'gray'}])}


                        {ViewUtils.getDefaultTab(["A", cyclePv.a && cyclePv.a.active_total_pv || '0', cyclePv.a && cyclePv.a.cycle_active_total_pv || '0', cyclePv.a && cyclePv.a.active_group_pv || '0', cyclePv.a && cyclePv.a.is_active5 === 0 ? 'no' : 'yes'],
                            () => {
                                this.startContent("A")
                            }, icon, {height: 50}, [StyleUtils.defaultTabTv, {fontSize: 14}])}

                        {ViewUtils.getDefaultTab(['B', cyclePv.b && cyclePv.b.active_total_pv || '0', cyclePv.b && cyclePv.b.cycle_active_total_pv || '0', cyclePv.b && cyclePv.b.active_group_pv || '0', cyclePv.b && cyclePv.b.is_active5 === 0 ? 'no' : 'yes'],
                            () => {
                                this.startContent("B")
                            }, icon, {height: 50}, [StyleUtils.defaultTabTv, {fontSize: 14}])}

                        {ViewUtils.getDefaultTab(['C', cyclePv.c && cyclePv.c.active_total_pv || '0', cyclePv.c && cyclePv.c.cycle_active_total_pv || '0', cyclePv.c && cyclePv.c.active_group_pv || '0', cyclePv.c && cyclePv.c.is_active5 === 0 ? 'no' : 'yes'],
                            () => {
                                this.startContent("C")
                            }, icon, {height: 50}, [StyleUtils.defaultTabTv, {fontSize: 14}])}

                        {ViewUtils.getDefaultTab([DATE_DATA.year + '-' + this.state.selectMount, cyclePv.d && cyclePv.d.active_total_pv || '0', cyclePv.d && cyclePv.d.monthly_active_group_pv || '0', cyclePv.d && cyclePv.d.active_group_pv || '0', cyclePv.d && cyclePv.d.is_active5 === 0 ? 'no' : 'yes'],
                            () => {
                                this.startContent("")
                            }, icon, {height: 50}, [StyleUtils.defaultTabTv, {fontSize: 14}])}

                    </View>
                </ScrollView>


                <ModalListUtils
                    title={DATE_DATA.year}
                    onPress={(index, text) => this.onModalItemClick(index, text)}
                    close={() => this.setState({
                        modalVisible: false,
                    })}
                    visible={this.state.modalVisible}
                    dataSource={ds.cloneWithRows(new CommonUtils().getMonthsData(true))}
                    dataParam={'name'}
                />
            </View>
        )
    }


    startContent(cycle) {
        const {userData, navigator} = this.props;
        navigator.push({
            component: CycleContent,
            params: {
                userData,
                years: DATE_DATA.year + '-' + (this.state.selectMount),
                cycle
            }
        })

    }

    /***
     * modal item click
     * @param index
     * @param text
     */
    onModalItemClick(index, text) {
        let month = new CommonUtils().getMonthsData()[index].code;
        this.setState({
            modalVisible: false,
            selectMount: month,
        });
        this.getCyclePv()
    }
}

const styles = StyleSheet.create({
    startTimeText: {
        borderWidth: 1,
        padding: 8,
        paddingRight: 26,
        paddingLeft: 26,
        borderColor: '#bcbcbc',
        textAlign: 'center',
        margin: 10,
    }
});
