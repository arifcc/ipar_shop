import React, {Component} from 'react'
import {View, Text, ScrollView, FlatList, DeviceEventEmitter} from 'react-native'
import StyleUtils from "../../../../res/styles/StyleUtils";
import ViewUtils from "../../../../utils/ViewUtils";
import I18n from "../../../../res/language/i18n";
import IparNetwork, {SERVER_TYPE} from "../../../../httpUtils/IparNetwork";
import CommonUtils from "../../../../common/CommonUtils";
import Navigation from "../../../../custom/Navigation";

export default class CycleContent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
        }

        this.iparNetwork = new IparNetwork();
    }


    componentDidMount() {
        this._getData();
    }


    _getData() {
        const {userData, years, cycle} = this.props;
        CommonUtils.showLoading();
        this.iparNetwork.getRequest(SERVER_TYPE.payServer + 'getCycleGroupPv?',
            'user_id=' + userData.user_id + '&years=' + years + '&cycle=' + cycle)
            .then((result) => {
                if (result.code === 200) {
                    this.setState({
                        data: result.data.team,
                    })
                } else {
                    DeviceEventEmitter.emit('toast', I18n.t('default.fail'))
                }
                CommonUtils.dismissLoading();
            })
            .catch((error) => {
                console.log('CycleContent-----getData--err:' + error);
                CommonUtils.dismissLoading();
                DeviceEventEmitter.emit('toast', I18n.t('default.fail'))
            })
    }

    render() {
        const {data} = this.state;
        return (
            <View style={[StyleUtils.flex]}>
                <Navigation
                    onClickLeftBtn={() => {
                        this.props.navigator.pop()
                    }}/>

                {ViewUtils.getTitleView(I18n.t('b.myTeamTreeNote'))}

                {this._renderItem('ID', I18n.t('default.userName'), I18n.t('default.volume'), I18n.t('default.activ'))}
                <FlatList
                    style={{paddingHorizontal: 8}}
                    data={data}
                    renderItem={(item) => this.renderItem(item.item)}
                    extraData={this.props}
                />

            </View>
        )
    }


    _renderItem(_1, _2, _3, _4, style) {
        return <View style={[StyleUtils.rowDirection, {height: 50, paddingHorizontal: 8}]}>
            <Text style={{flex: 1.4, textAlign: 'center', style}} numberOfLines={1}>{_1}</Text>
            <Text style={{flex: 2, textAlign: 'center', style}} numberOfLines={1}>{_2}</Text>
            <Text style={{flex: 1, textAlign: 'center', style}} numberOfLines={1}>{_3}</Text>
            <Text style={{flex: 1, textAlign: 'center', style}} numberOfLines={1}>{_4}</Text>
        </View>
    }


    renderItem(item) {
        let activName = item.is_active5 === 0 ? 'no' : 'yes';
        return this._renderItem(item.rand_id || ' ', (item.first_name + item.last_name) || ' ', item.total_pv, activName);
    }
}
