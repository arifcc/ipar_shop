/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {DeviceEventEmitter, Image, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import StyleUtils from "../../../res/styles/StyleUtils";
import Navigation from "../../../custom/Navigation";
import ViewUtils from "../../../utils/ViewUtils";
// import ProgressBar from 'react-native-simple-progressbar';
import IparNetwork, {SERVER_TYPE} from "../../../httpUtils/IparNetwork";
import CommonUtils from "../../../common/CommonUtils";

import RegisterIBMThreePage from "../../common/register/RegisterIBMThreePage";
import VerifyPersonalPage from "./VerifyPersonalPage";
import ContractPage from "./ContractPage";
import I18n from "../../../res/language/i18n";
import ProgressBar from "../../../custom/ProgressBar";
import PlansMainPage from "./PlansMainPage";

export default class BusinessPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userData: '',
            prSize: 0,

            isActivates: [],
            isNotActivates: [],
        };

        this.iparNetwork = new IparNetwork()
    }

    componentWillUnmount() {
        this.iparNetwork = null;
    }

    componentDidMount() {
        CommonUtils.showLoading();

        CommonUtils.getAcyncInfo('userInfo')
            .then((result) => {
                this.userInfo = JSON.parse(result);
                this.getUserInfo(this.userInfo.user_id)
            });
    }

    /*
    *网络请求
    *
    * 获取用户详细信息
    */
    getUserInfo(userId) {
        this.iparNetwork.getRequest(SERVER_TYPE.admin + 'getSubAccount?', 'user_id=' + userId)
            .then((result) => {
                CommonUtils.dismissLoading();

                if (result.code === 200) {
                    let userInfo = result.data;

                    let _isActivates = [];
                    let _isNotActivates = [];

                    // for (let i = 0, len = this.userStatussData.length; i < len; i++) {
                    //
                    // }


                    let activateBusinessPlan = {
                        name: I18n.t('default.activateBusinessPlan'),
                        content: I18n.t('default.activated'),
                        code: 'activate'
                    };

                    let verifyPerInf = {
                        name: I18n.t('default.verPersonal'),
                        content: I18n.t('default.verifiedAccounts'),
                        code: 'verify'
                    };


                    let contract_time = userInfo.contract_time ? userInfo.contract_time.substring(0, 11) : '';

                    let contract = {
                        name: I18n.t('default.contract'),
                        content: I18n.t('default.signedOn') + contract_time,
                        code: 'contract'
                    };


                    if (userInfo.open_plan_num > 1) {
                        _isActivates.push(activateBusinessPlan)
                    } else {
                        activateBusinessPlan.content = I18n.t('default.notActivated');
                        _isNotActivates.push(activateBusinessPlan)
                    }

                    let isNotNull = userInfo && userInfo.first_name && userInfo.last_name && userInfo.id_number;

                    if (isNotNull) {
                        _isActivates.push(verifyPerInf)
                    } else {
                        verifyPerInf.content = I18n.t('default.verifyYourInfo');
                        _isNotActivates.push(verifyPerInf)
                    }

                    if (userInfo.is_contract === 1) {
                        _isActivates.push(contract)
                    } else {
                        contract.content = I18n.t('default.notSign');
                        _isNotActivates.push(contract)
                    }


                    this.setState({
                        userData: userInfo,
                        isActivates: _isActivates,
                        isNotActivates: _isNotActivates
                    });
                    CommonUtils.saveUserInfo(userInfo);
                    this.progressSizeInfo(userInfo);
                }
            })
            .catch((error) => {
                CommonUtils.dismissLoading();

                console.log(error)
            })
    }


    /**
     * 还没激活的
     * */
    onItemClick(callback, isActivate) {
        let selectPage;
        let _params = {};
        switch (callback) {
            case 'activate':
                selectPage = isActivate ? PlansMainPage : RegisterIBMThreePage;
                _params = isActivate ? {userId: this.userInfo.main_user_id} : {};
                break;
            case 'contract':
                if (!isActivate) {
                    let isNotNull = this.userInfo && this.userInfo.first_name && this.userInfo.last_name && this.userInfo.id_number;

                    if (isNotNull) {
                        selectPage = ContractPage;
                        _params = {isContract: false};
                    } else {
                        selectPage = VerifyPersonalPage;
                    }
                }
                break;
            case 'verify':
                selectPage = VerifyPersonalPage;
                break;
        }
        if (selectPage) {
            this.props.navigator.push({
                component: selectPage,
                name: selectPage + '',
                params: _params,
            })
        }

    }


    /**
     * progress value
     */
    progressSizeInfo(userInfo) {
        if (userInfo) {
            let progressSize = 0;
            if (userInfo.open_plan_num > 1) {
                progressSize += 33;
            }
            if (userInfo.is_contract === 1) {
                progressSize += 33;
            }
            let isNotNull = userInfo && userInfo.first_name && userInfo.last_name && userInfo.id_number;

            if (isNotNull) {
                progressSize += 34;
            }

            this.setState({prSize: progressSize})
        }
    }

    render() {

        let userUi;
        let userData = this.state.userData;
        /*全部UI*/
        userUi = userData ? <View>

            {this.state.isNotActivates.length > 0 ?
                <View>
                    <Text style={[styles.successTv, StyleUtils.smallFont]}>{I18n.t('default.notActivated')}</Text>
                    {this.state.isNotActivates.map((item, i) => {
                        return ViewUtils.gitUserInformationTv(item.name, item.content,
                            require('../../../res/images/ic_selected.png'), () => this.onItemClick(item.code, false))
                    })}
                </View> : null}

            {this.state.isActivates.length > 0 ?
                <View>
                    <Text style={[styles.successTv, StyleUtils.smallFont]}>{I18n.t('default.activated')}</Text>
                    {this.state.isActivates.map((item, i) => {
                        return ViewUtils.gitUserInformationTv(item.name, item.content,
                            require('../../../res/images/ic_selected_in.png'), () => this.onItemClick(item.code, true))
                    })}
                </View> : null}

        </View> : null;


        return (
            <View style={[StyleUtils.flex, {backgroundColor: 'white'}]}>
                <Navigation
                    onClickLeftBtn={() => {
                        this.props.navigator.pop()
                    }}
                    leftButtonIcon={require('../../../res/images/ic_back.png')}
                />
                <ScrollView style={StyleUtils.flex}>
                    <View style={{paddingLeft: 16, paddingRight: 16}}>

                        {/*题目*/}
                        {ViewUtils.getTitleView(I18n.t('default.preRegistrationStatus'), I18n.t('default.identityContent'))}
                        <View style={{flexDirection: 'row', height: 40}}>
                            <ProgressBar
                                progress={this.state.prSize + '%'}
                            />
                            <Text
                                style={[StyleUtils.smallFont, styles.progressTv]}>{this.state.prSize + '%'}</Text>
                        </View>
                        {userUi}
                    </View>
                </ScrollView>

            </View>
        );
    }


}


const styles = StyleSheet.create({

    successTv: {
        fontSize: 25,
        color: 'black',
        marginTop: 16,
        marginBottom: 16,
        textAlign: 'left',
    },
    topBox: {
        height: 250,
        backgroundColor: 'white',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 55
    },
    progressTv: {
        width: 55,
        fontSize: 17,
        marginLeft: 5,
        height: 22,
        lineHeight: 22,
        textAlign: 'center',
    },
    progressBox: {
        width: '100%',
        height: 80,
        flexDirection: 'column',
        backgroundColor: '#f9f9f9',
        marginTop: 30,
        paddingLeft: 10
    },
    businessTv: {
        height: 40,
        fontSize: 25,
        color: 'black'
    },
    iparProvidesTv: {
        fontSize: 18,
        color: 'gray',
        textAlign: 'center'
    },
    identity: {
        height: 40,
        color: '#474848',
        lineHeight: 30,
    }
});
