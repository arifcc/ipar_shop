/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {DeviceEventEmitter, Image, ListView, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

import Navigation from "../../../custom/Navigation";
import StyleUtils from "../../../res/styles/StyleUtils";
import ViewUtils from "../../../utils/ViewUtils";
import IparNetwork, {IMAGE_URL, SERVER_TYPE} from "../../../httpUtils/IparNetwork";
import CommonUtils from "../../../common/CommonUtils";
import I18n from "../../../res/language/i18n";
import moment from 'moment';

export default class MyTeamPage extends Component {
    constructor(props) {
        super(props);
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds,
            dataIsNull: false,
        };
        this.iparNetwork = new IparNetwork();
    }

    componentWillUnmount() {
        this.iparNetwork = null;
    }

    componentDidMount() {
        // CommonUtils.getAcyncInfo('userInfo')
        //     .then((result) => {
        CommonUtils.showLoading();
        this.getRewardInfo();
        // });

        CommonUtils.getAcyncInfo('getPv')
            .then((result) => {
                this.swap_amount = JSON.parse(result).swap_amount;
            })

    }

    /*获取个人用户奖励信息*/

    getRewardInfo() {
        let userId = this.props.user_id;
        let _url = SERVER_TYPE.admin + 'getTeam?';
        let _params = 'user_id=' + userId;
        if (this.props.type === 'newRegistrants') {
            let date = moment(new Date());
            let year = date.format('YYYY');
            let month = date.format('MM');
            let day = date.format('DD');

            let startMonth = month - 2;
            let startYear = year;

            if (startMonth <= 0) {
                startYear = year - 1;
                startMonth = 12 + startMonth
            }

            _url = SERVER_TYPE.admin + 'getTeam?';
            let endTime = year + '-' + month + '-' + day;
            let startTime = startYear + '-' + startMonth + '-' + '01';
            _params = 'action_time=' + startTime + '&end_time=' + endTime + '&user_id=' + userId;
        }

        this.iparNetwork.getRequest(_url, _params)
            .then((result) => {
                CommonUtils.dismissLoading();

                if (result.code === 200) {
                    this.setState({dataSource: this.state.dataSource.cloneWithRows(result.data)})
                } else {
                    this.setState({dataIsNull: true})
                }

            })
            .catch((error) => {
                console.log(error);
                CommonUtils.dismissLoading();

                DeviceEventEmitter.emit('toast', I18n.t('default.fail'))
            })
    }


    render() {

        let _title = I18n.t('default.myTeam');
        let _content = I18n.t('default.myTeamNote');
        if (this.props.type === 'newRegistrants') {
            _title = I18n.t('default.newRegistrants');
            _content = I18n.t('default.newRegisNote');
        }

        return (
            <View style={StyleUtils.flex}>
                <Navigation
                    onClickLeftBtn={() => {
                        this.props.navigator.pop();
                    }}
                    leftButtonIcon={require('../../../res/images/ic_back.png')}
                    titleImage={require('../../../res/images/ic_ipar_logo.png')}
                />
                <ScrollView>
                    <View style={[StyleUtils.flex, {padding: 16}]}>
                        {ViewUtils.getTitleView(_title, _content)}
                        {/*<Text style={[styles.contentTv, StyleUtils.smallFont, {fontSize: 20, color: '#777777'}]}*/}
                        {/*numberOfLines={1}>{I18n.t('default.userInfo')}</Text>*/}
                        <ListView
                            dataSource={this.state.dataSource}
                            renderRow={this.renderRow.bind(this)}
                        />

                        {this.state.dataIsNull &&
                        <Text style={[StyleUtils.smallFont, {
                            flex: 1,
                            textAlign: 'center'
                        }]}>{I18n.t('default.noData')}</Text>}
                    </View>
                </ScrollView>

            </View>)


    }


    /*list RenderRow*/
    renderRow(rowData, sectionID, rowID) {

        let nick_name = rowData.child ? rowData.child.nick_name : rowData.nick_name;
        let mobile = rowData.child ? rowData.child.mobile : rowData.mobile;
        let reg_time = rowData.child ? rowData.child.reg_time : rowData.reg_time;
        let email = rowData.email;
        let level_name = rowData.level ? rowData.level.level_name : rowData.level_name;
        let head_img = rowData.child ? rowData.child.head_img : rowData.head_img;


        //主账号...
        let userMain = rowData.user_main;
        if (userMain) {
            nick_name = userMain.nick_name;
            mobile = userMain.mobile;
            reg_time = userMain.reg_time;
            email = userMain.email;
            level_name = userMain.level ? userMain.level.level_name : '';
            head_img = userMain.head_img;
        }


        return (<TouchableOpacity>

                <View style={StyleUtils.lineStyle}/>
                <View style={[StyleUtils.center, StyleUtils.rowDirection]}>

                    <Image
                        style={[{width: 40, height: 40, borderRadius: 20, resizeMode: 'cover', marginRight: 16}]}
                        source={head_img ? {uri: IMAGE_URL + head_img} : require('../../../res/images/ic_avatar.png')}/>

                    <View style={StyleUtils.flex}>
                        <Text numberOfLines={1}
                              style={[styles.titleTv, StyleUtils.smallFont]}>{nick_name}</Text>
                        <Text style={[styles.contentTv, StyleUtils.smallFont]}
                              numberOfLines={1}>{mobile}</Text>
                        <Text style={[styles.contentTv, StyleUtils.smallFont]}
                              numberOfLines={1}>{email}</Text>
                        <Text
                            style={[styles.contentTv, StyleUtils.smallFont, {
                                color: '#a7a7a7',
                                marginTop: 8,
                                fontSize: 12
                            }]}>{reg_time}</Text>
                    </View>
                    <Text style={[styles.contentTv, StyleUtils.smallFont, {width: 80, textAlign: 'center'}]}
                          numberOfLines={1}>{level_name}</Text>
                </View>

            </TouchableOpacity>

        )
    }


}
const styles = StyleSheet.create({
    titleTv: {
        marginTop: 10,
        marginBottom: 8,
        fontSize: 18,
        color: 'black',
        textAlign: 'left'

    },
    contentTv: {
        fontSize: 15,
        color: 'black'
    }
});

