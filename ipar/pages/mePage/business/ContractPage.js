/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {DeviceEventEmitter, Dimensions, ScrollView, Text, View, WebView} from 'react-native';

import IparNetwork, {SERVER_TYPE} from "../../../httpUtils/IparNetwork";
import StyleUtils from "../../../res/styles/StyleUtils";
import Navigation from "../../../custom/Navigation";
import CommonUtils from "../../../common/CommonUtils";
import I18n from "../../../res/language/i18n";
import ViewUtils from "../../../utils/ViewUtils";
import CheckBox from 'react-native-check-box'

const height = Dimensions.get('window').height;
export default class ContractPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: null,
            isCheck: false,
        };

        this.iparNetwork = new IparNetwork()
    }

    componentWillUnmount() {
        this.iparNetwork = null;
    }

    componentDidMount() {
        CommonUtils.getAcyncInfo('userInfo')
            .then((result) => {
                this.setState({userId: JSON.parse(result).main_user_id})
            })

    }

    /*用户合同*/
    getConInfo() {
        if (!this.state.isCheck) {
            DeviceEventEmitter.emit('toast', I18n.t('default.selectCamera'));
            return;
        }

        CommonUtils.showLoading();

        let formData = new FormData();
        formData.append("user_id", this.state.userId);
        formData.append("is_contract", "1");

        this.iparNetwork.getPostFrom(SERVER_TYPE.admin + 'updateUserInfo', formData)
            .then((result) => {
                console.log(result);
                CommonUtils.dismissLoading();

                if (result.code === 200) {
                    this.props.navigator.popToTop();
                    DeviceEventEmitter.emit('toast', I18n.t('default.success'))
                } else {
                    DeviceEventEmitter.emit('toast', I18n.t('default.fail'))
                }
            })
            .catch((error) => {
                CommonUtils.dismissLoading();

                console.log(error)
                DeviceEventEmitter.emit('toast', I18n.t('default.fail'))

            })
    }

    /*loading finish*/
    onLoadingFinish() {
        CommonUtils.showLoading();

    }

    render() {
        let newVar = SERVER_TYPE.adminIpar + 'contract?user_id=' + this.state.userId;

        // let html = '<html>' +
        // 	'<head>\n' +
        // 	'<meta name="browsermode" content="application">' +
        // 	'</head>' +
        // 	'<script>\n' +
        // 	'function button_onclick(){\n' +
        // 	'//这里写你要执行的语句\n' +
        // 	'alert(document.getElementById("checkbox").checked);\n' +
        // 	'}\n' +
        // 	'</script>' +
        // 	'<div align="center" style="height=100% ;">' +
        // 	'<iframe width="100%" height="100%" style="border: 0;" src=' + newVar + '></iframe>' +
        // 	'<div style="margin:auto;height: 60px;line-height: 80px">' +
        // 	'<input id="checkbox" type="checkbox" style="margin-top:10px;width: 40px;height: 40px;text-align:center;"/>' +
        // 	'<span style="font-size: 34px;text-align: center;">by clicking sign buttun here i agree to the tersm and conditions of IPAR IBM registration agreement</span>' +
        // 	'</div>' +
        // 	'<button onclick="javascript:button_onclick()" type="button" style="height: 80px;width: 89%;margin:10px 10% 10% 10%;color: white;font-size: 26px;background-color: #333333 ;">Click Me!</button>' +
        // 	'</div>' +
        // 	'</html>';


        return (
            <View style={StyleUtils.flex}>
                <Navigation
                    onClickLeftBtn={() => {
                        this.props.navigator.pop()
                    }}
                />

                <WebView
                    onMessage={() => {
                        console.log();
                    }}
                    startInLoadingState={true}
                         style={{height: height}}
                         source={{uri: newVar}}
                         javaScriptEnabled={true}
                         domStorageEnabled={true}
                         scalesPageToFit={true}
                />
                {!this.props.isContract ?
                    <View style={{backgroundColor: '#ffffff', borderTopWidth: 1, borderColor: '#c3c3c3', padding: 20}}>

                        <CheckBox rightTextStyle={{textAlign: 'center'}}
                                  style={{marginBottom: 16,}}
                                  onClick={() =>
                                      this.setState({isCheck: !this.state.isCheck})}
                                  rightText={'by clicking sign buttun here i agree to the tersm and conditions of IPAR IBM registration agreement'}
                                  isChecked={this.state.isCheck}/>
                        {ViewUtils.getButton(I18n.t('default.submit'), () => this.getConInfo())}
                    </View> : null
                }
            </View>

        );
    }

    //接收HTML发出的数据
    _onMessage = (e) => {
        alert(e.nativeEvent.data);
        // this.getConInfo();
        // this.setState({showLoading: true});
    }

}

