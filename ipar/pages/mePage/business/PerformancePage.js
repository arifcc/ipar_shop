/**
 * Sample React Native PerformancePage
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    Text,
    View,
    StyleSheet,
    ScrollView,
    Dimensions,
} from 'react-native';


import Echarts from 'native-echarts';
import StyleUtils from "../../../res/styles/StyleUtils";
import I18n from "../../../res/language/i18n";

const width = Dimensions.get('window').width;


export default class PerformancePage extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }


    getDataArray(array, param, type) {
        let resultArray = [];
        if (!array) return resultArray;


        for (let i = array.length - 1; i >= 0; i--) {
            let element = array[i][param];
            if (param === 'value') {

                if (type === 'system') {
                    element = (element[param] / element.size * 3).toFixed(0)
                } else {
                    element = element[param].toFixed(0)
                }
            }
            resultArray.push(element)
        }
        return resultArray;
    }

    render() {

        let dataCharts = this.props.dataSource;
        let monthData = [];
        let userValueData = [];
        let systemValueData = [];
        if (dataCharts) {

            let userCharts = dataCharts.user;
            let systemCharts = dataCharts.system;

            monthData = this.getDataArray(userCharts, 'month');
            userValueData = this.getDataArray(userCharts, 'value',);
            systemValueData = this.getDataArray(systemCharts, 'value', 'system');
        }

        const option = {

            legend: {
                data: [I18n.t('default.local'), I18n.t('default.average')]
            },

            calculable: true,
            xAxis: [
                {
                    type: 'category',
                    data: monthData,
                }
            ],
            yAxis: [
                {
                    type: 'value'
                }
            ],
            series: [
                {//user
                    color: ['black'],
                    name: I18n.t('default.local'),
                    type: 'bar',
                    data: userValueData,
                },
                {//system
                    color: ['#cdcdcd'],
                    name: I18n.t('default.average'),
                    type: 'bar',
                    data: systemValueData,
                }
            ]
        };

        const option1 = {
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b}: {c} ({d}%)"
            },
            // legend: {
            // 	orient: 'vertical',
            // 	x: 'left',
            // 	data: [I18n.t('default.MyPersonalPv'),I18n.t('default.MyPersonalPv'),]
            // },
            series: [
                {

                    color: this.props.performanceColors ? this.props.performanceColors :
                        ['black', '#333', '#626262', '#A0A0A0', '#D2D2D2', '#D2D2D2', '#A0A0A0'],
                    type: 'pie',
                    radius: ['40%', '60%'],
                    avoidLabelOverlap: false,


                    label: {
                        normal: {
                            show: false,
                            position: 'center'
                        },
                        emphasis: {
                            show: true,
                            textStyle: {
                                fontSize: '16',
                                fontWeight: 'bold'
                            }
                        },
                    },
                    labelLine: {
                        normal: {
                            show: false
                        }
                    },
                    data: this.props.performanceValues ? this.props.performanceValues : [
                        {value: (this.props.value || 0).toFixed(2),},
                    ]

                },


            ]
        };


        let text = (this.props.text || '');

        if (this.props.value && this.props.value > 0) {
            text = text + this.props.value.toFixed(2);
        } else {
            text = text + '0.00';
        }

        return (

            <View style={[StyleUtils.center]}>

                {this.props.type === 0 ?
                    <Text
                        style={{
                            position: 'absolute',
                            textAlign: 'center'
                        }}>{text}</Text>
                    : null}
                {this.props.type === 0 ? <Echarts option={option1} height={width} width={width}/> :
                    <View style={{marginTop: 40}}>
                        <Echarts
                            option={option} height={width / 1.5} width={width}/>
                    </View>
                }
            </View>);
    }
}
