/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {ART, DeviceEventEmitter, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import StyleUtils from "../../../res/styles/StyleUtils";
import Navigation from "../../../custom/Navigation";
import ViewUtils from "../../../utils/ViewUtils";
import BasicInfoPage from "./BasicInfoPage";
import I18n from "../../../res/language/i18n";
import BvPage from "./BvPage";
import IparNetwork, {SERVER_TYPE} from "../../../httpUtils/IparNetwork";
import CommonUtils from "../../../common/CommonUtils";
import PerformancePage from "./PerformancePage";
import MyTeamPage from "./MyTeamPage";
import RegisterIBMThreePage from "../../common/register/RegisterIBMThreePage";
import BasWebViewPage from "../../common/BasWebViewPage";
import SlideMenu from "../../../menu/SlideMenu";
import OrdersPage from "../order/OrdersPage";
import AlertDialog from "../../../custom/AlertDialog";
import GoodsIsBuyPage from "../../common/GoodsIsBuyPage";
import BaseModal from "../../../utils/BaseModal";
import SafeAreaView from 'react-native-safe-area-view';
import IparImageView from "../../../custom/IparImageView";
import TreeViewPage from "../../common/TreeViewPage";
import ActivityOrder from "./ActivityOrder";
import {DATE_DATA} from "../../../res/json/monthsJson";
import CyclePvPage from "./cycle/CyclePvPage";

// const moneyData = {
//     money: 0,
//     g_money: 0,
//     pha_money: 0,
//     pv_money: 0,
// };


export default class MyBusinessPage extends Component {

    constructor(props) {
        super(props);


        this.state = {
            alertVisible: false,
            baseModalVisible: false,
            cardImageUrl: null,
            userData: props.userData,
            chartsData: [],
            // s_money: 0,
            // globalMoney: 0,
            // phaMoney: 0,
            // pvMoney: 0,

            // cashBackValue: _cashBackValue || 0,
            // personalSales: _personalSales || 0,
            // growthValue: _growthValue || 0,
            // leadershipValue: _leadershipValue || 0,
            total_pv: props.userData ? props.userData.total_pv : 0,
            total_bbv: props.userData ? props.userData.total_bbv : 0,

            cyclePv: {a: {}, b: {}, c: {}, d: {}}
        };
        this.iparNetwork = new IparNetwork();
        this.onLoadRewardInfo = false;
        this.onLoadChartsInfo = false;
    }

    componentWillUnmount() {
        this.iparNetwork = null;
    }

    componentDidMount() {
        CommonUtils.getAcyncInfo('getPv')
            .then((result) => {
                this.swap_amount = JSON.parse(result).swap_amount;
                this.currency_code = JSON.parse(result).currency_code;
            });

        // this.getRewardInfo();
        this.getCharts();
        this.getCyclePv();
    }


    getCyclePv() {
        let userData = this.state.userData;
        if (!userData) {
            return
        }
        let user_id = userData ? userData.user_id : '';
        CommonUtils.showLoading();
        this.iparNetwork.getRequest(SERVER_TYPE.payServer + 'getCyclePv?', 'user_id=' + user_id + "&years=" + DATE_DATA.year + "-" + DATE_DATA.month)
            .then((result) => {
                CommonUtils.dismissLoading();
                if (result.code === 200) {
                    this.setState({
                        cyclePv: result.data,
                    })
                }
            })
            .catch((error) => {
                CommonUtils.dismissLoading();
            })
    }

    /**
     * 获取个人用户奖励信息
     * */
    getRewardInfo() {

        CommonUtils.showLoading();
        let mainUserId = this.state.userData ? this.state.userData.main_user_id : '';
        this.iparNetwork.getRequest(SERVER_TYPE.admin + 'getReward?', 'user_id=' + mainUserId)
            .then((result) => {
                this.onLoadRewardInfo = true;
                if (this.onLoadRewardInfo && this.onLoadChartsInfo) {
                    CommonUtils.dismissLoading();

                }

                if (result.code === 200) {
                    // for (i = 0; i < result.data.length; i++) {
                    //     let datum = result.data[i];
                    //     if (datum.split_type === 1) {
                    //         moneyData.money += datum.split_money;
                    //     } else if (datum.split_type === 0) {
                    //         moneyData.g_money += datum.split_money;
                    //     }
                    //     if (datum.split_rank === 9) {
                    //         moneyData.pha_money += datum.split_money
                    //     }
                    //     if (datum.split_rank > 1 &&
                    //         datum.split_rank < 5 ||
                    //         datum.split_rank === 6 ||
                    //         datum.split_rank === 7) {
                    //         moneyData.pv_money += datum.split_money
                    //     }
                    // }

                    // this.setState({
                    //     s_money: moneyData.money,
                    //     globalMoney: moneyData.g_money,
                    //     phaMoney: moneyData.pha_money,
                    //     pvMoney: moneyData.pv_money,
                    // });


                }

            })
            .catch((error) => {
                this.onLoadRewardInfo = true;
                console.log(error);
                if (this.onLoadRewardInfo && this.onLoadChartsInfo) {
                    CommonUtils.dismissLoading();

                }
                DeviceEventEmitter.emit('toast', I18n.t('default.fail'))
            })
    }


    /**
     * 获取订单统计
     */
    getCharts() {
        let userData = this.state.userData;
        if (!userData) {
            return
        }
        let user_id = userData ? userData.user_id : '';

        this.iparNetwork.getRequest(SERVER_TYPE.adminIpar + 'getCharts?', 'user_id=' + user_id + '&country=' + userData.country)
            .then((result) => {
                this.onLoadChartsInfo = true;

                if (this.onLoadRewardInfo && this.onLoadChartsInfo) {
                    CommonUtils.dismissLoading();

                }
                if (result.code === 200) {
                    this.setState({
                        chartsData: result.data,
                    })
                }
            })
            .catch((error) => {
                this.onLoadChartsInfo = true;

                if (this.onLoadRewardInfo && this.onLoadChartsInfo) {
                    CommonUtils.dismissLoading();

                }
                console.log('MyBusinessPage-----getCharts--error: ', error);
                DeviceEventEmitter.emit('toast', I18n.t('default.fail'))
            })


    }

    /**
     * type === 0 =>PV
     * */
    getPersonalPBV_View(text, marginTop, type) {
        return <TouchableOpacity
            onPress={() => {
                this.props.navigator.push({
                    component: OrdersPage,
                    params: {
                        userId: this.props.userData.user_id,
                        moneyType: type === 0 ? 'PV' : 'BV'
                    },
                })
            }}
            style={[StyleUtils.rowDirection, {marginTop: marginTop, alignItems: 'center'}]}>

            <View
                style={{width: 25, height: 15, backgroundColor: type === 0 ? 'black' : '#858585', borderRadius: 5}}/>
            <Text
                style={[{
                    paddingLeft: 8,
                    fontSize: 16,
                    textAlign: 'left'
                }, StyleUtils.smallFont]}>{text}</Text>

        </TouchableOpacity>
    }


    startNewShop(userData) {
        if (!userData) return;
        CommonUtils.saveUserInfo(userData);
        const {navigator} = this.props;
        navigator.resetTo({
            component: SlideMenu,
            name: 'SlideMenu',
            params: {
                ...this.props,
            }
        });

    }


    startBvPage(params, title, content) {

        if (!this.props.userData) {
            return;
        }

        this.props.navigator.push({
            component: BvPage,
            params: {
                bvType: params,
                user_id: this.props.userData.user_id,
                title: title,
                content: content
            }
        })
    }


    /**
     * 用户的奖金
     * */
    getRewardStatisticsData() {
        let props = this.props;
        let _cashBackValue = 0;
        let _personalSales = 0;
        let _sponsoship_award = 0;
        let _growthValue = 0;
        let _leadershipValue = 0;
        let _developmentAwardValue = 0;
        let _weeklyAwardValue = 0;

        let statisticsData = props.rewardStatisticsData;

        if (statisticsData && statisticsData.split[props.rank]) {
            let statisticDataSplt = statisticsData.split[props.rank];

            if (props.rank === 2 || props.rank === 3) {//rocket //new grow together
                for (let i = 0, len = statisticDataSplt.length; i < len; i++) {
                    let spltElement = statisticDataSplt[i];
                    _cashBackValue += spltElement[10].pv;
                    _personalSales += (spltElement[1].pv + spltElement[5].pv);
                    _growthValue += (spltElement[2].pv + spltElement[4].pv);
                    _leadershipValue += (spltElement[3].pv + spltElement[7].pv);
                }
            } else if (props.rank === 4) {//grow together
                _cashBackValue = statisticDataSplt['0']['10'].pv + statisticDataSplt[1]['10'].pv;

                for (let i = 1; i <= 16; i++) {
                    let element = statisticDataSplt[i];
                    _personalSales += element['1'].pv + element['5'].pv;
                    _growthValue += element['6'].pv + element['8'].pv + element['7'].pv + element['2'].pv;
                    _leadershipValue += element['9'].pv;
                    _sponsoship_award += element['11'].pv;
                }
            } else if (props.rank === 7) {//new grow together
                for (let i = 0, len = statisticDataSplt.length; i < len; i++) {
                    let element = statisticDataSplt[i];
                    _cashBackValue += element[10].pv;
                    _personalSales += element[1].pv + element[5].pv;
                    _growthValue += element[6].pv + element[8].pv + element[7].pv;
                    _leadershipValue += element[9].pv;
                    _developmentAwardValue += element[11].pv;
                    _weeklyAwardValue += element[12].pv;
                }
            }
        }

        return {
            cashBackValue: _cashBackValue,
            personalSales: _personalSales,
            sponsoshipAward: _sponsoship_award,
            growthValue: _growthValue,
            leadershipValue: _leadershipValue,
            developmentAwardValue: _developmentAwardValue,
            weeklyAwardValue: _weeklyAwardValue
        }
    }


    /**
     * 获取名片图片
     */
    createLabel() {

        if (this.state.cardImageUrl) {
            this.setState({
                baseModalVisible: true,
            });
            return
        }

        CommonUtils.showLoading();
        let formData = new FormData();
        let state = this.state;
        let userData = state.userData;
        formData.append("user_id", userData.user_id);
        formData.append("real_name", (userData.first_name + " " + userData.middle_name + " " + userData.last_name + ".").replace("null", ""));
        formData.append("area_code", userData.area_code);
        formData.append("mobile", userData.mobile);
        formData.append("rank_name", this.props.title);
        formData.append("business_center_id", userData.rand_id);
        formData.append("sponser_key", userData.code);
        formData.append("bar_content", userData.rand_id);

        this.iparNetwork.getPostFrom(SERVER_TYPE.payServer + 'createLabel', formData)
            .then((result) => {
                CommonUtils.dismissLoading();
                if (result.code === 200) {
                    this.setState({
                        baseModalVisible: true,
                        cardImageUrl: result.data,
                    })
                }
            })
            .catch((error) => {
                CommonUtils.dismissLoading();
                console.log("MyBusinessPage--createLabel--error-" + error)
            })
    }


    /**
     * image style == null 表达文字
     * */
    renderTotalPPVView(name, value, imageStyle) {
        return <View style={[{marginTop: 12,}, imageStyle != null && {alignItems: 'flex-end'}]}>
            <Text style={[{fontSize: 16, fontWeight: 'bold'}]}>
                {name}
            </Text>
            {imageStyle != null ? <Image style={[{marginTop: 6, width: 20, height: 20}, imageStyle]} source={value}/> :
                <Text style={{marginTop: 6}}>
                    {value}
                </Text>}

        </View>
    }

    render() {

        let state = this.state;
        let {cyclePv} = this.state;

        let userData = state.userData;
        let props = this.props;
        let _bonus = 0;
        if (props.rewardStatisticsData) {
            _bonus = props.rewardStatisticsData.rank[this.props.rank].pv
        }
        let _statisticsData = this.getRewardStatisticsData();

        let contentView =
            <View>
                <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={() => {
                        if (!state.userData) return;
                        this.props.navigator.push({
                            component: BasicInfoPage, params: {
                                ...this.props,
                                userData: state.userData,
                            }
                        })
                    }}
                    style={[StyleUtils.listItem, {backgroundColor: 'black'}]}
                >
                    <Text style={[StyleUtils.flex, StyleUtils.smallFont, StyleUtils.listItemTv, {
                        color: 'white',
                        backgroundColor: 'transparent',
                        textAlign: 'left'
                    }]}>{I18n.t('default.businessInfor')}</Text>
                    <Image
                        source={require('../../../res/images/ic_right_more.png')}
                        style={{width: 20, height: 20, tintColor: 'white', marginRight: 8}}/>
                </TouchableOpacity>


                {ViewUtils.renderOperationView(this.props.userData, this, null, null, null, true)}
                <View>
                    {/*{this.getPersonalPBV_View(I18n.t('default.MyPersonalPv') + state.total_pv, 40, 0)}*/}
                    {/*{this.getPersonalPBV_View(I18n.t('default.MyPersonalBv') + state.total_bbv, 8, 1)}*/}


                    <View style={[StyleUtils.justifySpace, {marginTop: 16}]}>

                        <View style={[StyleUtils.center, {flex: 1, paddingHorizontal: 6}]}>
                            <Text>{I18n.t('b.totalPPV')}</Text>
                            <Text style={[{
                                fontSize: 28,
                                marginTop: 6
                            }]}>{cyclePv.d && cyclePv.d.active_total_pv} PV</Text>
                        </View>
                        <View style={{flex: 1, paddingHorizontal: 6}}>
                            {this.renderTotalPPVView(I18n.t('b.cycleOne'), cyclePv.a && cyclePv.a.total_pv + " PV")}
                            {this.renderTotalPPVView(I18n.t('b.cycleTwo'), cyclePv.b && cyclePv.b.total_pv + " PV")}
                            {this.renderTotalPPVView(I18n.t('b.cycleThree'), cyclePv.c && cyclePv.c.total_pv + " PV")}
                        </View>
                        <View style={{flex: 1, paddingHorizontal: 6, alignItems: 'flex-end'}}>
                            {this.renderTotalPPVView(I18n.t('default.activ'), require('../../../res/images/ic_selected.png'),
                                {tintColor: cyclePv.a && cyclePv.a.is_active5 === 0 ? 'gray' : 'green'})}
                            {this.renderTotalPPVView("", require('../../../res/images/ic_selected.png'),
                                {tintColor: cyclePv.b && cyclePv.b.is_active5 === 0 ? 'gray' : 'green'})}
                            {this.renderTotalPPVView("", require('../../../res/images/ic_selected.png'),
                                {tintColor: cyclePv.c && cyclePv.c.is_active5 === 0 ? 'gray' : 'green'})}
                        </View>
                    </View>

                    <View style={[StyleUtils.justifySpace, {marginTop: 26}]}>
                        <TouchableOpacity>
                            <Text>{I18n.t("b.totalPPV")}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Text>{cyclePv.d && cyclePv.d.group_pv}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => {
                                this.props.navigator.push({
                                    component: CyclePvPage,
                                    params: {
                                        userData,
                                    }
                                })
                            }}>
                            <View style={[StyleUtils.rowDirection, {alignItems: 'center'}]}>
                                <Text>{I18n.t('b.checkDetails')}</Text>
                                <Image style={{width: 16, height: 16}}
                                       source={require('../../../res/images/ic_right_more.png')}/>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <PerformancePage
                        value={_bonus}
                        text={I18n.t('default.AwardPv') + '\n'}
                        type={0}
                    />
                    <Text style={[styles.topTitleTv, StyleUtils.smallFont]}>{I18n.t('default.myRewards')}</Text>
                    <View style={StyleUtils.lineStyle}/>
                    {ViewUtils.gitUserInformationTv(I18n.t('b.cashBack'), I18n.t('default.bonusesTheLocal'),
                        require('../../../res/images/ic_right_more.png'), () => {

                            this.startBvPage('10', I18n.t('b.cashBack'), I18n.t('default.bonusesTheLocal'));
                        }, (_statisticsData.cashBackValue).toFixed(2) + ' PV')}

                    {ViewUtils.gitUserInformationTv(I18n.t('default.MyPersonalSales'), I18n.t('default.bonusesTheLocal'),
                        require('../../../res/images/ic_right_more.png'), () => {
                            this.startBvPage('1', I18n.t('default.MyPersonalSales'), I18n.t('default.bonusesTheLocal'));
                        }, (_statisticsData.personalSales).toFixed(2) + ' PV')}

                    {ViewUtils.gitUserInformationTv(I18n.t('b.tui_jian'), I18n.t('o.tui_jian'),
                        require('../../../res/images/ic_right_more.png'), () => {
                            this.startBvPage('1', I18n.t('default.MyPersonalSales'), I18n.t('default.bonusesTheLocal'));
                        }, (_statisticsData.sponsoshipAward).toFixed(2) + ' PV')}

                    {ViewUtils.gitUserInformationTv(I18n.t('default.MyGrowth'), I18n.t('default.rewardsForMar'),
                        require('../../../res/images/ic_right_more.png'), () => {
                            this.startBvPage('pv', I18n.t('default.MyGrowth'), I18n.t('default.rewardsForMar'));
                        }, (_statisticsData.growthValue).toFixed(2) + ' PV')}


                    {ViewUtils.gitUserInformationTv(I18n.t('default.Leadership'), I18n.t('default.specialHonory'),
                        require('../../../res/images/ic_right_more.png'), () => {
                            this.startBvPage('9', I18n.t('default.Leadership'), I18n.t('default.specialHonory'));
                        }, (_statisticsData.leadershipValue).toFixed(2) + ' PV')}


                    {props.rank === 7 ? ViewUtils.gitUserInformationTv(I18n.t('b.developmentAward'), I18n.t('b.developmentAwardContent'),
                        require('../../../res/images/ic_right_more.png'), () => {
                            this.startBvPage('9', I18n.t('default.Leadership'), I18n.t('default.specialHonory'));
                        }, (_statisticsData.developmentAwardValue).toFixed(2) + ' PV') : null}

                    {props.rank === 7 ? ViewUtils.gitUserInformationTv(I18n.t('b.weeklyAward'), I18n.t('b.weeklyAwardContent'),
                        require('../../../res/images/ic_right_more.png'), () => {
                            this.startBvPage('9', I18n.t('default.Leadership'), '');
                        }, (_statisticsData.weeklyAwardValue).toFixed(2) + ' PV') : null}


                    {/*{ViewUtils.gitUserInformationTv(I18n.t('default.MyPersonalBv'), '',*/}
                    {/*require('../../../res/images/ic_right_more.png'), () => {*/}
                    {/*this.props.navigator.push({*/}
                    {/*component: BvPage,*/}
                    {/*params: {*/}
                    {/*user_id: this.props.userData.user_id*/}
                    {/*}*/}
                    {/*})*/}
                    {/*}, 1111 + ' PV')}*/}


                    {props.rank === 9 &&
                    <View style={{}}>
                        <Text
                            style={[styles.topTitleTv, StyleUtils.smallFont]}>{I18n.t('b.qualification')}</Text>


                        {this.renderStasView('Active 5 star', 'Active 5 star opens door to many opportunities', '-22PV', false)}
                        {this.renderStasView('Qualify for promotion', 'One needs to complelete certain volume to be promoted in thin bonus period', '+22PV', true)}
                        {this.renderStasView('Qaulify for the special sales bonus', 'there is a certain threshhold qualify for this bonus', '-22PV', false)}
                        {this.renderStasView('Qaulify for network bonus', 'Network bonus reqire some commitment to qualify', '+22PV', true)}
                        {this.renderStasView('Bonus updates in', 'Bonuses updated on the 28th of each months', '+22PV', true)}
                    </View>
                    }


                    <Text
                        style={[styles.topTitleTv, StyleUtils.smallFont]}>{I18n.t('default.businessActivity')}</Text>

                    <PerformancePage
                        dataSource={state.chartsData}
                        type={1}
                    />


                    {ViewUtils.gitUserInformationTv(I18n.t('default.myActivity'), I18n.t('default.bonusesTheGlobal'),
                        require('../../../res/images/ic_right_more.png'), () => {
                            if (userData)
                                this.props.navigator.push({
                                    // component: OrdersPage,
                                    component: ActivityOrder,
                                    params: {userData: state.userData},
                                })
                        })}

                    {ViewUtils.gitUserInformationTv(I18n.t('default.newRegistrants'), I18n.t('default.newRegisNote'),
                        require('../../../res/images/ic_right_more.png'), () => {
                            if (userData)
                                this.props.navigator.push({
                                    component: MyTeamPage,
                                    params: {
                                        type: 'newRegistrants',
                                        user_id: userData.user_id,
                                    }
                                });
                        })}

                    {ViewUtils.gitUserInformationTv(I18n.t('default.myTeam'), I18n.t('default.myTeamNote'),
                        require('../../../res/images/ic_right_more.png'), () => {
                            if (userData)
                                this.props.navigator.push({
                                    component: MyTeamPage,
                                    params: {
                                        user_id: userData.user_id,
                                    }
                                })
                        })}

                    {ViewUtils.gitUserInformationTv(I18n.t('b.myTeamTree'), I18n.t('b.myTeamTreeNote'),
                        require('../../../res/images/ic_right_more.png'), () => {
                            if (userData)
                                this.props.navigator.push({
                                    component: TreeViewPage,
                                    name: 'TreeViewPage',
                                    params: {
                                        userId: userData.user_id,
                                        urlPram: 'getTreeInfo?'
                                    }
                                })
                        })}

                    {ViewUtils.gitUserInformationTv(I18n.t('b.myBusinessCard'), I18n.t('b.myBusinessCard'),
                        require('../../../res/images/ic_right_more.png'), () => this.createLabel())}


                    {ViewUtils.getButton(I18n.t('default.startShoppingBtn'), () => this.startNewShop(userData), {
                        height: 48,
                        marginBottom: 34
                    })}


                </View>


            </View>;

        return (
            <View style={[StyleUtils.flex, {backgroundColor: 'white'}]}>
                <Navigation
                    onClickLeftBtn={() => {
                        this.props.navigator.popToTop()
                    }}
                    titleImage={require('../../../res/images/ic_ipar_logo.png')}

                />
                <ScrollView style={StyleUtils.flex}>
                    <View style={{paddingLeft: 16, paddingRight: 16}}>
                        {ViewUtils.getTitleView(props.title, I18n.t('default.myBusinessNot'))}

                        {contentView}

                    </View>

                </ScrollView>


                <AlertDialog
                    contentTv={I18n.t('b.oppsStartNewBusiness')}
                    singleBtnOnclick={() => {
                        this.setState({
                            alertVisible: false
                        })
                    }}
                    visible={this.state.alertVisible}
                    requestClose={() => {
                        this.setState({
                            alertVisible: false
                        })
                    }}/>

                <BaseModal
                    requestClose={() => {
                        this.setState({baseModalVisible: true})
                    }
                    }
                    visible={state.baseModalVisible}>
                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={() => {
                            this.setState({baseModalVisible: false,})
                        }}
                        style={{flex: 1}}>
                        <IparImageView
                            width={"100%"}
                            height={'100%'}
                            url={state.cardImageUrl}
                            httpUrlNotShow={true}
                            errorImage={require('../../../res/images/ic_image_fail.png')}/>

                        <TouchableOpacity
                            onPress={() => {
                                this.setState({baseModalVisible: false}, CommonUtils.downloadImage(state.cardImageUrl))
                            }}
                            style={[{
                                position: 'absolute',
                                bottom: 16,
                                right: 16,
                                width: 40,
                                padding: 4,
                                height: 40,
                                borderWidth: 1,
                            }]}>
                            <Image
                                style={{tintColor: 'black', width: 30, height: 30}}
                                source={require('../../../res/images/ic_download.png')}/>

                        </TouchableOpacity>

                    </TouchableOpacity>
                </BaseModal>

            </View>
        );
    }


    renderStasView(title, msj, value, isPlus) {
        return <TouchableOpacity
            activeOpacity={0.5}
            style={[StyleUtils.tvBox, StyleUtils.justifySpace,]}
        >
            <View style={StyleUtils.flex}>
                <Text
                    numberOfLines={2}
                    style={[StyleUtils.defaultTv, StyleUtils.smallFont, {textAlign: 'left',}]}>{title}</Text>
                <Text
                    numberOfLines={2}
                    style={[StyleUtils.netTv, StyleUtils.smallFont, {textAlign: 'left',}]}>{msj}</Text>
            </View>

            <View style={[{
                backgroundColor: isPlus ? 'red' : 'black',
                borderRadius: 4,
                paddingHorizontal: 8
            }]}>
                <Text
                    numberOfLines={2}
                    style={[{color: 'white'}]}>{value}</Text>
            </View>
        </TouchableOpacity>
    }


}

const styles = StyleSheet.create({

    topTitleTv: {
        fontSize: 18,
        color: 'black',
        marginBottom: 10,
        marginTop: 20,
        textAlign: 'left'
    },

    topBtnStyle: {
        backgroundColor: 'white',
        borderWidth: 1,
        height: 42,
        borderColor: '#e7e7e7',
        marginBottom: 28,
    },
});
