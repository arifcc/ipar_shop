import React, {Component} from 'react'


import {DeviceEventEmitter, Image, ListView, ScrollView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import StyleUtils from "../../../res/styles/StyleUtils";
import Navigation from "../../../custom/Navigation";
import ViewUtils from "../../../utils/ViewUtils";
import IparNetwork from "../../../httpUtils/IparNetwork";
import I18n from "../../../res/language/i18n";


export default class U_2ProgramContentPage extends Component {

    constructor(props) {
        super(props);
        this.iparNetwork = new IparNetwork();
        this.country = '';

        this.planNamesData = [
            {name: 'Sales commission', color: 'black', value: 0},
            {name: 'Development award', color: '#626262', value: 0},
            {name: 'Weekly award', color: '#A0A0A0', value: 0},
            {name: 'Leadership award', color: '#D2D2D2', value: 0},
            {name: 'Weekly award', color: '#A0A0A0', value: 0},
        ];


        this.planNamesBottomData = [
            {name: 'Qualification date', color: 'black', value: '19 days'},
            {name: 'Leadership bonus date', color: '#626262', value: 'in 5 days'},
            {name: 'Number of active months in row', color: '#A0A0A0', value: '3 weeks'},
        ];

        this.planNamesCenterData = [
            {name: 'Qualification date', color: 'black', value: '19 days'},
            {name: 'Leadership bonus date', color: '#626262', value: 'in 5 days'},
            {name: 'Number of active months in row', color: '#A0A0A0', value: '3 weeks'},
            {name: 'Auto order', color: '#A0A0A0', value: '3 weeks'},
        ];


        this.state = {
            plansData: [],
            planPerformanceValue: 3123,
        }

    }

    componentWillUnmount() {
        this.iparNetwork = null;
        this.country = null;
        this.planNamesData = null;
        this.planNamesBottomData = null;
        this.planNamesCenterData = null;
    }


    componentDidMount() {

        // CommonUtils.getAcyncInfo('country_iso')
        // 	.then((country) => {
        // 		this.country = country;
        // 	})
    }


    renderPlanLable(rowData) {

        return (<View style={[StyleUtils.justifySpace, {marginBottom: 22}]}>
            <View style={{width: 22, height: 22, backgroundColor: rowData.color}}/>
            <Text style={styles.planLable}>{rowData.name}</Text>
            <Text style={{fontSize: 16, color: 'black'}}>{rowData.value}</Text>
        </View>)
    }


    renderMenuTitle() {
        return (<View style={[StyleUtils.center, styles.menuTitle]}>
            <Text
                numberOfLines={2}
                style={{color: 'white', fontSize: 10, textAlign: 'center', paddingLeft: 4, paddingRight: 4}}>Business
                center</Text>
        </View>)
    }

    renderMenuViews() {
        return <View style={{width: 80, flex: 1, backgroundColor: 'black'}}>

            <Text style={{color: 'white'}}>asfs</Text>

        </View>
    }

    render() {
        return (<View style={StyleUtils.flex}>

            <Navigation
                titleImage={require('../../../res/images/ic_ipar_logo.png')}
                onClickLeftBtn={() => this.props.navigator.pop()}/>

            <ScrollView>

                <View style={{padding: 20}}>

                    {ViewUtils.getTitleView('U-2 program', '80253025-001')}


                    <TouchableOpacity
                        style={[StyleUtils.justifySpace, {backgroundColor: 'black', padding: 16}]}>

                        <Text style={{color: 'white'}}>80253025-001</Text>

                        <View style={[StyleUtils.rowDirection, StyleUtils.center]}>
                            <Text style={{color: 'white', paddingRight: 8}}>business info</Text>
                            <Image
                                style={{width: 12, height: 12, tintColor: 'white'}}
                                source={require('../../../res/images/ic_right_more.png')}/>
                        </View>

                    </TouchableOpacity>


                    <Text style={styles.titleText}>
                        Career prospeclive
                    </Text>

                    <View style={[StyleUtils.justifySpace, StyleUtils.center, {paddingTop: 26, paddingBottom: 22}]}>

                        <View style={[{
                            borderColor: '#c6c6c6',
                            borderRightWidth: 1,
                            flex: 1,
                            height: 80
                        }, StyleUtils.center]}>
                            <Text style={{textAlign: 'center', fontSize: 12, color: '#909090'}}>PV bank{'\n'}<Text
                                style={[styles.titleText]}>180 PV</Text></Text>
                        </View>
                        <View style={[{
                            borderColor: '#c6c6c6',
                            borderLeftWidth: 1,
                            flex: 1,
                            height: 80
                        }, StyleUtils.center]}>
                            <Text style={{textAlign: 'center', fontSize: 12, color: '#909090'}}>PV bank{'\n'}<Text
                                style={[styles.titleText]}>180 PV</Text></Text>
                        </View>

                    </View>


                    <Text style={styles.titleText}>
                        Tree view
                    </Text>

                    <View style={[StyleUtils.center, {paddingTop: 26, paddingBottom: 26,}]}>

                        <Text style={StyleUtils.text}>My planPerformanceValue</Text>

                        <View style={[StyleUtils.rowDirection, StyleUtils.center]}>

                            <Image
                                style={{width: 56, height: 56}}
                                source={require('../../../res/images/ic_class.png')}
                            />

                            <Text style={[StyleUtils.title, {fontSize: 20}]}>+ 16 IBM</Text>

                        </View>

                    </View>


                    {ViewUtils.getButton(I18n.t('b.myPerformance'), null, styles.topBtnStyle, {color: 'black'})}


                    {this.planNamesData.map((item, i) => {
                        return this.renderPlanLable(item);
                    })}


                    {ViewUtils.getButton(I18n.t('b.myPerformance'), null, [styles.topBtnStyle, StyleUtils.marginTop], {color: 'black'})}

                    {this.planNamesCenterData.map((item, i) => {
                        return this.renderPlanLable(item);
                    })}

                    {ViewUtils.getButton(I18n.t('b.myPerformance'), null, [styles.topBtnStyle, StyleUtils.marginTop], {color: 'black'})}

                    {this.planNamesBottomData.map((item, i) => {
                        return this.renderPlanLable(item);
                    })}
                </View>


            </ScrollView>



        </View>)
    }


}

const styles = StyleSheet.create({

    topBtnStyle: {
        backgroundColor: 'white',
        borderWidth: 1,
        height: 42,
        borderColor: '#e7e7e7',
        marginBottom: 28,
    },

    planLable: {
        borderColor: 'black',
        flex: 1,
        paddingLeft: 8,
        textAlign: 'left'
    },
    reentryViewStyle: {
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: '#e7e7e7',
        marginTop: 32,
        paddingTop: 16,
        paddingBottom: 16,

    },

    circularText: {
        color: 'white',
        textAlign: 'center',
    },

    menuTitle: {
        width: 88,
        height: 42,
        backgroundColor: 'black'
    },

    titleText: {
        marginTop: 16,
        fontSize: 18,
        color: 'black',
    },
});