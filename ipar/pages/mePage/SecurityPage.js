/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TouchableOpacity
} from 'react-native';

import Navigation from "../../custom/Navigation";
import StyleUtils from "../../res/styles/StyleUtils";
import ViewUtils from "../../utils/ViewUtils";
import CommonUtils from "../../common/CommonUtils";
import UserEmailEdit from "./userInfo/UserEmailEdit";
import I18n from "../../res/language/i18n";


export default class SecurityPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userEmail: '',
            phoneNumber: '',
        }
    }

    componentDidMount() {
        CommonUtils.getAcyncInfo('userInfo')
            .then((result) => {
                let parse = JSON.parse(result);
                this.setState({userEmail: parse.email, phoneNumber: parse.mobile});
            });
    }

    render() {


        return (
            <View style={StyleUtils.flex}>
                <Navigation
                    onClickLeftBtn={() => {
                        this.props.navigator.pop()
                    }}
                    leftButtonIcon={require('../../res/images/ic_back.png')}
                    titleImage={require('../../res/images/ic_ipar_logo.png')}

                />
                <ScrollView>
                    <View style={[StyleUtils.flex, {padding: 16}]}>
                        {ViewUtils.getTitleView(I18n.t('default.security'), I18n.t('default.changeYourCredentials'))}
                        {/*{ViewUtils.gitUserInformationTv('Phone number:', '+86131******32')}*/}

                        <Text
                            style={[styles.title, StyleUtils.smallFont]}>{I18n.t('default.phoneNumber')}</Text>
                        <Text
                            style={{
                                marginTop: 16, textAlign: 'left'
                                ,
                            }}>{new CommonUtils().phoneEncryption(this.state.phoneNumber)}</Text>


                        <Text style={[styles.title, StyleUtils.smallFont]}>{I18n.t('default.emailAddress')}</Text>
                        <Text style={[{
                            marginTop: 16, textAlign: 'left'
                        }, StyleUtils.smallFont]}>{this.state.userEmail}</Text>
                        {ViewUtils.getEditBtn(() => {
                            this.props.navigator.push({
                                component: UserEmailEdit,
                                params: {
                                    email: this.state.userEmail ? this.state.userEmail : I18n.t('default.interEmail'),
                                    mobile: this.state.phoneNumber,
                                    type: 0,
                                }
                            })
                        })}
                        <Text style={[styles.title, StyleUtils.smallFont]}>{I18n.t('default.password')}</Text>
                        <Text style={[{
                            marginTop: 16, textAlign: 'left'
                        }, StyleUtils.smallFont]}>*******</Text>
                        {ViewUtils.getEditBtn(() => {
                            this.props.navigator.push({
                                component: UserEmailEdit,
                                params: {
                                    mobile: this.state.phoneNumber,
                                    type: 1,
                                }
                            })
                        })}
                        <Text style={[{marginTop: 26}, StyleUtils.smallFont]}>{I18n.t('default.change')}</Text>
                        <Text style={[StyleUtils.smallFont, styles.title]}>{I18n.t('default.socialMediaAccount')}</Text>
                        {/*<Text style={[StyleUtils.smallFont, {marginTop: 10}]}>Wechat</Text>*/}
                        {/*<Text style={[StyleUtils.smallFont, {marginTop: 8}]}>Facebook</Text>*/}
                        {/*<Text style={[StyleUtils.smallFont, {marginTop: 14}]}>Add new</Text>*/}


                    </View>
                </ScrollView>

            </View>)


    }

}

const styles = StyleSheet.create({
    title: {
        marginTop: 16,
        fontSize: 18,
        color: 'black',
        textAlign: 'left'
    }
});

