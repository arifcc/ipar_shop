/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {Image, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

import Navigation from "../../custom/Navigation";
import StyleUtils from "../../res/styles/StyleUtils";
import ViewUtils from "../../utils/ViewUtils";
import IparNetwork, {SERVER_TYPE} from "../../httpUtils/IparNetwork";
import CommonUtils from "../../common/CommonUtils";
import I18n from "../../res/language/i18n";

export default class FavouritesPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user_id: '',
            voucherData: ''
        };

        this.iparNetwork = new IparNetwork()
    }

    componentWillUnmount() {
        this.iparNetwork = null;
    }

    componentDidMount() {
        CommonUtils.showLoading();

        CommonUtils.getAcyncInfo('userInfo')
            .then((result) => {
                this.setState({
                    userId: JSON.parse(result).user_id,
                });
                this.getUsersVoucherInfo();
            });


    }

    /*获取用户优惠卡详细信息表*/
    getUsersVoucherInfo() {
        this.iparNetwork.getRequest(SERVER_TYPE.admin + 'getUsersVoucher?', '&user_id=' + this.state.userId)
            .then((result) => {
                CommonUtils.dismissLoading();

                if (result.code === 200) {
                    this.setState({voucherData: result.data})
                }
            })
            .catch((error) => {
                CommonUtils.dismissLoading();

                console.log(error);
            });


    }

    render() {
        return (
            <View style={StyleUtils.flex}>
                <Navigation
                    onClickLeftBtn={() => {
                        this.props.navigator.pop()
                    }}
                    leftButtonIcon={require('../../res/images/ic_back.png')}
                    titleImage={require('../../res/images/ic_ipar_logo.png')}

                />

                <ScrollView>
                    <View style={[StyleUtils.flex, {padding: 16}]}>
                        {ViewUtils.getTitleView(I18n.t('default.favourites'), I18n.t('default.allDiscount'))}

                        <View style={StyleUtils.lineStyle}/>

                        {ViewUtils.gitUserInformationTv(I18n.t('default.bonusPoints'), I18n.t('default.youCanApplyIBM'), '', () => {
                            // this.props.navigator.push({component: DiscountPage})
                        }, I18n.t('default.more'))}

                        {ViewUtils.gitUserInformationTv(I18n.t('default.bonusCards'), I18n.t('default.youCanApplyIBM'), '', () => {
                            // this.props.navigator.push({
                            // 	component: PointsPage, params: {
                            // 		voucherData: this.state.voucherData
                            // 	}
                            // })
                        }, I18n.t('default.more'))}

                        {ViewUtils.gitUserInformationTv(I18n.t('default.giftCard'), I18n.t('default.youCanTransfer'), '', () => {
                            // this.props.navigator.push({
                            // 	component: GifCardPage,
                            // 	params: {
                            // 		voucherData: this.state.voucherData
                            // 	}
                            // })
                        }, I18n.t('default.more'))}

                        {ViewUtils.gitUserInformationTv(I18n.t('default.coupons'), I18n.t('default.youCanApplyTo'), '', () => {
                            // this.props.navigator.push({
                            // 	component: CouponsPage,
                            // 	params: {
                            // 		voucherData: this.state.voucherData
                            // 	}
                            // })
                        }, I18n.t('default.more'))}


                    </View>
                </ScrollView>

            </View>)


    }

}

const styles = StyleSheet.create({});

