/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    DeviceEventEmitter,
    Image,
    ListView,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TouchableHighlight,
    View
} from 'react-native';
import StyleUtils from "../../../res/styles/StyleUtils";
import Navigation from "../../../custom/Navigation";
import ModalListUtils from "../../../utils/ModalListUtils";
import ViewUtils from "../../../utils/ViewUtils";
import IparNetwork, {SERVER_TYPE} from "../../../httpUtils/IparNetwork";
import CommonUtils from "../../../common/CommonUtils";
import I18n from "../../../res/language/i18n";
import IparInput from "../../../custom/IparInput";
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'


const checkedImage = require('../../../res/images/ic_check_box.png');
const checkImage = require('../../../res/images/ic_check_box_outline_blank.png');
let countryData = [];
let provinceData = [];
let cityData = [];
let districtData = [];
export default class EditAddress extends Component {
    constructor(props) {
        super(props);
        let editData = this.props.editData;
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        countryData = [];
        provinceData = [];
        cityData = [];
        districtData = [];
        this.state = {
            countrySource: ds,
            provinceSource: ds,
            citySource: ds,
            districtSource: ds,
            modalDialog: false,
            isDateTimePickerVisible: false,
            selectCountry: editData.country ? editData.country : I18n.t('default.selectCountry'),
            selectProvince: editData.region1 ? editData.region1 : I18n.t('default.selectProvince'),
            selectCity: editData.region2 ? editData.region2 : I18n.t('default.selectCity'),
            selectDistrictCity: editData.region3 ? editData.region3 : I18n.t('default.selectDistrict'),
            replaceData: false,
            modalDataType: 'country',
            streetNumber: '',
            buildingName: '',
            statusCode: undefined,
            postcode: editData.postCode ? editData.postCode : I18n.t('default.postNumber'),
            countryId: '',
            userId: '',
            userName: '',
            phoneNumber: '',
            isChecked: this.props.isChecked || true

        };

        this.iparNetwork = new IparNetwork()
    }


    componentWillUnmount() {
        this.iparNetwork = null;

    }

    componentDidMount() {
        CommonUtils.showLoading();


        /*获取本地数据*/
        CommonUtils.getAcyncInfo('userInfo').then((result) => {
            this.setState({userId: JSON.parse(result).main_user_id});
            this.getAddressInfo();
        });

    }

    /*获取用户收货地址信息姐*/
    getAddressInfo() {
        this.iparNetwork.getRequest(SERVER_TYPE.logistics + 'region?')
            .then((result) => {
                countryData = result;
                this.setState({
                    countrySource: this.state.countrySource.cloneWithRows(result)
                });
                CommonUtils.dismissLoading();

            })
            .catch((error) => {
                CommonUtils.dismissLoading();

                console.log(error);
            })
    }


    /*获取用户收货地址信息姐*/
    getProvinceInfo(level, selectCountry) {

        CommonUtils.showLoading();

        this.iparNetwork.getRequest(SERVER_TYPE.logistics + 'region?', 'level=' + level + '&parent=' + selectCountry)
            .then((result) => {
                CommonUtils.dismissLoading();

                if (level === 1) {
                    provinceData = result;
                    this.setState({
                        provinceSource: this.state.provinceSource.cloneWithRows(result)
                    });


                } else if (level === 2) {
                    cityData = result;
                    if (result.length > 0) {
                        this.setState({
                            citySource: this.state.citySource.cloneWithRows(result),
                            cityType: true,
                        });
                    } else {
                        this.setState({cityType: false,});//cityCode: 'code'
                    }
                } else {
                    districtData = result;
                    if (result.length > 0) {
                        this.setState({
                            districtSource: this.state.districtSource.cloneWithRows(result),
                            districtType: true
                        });
                    } else {
                        this.setState({
                            districtType: false
                        });
                    }

                }

            })
            .catch((error) => {
                CommonUtils.dismissLoading();

                console.log(error)
            })
    }


    /*修改地址接口*/
    updateAddress() {
        CommonUtils.showLoading();
        let editData = this.props.editData;
        let formData = new FormData();
        formData.append("userId", this.state.userId);
        formData.append("addressId", this.state.countryId ? this.state.countryId : editData.addressId);
        formData.append("defaultAddress", this.getChecked() === false ? 0 : 1);
        formData.append("streetName", this.state.streetNumber ? this.state.streetNumber : editData.streetName);
        formData.append("building", this.state.buildingName ? this.state.buildingName : editData.building);
        formData.append("postCode", this.state.postcode);
        formData.append("userName", this.state.goodsUserName ? this.state.goodsUserName : editData.userName);
        formData.append("mobile", this.state.goodsUserMobile ? this.state.goodsUserMobile : editData.mobile);
        formData.append("id", editData.id);
        this.iparNetwork.getPostFrom(SERVER_TYPE.logistics + 'useraddress/update', formData)
            .then((result) => {
                CommonUtils.dismissLoading();
                if (result.code === 200) {
                    this.props.navigator.pop();
                    this.props.callback('full');
                    DeviceEventEmitter.emit('toast', I18n.t('default.success'))
                }
            })
            .catch((error) => {
                console.log(error);
                CommonUtils.dismissLoading();
            })
    }

    /**
     * dialog item 点击
     * */
    itemClickListener(index, text) {

        switch (this.state.modalDataType) {
            case 'country':
                this.getProvinceInfo(1, countryData[index].name);
                this.setState({
                    selectCountry: text,
                    modalDialog: false,
                    replaceData: true,
                    postcode: countryData[index].postcode ? countryData[index].postcode : '',
                    countryId: countryData[index].id,
                    cityType: false,
                    districtType: false,
                    selectProvince: I18n.t('default.selectCountry'),
                    selectCity: I18n.t('default.selectProvince'),
                    selectDistrictCity: I18n.t('default.selectDistrict'),
                });
                break;
            case 'region1':
                this.getProvinceInfo(2, provinceData[index].name);
                this.setState({
                    selectProvince: text,
                    modalDialog: false,
                    postcode: provinceData[index].postcode ? provinceData[index].postcode : '',
                    countryId: provinceData[index].id,
                    cityType: false,
                    districtType: false,
                    selectCity: I18n.t('default.selectProvince'),
                    selectDistrictCity: I18n.t('default.selectDistrict'),
                });
                break;
            case 'region2':
                this.getProvinceInfo(3, cityData[index].name);
                this.setState({
                    selectCity: text,
                    modalDialog: false,
                    postcode: cityData[index].postcode ? cityData[index].postcode : '',
                    countryId: cityData[index].id,
                    districtType: false,
                    selectDistrictCity: I18n.t('default.selectDistrict'),
                });
                break;
            case 'region3':
                this.getProvinceInfo(4, districtData[index].name);
                this.setState({
                    selectDistrictCity: text,
                    modalDialog: false,
                    postcode: districtData[index].postcode ? districtData[index].postcode : '',
                    countryId: districtData[index].id,
                });
                break;


        }


    }

    /*showDialog*/
    showDialog(type) {
        switch (type) {
            case 0:
                this.setState({
                    modalDialog: true,
                    modalDataType: 'country'
                });
                break;
            case 1:
                if (provinceData.length > 0) {
                    this.setState({
                        modalDialog: true,
                        modalDataType: 'region1'
                    });
                }
                break;
            case 2:
                if (cityData.length > 0) {
                    this.setState({
                        modalDialog: true,
                        modalDataType: 'region2'
                    });
                }
                break;
            case 3:
                if (districtData.length > 0) {
                    this.setState({
                        modalDialog: true,
                        modalDataType: 'region3'
                    });
                }
                break;

        }


    }

    getChecked() {
        return this.state.isChecked;
    }

    /*isChecked*/
    checkClick() {
        this.setState({
            isChecked: !this.state.isChecked
        });
    }

    render() {
        let editData = this.props.editData;

        let _dataSource = this.state.countrySource;
        switch (this.state.modalDataType) {
            case 'region1':
                _dataSource = this.state.provinceSource;
                break;
            case 'region2':
                _dataSource = this.state.citySource;
                break;
            case 'region3':
                _dataSource = this.state.districtSource;
                break
        }

        let isShowCity = this.state.replaceData ? cityData && cityData.length > 0 : true;
        let isShowDistrict = this.state.replaceData ? districtData && districtData.length > 0 : true;

        return (
            <View style={[StyleUtils.flex, {backgroundColor: 'white'}]}>
                <Navigation
                    onClickLeftBtn={() => {
                        this.props.navigator.pop()
                    }}
                    leftButtonIcon={require('../../../res/images/ic_back.png')}
                    titleImage={require('../../../res/images/ic_ipar_logo.png')}
                />
                <ModalListUtils
                    close={() => {
                        this.setState({
                            modalDialog: false
                        })
                    }}
                    onPress={(index, text) => this.itemClickListener(index, text)}
                    visible={this.state.modalDialog}
                    dataSource={_dataSource}
                    dataParam={this.state.modalDataType}
                />


                <KeyboardAwareScrollView style={StyleUtils.flex}>
                    <View style={{padding: 16}}>
                        {ViewUtils.getTitleView(I18n.t('default.editAddressTitle'), I18n.t('default.editAddressMInContent'))}

                        {ViewUtils.getButtunMenu(this.state.selectCountry, () => this.showDialog(0))}
                        {ViewUtils.getButtunMenu(this.state.selectProvince, () => this.showDialog(1), {marginTop: 16})}
                        {isShowCity && ViewUtils.getButtunMenu(this.state.selectCity, () => this.showDialog(2), {marginTop: 16})}
                        {isShowDistrict && ViewUtils.getButtunMenu(this.state.selectDistrictCity, () => this.showDialog(3), {marginTop: 16})}

                        <Text style={[styles.defaultTv, StyleUtils.smallFont]}>{I18n.t('default.streetNumber')}</Text>

                        <IparInput
                            onChangeText={(value) => {
                                this.setState({streetNumber: value})
                            }}
                            placeholder={this.state.streetNumber ? this.state.streetNumber : editData.streetName ? editData.streetName : I18n.t('default.streetNumber')}/>


                        <Text style={[styles.defaultTv, StyleUtils.smallFont]}>{I18n.t('default.buildingName')}</Text>

                        <IparInput
                            onChangeText={(value) => {
                                this.setState({buildingName: value})
                            }}
                            placeholder={this.state.buildingName ? this.state.buildingName : editData.building ? editData.building : I18n.t('default.buildingName')}/>


                        <Text style={[styles.defaultTv, StyleUtils.smallFont]}>{I18n.t('default.postNumber')}</Text>

                        <IparInput
                            onChangeText={(value) => {
                                this.setState({postcode: value})
                            }}
                            placeholder={this.state.postcode ? this.state.postcode : I18n.t('default.postNumber')}/>

                        <IparInput
                            onChangeText={(value) => {
                                this.setState({userName: value})
                            }}
                            placeholder={editData.userName ? editData.userName : I18n.t('default.goodsUserName')}/>

                        <IparInput
                            keyboardType={'number-pad'}
                            onChangeText={(value) => {
                                this.setState({phoneNumber: value})
                            }}
                            placeholder={editData.mobile ? editData.mobile : I18n.t('default.goodsUserMobile')}/>


                        <View style={[StyleUtils.rowDirection, {alignItems: 'center'}]}>
                            <TouchableHighlight underlayColor={'transparent'} onPress={() => this.checkClick()}>
                                <Image
                                    source={this.state.isChecked ? checkedImage : checkImage}
                                    style={styles.checkImage}/>
                            </TouchableHighlight>
                            <Text
                                style={[styles.checkBoxTv, StyleUtils.smallFont]}>{this.state.isChecked ? I18n.t('default.defaultTv') : I18n.t('default.setDefault')}</Text>
                        </View>

                        {ViewUtils.getButton(I18n.t('default.submit'), () => {
                            this.updateAddress();
                        }, {marginTop: 50})}
                    </View>

                </KeyboardAwareScrollView>
            </View>
        );
    }


}

const styles = StyleSheet.create({
    defaultTv: {
        marginTop: 16,
        textAlign: 'left'

    },
    checkImage: {
        marginTop: 16,
        height: 20,
        width: 20,
    },
    checkBoxTv: {
        marginTop: 12,
        color: 'black',
        marginLeft: 8
    }
});
