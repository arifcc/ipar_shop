/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {ListView, ScrollView, StyleSheet, Text, View} from 'react-native';

import Navigation from "../../../custom/Navigation";
import StyleUtils from "../../../res/styles/StyleUtils";
import ViewUtils from "../../../utils/ViewUtils";
import IparNetwork, {getSynPaysSellerAuth, SERVER_TYPE} from "../../../httpUtils/IparNetwork";
import CommonUtils from "../../../common/CommonUtils";
import I18n from "../../../res/language/i18n";

export default class WalletDetail extends Component {

    constructor(props) {
        super(props);
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows([]),
            userId: '',
            country: null,
        };
        this.iparNetwork = new IparNetwork()
    }

    componentWillUnmount() {
        this.iparNetwork = null;
    }

    componentDidMount() {

        CommonUtils.showLoading();

        /*获取本地数据*/
        CommonUtils.getAcyncInfo('userInfo')
            .then((result) => {
                this.setState({
                    userId: JSON.parse(result).main_user_id,
                    country: JSON.parse(result).country,
                });
                this.getTransactionsInfo();

            });
    }

    /*获取借记卡*/
    getTransactionsInfo() {
        this.iparNetwork.getRequest(SERVER_TYPE.synPays + 'Transactions?', '&UserID=')
            .then((result) => {
                CommonUtils.dismissLoading();

                if (result.ApiResponseCode === 200) {
                    this.setState({dataSource: this.state.dataSource.cloneWithRows(result.data)})
                }
            })
            .catch((error) => {
                CommonUtils.dismissLoading();

                console.log(error);
            });
    }

    render() {
        return (
            <View style={StyleUtils.flex}>
                <Navigation
                    onClickLeftBtn={() => {
                        this.props.navigator.pop()
                    }}
                    leftButtonIcon={require('../../../res/images/ic_back.png')}
                    titleImage={require('../../../res/images/ic_ipar_logo.png')}
                />
                <ScrollView>
                    <View style={[StyleUtils.flex, {padding: 16}]}>
                        {ViewUtils.getTitleView(I18n.t('default.wallet'), I18n.t('default.transactionDetails'))}

                        <View style={StyleUtils.lineStyle}/>


                        {/*默认显示tabText*/}
                        <View>{ViewUtils.getDefaultTab([I18n.t('default.date'), I18n.t('default.amount'), I18n.t('default.status'), I18n.t('default.type')], '', '', {height: 50}, StyleUtils.defaultTabTv)}</View>

                        <ListView
                            renderRow={this.renderRow.bind(this)}
                            dataSource={this.state.dataSource}/>


                    </View>
                </ScrollView>

            </View>)
    }

    renderRow(rowData, sectionID, rowID) {
        return (<View>
            {ViewUtils.getDefaultTab([rowData.TransactionTime, rowData.Amount + this.props.getPv, rowData.TransactionStatus, rowData.TransactionType], () => {
            })}
        </View>)
    }

}

const styles = StyleSheet.create({});

