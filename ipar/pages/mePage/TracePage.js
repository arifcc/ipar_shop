/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {Image, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';


import StyleUtils from "../../res/styles/StyleUtils";
import Navigation from "../../custom/Navigation";
import ViewUtils from "../../utils/ViewUtils";
import IparNetwork from "../../httpUtils/IparNetwork";
import I18n from "../../res/language/i18n";
import CommonUtils from "../../common/CommonUtils";

export default class TracePage extends Component<Props> {

	constructor(props) {
		super(props);
		this.state = {
		};
		this.iparNetwork = new IparNetwork()
	}


	componentWillUnmount() {
		this.iparNetwork = null;
	}

	componentDidMount() {
        CommonUtils.showLoading();

        this.getLogisticsInfo();
	}


	/*
	*网络请求
	*
	* 获取物流信息
	*/
	getLogisticsInfo() {
		orderStatus = [];
		this.iparNetwork.getRequest('http://goods.admin.ipar.mm/api/stockOrder/pageData?', 'order_id=' + this.props.orderId)
			.then((result) => {
                CommonUtils.dismissLoading();

                if (result.code === 200) {

					//this.setState({dataSource: ds.cloneWithRows(result.data)});

				}
			})
			.catch((error) => {
                CommonUtils.dismissLoading();

                console.log(error)
			})
	}


	/*地址信息Text*/
	getUserTv(addres) {
		let arrTv = [];
		let split = addres.split('-');
		for (let i = 0; i < split.length; i++) {
			if (split[i] !== '') {
				arrTv.push(
					<Text style={[{marginTop: 10},StyleUtils.smallFont]}>{split[i]}</Text>
				)
			}
		}
		return arrTv
	}


	render() {
		return (
			<View style={StyleUtils.flex}>
				<Navigation
					onClickLeftBtn={() => {
						this.props.navigator.pop()
					}}
					leftButtonIcon={require('../../res/images/ic_back.png')}
					titleImage={require('../../res/images/ic_ipar_logo.png')}
				/>
				<ScrollView>
					<View style={[StyleUtils.flex, {padding: 16}]}>
						{ViewUtils.getTitleView(I18n.t('default.traceOrder'), I18n.t('default.latestShipping'))}

						<View style={StyleUtils.lineStyle}/>

						{ViewUtils.gitUserInformationTv(I18n.t('default.orderNo'), '')}
						{ViewUtils.gitUserInformationTv(I18n.t('default.shippingStatus'), '')}
						{ViewUtils.gitUserInformationTv(I18n.t('default.carrier'), '')}
						{ViewUtils.gitUserInformationTv(I18n.t('default.waybillNo'), '')}
						{ViewUtils.gitUserInformationTv(I18n.t('default.trace'), '')}


						{/*Deliver Details*/}
						<Text style={[styles.defaultTv,StyleUtils.smallFont]}>{I18n.t('default.deliverAddress')}</Text>

						<Text style={[{marginTop: 16},StyleUtils.smallFont]}>fdgfd</Text>
						{/*{this.getUserTv([32])}*/}


					</View>
				</ScrollView>
			</View>
		);
	}
}
const styles = StyleSheet.create({

	goodsIcon: {
		width: 62,
		height: 62,
		backgroundColor: '#f4f4f4',
		resizeMode: 'contain'
	},
	goodsText: {
		flex: 1,
		color: '#333333',
		paddingRight: 8
	},

	itemBox: {
		height: 80,
		borderBottomWidth: 1,
		borderBottomColor: '#eeeeee',
	},
	applyTv: {
		width: 60,

		height: 30,
		justifyContent: 'center',
		backgroundColor: '#e1e2e3',
		alignItems: 'center'

	},
	defaultTv: {
		marginTop: 16,
		fontSize: 18,
		color: 'black',
        textAlign:'left'

    },

});