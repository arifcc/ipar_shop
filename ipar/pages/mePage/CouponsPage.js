/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    TouchableOpacity,
    DeviceEventEmitter,
    ListView
} from 'react-native';


import StyleUtils from "../../res/styles/StyleUtils";
import Navigation from "../../custom/Navigation";
import ViewUtils from "../../utils/ViewUtils";
import IparNetwork, {IMAGE_URL, SERVER_TYPE} from "../../httpUtils/IparNetwork";
import CommonUtils from "../../common/CommonUtils";
import I18n from "../../res/language/i18n";
import EditAddress from "./address/EditAddress";
import IparImageView from "../../custom/IparImageView";

let voucherArr = [];
export default class CouponsPage extends Component {

    constructor(props) {
        super(props);
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        voucherArr = [];
        this.state = {
            dataSource: ds,
            noData: false,
        }

    }

    componentDidMount() {
        this.showLoading(true);

        this.onLoad();
    }

    /**
     * 获取GifCard 数据
     */
    onLoad() {

        CommonUtils.getAcyncInfo('userInfo')
            .then((result) => {
                new IparNetwork().getRequest(SERVER_TYPE.admin + 'getUsersVoucher?',
                    'user_id=' + JSON.parse(result).user_id + '&type=0'
                    + '&status=0')
                    .then((result) => {
                        if (result.code === 200) {
                            this.setState({
                                dataSource: this.state.dataSource.cloneWithRows(result.data),
                            })
                            if (result.data.length <= 0) {
                                this.setState({noData: true})
                            }
                        } else {
                            this.setState({
                                noData: true,
                            })

                        }
                        this.showLoading(false);

                    })
                    .catch((error) => {
                        DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
                        console.log('GifCardPage--onLoad--error:' + error);
                        this.showLoading(false);
                        this.setState({noData: true})
                    })
            });


    }

    /**
     * 加载中--效果
     * @param visible
     */
    showLoading(visible) {
        if (visible) {
            CommonUtils.showLoading();
        } else {
            CommonUtils.dismissLoading();
        }
    }


    /**
     * listView 的 item 点击方法
     */
    onItemClick(rowData) {
        if (this.props.callback) {
            if (this.props.goods_amount >= rowData.limit) {
                this.props.callback(rowData);
                this.props.navigator.pop();
            } else {
                DeviceEventEmitter.emit('toast', I18n.t('default.orderAmountNotEnough'))
            }
        }
    }


    render() {


        return (
            <View style={StyleUtils.flex}>
                <Navigation
                    onClickLeftBtn={() => {
                        this.props.navigator.pop()
                    }}
                />
                <ScrollView>
                    <View style={{padding: 16}}>
                        {ViewUtils.getTitleView(I18n.t('default.coupons'), I18n.t('default.ordersReduce'))}

                        <View style={StyleUtils.lineStyle}/>
                        {this.state.noData ? <Text
                            style={[StyleUtils.noData, StyleUtils.smallFont]}>{I18n.t('default.noData')}</Text> : false}
                        <ListView
                            dataSource={this.state.dataSource}
                            renderRow={this.renderRow.bind(this)}
                        />
                    </View>
                </ScrollView>

            </View>
        );
    }

    /*list renderRow*/
    renderRow(rowData, sectionID, rowID) {
        return (
            <TouchableOpacity
                onPress={() => this.onItemClick(rowData)}
                style={[StyleUtils.rowDirection, StyleUtils.center, styles.itemBox]}>

                <IparImageView
                    style={{backgroundColor: '#f4f4f4'}}
                    width={62}
                    height={62}
                    url={rowData.img}/>

                <View style={[StyleUtils.flex, styles.tvBox]}>
                    <Text
                        numberOfLines={1}
                        style={[styles.goodsText, StyleUtils.smallFont, {fontSize: 16}]}>
                        {rowData.title}
                    </Text>
                    <Text
                        numberOfLines={1}
                        style={[styles.goodsText, StyleUtils.smallFont]}>
                        {rowData.status === 0 ? rowData.end_time : rowData.update_time}
                    </Text>
                </View>

                <View style={[styles.applyTv]}>
                    <Text
                        style={[{color: rowData.status === 0 ? 'black' : 'red'}, StyleUtils.smallFont]}>{rowData.status === 0 ? I18n.t('default.apply') : I18n.t('default.expired')}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}
const styles = StyleSheet.create({

    goodsText: {
        flex: 1,
        color: '#0f0f0f',
        paddingRight: 8,
        paddingLeft: 8
    },

    itemBox: {
        height: 80,
        borderBottomWidth: 1,
        borderBottomColor: '#eeeeee',
    },
    applyTv: {
        width: 60,
        height: 30,
        justifyContent: 'center',
        backgroundColor: '#e1e2e3',
        alignItems: 'center'

    },
    tvBox: {
        marginTop: 16,
        marginBottom: 8,
    },


});