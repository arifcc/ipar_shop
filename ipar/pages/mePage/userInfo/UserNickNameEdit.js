/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {DeviceEventEmitter, ScrollView, StyleSheet, Text, View} from 'react-native';
import StyleUtils from "../../../res/styles/StyleUtils";
import Navigation from "../../../custom/Navigation";
import ViewUtils from "../../../utils/ViewUtils";
import IparNetwork, {SERVER_TYPE} from "../../../httpUtils/IparNetwork";
import I18n from "../../../res/language/i18n";
import IparInput from "../../../custom/IparInput";
import CommonUtils from "../../../common/CommonUtils";

export default class UserNickNameEdit extends Component {
    constructor(props) {
        super(props);

        this.state = {
            nickName: '',
        };

        this.iparNetwork = new IparNetwork();

    }

    componentWillUnmount() {
        this.iparNetwork = null;
    }


    /*编辑用户联系NIck name*/
    editUserInfo() {
        let userData = this.props.data;
        if (!userData) return;
        // CommonUtils.dismissLoading();

        let formData = new FormData();
        formData.append("user_id", userData.main_user_id);
        let value = this.state.nickName ? this.state.nickName : userData.nick_name ? userData.nick_name : '';
        formData.append("nick_name", value);


        this.iparNetwork.getPostFrom(SERVER_TYPE.admin + 'updateUserInfo', formData)
            .then((result) => {
                this.setState({showDialog: false});
                if (result.code === 200) {

                    let newUserInfo = userData;
                    newUserInfo.nick_name = value;

                    this.props.callback(newUserInfo);
                    DeviceEventEmitter.emit('toast', I18n.t('default.success'));
                    this.props.navigator.pop();
                }
            }).catch((error) => {
            this.setState({showDialog: false});
            console.log(error)
        })
    }


    render() {

        return (
            <View style={[StyleUtils.flex, {backgroundColor: 'white'}]}>
                <Navigation
                    onClickLeftBtn={() => {
                        this.props.navigator.pop()
                    }}
                    leftButtonIcon={require('../../../res/images/ic_back.png')}
                    titleImage={require('../../../res/images/ic_ipar_logo.png')}
                />

                <ScrollView style={StyleUtils.flex}>
                    <View style={{paddingLeft: 16, paddingRight: 16}}>
                        {ViewUtils.getTitleView(I18n.t('default.editNickName'), I18n.t('default.editNickNameMinContent'))}

                        <IparInput
                            style={{marginTop: 30}}
                            onChangeText={(name) => {
                                this.setState({nickName: name})
                            }}
                            placeholder={this.props.data.nick_name ? this.props.data.nick_name : I18n.t('toast.interNickName')}/>

                        {ViewUtils.getButton(I18n.t('default.submit'), () => {
                            // if (this.state.nickName.trim() === '') {
                            // 	DeviceEventEmitter.emit('toast', I18n.t('toast.interNickName'))
                            // } else {
                            // 	this.editUserInfo();
                            // 	this.setState({showLoading:true})
                            // }

                            this.editUserInfo();

                        }, {marginTop: 16})}

                    </View>

                </ScrollView>
            </View>
        );
    }


}
