/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    Text,
    View,
    StyleSheet,
    DeviceEventEmitter,
} from 'react-native';
import StyleUtils from "../../../res/styles/StyleUtils";
import ViewUtils from "../../../utils/ViewUtils";
import UserInformationEdit from "./UserInformationEdit";
import UserNickNameEdit from "./UserNickNameEdit";
import UserEditAddress from "./UserEditAddress";
import EditLanguage from "./EditLanguage";
import I18n from "../../../res/language/i18n";
import CommonUtils from "../../../common/CommonUtils";


export default class PersonalPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userData: [],
            nickNameData: [],
        }
    }

    /*用户信息Text*/
    getUserTv(arry) {
        let arrTv = [];
        for (let i = 0; i < arry.length; i++) {
            arrTv.push(
                <Text style={[{marginTop: 4, textAlign:'left'}, StyleUtils.smallFont]}>{arry[i]}</Text>
            )
        }
        return arrTv
    }

    /*用户语言*/
    getUserLang(languages) {
        if (!languages) return '';
        // return languages !== null ? languages.split(',') : '';
        let _result = [];
        let lang = languages.split(',');

        for (i = 0; i < lang.length; i++) {
            switch (lang[i]) {
                case 'en':
                    _result.push('English');
                    break;
                case'zh':
                    _result.push('中文');
                    break;
                case 'ru':
                    _result.push('Русский');
                    break;
                case'uy':
                    _result.push('Uyghurche');
                    break;
            }
        }

        return _result;
    }

    render() {
        let userIN;
        let gender = I18n.t('default.secrecy');


        let data = this.state.userData.length === 0 ? this.props.userInfo : this.state.userData;

        let nullText = I18n.t('default.nullValue');

        if (data !== '') {

            let firstName = '';
            let middleName = '';
            let lastName = '';
            if (data) {
                firstName = data.first_name ? data.first_name : '';
                middleName = data.middle_name ? data.middle_name : '';
                lastName = data.last_name ? data.last_name : '';


                if (data.sex === 0) {
                    gender = I18n.t('default.female')
                } else if (data.sex === 1) {
                    gender = I18n.t('default.male')
                } else {
                    gender = I18n.t('default.secrecy')
                }
            }


            userIN = <View style={{paddingBottom: 20}}>
                {/*nickName*/}
                {ViewUtils.gitUserInformationTv(I18n.t('default.nickName'), data.nick_name ? data.nick_name : nullText, '', () => {
                    this.props.navigator.push({
                        component: UserNickNameEdit,
                        params: {
                            data: data,
                            callback: (result) => {
                                this.setState({userData: result});
                                DeviceEventEmitter.emit('upDateUserData', result);
                                this.props.updateInfo(result)
                            }
                        }
                    })
                }, I18n.t('default.edit'))}

                {/*middle_name*/}
                <Text style={[styles.topTv, StyleUtils.smallFont, {color: 'black'}]}>{I18n.t('default.fullName')}</Text>
                {this.getUserTv([firstName || middleName || lastName ? firstName + ' ' + middleName + ' ' + lastName : nullText])}

                <Text style={[styles.topTv, StyleUtils.smallFont, {color: 'black'}]}>{I18n.t('default.sex')}</Text>
                {this.getUserTv([gender])}


                <Text
                    style={[styles.topTv, StyleUtils.smallFont, {color: 'black'}]}>{I18n.t('default.dateOfBirth')}</Text>
                {this.getUserTv([data && data.birth_day ? data.birth_day : nullText])}

                <Text style={[styles.topTv, StyleUtils.smallFont, {color: 'black'}]}>{I18n.t('default.idNumber')}</Text>
                {this.getUserTv([data && data.id_number ? data.id_number : nullText])}

                <Text
                    style={[styles.topTv, StyleUtils.smallFont, {color: 'black'}]}>{I18n.t('default.phoneNumber')}</Text>
                {this.getUserTv([data && data.mobile ? new CommonUtils().phoneEncryption(data.mobile) : nullText])}

                {/*编辑用户信息editBn*/}
                {//data.status === 3 || data.status === 4 || data.status === 5 ? null :
                    ViewUtils.getEditBtn(() => {
                        this.props.navigator.push({
                            component: UserInformationEdit,
                            params: {
                                callback: (result) => {
                                    this.setState({userData: result});
                                    DeviceEventEmitter.emit('upDateUserData', result);
                                },
                                userData: data,

                            }
                        })
                    })}
                {/*/!*编辑用户收货地址*!/*/}
                <View style={StyleUtils.lineStyle}/>
                <Text style={[{
                    marginTop: 16,
                    fontSize: 18,
                    color: 'black',
                    textAlign:'left'
                }, StyleUtils.smallFont]}>{I18n.t('default.contactAddress')}</Text>
                {this.getUserTv([data && data.building, data && data.street_name, data && data.address, data && data.postcode])}
                {//data.status === 3 || data.status === 4 || data.status === 5 ? null :
                    ViewUtils.getEditBtn(() => {
                        this.props.navigator.push({
                            component: UserEditAddress,
                            params: {
                                callback: (result) => {
                                    this.setState({userData: result});
                                    DeviceEventEmitter.emit('upDateUserData', result);

                                },
                                editData: data,
                            }
                        })
                    })}
                {/*编辑语言*/}
                <View style={StyleUtils.lineStyle}/>
                <Text style={[{
                    marginTop: 16,
                    fontSize: 18,
                    color: 'black',
                    textAlign:'left'
                }, StyleUtils.smallFont]}>{I18n.t('default.languagesSpoken')}</Text>
                {this.getUserTv(this.getUserLang(data ? data.languages : nullText))}
                {
                    ViewUtils.getEditBtn(() => {
                        this.props.navigator.push({
                            component: EditLanguage,
                            params: {
                                callback: (result) => {
                                    this.setState({userData: result});
                                    DeviceEventEmitter.emit('upDateUserData', result);
                                },
                                lang: data ? data.languages : null
                            }
                        })
                    })}


            </View>
        }
        return (
            <View style={[StyleUtils.flex, {backgroundColor: 'white'}]}>


                {userIN}

            </View>
        );
    }


}

const styles = StyleSheet.create({
    editTv: {
        padding: 8,
        fontSize: 14,
        color: 'gray',
        flex: 1,
        marginTop: 10,
        textAlign: 'right',
    },
    topTv: {
        marginTop: 10,
        fontSize: 18,
        textAlign: 'left'
    }

});