/**
 * Created by zhuang.haipeng on 2017/9/12.
 */
import React, {Component} from 'react';
import {DeviceEventEmitter, Image, ListView, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import StyleUtils from "../../../res/styles/StyleUtils";
import Navigation from "../../../custom/Navigation";
import ViewUtils from "../../../utils/ViewUtils";
import IparNetwork, {SERVER_TYPE} from "../../../httpUtils/IparNetwork";
import CommonUtils from "../../../common/CommonUtils";

import I18n from '../../../res/language/i18n'
import IparInput from "../../../custom/IparInput";

export default class EditSelf extends Component {

    constructor(props) {
        super(props);
        let selfTv = this.props.self;
        this.state = {
            userID: '',
            selfInput: selfTv !== undefined ? selfTv : 'INTER SELF DESCRIPTION',
        };
        this.iparNetwork = new IparNetwork()
    }

    componentWillUnmount() {
        this.iparNetwork = null;
    }

    componentDidMount() {
        CommonUtils.getAcyncInfo('userInfo')
            .then((result) => {
                this.setState({userID: JSON.parse(result).main_user_id});
            });


    }

    /**修改用户收货地址信息姐*/
    getSelfInfo() {
        let formData = new FormData();
        formData.append("user_id", this.state.userID);
        formData.append("personal_desc", this.state.selfInput);

        this.iparNetwork.getPostFrom(SERVER_TYPE.admin + 'updateUserInfo', formData)
            .then((result) => {
                CommonUtils.dismissLoading();

                if (result.code === 200) {
                    this.props.navigator.popToTop();
                    DeviceEventEmitter.emit('toast', I18n.t('default.success'))
                }
            })
            .catch((error) => {
                CommonUtils.dismissLoading();

                console.log(error);
            })
    }


    render() {

        return (
            <View style={StyleUtils.flex}>
                <Navigation
                    onClickLeftBtn={() => {
                        this.props.navigator.pop()
                    }}
                    leftButtonIcon={require('../../../res/images/ic_back.png')}
                    titleImage={require('../../../res/images/ic_ipar_logo.png')}
                />
                <ScrollView>
                    <View style={{padding: 16}}>
                        {ViewUtils.getTitleView('Edit Self description', 'share a few words about your self to get to know more about your')}
                        <Text style={[{fontSize: 20, color: 'black'}, StyleUtils.smallFont]}>SELF INTRODUCTION</Text>

                        <IparInput
                            multiline={true}
                            style={styles.inputTv}
                            onChangeText={(value) => {
                                this.setState({selfInput: value})
                            }}
                            placeholder={this.state.selfInput}/>

                        {ViewUtils.getButton('Submit', () => {
                            if (this.state.selfInput.trim() === 'INTER SELF DESCRIPTION') {
                                DeviceEventEmitter.emit('toast', I18n.t('default.pleaseInterSelf'))
                            } else {
                                CommonUtils.showLoading();

                                this.getSelfInfo()
                            }
                        }, {marginTop: 16})}

                    </View>
                </ScrollView>
            </View>
        );
    }

};
const styles = StyleSheet.create({
    inputTv: {
        marginTop: 16,
        height: 100,
        fontSize: 16
    }
});

