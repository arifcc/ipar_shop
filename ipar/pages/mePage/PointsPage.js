/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {DeviceEventEmitter, Image, ListView, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';


import StyleUtils from "../../res/styles/StyleUtils";
import Navigation from "../../custom/Navigation";
import ViewUtils from "../../utils/ViewUtils";
import I18n from "../../res/language/i18n";
import IparNetwork, {SERVER_TYPE} from "../../httpUtils/IparNetwork";
import CommonUtils from "../../common/CommonUtils";

export default class PointsPage extends Component {
    constructor(props) {
        super(props);
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds,
        };
        this.iparNetwork = new IparNetwork();
    }

    componentWillUnmount() {
        this.iparNetwork = null;
    }

    componentDidMount() {
        CommonUtils.showLoading();

        this.onLoad()

    }

    /**
     * 获取PointsCard 数据
     */
    onLoad() {
        CommonUtils.getAcyncInfo('userInfo')
            .then((result) => {
                new IparNetwork().getRequest(SERVER_TYPE.admin + 'getUsersVoucher?', 'user_id=' + JSON.parse(result).user_id + '&type=2')
                    .then((result) => {
                        if (result.code === 200) {
                            this.setState({
                                dataSource: this.state.dataSource.cloneWithRows(result.data),
                            });
                        } else {
                            DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
                        }
                        CommonUtils.dismissLoading();

                    })
                    .catch((error) => {
                        DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
                        console.log('----' + error);
                        CommonUtils.dismissLoading();

                    })
            });


    }


    render() {
        return (
            <View style={StyleUtils.flex}>
                <Navigation
                    onClickLeftBtn={() => {
                        this.props.navigator.pop()
                    }}
                    leftButtonIcon={require('../../res/images/ic_back.png')}
                    titleImage={require('../../res/images/ic_ipar_logo.png')}
                />
                <ScrollView>
                    <View style={{padding: 16}}>

                        {ViewUtils.getTitleView(I18n.t('default.bonusCards'), I18n.t('default.youCanApplyIBM'))}

                        <View style={StyleUtils.lineStyle}/>
                        <ListView
                            dataSource={this.state.dataSource}
                            renderRow={this.renderRow.bind(this)}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }

    /*list renderRow*/
    renderRow(rowData, sectionID, rowID) {
        return (
            <View style={[StyleUtils.rowDirection, StyleUtils.center, styles.itemBox]}>
                <Image
                    source={{uri: rowData.img === null ? '' : rowData.img}}
                    defaultSource={require('../../res/ic_goods_test.png')}
                    style={styles.goodsIcon}/>
                <View style={[StyleUtils.flex, styles.tvBox]}>
                    <Text
                        numberOfLines={1}
                        style={[StyleUtils.smallFont, styles.goodsText, {fontSize: 16}]}>
                        {rowData.title}
                    </Text>
                    <Text
                        numberOfLines={1}
                        style={[StyleUtils.smallFont, styles.goodsText]}>
                        {rowData.status === 0 ? rowData.end_time : rowData.update_time}
                    </Text>
                </View>

                <View style={[styles.applyTv]}>
                    <Text
                        style={[{color: rowData.status === 0 ? 'black' : 'red'}, StyleUtils.smallFont]}>{rowData.status === 0 ? I18n.t('default.expired') : I18n.t('default.apply')}</Text>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({

    goodsIcon: {
        width: 62,
        height: 62,
        backgroundColor: '#f4f4f4',
        resizeMode: 'contain'
    },
    goodsText: {
        flex: 1,
        color: '#0f0f0f',
        paddingRight: 8,
        paddingLeft: 8
    },

    itemBox: {
        height: 80,
        borderBottomWidth: 1,
        borderBottomColor: '#eeeeee',
    },
    applyTv: {
        width: 60,
        height: 30,
        justifyContent: 'center',
        backgroundColor: '#e1e2e3',
        alignItems: 'center'

    },
    tvBox: {
        marginTop: 16,
        marginBottom: 8,
    }

});