import React, {Component} from 'react'
import {
    DeviceEventEmitter,
    Image,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import StyleUtils from "../../../res/styles/StyleUtils";
import Navigation from "../../../custom/Navigation";
import ViewUtils from "../../../utils/ViewUtils";
import I18n from '../../../res/language/i18n'
import CommonUtils from "../../../common/CommonUtils";
import PlansMainPage from "../business/PlansMainPage";

export default class OrderSuccessPage extends Component {
    render() {
        let data = this.props.orderData;
        return (
            <View style={StyleUtils.flex}>
                <Navigation
                    onClickLeftBtn={() => {
                        this.props.navigator.popToTop();
                    }}
                    titleImage={require('../../../res/images/ic_ipar_logo.png')}
                />
                <ScrollView style={{padding: 16, paddingTop: 0,}}>
                    {ViewUtils.getTitleView(I18n.t('default.paySuccess'), I18n.t('default.paySuccessNote'))}
                    {ViewUtils.gitUserInformationTv(I18n.t('default.orderNo'), data.order_sn,)}
                    {ViewUtils.gitUserInformationTv(I18n.t('default.paymentMethod'), data.pay_type)}
                    {ViewUtils.gitUserInformationTv(I18n.t('default.orderDate'), data.pay_time)}

                    {ViewUtils.getButton(I18n.t('default.keepShopping'), () => {
                        this.props.navigator.popToTop();
                        DeviceEventEmitter.emit('goToPage', 0)
                    }, {
                        marginTop: 30,
                        backgroundColor: 'white',
                        borderWidth: 1,
                        borderColor: '#d8d8d8'
                    }, {color: 'black'})}

                    {this.props.userInfo && this.props.userInfo.open_plan_num > 1 ?
                        ViewUtils.getButton(I18n.t('b.businessCenter'), () => {
                            this.props.navigator.push({
                                component: PlansMainPage,
                                name: 'PlansMainPage',
                                params: {
                                    userId: this.props.userInfo.main_user_id,
                                }
                            })
                        }, {
                            marginTop: 16,
                            backgroundColor: 'white',
                            borderWidth: 1,
                            borderColor: '#d8d8d8'
                        }, {color: 'black'}) : null}
                </ScrollView>
            </View>
        )
    }
}


