import React, {Component} from 'react'
import {
    DeviceEventEmitter,
    RefreshControl,
    Image,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import StyleUtils from "../../../res/styles/StyleUtils";
import Navigation from "../../../custom/Navigation";
import ViewUtils from "../../../utils/ViewUtils";
import IparNetwork, {getSynPaysSellerAuth, SERVER_TYPE} from "../../../httpUtils/IparNetwork";
import GifCardPage from "../GifCardPage";
import CouponsPage from "../CouponsPage";
import CommonUtils from "../../../common/CommonUtils";
import OnlinePaymentPage from "../../confirm/OnlinePaymentPage";
import SelectBankCardModal from "../../confirm/SelectBankCardModal";
import VerificationCodeInput from "../../../utils/VerificationCodeInput";
import BottomSelectionBox from "../../../utils/BottomSelectionBox";
import OrdersPage from "./OrdersPage";
import BaseWebViewPage from "../../common/BasWebViewPage";
import I18n from "../../../res/language/i18n/index";
import AddBackCardPage from "../bankCard/AddBackCardPage";
import OrderSuccessPage from "./OrderSuccessPage";
import ElsomAndQiwiPage from "./ElsomAndQiwiPage";
import AlertDialog from "../../../custom/AlertDialog";
import InputModal from "../../../utils/InputModal";
import UserEmailEdit from "../userInfo/UserEmailEdit";
import moment from 'moment';

let clickTypes = {
    coupons: 'Coupons',
    giftCard: 'gift-Card',
    onlinePayment: 'SynPays',
    submit: 'Submit',
};


export default class OrderPayPage extends Component {


    constructor(props) {
        super(props);
        this.synPaysParams = {};
        this.iparNetwork = new IparNetwork();
        this.balance = 0;

        this.handlePaymentNum = 0;
        this.state = {
            data: this.props.orderData,
            isLoading: false,
            alertVisible: false,
            swap_amountData: 0,
            bottomViewHeight: 0,
            modalVisible: false,
            verificationVisible: false,
            verificationCodeModalNum: 6,//发送验证码的输入框的状态--6是验证码输入框／3 是信用卡的svv2
            verificationCodeMessage: '',
            verificationCodeMessageValue: '',//选择的银行卡value
            verificationType: false,//验证码发送到什么（phone || email）的 modalView
            verificationTypeValue: false,//1=>email ; 2=> mobile
            userInfo: '',
            selectGiftCardsData: null,//用户选择的GiftCards数据
            selectCouponsData: null,//用户选择的Coupons数据
            selectOnlinePayment: null,//用户选择的online payment数据
            isRunOut_Gift_Coupons: false,
            showDialog: false,
            isFirstLoadingBankCard: false,//
            passWordModalVisible: false,//balance支付的收货使用（请输入登录密码。。。效果 ）
        }

    }


    componentDidMount() {
        CommonUtils.getAcyncInfo('userInfo')
            .then((result) => {
                this.setState({
                    userInfo: JSON.parse(result)
                });
            });

        CommonUtils.getAcyncInfo('getPv')
            .then((result) => {
                this.setState({
                    swap_amountData: JSON.parse(result)
                });
                //如果没有数据的话获取
                if (!this.state.data) {
                    this.onLoadData();
                }
            });
    }


    /**
     * loading 动画
     * @param visible
     */
    showLoad(visible) {
        if (visible) {
            CommonUtils.showLoading();
        } else {
            CommonUtils.dismissLoading();
        }
    }

    /**
     * 获取订单详细信息
     * */
    onLoadData() {
        this.showLoad(true);

        this.iparNetwork.getRequest(SERVER_TYPE.admin + 'checkOrder?', 'order_id=' + this.props.order_id)
            .then((result) => {
                this.showLoad(false);
                if (result.code === 200) {
                    this.setState({
                        data: result.data
                    })
                } else {
                    DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
                    console.log('OrderPayPage--onLoadData-else-', result)
                }
            })
            .catch((error) => {
                DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
                console.log('OrderPayPage--onLoadData-error-', error);
                this.showLoad(false);
            })
    }


    /**
     * 点击方法
     * */
    onClick(type) {
        let goodsData = this.state.data;
        let swapAmount = this.state.swap_amountData.swap_amount;

        let _component = null;
        let _name = null;
        let _params = null;

        switch (type) {
            case clickTypes.coupons:
                if (this.state.isRunOut_Gift_Coupons) {
                    DeviceEventEmitter.emit('toast', I18n.t('default.runOut'));
                    return
                }
                _component = CouponsPage;
                _name = 'CouponsPage';
                _params = {
                    goods_amount: goodsData.goods_amount,

                    callback: (data) => {
                        this.sendOtherPayment(data);
                        this.setState({
                            selectCouponsData: data
                        })
                    }
                };
                break;
            case clickTypes.giftCard:
                if (this.state.isRunOut_Gift_Coupons) {
                    DeviceEventEmitter.emit('toast', I18n.t('default.runOut'));
                    return
                }
                _component = GifCardPage;
                _name = 'GifCardPage';
                _params = {
                    callback: (data) => {
                        this.sendOtherPayment(data);
                        this.setState({
                            selectGiftCardsData: data
                        })
                    }
                };
                break;
            case clickTypes.onlinePayment:
                _component = OnlinePaymentPage;
                _name = 'OnlinePaymentPage';
                _params = {
                    order_type: goodsData.order_type,
                    callback: (data) => {
                        this.setState({
                            selectOnlinePayment: data
                        })
                    }
                };
                break;
            case clickTypes.submit:
                let selectOnlinePayment = this.state.selectOnlinePayment;
                if (!selectOnlinePayment) {
                    DeviceEventEmitter.emit('toast', I18n.t('default.selectPayment'));
                    _component = OnlinePaymentPage;
                    _name = 'OnlinePaymentPage';
                    _params = {
                        order_type: goodsData.order_type,
                        callback: (data) => {
                            this.setState({
                                selectOnlinePayment: data
                            })
                        }
                    };
                    break;
                }


                let payData = {
                    order_sn: goodsData.order_sn,
                    pay_type: selectOnlinePayment.payment_key,
                };

                switch (selectOnlinePayment.payment_id) {
                    case 'synpay':
                        this.setState({
                            modalVisible: true,
                        });
                        break;
                    case 'demir':
                        _component = BaseWebViewPage;
                        _params = {
                            urlParam: 'demirbank/pay?order_id=' + goodsData.order_id,
                            payData: payData,
                        };
                        break;
                    case 'kkb':
                        _component = BaseWebViewPage;
                        _params = {
                            urlParam: 'kkb/pay?order_id=' + goodsData.order_id,
                            payData: payData,
                        };
                        break;
                    case 'sberbank':
                        _component = BaseWebViewPage;
                        _params = {
                            urlParam: 'sberbank/pay?order_id=' + goodsData.order_id,
                            payData: payData,
                        };
                        break;
                    case 'paypal':
                        let amount = ((goodsData.pay_amount / swapAmount).toFixed(2));
                        _component = BaseWebViewPage;
                        _params = {
                            urlParam: 'paypal/pay?amount=' + amount + '&order_sn=' + goodsData.order_sn,
                            payData: payData,
                        };
                        break;
                    case 'elsom':
                        if (goodsData.transaction_number === null) {
                            _component = ElsomAndQiwiPage;
                            _params = {
                                urlParam: 'kgbank/pay?order_id=' + goodsData.order_id,
                                orderSn: goodsData.order_id,
                                paymentKey: this.state.selectOnlinePayment.payment_key,
                            };
                        } else {
                            this.elsomOTP = goodsData.transaction_number;
                            this.showDialog(true);
                        }
                        break;
                    case 'qiwi':
                    case 'qiwikg':
                        if (goodsData.transaction_number === null) {
                            _component = ElsomAndQiwiPage;
                            _params = {
                                urlParam: 'qiwi',
                                orderSn: goodsData.order_sn,
                                orderId: goodsData.order_id,
                                paymentKey: this.state.selectOnlinePayment.payment_key,
                            };
                        } else {
                            this.elsomOTP = goodsData.transaction_number;
                            this.showDialog(true);
                        }

                        break;

                }

                break;
            case 'finish':
                if (this.props.type === 'confirmOrder') {
                    _component = OrdersPage;
                    _name = 'OrdersPage';
                } else {
                    this.props.navigator.pop();
                }
                break
        }

        if (_component) {
            this.props.navigator.push({
                component: _component,
                name: _name,
                params: _params,
            })
        }
    }


    /**
     * 订单使用的优惠卷
     * */
    sendOtherPayment(voucherData) {

        this.setState({
            isRunOut_Gift_Coupons: true,
            isLoading: true,
        });

        let formData = new FormData();
        formData.append('order_id', this.state.data.order_id);
        formData.append('voucher_id', voucherData.voucher_id);
        this.iparNetwork.getPostFrom(SERVER_TYPE.adminIpar + 'saveOtherPayment', formData)
            .then((result) => {
                if (result.code === 200) {
                    this.setState({
                        data: result.data,
                        isLoading: false,
                    })
                } else {
                    this.setState({
                        isLoading: false,
                        isRunOut_Gift_Coupons: false,
                        selectCouponsData: null,
                        selectGiftCardsData: null
                    });
                    DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
                    console.log('OrderPayPage---sendOtherPayment--fail: ', result)
                }
            })
            .catch((error) => {
                this.setState({
                    isLoading: false,
                    isRunOut_Gift_Coupons: false,
                    selectCouponsData: null,
                    selectGiftCardsData: null
                });
                DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
                console.log('OrderPayPage---sendOtherPayment--error: ', error)
            })


    }


    render() {
        //coupons value
        let state = this.state;
        let swapAmount = state.swap_amountData.swap_amount;
        let currency_code = state.swap_amountData.currency_code;
        //gift card value
        let gifCardValue = state.selectGiftCardsData ?
            swapAmount * state.selectGiftCardsData.value : 0;

        let couponsValue = state.selectCouponsData ?
            state.selectCouponsData.value * swapAmount : 0;
        // online payment
        let paymentText = state.selectOnlinePayment ? state.selectOnlinePayment.payment_key : '';
        //data
        let orderData = state.data;
        let elsomOPT = orderData.pay_type === 'elsom' ? 'OPT NO: ' : I18n.t('default.transactionId') + ': ';
        let userInfo = state.userInfo;


        return (
            <View style={StyleUtils.flex}>


                <Navigation onClickLeftBtn={() => this.onClick('finish')}/>

                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={state.isLoading}
                            onRefresh={() => {
                            }}
                        />
                    }
                >
                    {ViewUtils.getTitleView(I18n.t('default.orderPay'), I18n.t('default.paymentMethods'))}


                    {/**支付方式*/}
                    <View style={{padding: 16,}}>
                        <Text
                            style={[styles.title, StyleUtils.smallFont, {
                                fontSize: 18,
                                marginTop: 0,
                                // color: 'white',
                                // paddingLeft: 8,
                                // paddingRight: 8,
                                // backgroundColor: '#000000',
                                fontWeight: 'bold',
                            }]}>
                            {I18n.t('default.paymentMethod')}
                        </Text>

                        {this.getPaymentMethodView(I18n.t('default.onlinePayment'), paymentText, null, () => this.onClick(clickTypes.onlinePayment))}
                    </View>


                    {/**info*/}
                    <View style={{
                        padding: 16, borderTopWidth: 8, borderColor: '#f7f7f7',
                        marginBottom: state.bottomViewHeight + 20
                    }}>
                        {/*Order total*/}

                        <View style={StyleUtils.justifySpace}>
                            <Text
                                style={[styles.title, StyleUtils.smallFont, {
                                    fontSize: 14,
                                    marginTop: 0,
                                    fontWeight: 'bold',
                                }]}>{I18n.t('default.orderNo')}</Text>
                            <Text>
                                {orderData.order_sn}
                            </Text>
                        </View>


                        {this.getDoubleTv(I18n.t('default.orderTotalAmount'), (orderData.goods_amount * state.swap_amountData.swap_amount).toFixed(1) + currency_code)}

                        {this.getDoubleTv(I18n.t('default.savings'), (orderData.discount_amount || 0).toFixed(1) + currency_code)}

                        {this.getDoubleTv(I18n.t('default.shippingCost'), orderData.shipping_amount.toFixed(1) + currency_code)}

                        {this.getPaymentMethodView(I18n.t('default.coupons'), (couponsValue.toFixed(1)) + currency_code, StyleUtils.marginTop,
                            () => this.onClick(clickTypes.coupons), {color: 'gray'})}
                        {this.getPaymentMethodView(I18n.t('default.giftCard'), (gifCardValue.toFixed(1)) + currency_code, {
                                marginBottom: 0
                            },
                            () => this.onClick(clickTypes.giftCard), {color: 'gray'})}
                    </View>


                </ScrollView>

                <View
                    onLayout={(event) => this.setState({bottomViewHeight: event.nativeEvent.layout.height})}
                    style={[styles.btnBox, styles.borderTop]}>
                    <View style={StyleUtils.justifySpace}>
                        <Text
                            style={[styles.title, StyleUtils.smallFont, {marginTop: 20}]}>{I18n.t('default.payingAmount')}</Text>
                        <Text>
                            {orderData.pay_amount.toFixed(1) + currency_code}
                        </Text>
                    </View>
                    {ViewUtils.getButton(I18n.t('default.payNow'), () => this.onClick(clickTypes.submit))}
                </View>

                {/**select bank card */}
                <SelectBankCardModal
                    onClose={() => {
                        this.setState({
                            modalVisible: false
                        })
                    }}
                    isFirstLoading={state.isFirstLoadingBankCard}
                    updateLoadingStatus={() => this.setState({isFirstLoadingBankCard: false})}
                    balance={(_balance) => this.balance = _balance}
                    onItemClick={(rowData) => this.onItemClick(rowData)}
                    userId={userInfo.main_user_id}
                    visible={state.modalVisible}
                    country={userInfo.country}
                />
                {/**send verification */}
                <VerificationCodeInput
                    close={() => {
                        this.setState({
                            verificationVisible: false,
                            verificationCodeMessage: ''
                        });
                    }}
                    country={userInfo.country}
                    centerValue={state.verificationCodeMessageValue}
                    message={state.verificationCodeMessage}
                    onChangeText={(value) => {
                        this.setState({
                            verificationVisible: false,
                            verificationCodeMessage: ''
                        }, this.paySynPays(value));

                    }}
                    VerifyMethod={state.verificationTypeValue}
                    userInfo={userInfo}
                    inputSize={state.verificationCodeModalNum}
                    visible={state.verificationVisible}/>

                {/**Selection box */}
                <BottomSelectionBox
                    requestClose={() => {
                        this.setState({
                            verificationType: false,
                        })
                    }}
                    onClick={(type) => this.verificationTypeModalClick(type)}
                    textTop={userInfo && userInfo.email ? I18n.t('default.email') : null}
                    title={I18n.t('pay.selectVeriType')}
                    textBottom={userInfo.mobile ? I18n.t('default.mobile') : null}
                    visible={state.verificationType}
                />


                {/*elsom OTP*/}
                <AlertDialog
                    visible={state.showDialog}
                    requestClose={() => this.showDialog(false)}
                    contentTv={I18n.t('default.completeYourPayment') + '\n' + I18n.t('default.orderNo') + ': ' + orderData.order_sn + '\n' + elsomOPT + this.elsomOTP}
                    leftBtnOnclick={() => {
                        this.showDialog(false);
                        this.onClick(clickTypes.onlinePayment);
                    }}
                />


                <AlertDialog
                    contentTv={I18n.t('toast.passwordNo')}
                    leftSts={I18n.t('default.tryAgain')}
                    rightSts={I18n.t('default.findPass')}
                    leftBtnOnclick={() => {
                        this.setState({
                            alertVisible: false,
                            passWordModalVisible: true,
                        });
                    }}
                    rightBtnOnclick={() => {
                        this.setState({alertVisible: false});
                        this.startFindPasswordPage()
                    }}
                    visible={state.alertVisible}
                    requestClose={null}/>

                <InputModal
                    submit={(value) => {
                        this.showLoad(true);
                        this.verificationUserPassword(value);
                    }}
                    onPressPass={() => {
                        this.setState({passWordModalVisible: false});
                        this.startFindPasswordPage()
                    }}
                    requestClose={() => this.setState({passWordModalVisible: false})}
                    isPassword={true}
                    inputPlaceholder={I18n.t('default.enterYourPassword')}
                    btnPlaceholder={I18n.t('default.submit')}
                    visible={state.passWordModalVisible}/>
            </View>
        )
    }


    startFindPasswordPage() {
        this.props.navigator.push({
            component: UserEmailEdit,
            params: {
                mobile: this.state.userInfo.mobile,
                type: 1,
            }
        })
    }

    /**
     * 验证用户登录密码
     */
    verificationUserPassword(userPass) {
        let _this = this;

        let formData = new FormData();
        formData.append('name', this.state.userInfo.mobile);
        formData.append('password', userPass);
        this.iparNetwork.getPostFrom(SERVER_TYPE.centerIpar + 'login', formData)
            .then((result) => {
                CommonUtils.dismissLoading();
                if (result.code === 200) {
                    _this.paySynPays('');
                } else if (result.message === '密码错误') {
                    _this.setState({
                        alertVisible: true,
                    });
                } else {
                    DeviceEventEmitter.emit('toast', I18n.t('toast.loginFail'))
                }

            })
            .catch((error) => {
                console.log('loginPage erroe--' + error);
                DeviceEventEmitter.emit('toast', I18n.t('toast.loginFail'))
            })

    }


    /*alertDialog关闭*/
    showDialog(boolean) {
        this.setState({showDialog: boolean})
    }

    /**
     * render view
     */

    getDoubleTv(defaultTv, content, boxStyle) {
        return <View style={[StyleUtils.center, StyleUtils.flex, StyleUtils.rowDirection, {height: 25}, boxStyle]}>
            <Text style={[StyleUtils.flex, StyleUtils.smallFont, {
                fontSize: 14,
                color: 'gray',
                textAlign: 'left'
            }]}>{defaultTv}</Text>
            <Text style={[StyleUtils.smallFont, {textAlign: 'right', color: 'gray'}]}>{content}</Text>
        </View>
    }


    getPaymentMethodView(title, value, style, callBack, textStyle) {
        return (
            <TouchableOpacity
                onPress={callBack}
                style={[StyleUtils.justifySpace, StyleUtils.marginBottom, style]}>
                <Text
                    style={[StyleUtils.smallFont, {color: 'gray'}, textStyle]}>{title}</Text>


                <View style={[StyleUtils.justifySpace]}>
                    <Text style={[styles.valueText, StyleUtils.smallFont, {color: 'gray'}]}>{value}</Text>
                    <Image
                        style={[{width: 16, height: 16, tintColor: 'gray',}]}
                        source={require('../../../res/images/ic_right_more.png')}/>
                </View>
            </TouchableOpacity>
        )
    }


    /**
     * synPays 支付
     * */
    paySynPays(verification) {

        CommonUtils.showLoading();

        let swapAmount = this.state.swap_amountData.swap_amount;
        // if (this.state.verificationCodeModalNum === 3) {
        this.synPaysParams.CVV2 = verification;
        // this.getVerificationType();
        // return;
        // }

        let _data = this.state.data;

        let formData = new FormData();
        formData.append('order_sn', _data.order_sn);
        formData.append('type', this.synPaysParams.PayFrom);
        formData.append('code', this.synPaysParams.Code);
        formData.append('cvv', this.synPaysParams.CVV2);
        formData.append('goods_name', _data.goods[0].goods_name);
        formData.append('goods_number', _data.goods[0].goods_number);
        formData.append('sms_code', verification);

        this.iparNetwork.getPostFrom(SERVER_TYPE.payServer + 'synpay/pay', formData)
            .then((result) => {
                CommonUtils.dismissLoading();
                if (result.code === 500 && result.message === 'Verfification Code is invalid.') {
                    this.setState({
                        verificationCodeMessage: I18n.t('default.verCodeError'),
                        verificationVisible: true,
                    });

                } else if (result.code == 200) {
                    // this.handlePayment(result.data.TransactionNumber);
                    DeviceEventEmitter.emit('toast', I18n.t('default.success'));
                    this.props.navigator.push({
                        component: OrderSuccessPage,
                        name: 'OrderSuccessPage',
                        params: {
                            orderData: {
                                order_sn: _data.order_sn,
                                pay_type: this.state.selectOnlinePayment.payment_key,
                                pay_time: moment(new Date()).format("YYYY-MM-DD HH:mm:ss"),
                            },
                            userInfo: this.state.userInfo,
                        }
                    });

                } else {
                    DeviceEventEmitter.emit('toast', I18n.t('pay.bankCardError'));
                }

            })
            .catch((error) => {
                CommonUtils.dismissLoading();
                DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
                console.log('error--', error);
            });
    }

    /**
     * 成功支付后处理订单信息
     * */
    // handlePayment(transactionNumber) {
    //     let _data = this.state.data;
    //     let formData = new FormData();
    //     formData.append("order_sn", _data.order_sn);
    //     formData.append("pay_type", this.state.selectOnlinePayment.payment_key);
    //     formData.append("transaction_number", transactionNumber);
    //
    //     this.iparNetwork.getPostFrom(SERVER_TYPE.adminIpar + 'handlePayment?', formData)
    //         .then((result) => {
    //             if (result.code === 200) {
    //                 this.setState({isLoading: false});
    //                 DeviceEventEmitter.emit('toast', I18n.t('default.success'));
    //                 this.props.navigator.push({
    //                     component: OrderSuccessPage,
    //                     name: 'OrderSuccessPage',
    //                     params: {
    //                         orderData: result.data,
    //                         userInfo: this.state.userInfo,
    //                     }
    //                 });
    //             } else {
    //                 this.handlePaymentNum++;
    //                 if (this.handlePaymentNum > 3) {
    //                     DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
    //                 } else {
    //                     this.handlePayment(transactionNumber);
    //                 }
    //             }
    //
    //         })
    //         .catch((error) => {
    //             this.handlePaymentNum++;
    //             if (this.handlePaymentNum > 3) {
    //                 console.log(error);
    //                 DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
    //                 this.setState({isLoading: false});
    //             } else {
    //                 this.handlePayment(transactionNumber);
    //             }
    //         })
    // }


    /**
     * modal view （please choose the way of payment）item click(synPays)
     */
    onItemClick(rowData) {
        if (rowData !== 'add') {
            let _payFrom = 3;
            if (rowData) {//select band card\
                this.synPaysParams.Code = rowData.CardID;

                if (rowData.ExpirationDate) {
                    _payFrom = 2;
                } else {
                    _payFrom = 1;
                }
            } else {//balance
                if (this.balance < this.state.data.pay_amount) {
                    this.setState({
                        modalVisible: false,
                    }, DeviceEventEmitter.emit('toast', I18n.t('pay.rechargeRemind')));
                } else {

                    this.setState({
                        modalVisible: false,
                        passWordModalVisible: true,
                    })

                }
            }


            this.synPaysParams.PayFrom = _payFrom;
            if (_payFrom === 2) {//如果信用卡的话获取cvv2
                this.getSvv2(rowData.CardNumber);
            } else {
                this.getVerificationType()
            }
        } else {
            this.setState({modalVisible: false});
            this.props.navigator.push({
                component: AddBackCardPage,//BankCardTypePage
                name: 'AddBackCardPage',
                params: {
                    callback: () => {
                        this.setState({isFirstLoadingBankCard: true})
                    }
                }
            });

        }


    }

    /**
     * 话获取信用卡cvv2(synPays)
     */
    getSvv2(CardNumber) {
        this.setState({
            modalVisible: false,
            verificationVisible: true,
            verificationCodeModalNum: 3,
            verificationCodeMessageValue: CardNumber
        })
    }


    /**
     * selection box (phone || email)(synPays)
     * 验证码发送（email）||（phone）选择框
     */
    getVerificationType() {
        // this.setState({
        // 	verificationVisible: false,
        // 	modalVisible: false,
        // 	verificationType: true,
        // })

    }

    /**
     * selection box (phone || email)(synPays)
     * onPress
     * @param type === 1 top / 2 bottom
     */
    verificationTypeModalClick(type) {
        this.sendVerificationCode(type)

    }

    /**
     * 发送验证码（synPays）
     */
    sendVerificationCode(verType) {
        this.setState({
            verificationType: false,
            verificationTypeValue: verType,
            verificationCodeMessageValue: '',
            verificationCodeModalNum: 6,
            verificationVisible: true,
        })
    }


}

const styles = StyleSheet.create({

    title: {
        fontSize: 16,
        marginTop: 26,
        marginBottom: 10,
        color: 'black',
        textAlign: 'left'
    },

    viewBorder: {
        borderTopWidth: 1,
        borderColor: '#e1e2e3',
        marginTop: 40,
        paddingTop: 8
    },
    btnBox: {
        padding: 16,
        paddingTop: 0,
        backgroundColor: 'rgba(255, 255, 255, 0.88)',
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0
    },

    valueText: {
        color: '#333333',
        paddingRight: 8,
        paddingLeft: 8,
        fontSize: 16,
    },

    borderTop: {
        borderTopWidth: 1,
        borderColor: '#e1e2e3',
    }
});
