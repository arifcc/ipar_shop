import React, {Component} from 'react'
import {DeviceEventEmitter, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import StyleUtils from "../../../res/styles/StyleUtils";
import Navigation from "../../../custom/Navigation";
import ViewUtils from "../../../utils/ViewUtils";
import I18n from '../../../res/language/i18n'
import CommonUtils from "../../../common/CommonUtils";
import IparNetwork, {SERVER_TYPE} from "../../../httpUtils/IparNetwork";

const url = 'https://admin.iparbio.com/';
export default class ElsomAndQiwiPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            orderData: '',
        };

        this.iparNetwork = new IparNetwork();
    }

    componentWillUnmount() {
        this.iparNetwork = null;
    }

    componentDidMount() {
        if (this.props.urlParam !== 'qiwi') {
            this.getElsomAndQiwiInfo()
        } else {
            this.getUpdateOrder(this.props.orderSn, this.props.orderId)
        }

    }

    /*支付方式*/
    getElsomAndQiwiInfo() {

        CommonUtils.showLoading();

        this.iparNetwork.getRequest(url, this.props.urlParam)
            .then((result) => {
                CommonUtils.dismissLoading();

                if (result.code === 200) {
                    this.getUpdateOrder(result.data.OTP)
                }
            })
            .catch((error) => {
                console.log(error);
                CommonUtils.dismissLoading();

                DeviceEventEmitter.emit('toast', I18n.t('default.fail'))
            });
    }

    /*成功支付后处理订单信息*/
    getUpdateOrder(transactionNumber, orderId) {


        CommonUtils.showLoading();

        let formData = new FormData();
        formData.append("order_id", orderId ? orderId : this.props.orderSn);
        formData.append("pay_type", this.props.paymentKey);
        formData.append("transaction_number", transactionNumber);

        this.iparNetwork.getPostFrom(SERVER_TYPE.adminIpar + 'updateOrder?', formData)
            .then((result) => {
                if (result.code === 200) {
                    this.setState({ orderData: result.data});
                } else {

                    DeviceEventEmitter.emit('toast', I18n.t('default.fail'))
                }
                CommonUtils.dismissLoading();

            })
            .catch((error) => {
                console.log(error);
                DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
                CommonUtils.dismissLoading();

            })
    }


    render() {
        let data = this.state.orderData;
        return (
            <View style={StyleUtils.flex}>
                <Navigation
                    onClickLeftBtn={() => {
                        this.props.navigator.popToTop();
                    }}
                    titleImage={require('../../../res/images/ic_ipar_logo.png')}
                />
                <ScrollView style={{padding: 16, paddingTop: 0,}}>
                    {ViewUtils.getTitleView(I18n.t('default.orderPayment'), I18n.t('default.payNote'))}
                    {ViewUtils.gitUserInformationTv(I18n.t('default.orderNo'), data.order_sn,)}
                    {ViewUtils.gitUserInformationTv(I18n.t('default.paymentMethod'), data.pay_type)}
                    {ViewUtils.gitUserInformationTv(I18n.t('default.transactionId'), data.transaction_number)}

                    {ViewUtils.getButton(I18n.t('default.yes'), () => {
                        this.props.navigator.popToTop();
                    }, {marginTop: 30})}
                </ScrollView>
            </View>
        )
    }


}


