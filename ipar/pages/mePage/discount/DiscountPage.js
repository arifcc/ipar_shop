/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
	StyleSheet,
	Text,
	View,
	ScrollView,
	Image,
	TouchableOpacity
} from 'react-native';

import Navigation from "../../../custom/Navigation";
import StyleUtils from "../../../res/styles/StyleUtils";
import ViewUtils from "../../../utils/ViewUtils";
import CommonUtils from "../../../common/CommonUtils";
import DiscountDetail from "./DiscountDetail";
import I18n from "../../../res/language/i18n";


export default class DiscountPage extends Component {

	constructor(props) {
		super(props);
		this.state={
			integral:''
		}

	}

	componentDidMount() {
		CommonUtils.getAcyncInfo('userInfo')
			.then((result) => {
				this.setState({
					integral: JSON.parse(result).integral,
				});
				//this.getUsersVoucherInfo();
			});
	}

	render() {
		return (
			<View style={StyleUtils.flex}>
				<Navigation
					onClickLeftBtn={() => {
						this.props.navigator.pop()
					}}
					leftButtonIcon={require('../../../res/images/ic_back.png')}
					titleImage={require('../../../res/images/ic_ipar_logo.png')}
					rightButtonIcon={require('../../../res/images/ic_detel.png')}
					onClickRightBtn={() => {
						this.props.navigator.push({component: DiscountDetail})
					}
					}

				/>
				<View style={{padding: 16, height: '100%'}}>

					{ViewUtils.getTitleView(I18n.t('default.bonusPoints'), I18n.t('default.redemptionOrders'))}

					<Text style={[styles.walletAmountTv,StyleUtils.smallFont]}>{this.state.integral}</Text>
					{ViewUtils.getButton(I18n.t('default.withdraw'), () => {

					}, {marginTop: 30})}

					<View style={styles.commonTvBox}>
						<Text style={[styles.commonTv,StyleUtils.smallFont]}>{I18n.t('default.commonQand')}</Text>

					</View>
				</View>
			</View>)


	}

}

const styles = StyleSheet.create({
	walletAmountTv: {
		textAlign: 'center',
		fontSize: 30,
		color: 'black'
	},
	commonTvBox: {
		flex: 1,
		marginBottom: '35%',
		justifyContent: 'flex-end'
	},
	commonTv: {
		textAlign: 'center',
		fontSize: 16
	}
});

