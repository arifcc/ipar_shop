/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
	StyleSheet,
	Text,
	View,
	ScrollView,
	Image,
	TouchableOpacity
} from 'react-native';

import Navigation from "../../../custom/Navigation";
import StyleUtils from "../../../res/styles/StyleUtils";
import ViewUtils from "../../../utils/ViewUtils";
import CouponsPage from "../CouponsPage";
import GifCardPage from "../GifCardPage";
import PointsPage from "../PointsPage";
import DiscountPage from "./DiscountPage";
import I18n from "../../../res/language/i18n";


export default class DiscountCardsPage extends Component {



	render() {
		return (
			<View style={StyleUtils.flex}>
				<Navigation
					onClickLeftBtn={() => {
						this.props.navigator.pop()
					}}
					leftButtonIcon={require('../../../res/images/ic_back.png')}
					titleImage={require('../../../res/images/ic_ipar_logo.png')}

				/>

				<ScrollView>
					<View style={[StyleUtils.flex, {padding: 16}]}>
						{ViewUtils.getTitleView(I18n.t('default.discountCards'), I18n.t('default.allDiscount'))}

						<View style={StyleUtils.lineStyle}/>

						{ViewUtils.gitUserInformationTv(I18n.t('default.bonusPoints'), I18n.t('default.redemptionOrders'), '', () => {
							this.props.navigator.push({component: DiscountPage})
						}, I18n.t('default.more'))}

						{ViewUtils.gitUserInformationTv(I18n.t('default.bonusCards'), I18n.t('default.youCanApplyIBM'), '', () => {
							this.props.navigator.push({
								component: PointsPage,
							})
						}, I18n.t('default.more'))}

						{ViewUtils.gitUserInformationTv(I18n.t('default.giftCard'), I18n.t('default.ordersReduce'), '', () => {
							this.props.navigator.push({
								component: GifCardPage,

							})
						}, I18n.t('default.more'))}

						{ViewUtils.gitUserInformationTv(I18n.t('default.coupons'), I18n.t('default.ordersReduce'), '', () => {
							this.props.navigator.push({
								component: CouponsPage,
							})
						}, I18n.t('default.more'))}


					</View>
				</ScrollView>

			</View>)


	}

}


