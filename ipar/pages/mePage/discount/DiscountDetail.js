/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {Image, ListView, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

import Navigation from "../../../custom/Navigation";
import StyleUtils from "../../../res/styles/StyleUtils";
import ViewUtils from "../../../utils/ViewUtils";
import IparNetwork, {SERVER_TYPE} from "../../../httpUtils/IparNetwork";
import CommonUtils from "../../../common/CommonUtils";
import I18n from "../../../res/language/i18n";

export default class DiscountDetail extends Component {

	constructor(props) {
		super(props);
		let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
		this.state = {
			userId: '',
			dataSource:ds.cloneWithRows([])
		};

		this.iparNetwork = new IparNetwork()
	}

	componentWillUnmount() {
		this.iparNetwork = null;
	}

	componentDidMount() {
        CommonUtils.showLoading();

        /*获取本地数据*/
		CommonUtils.getAcyncInfo('userInfo')
			.then((result) => {
				this.setState({
					userId: JSON.parse(result).user_id,
				});
				this.getUsersIntegralInfo();

			});
	}

	/* 获取用户历史消费积分详细信息*/
	getUsersIntegralInfo() {
		this.iparNetwork.getRequest(SERVER_TYPE.admin + 'getUsersIntegral?', 'user_id=' + this.state.userId)
			.then((result) => {
                CommonUtils.dismissLoading();

                if (result.code === 200) {
					this.setState({dataSource: this.state.dataSource.cloneWithRows(result.data)})
				}
			})
			.catch((error) => {
                CommonUtils.dismissLoading();

                console.log(error);
			});
	}

	render() {
		return (
			<View style={StyleUtils.flex}>
				<Navigation
					onClickLeftBtn={() => {
						this.props.navigator.pop()
					}}
					leftButtonIcon={require('../../../res/images/ic_back.png')}
					titleImage={require('../../../res/images/ic_ipar_logo.png')}
				/>
				<ScrollView>
					<View style={[StyleUtils.flex, {padding: 16}]}>
						{ViewUtils.getTitleView(I18n.t('default.discountDetail'), I18n.t('default.transactionDetails'))}

						<View style={StyleUtils.lineStyle}/>

						{/*默认显示tabText*/}
						<View>{ViewUtils.getDefaultTab([I18n.t('default.date'), I18n.t('default.amount'),  I18n.t('default.type'), I18n.t('default.status')], '', '', {height: 50}, StyleUtils.defaultTabTv)}</View>
						<ListView
							renderRow={this.renderRow.bind(this)}
							dataSource={this.state.dataSource}/>


					</View>
				</ScrollView>

			</View>)


	}

	renderRow(rowData, sectionID, rowID) {
		let type='';
		switch (rowData.type){
			case 0:
				type=I18n.t('default.spend');
				break;
			case 1:
				type=I18n.t('default.bonus');
				break

		}

		return (<View>
			{ViewUtils.getDefaultTab([rowData.add_time, rowData.value, type, I18n.t('default.success')], () => {})}
		</View>)
	}

}


