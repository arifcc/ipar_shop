import React, {Component} from 'react'
import {View, TouchableOpacity, Text, StyleSheet, Image, Dimensions} from 'react-native'
import StyleUtils from "../res/styles/StyleUtils";
import IparImageView from "../custom/IparImageView";
import IparNetwork, {SERVER_TYPE} from "../httpUtils/IparNetwork";
import SlideMenu from "../menu/SlideMenu";
import CommonUtils from "../common/CommonUtils";
import TouchableImage from "react-native-image-page/src/touchable-image";
import GoodsPage from "./common/GoodsPage";
import ArticleDetail from "./article/ArticleDetail";
import CategoryMinPage from "./category/CategoryMinPage";
import I18n from '../res/language/i18n'

const {width, height} = Dimensions.get('window');
export default class WelcomePage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: null,
            time: 5,
        }
    }


    componentDidMount() {
        CommonUtils.getAcyncInfo('country_iso')
            .then((res) => {
                this.country = res;
                this.getWelcomeData(res);
            })
            .catch((err) => {
                this.startHomePage();
            });
    }

    getWelcomeData(country) {
        new IparNetwork().getRequest(SERVER_TYPE.goods, 'start?country=' + country + '&lang=' + I18n.locale, null, 3000)
            .then((res) => {
                if (res.code === 200) {
                    this.setState({
                        data: res.data,
                    });
                    this.startNum();
                } else {
                    this.startHomePage();
                }
            })
            .catch((err) => {
                this.startHomePage();
            })
    }


    /**
     * finish page的时候
     */
    componentWillUnmount() {
        this.timeInterval && clearInterval(this.timeInterval);
    }


    /**
     * 计算
     */
    startNum() {
        this.timeInterval = setInterval(() => {
            let newTime = this.state.time -= 1;
            if (newTime <= 0) {
                this.timeInterval && clearInterval(this.timeInterval);
                this.startHomePage();
            } else
                this.setState({
                    time: newTime,
                })
        }, 1000)
    }

    startHomePage(params) {
        this.timeInterval && clearInterval(this.timeInterval);

        const {navigator} = this.props;
        navigator.resetTo({
            component: SlideMenu,
            name: 'SlideMenu',
            params: params
        });
    }


    /**
     *  点击Adver
     */
    onPressAdver() {
        const {data} = this.state;
        const {navigator} = this.props;
        if (!data) return;

        let component;
        let params;

        let url = data.adver_url;
        if (url) {
            switch (data.rel_type) {
                case 1://产品
                    component = GoodsPage;
                    params = {
                        goods_id: url
                    }
                    break;
                case 2://文章
                    component = ArticleDetail;
                    params = {
                        artID: url,
                        image: data.adver_img,
                        title: data.title || data.adver_name
                    };
                    break;
                case 3://分类
                    component = CategoryMinPage;
                    params = {
                        title: data.title,
                        cate_id: url,
                        country: this.country,
                    };
                    break;
            }
        }


        if (component)
            this.startHomePage({startPage: {component, params}});

    }


    render() {
        const {data, time} = this.state;
        return (
            <View style={[StyleUtils.flex]}>

                {data ? <TouchableOpacity
                        onPress={() => this.onPressAdver()}
                        style={{width, height, flex: 1}}>
                        <IparImageView
                            errorImage={require('../res/images/ic_welcome.png')}
                            width={width} height={height} url={data.adver_img}/>
                    </TouchableOpacity> :
                    <Image style={{width: '100%', height: '100%', backgroundColor: '#ffffff', resizeMode: 'contain'}}
                           source={require('../res/images/ic_welcome.png')}/>}


                {data && <TouchableOpacity
                    onPress={() => {
                        this.startHomePage();
                    }}
                    style={styles.startBtn}>
                    <Text>skip ({time}s)</Text>
                </TouchableOpacity>}

            </View>
        )
    }
}

const styles = StyleSheet.create({
    startBtn: {
        position: 'absolute',
        top: 16,
        right: 26,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: '#f3f3f3',
        paddingHorizontal: 16,
        paddingVertical: 6,
        backgroundColor: 'rgba(255,255,255,0.2)'
    }
});
