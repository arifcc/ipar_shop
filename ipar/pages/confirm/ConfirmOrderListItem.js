/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    Image,
    StyleSheet,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View,
} from 'react-native';
import StyleUtils from "../../res/styles/StyleUtils";
import CommonUtils from "../../common/CommonUtils";
import I18n from '../../res/language/i18n'
import {IMAGE_URL} from "../../httpUtils/IparNetwork";
import IparImageView from "../../custom/IparImageView";

export default class ConfirmOrderListItem extends Component {

    constructor(props) {
        super(props);
        this.commonUtils = new CommonUtils();
        this.state = {
            isChecked: this.props.isChecked,
            country: '',
        }
    }

    componentDidMount() {

        CommonUtils.getAcyncInfo('country_iso')
            .then((result) => {
                this.setState({
                    country: result,
                });
            })
    }

    componentWillReceiveProps(nexProps) {
        this.setState({
            isChecked: nexProps.isChecked,
        })
    }

    componentWillUnmount() {
        this.commonUtils = null
    }


    /**
     *
     */
    getVipPrice(goodsInfo) {
        if (!this.props.userData) return null;
        if (!goodsInfo) return null;


        let swapAmount = this.props.currencyInfo.swap_amount;

        let _vipPrice = this.commonUtils.getPercentOwnAmount(this.props.userData, goodsInfo, swapAmount);
        //
        if (_vipPrice <= 0) return null;
        return (<View style={[StyleUtils.rowDirection, {alignItems: 'center', marginLeft: 8}]}>
            <Text style={[styles.goodsImageTitle, {marginTop: 8, minWidth: 10}]}>
                {this.props.userData && this.props.userData.user_level ? this.props.userData.user_level.level_name : ''} :
            </Text>
            <Text style={[styles.title, {marginTop: 8, fontSize: 14, marginLeft: 2, minWidth: 10, color: 'black'}]}>
                {((_vipPrice).toFixed(1)) + ' ' + this.props.currencyInfo.currency_code}
            </Text>
        </View>);
    }


    render() {

        let data = this.props.dataSource;


        //goods name
        let _goodsName = data.goods_local_name;
        if (this.state.country === 'CN') {
            if (I18n.locale === 'uy') {
                _goodsName = data.goods_name;
            }
        } else {
            if (I18n.locale === 'ru') {
                _goodsName = data.goods_name;
            }
        }


        return (
            <TouchableHighlight
                onPress={() => {

                }}
                activeOpacity={0.9}
                style={[StyleUtils.flex, StyleUtils.center, StyleUtils.rowDirection, styles.viewBox]}>


                {/*tight view*/}
                <View
                    style={StyleUtils.flex}>

                    {/**top view*/}
                    <View style={StyleUtils.rowDirection}>
                        <View style={[styles.goodsIconBox]}>
                            <IparImageView width={56} height={56} url={data.image}/>
                        </View>
                        <View style={{flex: 1}}>
                            <Text
                                numberOfLines={2}
                                style={[StyleUtils.text, styles.goodsText, StyleUtils.smallFont]}>
                                {_goodsName}
                            </Text>

                            {/*<Text style={[StyleUtils.goodsLineThroughText, {*/}
                            {/*paddingLeft: 8,*/}
                            {/*paddingBottom: 4*/}
                            {/*}]}>92.4 {this.props.currency_code.currency_code}</Text>*/}

                            <View style={[StyleUtils.justifySpace]}>
                                <View>
                                    <Text style={[styles.title,]}>
                                        {(data.goods_pv * this.props.currencyInfo.swap_amount).toFixed(1)} {this.props.currencyInfo.currency_code}
                                    </Text>
                                    {this.getVipPrice(data)}
                                </View>
                                <Text style={[styles.title, {
                                    marginRight: 16,
                                    color: 'gray'
                                }]}>X {data.goods_number}</Text>
                            </View>

                        </View>
                    </View>

                </View>


            </TouchableHighlight>
        );
    }
}
const styles = StyleSheet.create({


    goodsIconBox: {
        width: 86,
        height: 96,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#f4f4f4',
        resizeMode: 'contain'
    },
    goodsText: {
        color: '#333333',
        paddingRight: 0,
    },
    goodsNumView: {
        padding: 0,
        fontSize: 14,
        color: '#333'
    },
    viewBox: {
        borderColor: '#c3c3c3',
        marginTop: 8,
        marginBottom: 8,
    },
    addImageBox: {
        width: 22,
        height: 14,
    },
    addImage: {
        width: 8,
        height: 8,
        tintColor: '#a3a3a3',
    },
    addBox: {
        width: 80,
        borderWidth: 1,
        borderColor: '#a3a3a3',
    },
    title: {
        fontSize: 16,
        color: '#4e4e4e',
        paddingLeft: 8,
        fontWeight: 'bold'
    },
    goodsImageTitle: {
        backgroundColor: '#ff6056',
        padding: 2,
        fontSize: 12,
        color: 'white'
    },
});
