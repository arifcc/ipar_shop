import React, {Component} from 'react'
import {
	Image,
	ListView,
	Dimensions,
	DeviceEventEmitter,
	StyleSheet,
	ScrollView,
	Text,
	View,
	TouchableOpacity, RefreshControl
} from "react-native";
import StyleUtils from "../../res/styles/StyleUtils";
import Navigation from "../../custom/Navigation";
import ViewUtils from "../../utils/ViewUtils";
import IparNetwork, {SERVER_TYPE} from "../../httpUtils/IparNetwork";
import CommonUtils from "../../common/CommonUtils";
import I18n from "../../res/language/i18n";

let width = Dimensions.get('window').width;

export default class OnlinePaymentPage extends Component {


	constructor(props) {
		super(props);

		let ds = new ListView.DataSource({
			rowHasChanged: (r1, r2) => r1 !== r2,
		});
		this.state = {
			dataSource: ds,
			refreshing: false,
		}
	}

	componentDidMount() {
		this.onLoadData();
	}

	onLoadData() {
		this.setState({
			refreshing: true,
		});

		CommonUtils.getAcyncInfo('country_iso')
			.then((result) => {
				new IparNetwork().getRequest(SERVER_TYPE.adminIpar + 'getPayment?',
					'country=' + result + ',ALL' + '&show_app=1')
					.then((result) => {
						if (result.code === 200) {


							let resultData = [];
							let data = result.data;
							if (this.props.order_type === 3) {
								let length = data.length;
								for (let i = 0; i < length; i++) {
									if ((data[i].payment_id === 'synpay') || (data[i].payment_id === 'paypal')) {
										resultData.push(data[i])
									}
								}
							} else {
								resultData = data
							}


							this.setState({
								refreshing: false,
								dataSource: this.state.dataSource.cloneWithRows(resultData),
							})
						} else {
							this.setState({
								refreshing: false,
							});
							DeviceEventEmitter.emit('toast', I18n.t('default.fail'))
						}
					})
					.catch((error) => {
						this.setState({
							refreshing: false,
						});
						console.log('OnlinePaymentPage---error-', error);
						DeviceEventEmitter.emit('toast', I18n.t('default.fail'))
					})
			});
	}

	render() {
		return (
			<View style={StyleUtils.flex}>
				<Navigation
					onClickLeftBtn={() => this.props.navigator.pop()}
				/>
				<ScrollView

					refreshControl={
						<RefreshControl
							refreshing={this.state.refreshing}
							onRefresh={() => this.onLoadData()}
						/>
					}
				>
					{ViewUtils.getTitleView(I18n.t('default.paymentMethod'), I18n.t('default.selectPayment'))}


					<ListView
						contentContainerStyle={styles.gridListStyle}
						dataSource={this.state.dataSource}
						renderRow={(rowData, sectionID, rowID) => this.renderRow(rowData, rowID)}
					/>


				</ScrollView>


			</View>
		)
	}

	/**
	 * 点击方法
	 */
	onItemClick(rowData) {
		if (this.props.callback) {
			this.props.callback(rowData);
			this.props.navigator.pop();
		}
	}

	renderRow(rowData, rowID) {
		return (
			<TouchableOpacity
				onPress={() => this.onItemClick(rowData)}
				style={[StyleUtils.rowDirection, {
					alignItems: 'center',
					marginTop: 16,

				}]}>
				<View style={{borderWidth: 1, padding: 16, borderColor: '#e8e8e8', marginLeft: 16}}>
					<Image
                        resizeMode="cover"
                        style={[styles.icons]}
						source={{uri: 'http:' + rowData.icon}}/>
				</View>
			</TouchableOpacity>
		)
	}
}

const styles = StyleSheet.create({

	icons: {
		width: (width - 116) / 2,
		height: 40,
		resizeMode: 'cover'
	},

	name: {
		fontSize: 16,
		color: '#333',
		padding: 16,
	},
	gridListStyle: {
		flexDirection: 'row',
		flexWrap: 'wrap'
	},

});