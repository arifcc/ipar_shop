/**
 * 这只是支付的web view页面
 */


import React, {Component} from 'react'
import {Text, View, WebView, DeviceEventEmitter} from "react-native";
import StyleUtils from "../../res/styles/StyleUtils";
import Navigation from "../../custom/Navigation";
import I18n from "../../res/language/i18n";
import OrderSuccessPage from "../mePage/order/OrderSuccessPage";
import IparNetwork, {SERVER_TYPE} from "../../httpUtils/IparNetwork";
import moment from 'moment';


export default class BasWebViewPage extends Component {


    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {};

        this.baseUrl = SERVER_TYPE.payServer;

    }

    onNavigationStateChange = (navState) => {
        if (navState.url.indexOf('www.iparbio.com') !== -1) {
            let data = this.props.payData;
            data.pay_time = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
            this.props.navigator.push({
                component: OrderSuccessPage,
                name: 'OrderSuccessPage',
                params: {
                    orderData: data,
                }
            });
        }
    };

    render() {


        let url = this.props.uri ? this.props.uri : this.baseUrl + this.props.urlParam;
        let source = {uri: url};
        let typeIsHtml = this.props.type === 'html';
        if (typeIsHtml) {
            source = {
                html: '<html>' +
                    '<div style="height: 100%">'
                    + '<iframe id="Iframe4" style="border-width: 0px" width="100%" height="100%" src="' + url + '"></iframe>'
                    + '</div>' +
                    '</html>'
            };
        }

        console.log(url);
        return (
            <View style={StyleUtils.flex}>

                <Navigation
                    title={this.props.uri ? '' : 'pay'}
                    onClickLeftBtn={() => this.props.navigator.pop()}
                />

                <WebView
                    onMessage={() => {
                        console.log();
                    }}
                    javaScriptEnabled={true}
                    startInLoadingState={!typeIsHtml}
                    onNavigationStateChange={this.onNavigationStateChange}
                    source={source}
                    style={{width: '100%', height: '100%'}}
                />

            </View>
        )
    }

}