/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * email:nu01@qq.com
 * @flow
 */

import React, {Component} from 'react';
import {Image, ListView, Platform, StyleSheet, Text, View} from 'react-native';
import StyleUtils from "../../../res/styles/StyleUtils";
import ViewUtils from "../../../utils/ViewUtils";
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'
import Navigation from "../../../custom/Navigation";
import LoginPage from "../LoginPage";
import IparInput from "../../../custom/IparInput";

export default class RegisterIBMFourPage extends Component {


    constructor(props) {
        super(props);


    }

    startNex() {
        this.props.navigator.resetTo({
            component: LoginPage,
        })
    }


    render() {
        return (
            <View style={StyleUtils.flex}>
                <Navigation
                    titleImage={require('../../../res/images/ic_ipar_logo.png')}
                    onClickLeftBtn={() => {
                        this.props.navigator.pop()
                    }}
                />

                <KeyboardAwareScrollView>

                    <View style={{padding: 16}}>

                    {/*title*/}
                    {ViewUtils.getTitleView('Register', 'personal information')}

                    <View style={[StyleUtils.justifySpace]}>
                        {ViewUtils.getButtunMenu('国家区号', null, [StyleUtils.marginTop, {width: '33%'}])}
                        <IparInput
                            style={{width: '64%'}}
                            placeholder={'请输入手机号'}/>

                    </View>
                    <IparInput
                        isPassword={true}
                        placeholder={'请输入密码'}/>

                    {ViewUtils.getButton('NEX', () => this.startNex(), [{marginBottom: 40}, StyleUtils.marginTop])}

                    </View>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}




