/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {Image, ListView, StyleSheet, Text, ScrollView, View} from 'react-native';
import StyleUtils from "../../../res/styles/StyleUtils";
import Navigation from "../../../custom/Navigation";
import ViewUtils from "../../../utils/ViewUtils";
import RegisterCustomerPage from "./RegisterCustomerPage";
import ActivateOnePage from "./ActivateOnePage";
import I18n from '../../../res/language/i18n'
import RegisterContractModal from "./RegisterContractModal";
import CommonUtils from "../../../common/CommonUtils";
import IparNetwork, {SERVER_TYPE} from "../../../httpUtils/IparNetwork";

const clickTypes = {
    registerIBM: 'REGISTER IBM',
    registerCustomer: 'registerCustomer',
    activateAccount: 'activateAccount'
};

export default class SelectRegisterPage extends Component {


    constructor(props) {
        super(props);
        this.countryData = this.props.countryData;

        this.iparNetwork = new IparNetwork();

        this.state = {
            modalVisible: false,
            modalType: '',
            country: '',
        }
    }


    componentDidMount() {
        CommonUtils.getAcyncInfo('country_iso')
            .then((country) => {
                this.setState({
                    country: country
                })
            });


        //如果没有数据的话重新获取ok
        if (this.countryData === null || this.countryData.length <= 0) {
            CommonUtils.showLoading();
            this.onLoadCountryData();
        }
    }

    /**
     * 获取各位国家的数据
     */
    onLoadCountryData() {
        this.iparNetwork.getRequest(SERVER_TYPE.logistics + 'region?', '')
            .then((result) => {
                this.countryData = result;
                CommonUtils.dismissLoading();
            })
            .catch((error) => {
                CommonUtils.dismissLoading();
                console.log('SelectRegisterPage--onLoadCountryData-error-', error);
            })
    }


    /**
     * 点击
     * */
    _onClick(type) {
        console.log(this.countryData);

        let _component = null;
        switch (type) {
            case clickTypes.registerIBM:
                this.setState({
                    modalVisible: true,
                    modalType: type,
                });
                break;
            case clickTypes.registerCustomer:
                this.setState({
                    modalVisible: true,
                    modalType: type,
                });
                break;
            case clickTypes.activateAccount:
                _component = ActivateOnePage;
                break;
        }
        if (_component) {
            this.props.navigator.push({
                component: _component,
                params: {
                    countryData: this.countryData
                },
            })
        }
    }


    startRegister() {
        this.setState({
            modalVisible: false,
        });
        let registerType = 'vip';
        if (this.state.modalType === clickTypes.registerIBM) {
            registerType = 'ibm';
        }
        this.props.navigator.push({
            component: RegisterCustomerPage,
            params: {
                registerType: registerType,
                countryData: this.countryData
            },
        })

    }


    render() {
        return (
            <View style={[StyleUtils.flex, {backgroundColor: 'white'}]}>
                <Navigation
                    onClickLeftBtn={() => this.props.navigator.pop()}
                    titleImage={require('../../../res/images/ic_ipar_logo.png')}
                />
                <ScrollView>
                    <View style={[StyleUtils.center, styles.boxStyle]}>


                        {/*title*/}
                        {ViewUtils.getTitleView(I18n.t('default.register'),
                            I18n.t('default.welcomeToIPAR') + '\n' + I18n.t('default.registerIsIBM_minTitle'),
                            () => this._onClick(clickTypes.register))}


                        {/*register btn*/}
                        {ViewUtils.getButton(I18n.t('default.createAnIbm'), () => this._onClick(clickTypes.registerIBM), styles.btn)}
                        <Text style={styles.orTv}>{I18n.t('default.orVip')}</Text>
                        {ViewUtils.getButton(I18n.t('default.registerAs'), () => this._onClick(clickTypes.registerCustomer), [styles.btn, {backgroundColor: '#646a72'}])}

                        {/*activate*/}


                        {this.state.country !== 'CN' ?
                            <Text style={[StyleUtils.text, styles.test, StyleUtils.smallFont]}>
                                {I18n.t('default.ifYouAlready')}
                            </Text>
                            : null}

                        {this.state.country !== 'CN' ?
                            ViewUtils.getButton(I18n.t('default.activateAccount'), () => this._onClick(clickTypes.activateAccount), styles.btn)
                            : null}
                    </View>
                </ScrollView>


                <RegisterContractModal
                    submit={() => this.startRegister()}
                    requestClose={() => this.setState({
                        modalVisible: false,
                    })}
                    visible={this.state.modalVisible}
                />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    boxStyle: {
        padding: 16,
    },
    topTest: {
        marginBottom: 20,
        marginTop: 16
    },
    test: {
        marginTop: 90,
        textAlign: 'center'
    },

    btn: {
        marginBottom: 16,
    },
    orTv: {
        fontSize: 15,
        color: 'gray',
        marginBottom: 14,
        textAlign: 'center'
    }

});

