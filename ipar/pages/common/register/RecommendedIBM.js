/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * email:nur01@qq.com
 * @flow
 */

import React, {Component} from 'react';
import {
    DeviceEventEmitter,
    Image,
    Linking,
    ListView,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import StyleUtils from "../../../res/styles/StyleUtils";
import Navigation from "../../../custom/Navigation";
import IparNetwork, {SERVER_TYPE} from "../../../httpUtils/IparNetwork";
import ViewUtils from "../../../utils/ViewUtils";
import I18n from "../../../res/language/i18n";
import CommonUtils from "../../../common/CommonUtils";

export default class RecommendedIBM extends Component {

	constructor(props) {
		super(props);
		let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
		this.state = {
			dataSource: ds,
			noRecommendUser: false,
		};
		this.iparNetwork = new IparNetwork();
	}

	componentWillUnmount() {
		this.iparNetwork = null;
	}


	componentDidMount() {
        CommonUtils.showLoading();

        this.getMarketingPlan();

	}

	/*获取奖励计划信息*/
	getMarketingPlan() {
		this.iparNetwork.getRequest(SERVER_TYPE.admin + 'recommendUser?', 'country=' + this.props.country + '&rank=' + this.props.rank)
			.then((result) => {
				if (result.code === 200) {
					this.setState({
						dataSource: this.state.dataSource.cloneWithRows(result.data),
						noRecommendUser: result.data.length <= 0,
					});
				} else {
					DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
					this.setState({
						noRecommendUser: true,
					});
				}
                CommonUtils.dismissLoading();

			})
			.catch((error) => {
				console.log(error);
				this.setState({
					noRecommendUser: true,
				});
                CommonUtils.dismissLoading();

                DeviceEventEmitter.emit('toast', I18n.t('default.fail'));

			})
	}


	render() {
		return (
			<View style={StyleUtils.flex}>
				<Navigation
					titleImage={require('../../../res/images/ic_ipar_logo.png')}
					onClickLeftBtn={() => {
						this.props.navigator.pop()
					}}
				/>
				<ScrollView>
					<View style={{padding: 16}}>
						{/*title*/}
						{ViewUtils.getTitleView(I18n.t('default.recommendedIBM'), I18n.t('default.selectIBM'))}

						<ListView
							renderRow={this.renderRow.bind(this)}
							dataSource={this.state.dataSource}/>

						{this.state.noRecommendUser ?
							<View><Text
								style={[StyleUtils.smallFont, styles.companyTv]}>{I18n.t('default.contactUs')}</Text>
								{ViewUtils.gitUserInformationTv('Monday-Friday', '8:00 a.m- 8:00 P.M.ET', null, null, null, styles.borderBottom)}
								{ViewUtils.gitUserInformationTv('Saturday', '9:00 a.m- 5:00 P.M.ET', null, null, null, styles.borderBottom)}
								{ViewUtils.gitUserInformationTv('Кыргызстан', '0-800- 0000-4727', null, null, null, styles.borderBottom)}
								{ViewUtils.gitUserInformationTv('Россия', '8800- 0000- 4727', null, null, null, styles.borderBottom)}
								{ViewUtils.gitUserInformationTv('Қазақстан', '810 - 800 - 0000 - 4727', null, null, null, styles.borderBottom)}
								{ViewUtils.gitUserInformationTv('US and Canada', '(833)- 843- 4727', null, null, null, styles.borderBottom)}
								{ViewUtils.gitUserInformationTv('China mainland', '400-000-4727', null, null, null, styles.borderBottom)}
								{ViewUtils.gitUserInformationTv('Email us at', 'info@iparbio.com', null, null, null, styles.borderBottom)}
							</View> :
							null}

					</View>
				</ScrollView>
			</View>
		);
	}

	/*list render row*/
	renderRow(rowData, sectionID, rowID) {
		return (<View>
			{ViewUtils.gitUserInformationTv(rowData.user_name, rowData.mobile, '', () => {
				Linking.openURL(`tel:${rowData.mobile}`)// 给拨打电话
			})}
		</View>)
	}

}
const styles = StyleSheet.create({
	companyTv: {
		fontSize: 18,
		color: 'black',
		marginBottom: 10
	},
	borderBottom: {
		borderBottomColor: 'transparent',
	}
});



