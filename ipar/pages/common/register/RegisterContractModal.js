import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    DeviceEventEmitter,
    Modal,
    Image,
    Platform, ActivityIndicator
} from 'react-native';
import StyleUtils from "../../../res/styles/StyleUtils";
import I18n from "../../../res/language/i18n";
import ViewUtils from "../../../utils/ViewUtils";
import CheckBox from 'react-native-check-box'

export default class RegisterContractModal extends Component {


    constructor(props) {
        super(props);
        this.state = {

            visible: this.props.visible,

            checkBoxOne: false,
            checkBoxTow: false,
            checkBoxThree: false,
            checkBoxFor: false,
        }
    }


    renderCheckBox(rightText, checked, onclick) {
        return <CheckBox
            style={[StyleUtils.marginTop]}
            rightTextStyle={[StyleUtils.text, {padding: 0, color: '#333'}]}
            onClick={onclick}
            rightText={rightText}
            checked={checked}
        />
    }


    submit() {
        if (this.state.checkBoxOne && this.state.checkBoxTow
            && this.state.checkBoxThree && this.state.checkBoxFor) {
            this.setState({
                checkBoxOne: false,
                checkBoxTow: false,
                checkBoxThree: false,
                checkBoxFor: false,
            });
            this.props.submit();
        } else {
            DeviceEventEmitter.emit('toast', I18n.t('registerContractModal.modalTitle'))
        }
    }

    componentWillReceiveProps(nexProps) {
        if (nexProps.visible !== this.state.visible) {
            this.setState({
                visible: nexProps.visible,
                checkBoxOne: false,
                checkBoxTow: false,
                checkBoxThree: false,
                checkBoxFor: false,
            })
        }
    }


    render() {
        return (
            <Modal//* 初始化Modal */
                animationType='fade'           // 从底部滑入
                transparent={true}             // 不透明
                visible={this.state.visible}    // 根据isModal决定是否显示
                onRequestClose={this.props.requestClose}  // android必须实现
            >
                <View style={[styles.boxStyle, StyleUtils.center, {padding: 16}]}>


                    <View style={{backgroundColor: 'white', width: '100%', padding: 16, borderRadius: 10}}>

                        <Text
                            style={[StyleUtils.title, StyleUtils.smallFont, {textAlign: null}]}>{I18n.t('registerContractModal.modalTitle')}</Text>


                        <Text style={[StyleUtils.text, StyleUtils.smallFont, {padding: 0, marginTop: 16}]}>
                            {I18n.t('registerContractModal.topContent')}
                        </Text>


                        {this.renderCheckBox(I18n.t('registerContractModal.contractOne'), this.state.checkBoxOne, () => {
                            this.setState({
                                checkBoxOne: !this.state.checkBoxOne
                            })
                        })}

                        {this.renderCheckBox(I18n.t('registerContractModal.contractTow'), this.state.checkBoxTow, () => {
                            this.setState({
                                checkBoxTow: !this.state.checkBoxTow
                            })
                        })}
                        {this.renderCheckBox(I18n.t('registerContractModal.contractThree'), this.state.checkBoxThree, () => {
                            this.setState({
                                checkBoxThree: !this.state.checkBoxThree
                            })
                        })}
                        {this.renderCheckBox(I18n.t('registerContractModal.contractFour'), this.state.checkBoxFor, () => {
                            this.setState({
                                checkBoxFor: !this.state.checkBoxFor
                            })
                        })}


                        <Text style={[StyleUtils.text, StyleUtils.smallFont, {padding: 0, paddingTop: 16}]}>
                            {I18n.t('registerContractModal.bottomContent')}
                        </Text>

                        <View style={[StyleUtils.rowDirection, {alignSelf: 'flex-end', marginTop: 30}]}>
                            <TouchableOpacity onPress={this.props.requestClose} style={[{padding: 8, marginRight: 16}]}>
                                <Text>{I18n.t('default.cancel')}</Text>
                            </TouchableOpacity>

                            {<TouchableOpacity
                                onPress={() => {
                                    this.submit()
                                }}
                                style={[styles.submitBtn,]}>
                                <Text style={[{color: 'white',}]}> {I18n.t('default.yes')} </Text>
                            </TouchableOpacity>}

                        </View>
                        {/*{ViewUtils.getButton(I18n.t('default.yes'), () => this.submit(), {marginTop: 20})}*/}
                        {/*{ViewUtils.getButton(I18n.t('default.cancel'), this.props.requestClose, {*/}
                        {/*    marginTop: 16,*/}
                        {/*    backgroundColor: 'white',*/}
                        {/*    borderWidth: 1,*/}
                        {/*    borderColor: '#d1d1d1'*/}
                        {/*}, {color: '#333'})}*/}
                    </View>


                </View>
            </Modal>
        )
    }


}
const styles = StyleSheet.create({
    boxStyle: {
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
        flex: 1,
        flexDirection: 'column-reverse'
    },
    submitBtn: {
        backgroundColor: 'black',
        padding: 8,
        paddingHorizontal: 16,
        borderRadius: 4,
    }
});

