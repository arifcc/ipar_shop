import React, {Component} from 'react'
import StyleUtils from "../../res/styles/StyleUtils";
import Navigation from "../../custom/Navigation";
import {Animated, DeviceEventEmitter, Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import ViewUtils from "../../utils/ViewUtils";
import I18n from "../../res/language/i18n";
import IparNetwork, {SERVER_TYPE} from "../../httpUtils/IparNetwork";
import CommonUtils from "../../common/CommonUtils";
import SlideMenu from "../../menu/SlideMenu";

export default class QrCodeLoginPage extends Component {

	constructor(props) {
		super(props);

		this.state = {
			fadeAnim: new Animated.Value(0), // init opacity 0
		}
	}


	componentDidMount() {
		Animated.timing(  // Uses easing functions
			this.state.fadeAnim,  // The value to drive
			{
				toValue: 1,
				duration: 700
			},  // Configuration
		).start(); // Don't forget start!
	}

	/**
	 * 登录
	 * @param rid
	 * @private
	 */
	_login(rid) {

        CommonUtils.showLoading();


		let formData = new FormData();
		formData.append('user_id', this.props.user_id);
		formData.append('rid', rid);

		new IparNetwork().getPostFrom(SERVER_TYPE.adminIpar + 'qrSave', formData)
			.then((result) => {
                CommonUtils.dismissLoading();
				if (result.code === 200) {
					DeviceEventEmitter.emit('toast', I18n.t('default.success'));
					new CommonUtils().popToRoute(this, 'SlideMenu');
				} else {
					console.log('QrCodeLoginPage-----_login--fail--', result);
					DeviceEventEmitter.emit('toast', I18n.t('default.fail'))
				}
			})
			.catch((error) => {
                CommonUtils.dismissLoading();

                DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
				console.log('QrCodeLoginPage-----_login--error--', error)
			})


	}

	render() {
		return (<View style={[StyleUtils.flex]}>
			<Navigation
				onClickLeftBtn={() => this.props.navigator.pop()}/>

			<Animated.View
				style={[StyleUtils.flex, {
					alignItems: 'center',
					marginTop: 40,
					opacity: this.state.fadeAnim
				}]}>

				<Image
					style={[styles.iparIcon]}
					source={require('../../res/images/ic_ipar_logo.png')}/>
				<Text
					style={[StyleUtils.text, StyleUtils.marginTop, {color: '#333'}]}>{I18n.t('default.loginConfirmation')}</Text>

			</Animated.View>
			<View style={[StyleUtils.flex, StyleUtils.center]}>

				{ViewUtils.getButton(I18n.t('default.login'), () => this._login(this.props.rid), {width: '90%'})}

				{ViewUtils.getButton(I18n.t('default.cancel'), () => this.props.navigator.pop(), {
					width: '90%',
					backgroundColor: 'white',
					marginTop: 16,
					borderWidth: 0.5,
					borderColor: '#888888'
				}, {color: '#888888'})}

			</View>

		</View>)
	}
}

const styles = StyleSheet.create({
	iparIcon: {
		width: 180,
		height: 60,
		resizeMode: 'contain'
	}
});