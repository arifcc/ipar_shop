import React, {Component} from 'react'
import {
    DeviceEventEmitter,
    Dimensions,
    Image,
    ListView,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,

    View
} from "react-native";
import StyleUtils from "../../res/styles/StyleUtils";
import Navigation from "../../custom/Navigation";
import GoodsRender from "../../utils/GoodsRender";
import IparNetwork, {IMAGE_URL, SERVER_TYPE} from "../../httpUtils/IparNetwork";
import {DefaultTabBar} from 'react-native-scrollable-tab-view';
import ArticleDetail from "../article/ArticleDetail";
import I18n from "../../res/language/i18n";
import CommonUtils from "../../common/CommonUtils";
import ScrollableTabView from 'react-native-scrollable-tab-view'
import IparImageView from "../../custom/IparImageView";

const width = Dimensions.get('window').width;

let ds = new ListView.DataSource({
    rowHasChanged: (r1, r2) => r1 !== r2,
});
export default class GoodsAndArticlePage extends Component {

    constructor(props) {
        super(props);


        this.state = {

            dataSourceArticle: ds,
            dataSourceProduct: ds,
        }
    }


    componentDidMount() {
        CommonUtils.getAcyncInfo('country_iso')
            .then((country_iso) => {
                this.onLoadData(country_iso);
            });

    }


    /**
     * 加载数据
     */
    onLoadData(country) {
        CommonUtils.showLoading();


        new IparNetwork().getRequest(SERVER_TYPE.uiAdminIpar + 'search?', 'like=' + this.props.dataKey.toLowerCase() + '&country=' + country)
            .then((result) => {
                CommonUtils.dismissLoading();

                if (result.code === 200) {
                    let dataBlob = result.data;
                    this.setState({
                        dataSourceArticle: ds.cloneWithRows(dataBlob.article),
                        dataSourceProduct: ds.cloneWithRows(dataBlob.product),
                    });
                } else {
                    DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
                }
            })
            .catch((error) => {
                CommonUtils.dismissLoading();

                DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
                console.log('GoodsAndArticlePage---onLoadData--error: ', error)
            })

    }


    /**
     * 初始化listView
     */
    getListView(dataSource, renderRow, tabLabel) {
        return <ListView
            tabLabel={tabLabel}
            pageSize={1}
            contentContainerStyle={styles.gridListStyle}
            automaticallyAdjustContentInsets={false}
            renderRow={renderRow}
            dataSource={dataSource}
        />
    }


    renderGoods(rowData, rowID) {
        return <GoodsRender
            key={rowID}
            navigator={this.props.navigator}
            dataSource={rowData}
        />
    }


    itemClickListener(rowData) {
        this.props.navigator.push({
            component: ArticleDetail,
            params: {artID: rowData.art_id}
        })
    }

    renderArticle(rowData, rowID) {
        return (
            <TouchableOpacity
                activeOpacity={0.5}
                onPress={() => this.itemClickListener(rowData, rowID)}
                style={[styles.articleItemBox]}
            >
                <IparImageView
                    borderRadius={5} resizeMode={'cover'} width={'100%'} height={width / 2.5}
                    url={rowData.image}/>


                <View style={styles.titleBox}>
                    <Text numberOfLines={1} style={[styles.tv, StyleUtils.smallFont, {
                        fontSize: 16,
                        marginTop: 4,
                        color: 'black'
                    }]}>{rowData.title}</Text>
                    <Text numberOfLines={1} style={[styles.tv, StyleUtils.smallFont]}>{rowData.description}</Text>
                </View>
            </TouchableOpacity>
        )
    }


    render() {

        return (
            <View style={StyleUtils.flex}>

                <Navigation
                    title={this.props.dataKey}
                    onClickLeftBtn={() => this.props.navigator.pop()}/>


                <ScrollableTabView
                    renderTabBar={() => <DefaultTabBar/>}
                    tabBarUnderlineStyle={{backgroundColor: 'black', height: 2}}
                    tabBarActiveTextColor='black'
                    tabBarInactiveTextColor='#999999'

                    tabBarTextStyle={{fontSize: 18}}
                >

                    {this.getListView(this.state.dataSourceProduct, (rowData, sectionID, rowID) => this.renderGoods(rowData, rowID), I18n.t('default.product'))}
                    {this.getListView(this.state.dataSourceArticle, (rowData, sectionID, rowID) => this.renderArticle(rowData, rowID), I18n.t('default.article'))}

                </ScrollableTabView>


            </View>
        )

    }
}

const styles = StyleSheet.create({
    gridListStyle: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },

    articleItemBox: {
        width: width,
        padding: 16,
        paddingTop: 8,
    },
    titleBox: {
        marginTop: 8,
        height: 55,
        width: '100%',
    },
    tv: {
        flex: 1,
        fontSize: 13,
        color: '#404040'
    },
});
