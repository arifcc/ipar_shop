import React, {Component} from 'react'

import {View, DeviceEventEmitter, TouchableOpacity, Image, RefreshControl, Text, FlatList} from 'react-native'
import StyleUtils from "../../res/styles/StyleUtils";
import Navigation from "../../custom/Navigation";
import I18n from "../../res/language/i18n";
import IparNetwork, {SERVER_TYPE} from "../../httpUtils/IparNetwork";
import CommonUtils from "../../common/CommonUtils";
import ViewUtils from "../../utils/ViewUtils";
import MessagePage from "./MessagePage";

export default class MessageListPage extends Component {


    // 构造
    constructor(props) {
        super(props);

        this.iparNetwork = new IparNetwork();
        // 初始状态
        this.state = {
            // mainUserId: null,
            messageData: [],
            refreshing: false,
        };
    }


    componentDidMount() {
        CommonUtils.getAcyncInfo('userInfo')
            .then((userData) => {
                // this.setState({
                //     mainUserId: JSON.parse(userData).main_user_id,
                // })
                this.getMyMessage(JSON.parse(userData).main_user_id);
            })
    }


    getMyMessage(mainUserId) {
        this.setState({refreshing: true});
        this.iparNetwork.getRequest(SERVER_TYPE.admin + 'getMessage?',
            'receiver_main_user_id=' + mainUserId + '&type=0')
            .then((result) => {
                this.setState({refreshing: false});
                if (result.code === 200) {
                    this.setState({
                        messageData: result.data,
                    })
                }
            })
            .catch((error) => {
                this.setState({refreshing: false});
                console.log('MessageListPage-----getMyMessage----error:', error);
                DeviceEventEmitter.emit('toast', I18n.t('default.fail'))
            })
    }


    renderItem(item) {
        return (<TouchableOpacity
            onPress={() => {
                this.props.navigator.push({
                    component: MessagePage,
                    name: 'MessagePage',
                    params: {
                        messageId: item.message_id,
                        title: item.title
                    }
                });
            }}
            style={[StyleUtils.justifySpace, {padding: 16}]}>
            <View style={{marginRight: 8, flex: 1}}>

                <Text style={[{fontSize: 18, color: 'black'}]}>{item.title}</Text>
                <Text
                    numberOfLines={2}
                    style={[{fontSize: 12, color: 'gray'}]}>{item.content}</Text>

            </View>

            <Image
                style={[StyleUtils.tabIconSize]}
                source={require('../../res/images/ic_right_more.png')}/>

        </TouchableOpacity>)
    }

    render() {
        return (
            <View style={StyleUtils.flex}>

                <Navigation title={I18n.t('default.message')} onClickLeftBtn={() => this.props.navigator.pop()}/>


                {this.state.messageData.length > 0 && <FlatList
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={() => {
                            }}
                        />
                    }
                    data={this.state.messageData}
                    renderItem={(item) => this.renderItem(item.item)}/>}

                {this.state.messageData.length <= 0 &&
                <View style={[StyleUtils.center, StyleUtils.flex]}><Text
                    style={StyleUtils.smallFont}>{I18n.t('default.noData')}</Text></View>}

            </View>
        );
    }

}