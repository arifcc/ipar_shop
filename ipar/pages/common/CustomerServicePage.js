import React, {Component} from 'react'
import {Text, View, StyleSheet, Clipboard, Linking, ScrollView, DeviceEventEmitter} from "react-native";
import StyleUtils from "../../res/styles/StyleUtils";
import Navigation from "../../custom/Navigation";
import I18n from '../../res/language/i18n'
import ViewUtils from "../../utils/ViewUtils";
import AlertDialog from "../../custom/AlertDialog";

export default class CustomerServicePage extends Component {


    constructor(props) {
        super(props);
        this.state = {
            alertVisible: false,
            copyValue: '',
        }
    }


    /**
     * 打电话
     * @param phoneNumber
     */
    callPhone(phoneNumber) {
        Linking.openURL(`tel:${phoneNumber}`);
    }


    /**
     * 复制
     */
    copy(value) {
        this.setState({
            alertVisible: false,
        });
        Clipboard.setString(value);
        DeviceEventEmitter.emit('toast', I18n.t('default.copy'))
    }


    render() {
        return (
            <View style={StyleUtils.flex}>

                <Navigation
                    onClickLeftBtn={() => this.props.navigator.pop()}
                    title={I18n.t('default.customerService')}
                />

                <ScrollView>
                    <View style={{padding: 20}}>
                        {ViewUtils.gitUserInformationTv('Monday-Friday', '8:00 a.m- 8:00 P.M.ET', null, () => {
                            this.setState({
                                copyValue: '8:00 a.m- 8:00 P.M.ET',
                                alertVisible: true,
                            })
                        }, null, styles.borderBottom)}
                        {ViewUtils.gitUserInformationTv('Saturday', '9:00 a.m- 5:00 P.M.ET', null, () => {
                            this.setState({
                                copyValue: '9:00 a.m- 5:00 P.M.ET',
                                alertVisible: true,
                            })
                        }, null, styles.borderBottom)}
                        {ViewUtils.gitUserInformationTv('Кыргызстан', '0-800- 0000-4727', null, () => this.callPhone('080000004727'), null, styles.borderBottom)}
                        {ViewUtils.gitUserInformationTv('Россия', '8800- 0000- 4727', null, () => this.callPhone('880000004727'), null, styles.borderBottom)}
                        {ViewUtils.gitUserInformationTv('Қазақстан', '810 - 800 - 0000 - 4727', null, () => this.callPhone('81080000004727'), null, styles.borderBottom)}
                        {ViewUtils.gitUserInformationTv('US and Canada', '(833)- 843- 4727', null, () => this.callPhone('(833)8434727'), null, styles.borderBottom)}
                        {ViewUtils.gitUserInformationTv('China mainland', '400-000-4727', null, () => this.callPhone('4000004727'), null, styles.borderBottom)}
                        {ViewUtils.gitUserInformationTv('Email us at', 'info@iparbio.com', null, () => {
                            this.setState({
                                copyValue: 'info@iparbio.com',
                                alertVisible: true,
                            })
                        }, null, styles.borderBottom)}
                    </View>
                </ScrollView>


                <AlertDialog
                    contentTv={I18n.t('default.copy')}
                    visible={this.state.alertVisible}
                    requestClose={() => this.setState({alertVisible: false})}
                    leftBtnOnclick={() => this.copy(this.state.copyValue)}
                />


            </View>
        )
    }

}
const styles = StyleSheet.create({
    companyTv: {
        fontSize: 18,
        color: 'black',
        marginBottom: 10
    },
    borderBottom: {
        borderBottomColor: 'transparent',
    }
});
