import React, {Component} from 'react';

import {View, ScrollView, FlatList, Text, Di, Dimensions} from 'react-native';
import StyleUtils from "../../res/styles/StyleUtils";
import Navigation from "../../custom/Navigation";
import ViewUtils from "../../utils/ViewUtils";
import GoodsRender from "../../utils/GoodsRender";
import CommonUtils from "../../common/CommonUtils";
import IparNetwork, {SERVER_TYPE} from "../../httpUtils/IparNetwork";
import I18n from '../../res/language/i18n'

const {width} = Dimensions.get('window');

export default class MandatoryProductPage extends Component {


    constructor(props) {
        super(props);

        this.iparNetwork = new IparNetwork();
        this.state = {

            goodsData: []
        }
    }


    componentDidMount() {
        const {userData} = this.props;
        CommonUtils.getAcyncInfo('getPv')
            .then((result) => {
                this.swap_amount = JSON.parse(result).swap_amount;
                this.currencyCode = JSON.parse(result).currency_code;
                this._getData(userData);
            });
    }


    _pop() {
        this.props.navigator.popToTop();
    }

    _getData(userData) {
        if (!userData) {
            this._pop();
            return;
        }
        if (!userData.country) {
            this._pop();
            return;
        }
        const userRank = userData.user_rank;
        if (!userRank) {
            this._pop();
            return;
        }

        if (!userRank.reg_goods_id && !userRank.sell_goods_id) {
            this._pop();
            return;
        }
        CommonUtils.showLoading();
        this.getRegProducts(userData);
        this.getSellProducts(userData);
    }

    /**
     * 获取注册包
     * @param userData
     */
    getRegProducts(userData) {

        let goodsId = userData.user_rank.reg_goods_id;
        this.onLoadRegGoods = false;
        if (!goodsId) {
            this.onLoadRegGoods = true;
            if (this.onLoadSellGoods) {
                CommonUtils.dismissLoading();
            }
            return
        }

        this.iparNetwork.getRequest(SERVER_TYPE.goods + 'countryGoods?', 'goods_id=' + goodsId + '&country=' + userData.country)
            .then((result) => {
                this.onLoadRegGoods = true;
                if (this.onLoadSellGoods) {
                    CommonUtils.dismissLoading();
                }
                if (result.code === 200) {
                    let data = result.data.goods || [];

                    if (data.length > 0) {
                        let _data = [{type: '_Title', title: I18n.t('b.regsitrationPack')}];
                        let resultData = _data.concat(data);
                        this.onLoadData(resultData)
                    }
                }
            })
            .catch(() => {
                this.onLoadRegGoods = true;
                if (this.onLoadSellGoods) {
                    CommonUtils.dismissLoading();
                }
            });
    }

    /**
     * 获取商务包
     * @param userData
     */
    getSellProducts(userData) {

        let goodsId = userData.user_rank.sell_goods_id;
        this.onLoadSellGoods = false;
        if (!goodsId) {
            this.onLoadSellGoods = true;
            if (this.onLoadRegGoods) {
                CommonUtils.dismissLoading();
            }
            return;
        }
        this.iparNetwork.getRequest(SERVER_TYPE.goods + 'countryGoods?', 'goods_id=' + goodsId + '&country=' + userData.country)
            .then((result) => {
                this.onLoadSellGoods = true;
                if (this.onLoadRegGoods) {
                    CommonUtils.dismissLoading();
                }
                if (result.code === 200) {
                    let data = result.data.goods;

                    if (data.length > 0) {
                        let _data = [{type: '_Title', title: I18n.t('default.ibmStarterPack')}];
                        let resultData = _data.concat(data);
                        this.onLoadData(resultData)
                    }
                }
            })
            .catch(() => {
                this.onLoadSellGoods = true;
                if (this.onLoadRegGoods) {
                    CommonUtils.dismissLoading();
                }
            });

    }


    onLoadData(goodsData) {
        if (this.goodsData && this.goodsData.length > 0) {
            this.goodsData = this.goodsData.concat(goodsData);
            this.setState({
                goodsData: this.goodsData,
            })
        } else {
            this.goodsData = goodsData;
        }
    }

    render() {
        const {goodsData} = this.state;
        const {userData} = this.props;

        return (
            <View style={[StyleUtils.flex]}>

                <Navigation
                    onClickLeftBtn={() => this._pop()}/>
                <ScrollView>

                    {ViewUtils.getTitleView(I18n.t('b.packages'), I18n.t('b.registerIsIBMMinContent'))}

                    <FlatList
                        contentContainerStyle={{
                            flexDirection: 'row',
                            flexWrap: 'wrap',
                        }}
                        keyExtractor={(item, index) => index}
                        renderItem={(item) => {
                            let data = item.item;
                            if (data.type === '_Title') {
                                return <View
                                    style={[{
                                        flex: 1,
                                        width,
                                        padding: 16,
                                        backgroundColor: '#f3f3f3'
                                    }]}>
                                    <Text style={{fontSize: 18}}>{data.title}</Text>
                                </View>
                            } else
                                return <GoodsRender
                                    userData={userData}
                                    // directBuy={true}
                                    dataSource={data}
                                    navigator={this.props.navigator}/>
                        }}
                        data={goodsData}/>

                    {goodsData && goodsData.length <= 0 &&
                    <Text style={{textAlign: 'center', color: 'gray'}}>{I18n.t('default.noData')}</Text>}
                </ScrollView>
            </View>
        );
    }

}