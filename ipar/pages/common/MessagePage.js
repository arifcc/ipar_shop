import React, {Component} from 'react'

import {
    View,
    DeviceEventEmitter,
    ScrollView,
    TouchableOpacity,
    Image,
    RefreshControl,
    Text,
    FlatList
} from 'react-native'
import StyleUtils from "../../res/styles/StyleUtils";
import Navigation from "../../custom/Navigation";
import I18n from "../../res/language/i18n";
import IparNetwork, {SERVER_TYPE} from "../../httpUtils/IparNetwork";
import CommonUtils from "../../common/CommonUtils";
import ViewUtils from "../../utils/ViewUtils";

export default class MessagePage extends Component {


    // 构造
    constructor(props) {
        super(props);

        this.iparNetwork = new IparNetwork();
        // 初始状态
        this.state = {
            refreshing: false,
            messageInfo: null
        };
    }

    componentDidMount() {
        this.showMessage();
    }

    showMessage() {
        this.setState({refreshing: true});
        this.iparNetwork.getRequest(SERVER_TYPE.admin + 'showMessage?',
            'message_id=' + this.props.messageId)
            .then((result) => {
                this.setState({refreshing: false});
                if (result.code === 200) {
                    this.setState({
                        messageInfo: result.data,
                    })
                }
            })
            .catch((error) => {
                this.setState({refreshing: false});
                console.log('MessagePage-----showMessage----error:', error);
                DeviceEventEmitter.emit('toast', I18n.t('default.fail'))
            })
    }


    render() {
        let messageInfo = this.state.messageInfo || {};
        return (
            <View style={StyleUtils.flex}>
                <Navigation title={this.props.title}
                            onClickLeftBtn={() => this.props.navigator.pop()}/>
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={() => {
                            }}
                        />
                    }>
                    <Text style={[{padding: 16}]}>{messageInfo.content}</Text>

                </ScrollView>
            </View>
        );
    }

}