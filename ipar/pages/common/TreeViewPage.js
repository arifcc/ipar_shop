import React, {Component} from 'react'
import StyleUtils from "../../res/styles/StyleUtils";
import {DeviceEventEmitter, Text, View} from "react-native";
import Navigation from "../../custom/Navigation";
import IparTreeView from "../../custom/IparTreeView";
import IparNetwork, {SERVER_TYPE} from "../../httpUtils/IparNetwork";
import CommonUtils from "../../common/CommonUtils";
import I18n from "../../res/language/i18n";
import IparUserInfoModal from "../../utils/IparUserInfoModal";
import AlertDialog from "../../custom/AlertDialog";


export default class TreeViewPage extends Component {

    // 构造
    constructor(props) {
        super(props);

        this.iparNetwork = new IparNetwork();
        // 初始状态
        this.state = {
            data: [],
            alertVisible: false,
            selectUserInfo: null,
            userInfoModalVisible: false,
        };
    }

    componentDidMount() {
        this.onLoadData(this.props.userId)
    }


    /**
     * 获取数据
     * */
    onLoadData(userId, item) {
        CommonUtils.showLoading();

        let param = 'getChild?';
        if (this.props.urlPram) {
            param = this.props.urlPram
        }
        this.iparNetwork.getRequest(SERVER_TYPE.admin + param, 'parent_id=' + userId)
            .then((result) => {
                console.log(result);

                if (result.code === 200) {

                    let _data = result.data;

                    if (item) {
                        item.isOpen = !item.isOpen;
                        item.data = _data;
                    } else {
                        this.setState({
                            data: _data,
                        });
                    }

                }
                CommonUtils.dismissLoading();

            })
            .catch((error) => {
                CommonUtils.dismissLoading();

                DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
                console.log('TreeViewPage-----onLoadData--error: ', error);
            })

    }

    /**
     * 移除用户
     */
    removeUnderUser() {
        CommonUtils.showLoading();

        this.setState({
            alertVisible: false,
        });
        let userInfo = this.state.selectUserInfo;

        this.iparNetwork.getRequest(SERVER_TYPE.admin + 'removeUnderUser?',
            'remove_user_id=' + userInfo.user_id)
            .then((result) => {
                CommonUtils.dismissLoading();
                if (result.code === 200) {
                    DeviceEventEmitter.emit('toast', I18n.t('default.success'));
                    this.setState({
                        data: result.data,
                    });
                } else {
                    DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
                }
            })
            .catch((error) => {

                CommonUtils.dismissLoading();

                DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
                console.log('TreeViewPage-----onLoadData--error: ', error);
            })
    }


    /**
     * onMoreClick
     * */
    onMoreClick(item) {
        if (item.data) {
            item.isOpen = !item.isOpen;
            this.setState({
                test: 0,
            })
        } else {
            this.onLoadData(item.user_id, item)
        }

    }

    /**
     * onItemClick
     * */
    onItemClick(item) {
        this.setState({
            selectUserInfo: item,
            userInfoModalVisible: true,
        })
    }

    render() {

        let state = this.state;
        return (<View style={StyleUtils.flex}>

            <Navigation
                title={I18n.t('b.treeView')}
                onClickLeftBtn={() => this.props.navigator.pop()}/>

            {/**tree view*/}
            <IparTreeView
                data={state.data}
                onItemClick={(item) => this.onItemClick(item)}
                onMereClick={(item) => this.onMoreClick(item)}/>

            {/**data is null*/
                state.data.length <= 0 && <Text style={{flex: 1, textAlign: 'center'}}>{I18n.t('default.noData')}</Text>

            }


            <IparUserInfoModal
                requestClose={() => {
                    this.setState({
                        userInfoModalVisible: false,
                    })
                }}
                removeUnderUser={() => {
                    this.setState({
                        userInfoModalVisible: false,
                        alertVisible: true,
                    })
                }}
                userInfo={state.selectUserInfo}
                visible={state.userInfoModalVisible}/>

            <AlertDialog
                contentTv={'remove'}
                leftBtnOnclick={() => this.removeUnderUser()}
                visible={state.alertVisible}
                requestClose={() => this.setState({alertVisible: false})}/>

        </View>)
    }

}
