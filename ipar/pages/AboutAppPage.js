import React, {Component} from 'react'
import StyleUtils from "../res/styles/StyleUtils";
import {Image, View, NativeModules, StyleSheet, Animated, Text, TouchableOpacity, Platform} from "react-native";
import Navigation from "../custom/Navigation";
import ViewUtils from "../utils/ViewUtils";
import I18n from "../res/language/i18n";
import BasWebViewPage from "./common/BasWebViewPage";
import CustomerServicePage from "./common/CustomerServicePage";
import MyWebView from "react-native-webview-autoheight";
import ArticleDetail from "./article/ArticleDetail";


export default class AboutAppPage extends Component {

    constructor(props) {
        super(props);

        let animValue = new Animated.Value(20);
        this.state = {
            iconViewHeight: 0,
            iconHeight: 0,
            marginTopAnim: animValue,
            opacityAnim: animValue,
            version: '',
        }
    }

    // 获取版本号
    getVerSion() {
        let iparAppVersion = NativeModules.IparAppVersion;
        if (!iparAppVersion) return;
        iparAppVersion.getAppVersion((err, event) => {
            this.setState({
                version: Platform.OS === 'ios' ? event : err,
            })
        })
    }

    componentDidMount() {
        this.getVerSion();
        this.startAlphaAnim();
    }

    startAlphaAnim() {

        Animated.timing(  // Uses easing functions
            this.state.opacityAnim,  // The value to drive
            {
                toValue: 1,
                duration: 500
            },  // Configuration
        ).start();
    }

    /**
     * @param tv 显示内容
     * @param callback item 点击聆听
     * @param icon
     */
    gitListTvItem(tv, callback, iconIndex) {
        return <TouchableOpacity
            activeOpacity={0.5}
            onPress={callback}
            style={[StyleUtils.listItem]}
        >
            <Text style={[StyleUtils.flex, StyleUtils.smallFont, {
                fontSize: 16,
                color: 'black',
                paddingLeft: 16,
            }]}>{tv}</Text>
            <Image
                source={require('../res/images/ic_right_more.png')}
                style={{width: 18, height: 18, marginRight: 16}}/>
        </TouchableOpacity>
    }


    onClick(type) {
        let _component = null;
        let _params = null;
        switch (type) {
            case 'officialSite':
                _component = ArticleDetail;
                _params = {uri: 'https://iparbio.com'};
                break;
            case 'customerSupport':
                _component = CustomerServicePage;
                break;
            case 'userPrivacy':
                _component = ArticleDetail;
                _params = {uri: 'https://article.iparbio.com/api/article/detail?art_id=223'};
                break;
        }

        if (_component) {
            this.props.navigator.push({
                component: _component,
                name: _component + '',
                params: _params
            })
        }


    }

    render() {
        return (<View style={[StyleUtils.flex]}>
            <Animated.View style={{opacity: this.state.opacityAnim}}>
                <Navigation
                    title={'about'}
                    onClickLeftBtn={() => this.props.navigator.pop()}/>

                <View
                    onLayout={(event) => this.setState({iconViewHeight: event.nativeEvent.layout.height / 2.5})}
                    style={[{height: '100%',}]}>

                    <View
                        style={{
                            height: this.state.iconViewHeight,
                            width: '100%',
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderBottomWidth: 0.5,
                            borderColor: '#e7e7e7',
                            marginBottom: 20,
                        }}>
                        <Image
                            style={[styles.iconSize]}
                            source={require('../../android/app/src/main/res/mipmap-xxhdpi/ipar_logo.png')}/>

                        <Text style={[styles.appName]}>IPAR BUSINESS</Text>

                        <Text style={[styles.version]}>V{this.state.version}</Text>

                    </View>

                    {/*{this.gitListTvItem('升级版本')}*/}
                    {this.gitListTvItem(I18n.t('about.officialSite'), () => this.onClick('officialSite'))}
                    {this.gitListTvItem(I18n.t('about.customerSupport'), () => this.onClick('customerSupport'))}
                    {this.gitListTvItem(I18n.t('about.userPrivacy'), () => this.onClick('userPrivacy'))}

                </View>
            </Animated.View>
        </View>)
    }

}
const styles = StyleSheet.create({
    iconSize: {
        width: 76,
        height: 76,
    },
    version: {
        fontSize: 12,
        color: '#727272'

    },
    appName: {
        fontSize: 14,
        fontWeight: 'bold',
        color: 'black',
        paddingTop: 16,
        paddingBottom: 4,
    }
});