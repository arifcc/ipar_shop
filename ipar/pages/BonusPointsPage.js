/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
	StyleSheet,
	Text,
	View,
	ScrollView,
	Image,
	TouchableOpacity
} from 'react-native';

import Navigation from "../custom/Navigation";
import StyleUtils from "../res/styles/StyleUtils";
import ViewUtils from "../utils/ViewUtils";
import OrderDetail from "./OrderDetail";


type Props = {};


export default class BonusPointsPage extends Component<Props> {

	constructor(props) {
		super(props);

	}

	render() {
		return (
			<View style={StyleUtils.flex}>
				<Navigation
					onClickLeftBtn={() => {
						this.props.navigator.pop()
					}}
					leftButtonIcon={require('../res/images/ic_back.png')}
					titleImage={require('../res/images/ic_ipar_logo.png')}
				/>
				<ScrollView>
					<View style={[StyleUtils.flex, {padding: 16}]}>
						{ViewUtils.getTitleView('Bonus points', 'All the discount cards, coupons and promotion cards')}

						<View style={StyleUtils.lineStyle}/>

						{ViewUtils.gitUserInformationTv('Coupons:', 'you can apply to orders', '', () => {
						})}
						{ViewUtils.gitUserInformationTv('Discount card:', 'you can apply to orders', '', () => {
						})}
						{ViewUtils.gitUserInformationTv('Gift card:', 'you can transfer or purchase', '', () => {
						})}
						{ViewUtils.gitUserInformationTv('Points:', 'apply to credit orders', '', () => {
						})}
					</View>
				</ScrollView>

			</View>)


	}

}

const styles = StyleSheet.create({});

