/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';

import {DeviceEventEmitter, Image, StyleSheet, Text, View, WebView} from 'react-native';
import StyleUtils from "../../res/styles/StyleUtils";
import Navigation from "../../custom/Navigation";
import IparNetwork, {SERVER_TYPE} from "../../httpUtils/IparNetwork";
import I18n from "../../res/language/i18n";
import CommonUtils from "../../common/CommonUtils";
import LoginPage from "../common/LoginPage";


export default class ArticleDetail extends Component {
    constructor(props) {
        super(props);

        this.iparNetwork = new IparNetwork();

        this.state = {}
    }


    componentDidMount() {
        CommonUtils.getAcyncInfo('userInfo')
            .then((result) => {
                CommonUtils.getAcyncInfo('country_iso')
                    .then((country) => {
                        this.userData = JSON.parse(result);
                        this.country = country;
                    });
            });
    }

    /**
     * 产品收藏
     */
    onClickCollection() {
        if (!this.userData || !this.country) {
            this.props.navigator.push({
                component: LoginPage,
                name: 'LoginPage'
            });
            return;
        }

        CommonUtils.showLoading();

        let formData = new FormData();
        formData.append('type', 1);
        formData.append('obj_id', this.props.artID);
        formData.append('country', this.country);
        formData.append('user_id', this.userData.main_user_id);
        formData.append('title', this.props.title);
        formData.append('image', this.props.image);

        this.iparNetwork.getPostFrom(SERVER_TYPE.adminIpar + 'saveCollection', formData)
            .then((result) => {
                CommonUtils.dismissLoading();
                if (result.code === 200) {
                    DeviceEventEmitter.emit('toast', I18n.t('default.success'));
                } else {
                    DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
                }
            })
            .catch((error) => {
                CommonUtils.dismissLoading();

                DeviceEventEmitter.emit('toast', I18n.t('default.fail'));
                console.log('ArticleDetail----onClickCollection--', error)
            })
    }


    render() {

        let url = this.props.uri || SERVER_TYPE.uiAdminIpar + 'article_detail?art_id=' + this.props.artID;

        console.log(url);
        return (
            <View style={StyleUtils.flex}>
                <Navigation
                    onClickLeftBtn={() => {
                        this.props.navigator.pop()
                    }}
                    leftButtonIcon={require('../../res/images/ic_back.png')}
                    titleImage={require('../../res/images/ic_ipar_logo.png')}
                    rightButtonIcon={require('../../res/images/ic_collection.png')}
                    onClickRightBtn={() => this.onClickCollection()}
                />
                <View style={[StyleUtils.flex]}>
                    <WebView
                        onMessage={() => {
                            console.log();
                        }}
                        startInLoadingState={true}
                        style={{width: '100%', height: '100%'}}
                        source={{uri: url}}
                    >

                    </WebView>
                </View>

            </View>
        );
    }

}

