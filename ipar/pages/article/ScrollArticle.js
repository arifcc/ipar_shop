/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {Image, RefreshControl, ScrollView, StyleSheet, Text, TouchableOpacity, View,} from 'react-native';
import StyleUtils from "../../res/styles/StyleUtils";
import ScrollableTabView from 'react-native-scrollable-tab-view';
import ArticlePage from "./ArticlePage";
import IparNetwork, {SERVER_TYPE} from "../../httpUtils/IparNetwork";
import CommonUtils from "../../common/CommonUtils";
import I18n from "../../res/language/i18n";


export default class ScrollArticle extends Component {

	constructor(props) {
		super(props);
		this.state = {
			dataTab: '',
			tabData: [],
		};
		this.iparNetwork = new IparNetwork()
	}

	componentWillUnmount() {
		this.iparNetwork = null;
	}

	componentDidMount() {
		CommonUtils.getAcyncInfo('country_iso')
			.then((country) => {
				this.getTransactionsInfo(country);
			});
	}


	/*获取文章来表*/
	getTransactionsInfo(country) {

        CommonUtils.showLoading();

        this.iparNetwork.getRequest(SERVER_TYPE.article + 'cate/list?', 'region=' + country)
			.then((result) => {
				if (result.code === 200) {
					this.setState({
						tabData: result.data,
					})
				}
                CommonUtils.dismissLoading();

            })
			.catch((error) => {
                CommonUtils.dismissLoading();

                console.log(error);
			});
	}


	/*select index*/
	onChangeTabs = ({i}) => {
		this.setState({dataTab: this.state.tabData[i].cate_id});
	};

	render() {
		let page = [];
		for (i = 0; i < this.state.tabData.length; i++) {
			page.push(<ArticlePage key={i} {...this.props} indexPage={this.state.dataTab}/>)
		}
		return (
			<View style={[StyleUtils.flex]}>
				<ScrollableTabView
					renderTabBar={() =>
						<TabBar
							tabNames={this.state.tabData}
						/>
					}
					onChangeTab={this.onChangeTabs.bind(this)}
				>
					{page}
				</ScrollableTabView>


			</View>
		);
	}


}

class TabBar extends Component {
	componentWillReceiveProps(PropTypes) {
		if (PropTypes.tabNames !== this.props.tabNames) {
			this.setState({
				tabNames: PropTypes.array,
				activeTab: PropTypes.number,
				goToPage: PropTypes.func,
			})

		}
	}

	/**
	 *select tabBar
	 * */
	getTabBar(index) {


		/**
		 * 用户手该语言的时候刷新 数据
		 */
		let tabName = this.props.tabNames[index];
		let title = tabName.name;
		switch (I18n.locale) {
			case 'en':
				title = tabName.name_en ? tabName.name_en : title;
				break;
			case 'ru':
				title = tabName.name_ru ? tabName.name_ru : title;
				break;
			case 'uy':
				title = tabName.name_uy ? tabName.name_uy : title;
				break;
		}


		return (
			<TouchableOpacity
				key={index}
				activeOpacity={0.8}
				style={styles.tabTvBox}
				onPress={() => this.props.goToPage(index)}
			>
				<Text
					style={[StyleUtils.text, StyleUtils.smallFont, this.props.activeTab === index ? styles.select : null]}>{title}</Text>
			</TouchableOpacity>

		)
	}

	render() {
		return (
			<View style={[StyleUtils.rowDirection, StyleUtils.shadowTop]}>
				<ScrollView
					horizontal={true}
					showsHorizontalScrollIndicator={false}

				>
					{this.props.tabNames.map((tab, i) => {
						return (this.getTabBar(i));
					})}
				</ScrollView>
			</View>

		)
	}
}

const styles = StyleSheet.create({

	select: {
		fontSize: 18,
		color: '#333333',
		fontWeight: ('bold', '900')
	},
	tabTvBox: {
		alignItems: 'center',
		marginTop: 8,
		marginBottom: 8,
		marginLeft: 8,
	}
});