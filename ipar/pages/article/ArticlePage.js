/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    ListView,
    RefreshControl,
    Dimensions,
    TouchableOpacity,
    DeviceEventEmitter
} from 'react-native';
import StyleUtils from "../../res/styles/StyleUtils";
import IparNetwork, {IMAGE_URL, SERVER_TYPE} from "../../httpUtils/IparNetwork";
import CommonUtils from "../../common/CommonUtils";
import ArticleDetail from "./ArticleDetail";
import I18n from '../../res/language/i18n'
import IparImageView from "../../custom/IparImageView";

const width = Dimensions.get('window').width;

export default class ArticlePage extends Component {

    constructor(props) {
        super(props);
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.country;
        this.lang = I18n.locale;
        this.state = {
            dataSource: ds.cloneWithRows([]),
            showLoading: true,
        };
        this.iparNetwork = new IparNetwork()
    }

    componentWillUnmount() {
        this.iparNetwork = null;
        this.country = null;
    }


    /**
     * 手该语言的时候刷新页面
     */
    componentWillReceiveProps() {
        if (this.lang !== I18n.locale) {
            this.lang = I18n.locale;
            this.getTransactionsInfo(I18n.locale);
        }
    }

    componentDidMount() {
        CommonUtils.getAcyncInfo('country_iso')
            .then((country) => {
                this.country = country;
                this.lang = I18n.locale;
                this.getTransactionsInfo(I18n.locale);
            });
    }

    /**
     * 获取文章来表
     * */
    getTransactionsInfo(lag) {
        this.setState({
            showLoading: true,
        });
        let _index = this.props.indexPage === 16 ? '' : this.props.indexPage;
        this.iparNetwork.getRequest(SERVER_TYPE.article + 'article/list?',
            'cate_id=' + _index + '&region=' + this.country + '&lang=' + lag)//+ '&lang=' + lang
            .then((result) => {

                if (result.code === 200) {
                    if (result.count !== 0) {
                        this.setState({
                            showLoading: false,
                            dataSource: this.state.dataSource.cloneWithRows(result.data)
                        })
                    } else {
                        this.setState({
                            showLoading: false,
                            dataSource: this.state.dataSource.cloneWithRows([])
                        })
                    }

                }
            })
            .catch((error) => {
                this.setState({showLoading: false});
                console.log(error);
            });
    }

    render() {
        return (
            <View style={[StyleUtils.flex, {padding: 16}]}>
                <ListView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.showLoading}
                            onRefresh={() => this.getTransactionsInfo(I18n.locale)}
                        />
                    }
                    dataSource={this.state.dataSource}
                    renderRow={this.renderRow.bind(this)}
                />
            </View>
        );
    }

    renderRow(rowData, sectionID, rowID) {
        return (
            <TouchableOpacity
                activeOpacity={0.5}
                onPress={() => this.itemClickListener(rowData, rowID)}
                style={[styles.articleItemBox]}
            >


                <IparImageView borderRadius={5} resizeMode={'cover'} width={'100%'} height={width / 2.5}
                               url={rowData.image}/>


                <View style={styles.titleBox}>
                    <Text numberOfLines={1} style={[styles.tv, StyleUtils.smallFont, {
                        fontSize: 18,
                        color: 'black'
                    }]}>{rowData.title}</Text>
                    <Text numberOfLines={2} style={[styles.tv, StyleUtils.smallFont]}>{rowData.description}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    /*item click*/
    itemClickListener(data, index) {
        this.props.navigator.push({
            component: ArticleDetail,
            params: {
                artID: data.art_id,
                image: data.image,
                title: data.title,
            }
        })
    }

}

const styles = StyleSheet.create({

    articleItemBox: {
        padding: 8,
        backgroundColor: 'white',
        borderBottomWidth: 3,
        borderBottomColor: '#f7f8f9'
    },
    image: {
        flex: 1,
        width: '100%',
        borderRadius: 5,
        resizeMode: "cover"
    },
    titleBox: {
        marginTop: 8,
        height: 55,
        width: '100%',
    },
    tv: {
        flex: 1,
        lineHeight: 26,
        fontSize: 15,
        color: '#404040'
    }

});