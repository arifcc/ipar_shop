import React, {Component} from 'react'
import {View, Text, TouchableOpacity, StyleSheet, DeviceEventEmitter} from 'react-native'
import StyleUtils from "../../../res/styles/StyleUtils";
import IparImageView from "../../../custom/IparImageView";
import I18n from '../../../res/language/i18n'
import ArticleDetail from "../ArticleDetail";
import ViewUtils from "../../../utils/ViewUtils";
import SelectRegisterPage from "../../common/register/SelectRegisterPage";
import CommonUtils from "../../../common/CommonUtils";

export default class ArticleItem extends Component {

    constructor(props) {
        super(props);
        this.state = {};

    }

    propTypes: {
        itemData: PropTypes.request.objectOf
    };


    static defaultProps = {
        itemData: {},
    };


    getBtnName(data = {}) {
        let res = '';
        switch (I18n.locale) {
            case 'en':
                res = data.name_en;
                break;
            case 'ru':
                res = data.name_ru;
                break;
            case 'uy':
                res = data.name_uy;
                break;
            case 'zh':
                res = data.name_zh;
                break
        }
        return res;
    }

    getView() {
        let itemData = this.props.itemData;

        let view = null;
        switch (itemData.template_name) {
            case 'only_title_back_img':
                view = this.getOnly_title_back_img(itemData);
                break;
            case 'right_img_back_left_title_content_btn':
            case 'left_img_back_right_btn_content':
            case 'back_content_btn':

                view = this.getRight_img_back_left_title_content_btn(itemData);
                break;
            case 'big_img_down_content':
                view = this.getBig_img_down_content(itemData);
                break;
            case 'left_img_right_content':
            case 'left_content_btn_right_img':
            case 'eq_right_img_left':

                view = this.getLeft_img_right_content(itemData);
                break;
            case 'left_title_right_content':
                view = this.getLeft_title_right_content(itemData);
                break;
            case 'only_short_content':
            case 'semi_div':
                view = this.getOnly_short_content(itemData);
                break;
            case 'sanfen_zhi_yi':
                view = this.getSanfen_zhi_yi(itemData);
                break;

        }
        return view;
    }

    /**
     * sanfen_zhi_yi
     * @returns {*}
     */
    getSanfen_zhi_yi(data = {}) {
        return <View style={{borderWidth: 1, margin: 20, padding: 20}}>

            {data.is_short_title === 1 && <Text style={[styles.title, {
                paddingVertical: 20,
                textAlign: 'center',
                color: 'black',
            }]}>{data.short_title}</Text>}


            {data.is_description === 1 && <Text style={[{
                color: 'black',
                paddingBottom: 20,
                textAlign: 'center'
            }]}>{data.description}</Text>}

        </View>
    }


    /**
     * only_title_back_img
     * @returns {*}
     */
    getOnly_title_back_img(data = {}) {
        return <View
            style={[StyleUtils.center, {}]}>
            <IparImageView
                url={data.image}
                height={220}
                width={'100%'}
                resizeMode={'cover'}
            />

            <View style={{
                position: 'absolute',
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                backgroundColor: 'rgba(0,0,0,0.33)'
            }}/>

            {data.is_short_title === 1 && <Text style={[{
                fontSize: 22,
                fontWeight: 'bold', position: 'absolute',
                backgroundColor: 'transparent',
                color: 'white'
            }]}>{data.short_title}</Text>}
        </View>
    }


    /**
     * right_img_back_left_title_content_btn
     * @returns {*}
     */
    getRight_img_back_left_title_content_btn(data = {}) {
        let btnName = this.getBtnName(data);
        return <View
            style={[{padding: 20}]}>

            {data.is_short_title === 1 && <Text style={[styles.title, {
                paddingVertical: 20,
                textAlign: 'center',
            }]}>{data.short_title}</Text>}

            {data.is_description === 1 && <Text style={[{
                color: 'black',
                paddingBottom: 20, textAlign: 'center'
            }]}>{data.description}</Text>}


            {data.image && <IparImageView
                url={data.image}
                height={220}
                resizeMode={'cover'}
            />}

            {data.is_btn === 1 &&
            ViewUtils.getButton(btnName, () => this.onPress(data), {
                backgroundColor: 'white',
                borderWidth: 1,
                borderColor: 'gray',
                marginTop: 10
            }, {color: 'black'})}


        </View>
    }


    /**
     * big_img_down_content
     * @returns {*}
     */
    getBig_img_down_content(data = {}) {
        return <View
            style={[{padding: 20}]}>
            {data.is_short_title === 1 && <Text style={[styles.title, {

                paddingVertical: 20,
                textAlign: 'center',
            }]}>{data.short_title}</Text>}

            <IparImageView
                url={data.image}
                height={230}
                resizeMode={'cover'}
            />

            {data.is_description === 1 && <Text style={[{
                textAlign: 'center', color: '#494949', padding: 10
            }]}>{data.description}</Text>}
        </View>
    }


    /**
     * left_img_right_content
     * @returns {*}
     */
    getLeft_img_right_content(data = {}) {
        return <View
            style={[{padding: 20}]}>

            {data.is_short_title === 1 && <Text style={[styles.title, {
                textAlign: 'center',
            }]}>{data.short_title}</Text>}

            {data.is_description === 1 && <Text style={[{
                textAlign: 'center', paddingBottom: 10, color: 'black', padding: 10
            }]}>{data.description}</Text>}


            <IparImageView
                url={data.image}
                height={230}
                resizeMode={'cover'}
            />
        </View>
    }


    /**
     * only_short_content
     * @returns {*}
     */
    getOnly_short_content(data = {}) {
        let btnName = this.getBtnName(data);

        return <View
            style={{padding: 20}}>

            {data.is_short_title === 1 && <Text style={[styles.title, {
                paddingVertical: 16,
            }]}>{data.short_title}</Text>}

            {data.is_description === 1 && <Text style={[{
                textAlign: 'center', paddingBottom: 10, color: 'black'
            }]}>{data.description}</Text>}

            {data.is_btn === 1 && <View style={[StyleUtils.rowDirection, {justifyContent: 'center'}]}>
                <TouchableOpacity
                    onPress={() => this.onPress(data)}
                    style={{
                        borderWidth: 1,
                        borderColor: 'rgba(96,96,96,0.57)',
                        paddingVertical: 8,
                        paddingHorizontal: 14,
                    }}>
                    <Text style={{
                        backgroundColor: 'transparent',
                    }}>{btnName}</Text>
                </TouchableOpacity>

            </View>}
        </View>
    }


    /**
     * left_title_right_content
     * @returns {*}
     */
    getLeft_title_right_content(data = {}) {
        return <View
            style={[StyleUtils.rowDirection, {alignItems: 'center', marginTop: 20}]}>

            {data.is_short_title === 1 && <View style={[StyleUtils.rowDirection, {width: 120, height: '100%'}]}>
                <Text style={{
                    textAlign: 'center',
                    fontSize: 30,
                    width: '100%',
                    flex: 1,
                    paddingHorizontal: 20
                }}>{data.short_title}</Text>
                <View style={{width: 2, backgroundColor: 'black', height: '100%'}}/>
            </View>}

            {data.is_description === 1 && <Text style={[{
                textAlign: 'center', padding: 20, color: 'black', flex: 1
            }]}>{data.description}</Text>}

        </View>
    }


    onPress(data) {


        let params = {
            artID: data.art_id,
            image: data.image,
            title: data.title,
        };

        let component = null;

        if ('/register_way' === data.url) {
            if (this.props.isLogin) {
                DeviceEventEmitter.emit('goToPage', 4)
            } else {
                component = SelectRegisterPage;
                params = {
                    countryData: null,
                }
            }
        } else {
            component = ArticleDetail;
        }

        if (component) {

            this.props.navigator.push({
                component,
                params
            });
        }
    }

    render() {
        return (
            <View style={[]}>
                {this.getView()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        fontSize: 22,
        color: "#7cc11b",
        fontWeight: 'bold'
    }
});
