import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    Modal,
    Image,
    Platform,
    Dimensions,
    ActivityIndicator
} from 'react-native';
import StyleUtils from "../res/styles/StyleUtils";
import I18n from "../res/language/i18n/index";

let height = Dimensions.get('window').height;

export default class AlertDialog extends Component {

    propTypes: {
        visible: PropTypes.isRequired,//dialog 关
        requestClose: PropTypes.isRequired,//安卓onBack点击时候关dialog
        centerIcon: PropTypes.string,//center icon
        contentTv: PropTypes.string,//内容
        leftBtnOnclick: PropTypes.func,//左边按钮点击---yes btn
        rightBtnOnclick: PropTypes.func,//右边按钮点击---yes btn
        singleBtnOnclick: PropTypes.func,//中单按钮点击

        leftSts: PropTypes.string,//左边的按钮文件
        centerSts: PropTypes.string,//中间的按钮文件
        rightSts: PropTypes.string,//右边的按钮文件
    };


    /*
    <AlertDialog
    visible={this.state.showDialog}
    requestClose={() => this.setState({showDialog: false})}
    centerIcon={require('../res/images/ic_close.png')}
    contentTv={'Ipar'}
    //leftBtnOnclick={() => alert('ok')}
    singleBtnOnclick={() => this.setState({showDialog: false})}
    />
    */


    render() {
        return (
            <Modal//* 初始化Modal */
                animationType='fade'           // 从底部滑入
                transparent={true}             // 不透明
                visible={this.props.visible}    // 根据isModal决定是否显示
                onRequestClose={this.props.requestClose}  // android必须实现
            >
                <View style={[styles.boxStyle, StyleUtils.center]}>
                    <View style={styles.dialogBox}>

                        <Text style={[StyleUtils.smallFont, {
                            fontSize: 18,
                            fontWeight: 'bold',
                        }]}>{I18n.t('alert.warning')}</Text>
                        {/*center*/}
                        <View style={[StyleUtils.center, {flex: 1}]}>
                            {this.props.centerIcon ? <Image
                                source={this.props.centerIcon}
                                style={styles.centerImage}
                            /> : null}
                            <Text
                                numberOfLines={8}
                                style={[{
                                    color: 'black',
                                    textAlign: 'center',
                                }, StyleUtils.smallFont]}>{this.props.contentTv}</Text>
                        </View>
                        {/*btn*/}
                        {this.props.leftBtnOnclick ? <View style={[styles.btnBox, StyleUtils.rowDirection]}>
                            <TouchableOpacity
                                onPress={this.props.rightBtnOnclick ?
                                    this.props.rightBtnOnclick : this.props.requestClose}
                                style={[StyleUtils.flex, {
                                    marginRight: 8,
                                    height: 32,
                                    borderColor: '#707070',
                                    borderWidth: 1,
                                }, StyleUtils.center]}>
                                <Text
                                    style={[styles.rightBtn, StyleUtils.smallFont]}>{this.props.rightSts ?
                                    this.props.rightSts : I18n.t('default.cancel')}</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={this.props.leftBtnOnclick}
                                style={[StyleUtils.flex, StyleUtils.center, {
                                    marginLeft: 8,
                                    backgroundColor: 'black',
                                    height: 32,
                                }, StyleUtils.center]}>
                                <Text
                                    style={[styles.leftBtn, StyleUtils.smallFont]}>{this.props.leftSts ?
                                    this.props.leftSts : I18n.t('default.yes')}</Text>
                            </TouchableOpacity>

                        </View> : null}
                        {/*中单btn*/}
                        {this.props.singleBtnOnclick ? <TouchableOpacity
                            onPress={this.props.singleBtnOnclick}
                            style={[styles.btnBox, StyleUtils.center, {height: 38}]}>
                            <Text
                                style={[styles.rightBtn, styles.centerBtn, StyleUtils.smallFont]}>{this.props.centerSts ?
                                this.props.centerSts : I18n.t('default.yes')}</Text>
                        </TouchableOpacity> : null}
                    </View>
                </View>
            </Modal>
        )
    }


}
const styles = StyleSheet.create({
        boxStyle: {
            backgroundColor: 'rgba(0, 0, 0, 0.6)',
            flex: 1,
            flexDirection: 'column-reverse',
        },

        centerBtn: {
            backgroundColor: '#333333',
            height: 30,
            color: 'white',
            width: 120,
            textAlign: 'center',
            lineHeight: Platform.OS === 'ios' ? 32 : 26,

        },

        dialogBox: {
            width: '78%',
            minHeight: 240,
            maxHeight: height - 300,
            backgroundColor: 'white',
            paddingTop: 18,
            paddingRight: 25,
            paddingLeft: 25,
            paddingBottom: 18,
            borderRadius: 10,
        },
        centerImage: {
            height: 25,
            width: 25,
            marginBottom: 16,
        },
        btnBox: {
            height: 30,
        },
        leftBtn: {
            color: 'white',
            fontSize: 14,
            backgroundColor: 'transparent'
        },

        rightBtn: {
            fontSize: 14,
        },


    })
;

