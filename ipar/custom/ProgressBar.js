import React, {Component, PropTypes} from 'react';
import {
	View,
	Text,
	Animated,
	StyleSheet
} from 'react-native';


export default class ProgressBar extends Component {

	constructor(props) {
		super(props);
		this.state = {
			fadeAnim: new Animated.Value(0),
			width: 0,
		};
	}

	/**
	 * 动画open&&close
	 */
	startAnimation() {
		let animValue = this.state.width;
		Animated.timing(       // 随时间变化而执行的动画类型
			this.state.fadeAnim, { // 动画中的变量值
				toValue: animValue,   // 透明度最终变为1，即完全不透明
				duration: 750, // 动画时间
			}
		).start();
	}

	render() {
		if (this.props.progress !== '0%') {
			this.startAnimation();
		}
		return (
			<View
				onLayout={(event) => this.setState({
					width: event.nativeEvent.layout.width
				})}
				style={[styles.processBox]}>
				<Animated.View  // 可动画化的视图组件
					style={[{flex: 1}, {
						width: this.state.fadeAnim,
					}]}>
					<View style={[styles.process, {width: this.props.progress}]}/>
				</Animated.View>
			</View>
		);
	}


}


const styles = StyleSheet.create({
	processBox: {
		height: 23,
		backgroundColor: '#fff',
		borderWidth: 1,
		borderColor: '#c0c0c0',
		flex: 1
	},
	process: {
		height: '100%',
		backgroundColor: 'black',
	}
});