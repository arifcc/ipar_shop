/**
 * create: 2018-10-23 （10:40 早）
 * email : nur01@qq.com
 */


import React, {Component} from 'react'
import {Dimensions, FlatList, Image, Text, TouchableOpacity, View} from "react-native";
import I18n from "../res/language/i18n";

const {height} = Dimensions.get('window');
export default class NurTreeView extends Component {

    constructor(props) {
        super(props);
        this.height = height / 1.6;
    }


    propTypes: {
        data: PropTypes.isRequired,//array([])
        onItemClick: PropTypes.isRequired,//func
    };


    /**
     * render flatList item
     * @param item
     * @param index
     * @param style
     * @returns {*}
     */
    renderItem(item, index, style) {


        let _item = item.menu_lang;
        let tabName = _item.name_zh;
        switch (I18n.locale) {
            case 'en':
                tabName = _item.name_en;
                break;
            case 'ru':
                tabName = _item.name_ru;
                break;
            case 'uy':
                tabName = _item.name_uy;
                break;
        }

        return (<View
            style={[style]}>
            <TouchableOpacity
                onPress={() => this.props.onItemClick && this.props.onItemClick(item, index)}
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}
            >

                <Text style={{paddingHorizontal: 16, flex: 1,}}>{tabName}</Text>

                {item.children_app && item.children_app.length > 0 ?
                    <Image
                        style={[{width: 32, tintColor: 'gray', height: 32, margin: 8}]}
                        source={require('../res/images/ic_more.png')}/>
                    : <View style={[{height: 32, margin: 8}]}
                    />
                }
            </TouchableOpacity>

            {/**items*/}
            {item.isOpen && this.renderFlatList(item.children_app,
                ({item, index}) => this.renderItem(item, index, {
                    paddingLeft: 20,
                }))}

        </View>);
    }


    /**
     * render FlatList
     */
    renderFlatList(data, renderItem) {
        return <FlatList
            data={data}
            renderItem={renderItem}
            extraData={this.props}
        />
    }

    render() {
        return (<View style={[{maxHeight: this.height}, ...this.props]}>
            {this.renderFlatList(this.props.data,
                ({item, index}) => this.renderItem(item, index, {
                    backgroundColor: index % 2 === 0 ? '#fff' : '#f1f1f1',
                }))}

        </View>)
    }
}
