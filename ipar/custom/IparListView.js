import React, {Component} from 'react';

import {View, FlatList, ScrollView, ActivityIndicator} from 'react-native';
import StyleUtils from "../res/styles/StyleUtils";

export default class IparListView extends Component {

    /**
     * 上传滑动到底部事件
     * */
    _contentViewScroll(e) {
        console.log('ss')
        let offsetY = e.nativeEvent.contentOffset.y; //滑动距离
        let contentSizeHeight = e.nativeEvent.contentSize.height; //scrollView contentSize高度
        let oriageScrollHeight = e.nativeEvent.layoutMeasurement.height; //scrollView高度
        if (offsetY + oriageScrollHeight >= contentSizeHeight) {
            this.props.onRefreshUp && this.props.onRefreshUp();
        }
    }


    render() {
        let props = this.props;
        return (
            <ScrollView
                onMomentumScrollEnd={(e) => this._contentViewScroll(e)}>
                <FlatList
                    data={props.data}
                    renderItem={props.renderItem}
                    keyExtractor={(item, index) => index}
                />

                {props.isRefreshingUp && <View style={[StyleUtils.center, {height: 50, width: '100%'}]}>
                    <ActivityIndicator color={'black'} size={'small'}/>
                </View>}
            </ScrollView>
        );
    }

}