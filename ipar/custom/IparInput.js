import React, {Component} from 'react'
import StyleUtils from "../res/styles/StyleUtils";
import {LayoutAnimation, Platform, TextInput, UIManager, View} from "react-native";

export default class IparInput extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isFocused: false,
        }
    }


    propTypes: {
        ...ViewPropTypes,

        maxLength: PropTypes.number,
        /**
         * Sets the number of lines for a `TextInput`. Use it with multiline set to
         * `true` to be able to fill the lines.
         * @platform android
         */
        numberOfLines: PropTypes.number,
        /**
         * When `false`, if there is a small amount of space available around a text input
         * (e.g. landscape orientation on a phone), the OS may choose to have the user edit
         * the text inside of a full screen text input mode. When `true`, this feature is
         * disabled and users will always edit the text directly inside of the text input.
         * Defaults to `false`.
         * @platform android
         */
        disableFullscreenUI: PropTypes.bool,

        /**
         * If `true`, the text input can be multiple lines.
         * The default value is `false`.
         */
        multiline: PropTypes.bool,
        /**
         * Callback that is called when the text input is blurred.
         */
        onBlur: PropTypes.func,
        /**
         * Callback that is called when the text input is focused.
         */
        onFocus: PropTypes.func,
        /**
         * Callback that is called when the text input's text changes.
         */
        onChange: PropTypes.func,
        /**
         * Callback that is called when the text input's text changes.
         * Changed text is passed as an argument to the callback handler.
         */
        onChangeText: PropTypes.func,
        /**
         * The string that will be rendered before text input has been entered.
         */
        placeholder: PropTypes.node,


        /**
         * 是否密码
         */
        isPassword: PropTypes.bool,


        defaultValue: PropTypes.string,


    };


    render() {
        let props = this.props;
        return (
            <TextInput
                autoCapitalize={'none'}
                {...props}
                onBlur={() => {
                    this.props.onBlur && this.props.onBlur();
                    this.setState({
                        isFocused: false,
                    })
                }}
                onFocus={() => {
                    this.setState({
                        isFocused: true,
                    })
                }}
                placeholder={props.placeholder ? props.placeholder.toUpperCase() : ''}
                secureTextEntry={props.isPassword === true}
                underlineColorAndroid='transparent'
                style={[StyleUtils.input, StyleUtils.labelBorder, StyleUtils.smallFont, props.style, {borderColor: this.state.isFocused ? 'black' : '#b4b4b4'}]}
            />
        );
    }
}