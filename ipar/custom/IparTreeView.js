/**
 * create: 2018-10-23 （10:40 早）
 * email : nur01@qq.com
 */


import React, {Component} from 'react'
import {
    View,
    FlatList,
    Platform,
    LayoutAnimation,
    UIManager,
    Animated,
    Text,
    Image,
    TouchableOpacity,
    TouchableHighlight
} from "react-native";
import StyleUtils from "../res/styles/StyleUtils";
import CommonUtils from "../common/CommonUtils";

export default class IparTreeView extends Component {


    propTypes: {
        data: PropTypes.isRequired,
        onMereClick: PropTypes.isRequired,
        onItemClick: PropTypes.func,
    };


    /**
     * render flatList item
     * @param item
     * @param index
     * @param style
     * @returns {*}
     */
    renderItem(item, index, style) {
        let userName = ((item.first_name || '') + (item.last_name || '') || item.rand_id);

        let isRemoveUser = false;

        let userRank = item.user_rank;
        if (userRank) {
            let resultDay = new CommonUtils().addDate(item.reg_time, userRank.reg_pack_day);
            let date = new Date(resultDay);
            let serverTime = new Date(item.server_time);
            isRemoveUser = date < serverTime && item.is_reg_pack === 0;
        }

        return (<View
            style={[style]}>
            <View
                style={[StyleUtils.rowDirection, {alignItems: 'center'}]}
            >

                <TouchableOpacity
                    onPress={() => this.props.onItemClick && this.props.onItemClick(item)}
                    style={[{
                        flexDirection: 'row',
                        alignItems: 'center',
                        flex: 1
                    }]}>
                    <Image
                        style={[{width: 32, height: 32, margin: 8, marginLeft: 12}]}
                        source={require('../res/images/ic_avatar.png')}/>
                    <Text>{userName}</Text>
                </TouchableOpacity>


                {isRemoveUser ? <Image
                        style={[{width: 14, height: 14}]}
                        source={require('../res/images/ic_delete_user.png')}/>
                    : null}
                <TouchableHighlight
                    underlayColor={'#eeeeee'}
                    style={[StyleUtils.center, {width: 50, height: 50}]}
                    onPress={() => this.props.onMereClick && this.props.onMereClick(item)}
                >
                    <Image
                        style={[{width: 14, height: 14}]}
                        source={!item.isOpen ? require('../res/images/ic_add.png') :
                            require('../res/images/ic_reduce.png')}/>
                </TouchableHighlight>
            </View>

            {/**items*/}
            {item.isOpen ? this.renderFlatList(item.data,
                ({item, index}) => this.renderItem(item, index, {paddingLeft: 16})) : null}

        </View>);
    }


    /**
     * render FlatList
     */
    renderFlatList(data, renderItem) {
        return <FlatList
            data={data}
            renderItem={renderItem}
            extraData={this.props}
        />
    }

    render() {
        return (<View style={[StyleUtils.flex, ...this.props]}>

            {this.renderFlatList(this.props.data,
                ({item, index}) => this.renderItem(item, index, {
                    marginTop: 8,
                    paddingBottom: 8,
                    // backgroundColor: index % 2 === 0 ? '#f0f0f0' : '#d7d7d7',
                    borderBottomWidth: 1,
                    borderColor: '#dddddd',
                }))}

        </View>)
    }
}

