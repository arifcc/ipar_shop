import React, {Component} from 'react'
import StyleUtils from "../res/styles/StyleUtils";
import Navigation from "./Navigation";
import {Image, Text, View} from "react-native";

export default class StarView extends Component {


	render() {

		let centerView = [];
		for (let i = 0; i < this.props.num; i++) {
			centerView.push(<Image
				style={{width: 16, height: 16, marginRight: 2, tintColor: '#b9b9b9'}}
				source={require('../res/images/ic_star.png')}/>)
		}

		return (<View style={[{flex: 1}, StyleUtils.rowDirection, this.props.style]}>
			{centerView}
		</View>)
	}
}