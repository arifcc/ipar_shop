/**
 *  2018-6-22
 *  email:nur01@qq.com
 */
import React, {Component} from 'react'
import {DeviceEventEmitter, Image, Platform, StatusBar, StyleSheet, Text, TouchableOpacity, View} from 'react-native'
import StyleUtils from "../res/styles/StyleUtils";
import ViewUtils from "../utils/ViewUtils";

const navigationSize = Platform.OS === 'ios' ? 40 : 50;//NAV 的大小

//status bar text 的颜色
export var StatusBarTextStyle = {black: 'dark-content', white: 'light-content'};
let statusBarBackgroundColor = 'white';
export default class Navigation extends Component {


    /**
     * 参数的type
     */
    propTypes: {
        bgColor: PropTypes.string,//nav 的颜色
        title: PropTypes.isRequired,//nav 的题目
        titleImage: PropTypes.string,//nav 的题目
        titleColor: PropTypes.string,//nav 的题目color
        iconTintColor: PropTypes.string,//nav icon color
        leftButtonIcon: PropTypes.require,//左边的 btn icon
        rightButtonIcon: PropTypes.require,//右边的 btn icon
        rightButtonIconTow: PropTypes.require,//右边的 btn icon
        barTextStyle: PropTypes.string,//status bar 的 text 颜色
        onClickLeftBtn: PropTypes.isRequired,//左边的点击事件
        onClickRightBtn: PropTypes.func,//右边的点击事件
        onClickRightBtnTow: PropTypes.func,//右边的点击事件
    };

    /**
     * 默认value
     */
    static defaultProps = {
        bgColor: statusBarBackgroundColor,
        barTextStyle: StatusBarTextStyle.black,
        leftButtonIcon: require('../res/images/ic_back.png'),
    };

    componentDidMount() {
        DeviceEventEmitter.addListener('smallNavVisible', (boolean) => {

            if (boolean && statusBarBackgroundColor === 'white') {
                statusBarBackgroundColor = 'rgba(0, 0, 0, 0.5)';
                StatusBar.setBarStyle(StatusBarTextStyle.white);
                Platform.OS !== 'ios' && StatusBar.setBackgroundColor('rgba(0, 0, 0, 0.5)');
            } else if (!boolean && statusBarBackgroundColor === 'rgba(0, 0, 0, 0.5)') {
                StatusBar.setBarStyle(StatusBarTextStyle.black);
                statusBarBackgroundColor = 'white';
                Platform.OS !== 'ios' && StatusBar.setBackgroundColor('white');
            }
        });
    }

    /**
     * init 初始化
     * @returns {*}
     */
    render() {
        /**
         * status bar
         */
        let statusBar = <StatusBar
            ref={'StatusBar'}
            translucent={false}
            backgroundColor={this.props.bgColor}
            barStyle={this.props.barTextStyle}
        />;

        /**
         * 左边的btn
         */
        let leftBtn = this.props.leftButtonIcon
            ? ViewUtils.getImageBtnView([StyleUtils.center, styles.navBtn],
                this.props.leftButtonIcon,
                [styles.navBtnImage, {tintColor: this.props.iconTintColor ? this.props.iconTintColor : '#313131'}],
                this.props.onClickLeftBtn) : null;


        /**
         * nav 的题目
         */
        let center = (<View style={[StyleUtils.center, styles.navTitleView]}>
            {this.props.titleImage ? <Image
                    style={styles.titleImage}
                    source={this.props.titleImage}/> :
                <Text
                    style={[StyleUtils.title, StyleUtils.smallFont, this.props.titleColor ? {color: this.props.titleColor} : null]}>{this.props.title}
                </Text>
            }
        </View>);

        /**
         * 右边的btn
         */
        let rightBtn = this.props.rightButtonIcon ? ViewUtils.getImageBtnView([StyleUtils.center, styles.navBtn],
            this.props.rightButtonIcon,
            [styles.navBtnImage, {tintColor: this.props.iconTintColor ? this.props.iconTintColor : '#313131'}],
            this.props.onClickRightBtn) : null;


        /**
         * 右边的btn
         */
        let rightBtnTow = this.props.rightButtonIconTow ? ViewUtils.getImageBtnView([StyleUtils.center, styles.navBtn],
            this.props.rightButtonIconTow,
            [styles.navBtnImage, {tintColor: this.props.iconTintColor ? this.props.iconTintColor : '#313131'}],
            this.props.onClickRightBtnTow) : null;


        return (
            <View style={[{backgroundColor: this.props.bgColor}]}>
                {statusBar}
                <View style={[StyleUtils.justifySpace, {
                    height: navigationSize,
                }]}>
                    {leftBtn}
                    {center}
                    <View style={[StyleUtils.rowDirection]}>
                        {rightBtnTow}
                        {rightBtn}
                    </View>
                </View>
            </View>
        )
    }


    get() {

    }

}
const styles = StyleSheet.create({
    navBtn: {
        width: navigationSize,
        height: navigationSize,
    },
    navBtnImage: {
        width: 18,
        height: 18,
    },
    titleImage: {
        maxWidth: '100%',
        height: navigationSize - 18,
        resizeMode: 'contain',
    },
    navTitleView: {
        position: 'absolute',
        left: navigationSize,
        top: 0,
        right: navigationSize,
        bottom: 0
    },

});
