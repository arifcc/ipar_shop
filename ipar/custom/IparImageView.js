import React, {Component} from 'react';
import {View, Image, Animated, StyleSheet, ActivityIndicator} from 'react-native';
import {IMAGE_URL} from "../httpUtils/IparNetwork";

export default class IparImageView extends Component {

    constructor(props) {
        super(props);

        this.state = {
            onLoadEnd: false,
            onError: !props.url,
            fadeInOpacity: new Animated.Value(0),
        }
    }

    propTypes: {
        width: PropTypes.number.isRequired,
        height: PropTypes.number.isRequired,
        borderRadius: PropTypes.number,
        url: PropTypes.string.isRequired,
        resizeMode: PropTypes.style,
        httpUrlNotShow: PropTypes.bool,
        httpUrl: PropTypes.string,

        errorImage: PropTypes.source,
    };


    render() {

        let width = this.props.width;
        let height = this.props.height;
        let onLoadEnd = this.props.onLoadEnd;
        let props = this.props;
        let state = this.state;

        let loadingView = !state.onLoadEnd && !state.onError && <ActivityIndicator
            style={styles.loadingImage}
            size={'small'}
            color={'black'}/>;


        return <View
            style={[{width: width, height: height,}]}
        >

            <Animated.View style={{opacity: state.fadeInOpacity,}}>

                <Image
                    {...this.props}
                    onLoadStart={() => {
                        if (state.onLoadEnd)
                            this.setState({
                                onError: false,
                                onLoadEnd: false,
                            });
                    }}
                    onError={() => {
                        this.setState({
                            onError: true,
                            onLoadEnd: true,
                        })
                    }}
                    onLoadEnd={() => {
                        this.setState({
                            onLoadEnd: true,
                        }, Animated.timing(
                            state.fadeInOpacity,
                            {
                                toValue: 1,
                                duration: 1000,
                            }
                        ).start());
                        onLoadEnd && onLoadEnd();
                    }}
                    style={{
                        width: width,
                        height: height,
                        resizeMode: (props.resizeMode || 'contain'),
                        borderRadius: props.borderRadius,
                    }}
                    source={props.url && !state.onError ? {uri: props.httpUrl ? props.httpUrl + props.url : props.httpUrlNotShow ? props.url : IMAGE_URL + props.url} : props.errorImage || require('../res/images/ic_image_fail_simlal.png')}/>

            </Animated.View>

            {loadingView}


        </View>
    }
}
const styles = StyleSheet.create({
    loadingImage: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        top: 0,
    },

});
