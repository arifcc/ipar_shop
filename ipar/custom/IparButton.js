import React, {Component} from 'react';
import {
    TouchableNativeFeedback,
    TouchableOpacity,
    Platform,
    View,
} from 'react-native';


export default class IparButton extends Component {
    render() {
        const props = this.props;

        if (Platform.OS === 'ios') {
            return <TouchableOpacity
                // delayPressIn={0}
                // background={TouchableNativeFeedback.SelectableBackground()} // eslint-disable-line new-cap
                {...props}
            >
                {props.children}
            </TouchableOpacity>;
        } else
            return <TouchableNativeFeedback
                // delayPressIn={0}
                // background={TouchableNativeFeedback.SelectableBackground()} // eslint-disable-line new-cap
                {...props}
            >
                {props.children}
            </TouchableNativeFeedback>;
    }
}
